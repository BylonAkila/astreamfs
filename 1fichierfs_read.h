/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Callbacks defined in 1fichier_read.c to fuel the read engine
 */

bool unfichier_close_if_idle(uint64_t fh,
			     void *sh, 
			     time_t last_read,
			     time_t now,
			     bool force);
int unfichier_start_stream(const char *path,
			   struct fuse_file_info *fi,
			   size_t size,
			   off_t start_offset,
			   void **sh,
			   int *fd);
