/*
 * 1fichierfs: read parts of 1fichierfs
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _FILE_OFFSET_BITS 64

#include "1fichierfs.h"
#include <stdbool.h>


#define CURL_KEEP_LOCATION_TIME (1800)  /* 30 minutes as documented.          */
#define S_ERROR(s) (0 != (LOAD(((s)->counter)) & FL_ERROR))

#define CURL_IDLE_TIME		(  55)	/* As of 1.3.0: Strange behaviour when
					   reusing a "keep-alived" TCP stream
					   after 1 min, there is ~20 sec delay
					   to restart (or even sometimes curl
					   error 50: RCV_ERROR). Keeping readers
					   only for 55 seconds avoids the issue,
					   at the expense of more 'curling'
					   which is faster than waiting
					   ~20/25 seconds!
					    Was: 300 for 1fichier, 295 safer! */


/* Variables and define used when fallback on download/get_token API is needed.
 */
static pthread_mutex_t	        fallback_mutex = PTHREAD_MUTEX_INITIALIZER;
static unsigned long		fallback_tested = 0;
static _Atomic time_t		fallback_until  = 0;
static _Atomic int		fallback_in_use = 0;
#define KEEP_FALLBACK_TIME (300)


/*
 * @brief stores location-related informations.
 *
 * @param pointer on the current stream
 * @param location string
 * @param size of the location string (strlen, ie not counting '\0')
 * @param time of storage
 * @return none
 */
static void store_loc(struct lfile  *s,
		      const char    *location,
		      const size_t   sz_location,
		      const bool     single,
		      const time_t   now_sec)
{
	s->location = realloc(s->location, sz_location + 1);
	s->nb_loc ++;
	if (single)
		STORE(s->loc_until, 0, relaxed);
	else
		STORE(s->loc_until, now_sec + CURL_KEEP_LOCATION_TIME, relaxed);
	memcpy(s->location, location, sz_location);
	s->location[sz_location]= '\0';
	DEBUG(	"store_loc: %s for URL=%s until=%ld\n",
		s->location, s->URL,
		(single) ? 0 : s->loc_until - main_start.tv_sec);
}


/*
 * @brief does the actual request for the donwload/get_token fallback
 *
 * It is expected to get a 302 (redirect) = HTTP_FOUND. If so the location
 * is returned, otherwise it means the 'redirect' is not supported and
 * the function returns NULL. We use a HEAD req since we want only headers.
 *
 * @param pointer on the current stream
 * @param pointer on a curl handle
 * @return pointer on the location (or NULL if errors)
 */
static char *get_loc_fallback_wrapped(struct lfile *s, CURL *curl
						     , long *http_code)
{
	CURLcode    res;
	char *location;

	CURL_EASY_SETOPT(curl, CURLOPT_HTTPHEADER, NULL, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, NULL, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_NOBODY, 1L, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_POSTFIELDS, NULL, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_URL, s->URL, "%s");

	res = curl_easy_perform(curl);

	if (CURLE_OK != res) {
		lprintf(LOG_ERR,
			"%ld on fallback get_token curl_easy_perform url=`%s`.\n",
			res, s->URL);
		return NULL;
	}
	CURL_EASY_GETINFO(curl, CURLINFO_RESPONSE_CODE, http_code);
	if (HTTP_NOT_FOUND == *http_code) {     /* Here it is unknown      */
		refresh_root(REFRESH_404);	/* whether fallback works  */
		return NULL;		   	/* or not.                 */
	}
	if (HTTP_FOUND != *http_code) {
		lprintf(LOG_ERR,
			"expecting http_code 302, got %ld on fallback for url=`%s`.\n",
			*http_code, s->URL);
		return NULL;
	}
	CURL_EASY_GETINFO(curl, CURLINFO_REDIRECT_URL, &location);
	return location;
}

static const char *get_loc_fallback_wrap_stats(struct lfile *s, bool *single
							      ,	long *http_code)
{
	const char *location, *p;
	CURL *curl;

	curl = curl_init(true);
	location = get_loc_fallback_wrapped(s, curl, http_code);
	if (NULL != location)			/* Dup string before cleaning */
		location = strdup(location);	/* curl: returned ptr is freed*/
	curl_easy_cleanup(curl);

	if (NULL == location) {
		if (fallback_tested) {
			if (0 != AND(fallback_until, 0))
				lprintf(LOG_ERR,
					"fallback to get download tokens stopped working or network identification changed.\n");
		}
		else {
			lprintf(LOG_WARNING,
				"fallback to get download tokens does not work or network identification not used.\n");
		}
		return NULL;
	}
	if (!fallback_tested)
		lprintf(LOG_NOTICE, "fallback to get download tokens works!\n");
	p = strrchr(location, '/');
	if (NULL == p || p[1] != 'p')
		*single = true;
	return location;
}

/*
 * @brief fallback method to get a download token
 *
 *  If, for whatever reason (except 404 meaning the file is not there), the
 *  get_token API does not work, we try the "good old redirect method".
 *  Since it is automatic and we do not ask for any credentials beside the
 *  API key, this will only work if the "network identification" has been set
 *  and the program is used at the corresponding IP.
 *
 *  The function will test if the "fallback" works, if it does not work (or
 *  at some point stops working), it will never try to use it again, and
 *  the program needs to be restarted if the situation was fixed.
 *
 *  NOTE: when a location is returned, if the path starts with 'p' it is a
 *        'standard' location same as what the API returns with "single":0
 *        or without "single", because 0 is the default.
 *        If the path starts with 's' it is a "single" location, same as
 *        calling the API with "single":1
 *        For a "single", s->loc stays NULL, and the function only duplicates
 *        the string because curl_easy_cleanup will free it. The duplicated
 *        string is used only within stream_fs and will be freed there.
 *
 *  When testing for the first time, the function is locked on a global lock
 *  by the caller to avoid parallel tests. The caller then sets fallback_tested.
 *  fallback_tested changes only once under lock, so it does not need atomics.
 *  The other global fallback variables do.
 *
 * @param pointer on the current stream
 * @param time (seconds only) when the request started
 * @param pointer on a curl handle
 * @return pointer on the location (or NULL if errors)
 */

static enum hCode get_loc_fallback(struct lfile *s, const time_t now_sec)
{
	const char *location;
	long http_code;
	bool single = false;
	enum hCode res;
	
	if (fallback_tested) {
		if (0 == LOAD(fallback_until))
			return hEIO;
	}
	else {
		lprintf(LOG_NOTICE,
			"testing fallback to get download tokens.\n");
	}

#ifdef _STATS
	struct timespec	 start;

	/* needed for stats (see comment below) */
	if (NULL != params.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);
#endif

	location = get_loc_fallback_wrap_stats(s, &single, &http_code);

#ifdef _STATS
	/* The common function curl_json_perform also updates the stats, but it
	 * is not used for fallback, so stats must be updated here instead.   */
	if (NULL != params.stat_file)
		update_api_stats(FALLBACK_GET_TOKEN,
				 &start,
				 (NULL == location) ? 0 : HTTP_OK,
				 single ? 1 : 0);
#endif

	if (NULL != location) {
		store_loc(s, location, strlen(location), single, now_sec);
		free((char *)location);
		res = hOk;
	}
	else {
		res = (HTTP_NOT_FOUND == http_code) ? hNotFound : hEIO;
	}
	return res;
}

/*
 * @brief gets a new location for the URL (or file name if hidden)
 *
 *  This function will use the API donwload/get_token first.
 *  If it does not work, it will revert to the fallback above.
 *  When that happens and fallback is working, to avoid spamming the API
 *  that returned an error, the fallback method is used for KEEP_FALLBACK_TIME
 *  seconds. When that is elapsed, the function tries the API again.
 *
 *  For "hidden" situation, the fallback cannot be used, because there is no
 *  URL to redirect to since precisely the URL are hidden!
 *
 * @param pointer on the current stream
 * @param time (seconds only) when the request started
 * @param initialised json structure pointer
 * @return hCode
 */
static enum hCode get_new_location(struct lfile *s, 
				   time_t now_sec, struct json *j)
{
	bool fallback_used = true;
	char *p, *q = NULL;
	static const char down_post[]	= "{\"url\":\"%s\",\"single\":%d,\"no_ssl\":%d}";
	static const char down_hidden[]	= "{\"filename\":\"%s\",\"sharing_user\":\"%s\",\"folder_id\":%lld,\"no_ssl\":%d}";
	static const char url_pattern[]	= "\"url\":\"";
	enum hCode res;

	if (s->hidden) {
		char buf[sizeof(down_hidden) + strlen(s->URL)
					     + strlen(s->email)
					     + STRLEN_MAX_INT64];
		sprintf(buf, down_hidden, s->URL
					, s->email
					, s->id
					, (s->no_ssl) ? 1 : 0);
		res = curl_json_perform(buf, j, DOWNLOAD_GET_TOKEN, s->path);
		fallback_used = false;
	}
	else
	{
		/* Using directly fallback avoids spamming a not working API. */
		if (fallback_tested && now_sec < LOAD(fallback_until))
			return get_loc_fallback(s, now_sec);

		char buf[sizeof(down_post) + strlen(s->URL)];

		sprintf(buf, down_post, s->URL,
			((s->size <= engine_info.max_gap) ? 1 : 0),
		        ((s->no_ssl) ? 1 : 0));
		res = curl_json_perform(buf, j, DOWNLOAD_GET_TOKEN, s->path);
		fallback_used = false;
	}
	if (hOk != res) {
		if (hBusy == res)	   /* Don't try to use fallback when  */
			return res;	   /* file is unavailable, don't log  */
		lprintf(LOG_ERR,
			"hCode %d on get_token json resp.:`%s` for %s\n",
			(int)res, j->pb, s->URL);
		if (hNotFound == res) {
			/* Cache tree out of sync, resync all */
			refresh_root(REFRESH_404);
			return res;
		}
		if (s->hidden)		   /* Can't use fallback on "hidden": */
			return res;	   /*     no URL to redirect to!      */

		/* Trying fallback here. If it works keep it for some time.   */
		lock(&fallback_mutex, NULL);
		res = get_loc_fallback(s, now_sec);
		if (hNotFound != res) {
			STORE(fallback_until, now_sec + KEEP_FALLBACK_TIME);
			if (fallback_tested) {
				if (0 == OR(fallback_in_use, 1))
					lprintf(LOG_NOTICE,
						"Using fallback to get download tokens again!\n");
			}
			else {
				STORE(fallback_in_use, 1);
			}
			fallback_tested = true;
		}
		unlock(&fallback_mutex, NULL);
		return res;
	}
	if (fallback_tested && !fallback_used && 0 != AND(fallback_in_use, 0))
		lprintf(LOG_NOTICE, "API to get download tokens works again!\n");
	p = strstr(j->pb, url_pattern);
	if (NULL != p) {
		p += lengthof(url_pattern);
		q = strchr(p, '"');
	}
	if (NULL == q)
		lprintf(LOG_CRIT,
			"Unexpected get_token json response:`%s` for %s\n",
			j->pb, s->URL);

	store_loc(s, p, q - p, (s->size <= engine_info.max_gap), now_sec);
	return res;
}

/*
 * @brief gets a valid location for a stream
 *
 * Gets a location only when either there is no location or it is outdated 
 *
 * @param pointer on the current stream
 * @return hOK or relevant error code
 */

static int get_location(struct lfile *s)
{
	struct timespec now = {0};
	enum hCode	res = hOk;
	struct json	j;

	if (S_ERROR(s))
		return -EIO;

	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	if ( NULL == s->location || now.tv_sec > LOAD(s->loc_until, relaxed) ) {
		DEBUG("Getting new location for %s\n", s->URL);
		json_init(&j);
		res = get_new_location(s, now.tv_sec, &j);
		json_free(&j);
		if (hOk != res && hBusy != res)
			OR(s->counter, FL_ERROR, relaxed);
	}
	switch (res) {
		case hOk	: return 0;
		case hBusy	: return -EBUSY;
		case hNotFound	: return -ENOENT;
		default		: return -EIO;
	}
}



struct header_info {
	int http_code;
	off_t sz;
};

/* Extract and check information from the header
 * - Get the HTTP response code and check it is 206 (what is expected)
 * - Grabs the size from the Content-Range: header
 *
 * Note: this is the same prototype as curl's header function
 * 
 * @param buffer with the header
 * @param size of each member of data
 * @param number of members of data
 * @param pointer to user data
 * @return either size*nemb if Ok or anyting else when error
 */
static size_t extract_header_info(char *buf, size_t size, size_t nmemb
					   , void * userdata)
{
	struct header_info *h_info = userdata;
	size_t sz = size * nmemb; 	/* Readbility */

	if (0 == h_info->http_code) {
		http_response_status_code(buf, sz, &h_info->http_code);
		if (0 == h_info->http_code)
			return 0;
		if (HTTP_PARTIAL_CONTENT != h_info->http_code) {
			lprintf(LOG_ERR ,
				"Expected HTTP response code %d, got: %d\n",
				(int)HTTP_PARTIAL_CONTENT, h_info->http_code);
			return 0;
		}
	}
	else {
		if (0 == h_info->sz && !http_file_size(buf, sz, &h_info->sz))
			return 0;
	}
	return sz;
} 

/* Function called to start a new 'stream' at a given offset
 *
 *	It is expected that start_stream will do a range request
 *	starting at offset and up to the end of file (like: offset-)
 *	The return value is zero if all Ok and the opaque stream pointer will
 *	be set to a non-NULL value. If an error occurs it will be returned
 *	to the read() client and the opaque stream pointer is irrelevant.
 *
 * @param struct fi from fuse (return at open)
 * @param offset where to start reading
 * @param size when doing "single read" for regular streams : 0
 * @param where to store the opaque stream handle (passed to read/close)
 * @param where to store the socket fd
 * @param where to store the curl easy handle
 * @return Ok = 0, Error = negative number returned as read error.
 */
static int unfichier_start_stream_wrapped(struct lfile	*s,
					  off_t start_offset,
					  size_t size,
					  void **sh,
					  int *fd,
					  CURL **curl)
{
	int		res;
	long		http_code = 0;
	bool		retry;
	struct curl_raw_handle *crh;
	struct header_info h_info = { 0, 0 };
	const char 	hdr[] = { '\0' };  /* No additional header */

	/* Don't do anything if the file is already marked as error. */
	if ((0 != (LOAD(((s)->counter)) & FL_ERROR)))
		return -EIO;

	*curl = curl_init(true);
	*sh   = NULL;

	/* Get the URL ('location') */
	retry = false;
	do {
		res = 0;
		if (retry)
			lprintf(LOG_WARNING,
				"retrying unfichier_start_stream (URL=%s) on http_code: %ld\n",
				s->URL, http_code);
		else
			DEBUG(	"unfichier_start_stream (URL=%s)\n", s->URL);

		lock(&s->loc_mutex, NULL);
		res = get_location(s);

		/* That must be done before unlocking othewise s->location and
		 * s->loc_n might have changed already */
		if (0 == res) {
			DEBUG(	"raw curling %s (URL=%s) at %"PRIu64" sh=%p\n",
				s->location, s->URL, (uint64_t)start_offset, *sh);
			CURL_EASY_SETOPT(*curl, CURLOPT_URL, s->location, "%s");
		}
		unlock(&s->loc_mutex, NULL);

		if (0 != res) {
			lprintf(((-EBUSY == res) ? LOG_INFO : LOG_ERR) ,
				"Failed to get a location for URL=%s at %"PRIu64"\n",
				s->URL, (uint64_t)start_offset);
			return res;
		}

		/* Build and send the request */
		crh = send_request(*curl, s->location, hdr
					, extract_header_info, &h_info
					, start_offset, size, NULL);

		if (NULL == crh) {
			if (HTTP_NOT_FOUND == h_info.http_code ||
			    HTTP_GONE	   == h_info.http_code   ) {
				if (retry) {
					lprintf(LOG_ERR,
						"already retried, still %d http response code.\n",
						h_info.http_code);
					return -ENOENT;
				}
				retry = true;
				STORE(s->loc_until, 0, relaxed);
				continue;
			}
			lprintf(LOG_ERR, "cnx only error reading headers\n");
			return -EIO;
		}

		if (0 == h_info.sz || h_info.sz != s->size) {
			lprintf(LOG_ERR, "incoherent sizes: %lld reported: %lld\n",
				s->size, h_info.sz);
			free(crh);
			return -EIO;
		}
		break;
		
 	} while(1);
	
	*sh = crh;
	*fd = crh->sockfd;
	return 0;
}


/**
 * Callback functions for the new common read engine
 **/

int unfichier_error(int err, int line)
{
	if (0 != err)
		lprintf(LOG_CRIT,
			"system error %d in the read engine at line %d.\n",
			err, line);
	return err;
}

bool unfichier_close_if_idle(uint64_t fh,
			     void *sh, time_t last_read, time_t now, bool force)
{
	if (!force && last_read + CURL_IDLE_TIME > now)
		return false;

	struct curl_raw_handle *crh = (struct curl_raw_handle *)sh;
	curl_easy_cleanup(crh->curl);
	free(crh);

#ifdef _STATS
/* Stream is now closed on this file */
	struct lfile *s	= (struct lfile *)((uintptr_t)fh);	

	rcu_register_thread();
	rcu_read_lock();
	
	SUB(s->counter, OPEN_INC);

	rcu_read_unlock();
	rcu_unregister_thread();
#endif

	return true;
}

int unfichier_start_stream(const char *path, struct fuse_file_info *fi,
			   size_t size, off_t start_off, void **sh,
			   int *fd)
{
	int res;
	CURL *curl = NULL;
	struct fi_to_fh *fifh;
	struct lfile *s;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));
	s = (struct lfile *)((uintptr_t)(fifh->fh));	
	if (0 > start_off || start_off >= s->size)
		return -ENODATA;

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_start_stream_wrapped(s, start_off, size, sh, fd, &curl);

#ifdef _STATS
/* This is to keep the file in the list for statistics. It becomes useful
 * when using "single" (small files) otherwise the file is removed from
 * the list at the next open and stats displays "?????" 
 * So the streams are counted when opened and closed above */
	if (0 == res)
		ADD(s->counter, OPEN_INC);
#endif

	rcu_read_unlock();
	rcu_unregister_thread();

	if (0 != res && NULL != curl)
		curl_easy_cleanup(curl);
	return res;
}

