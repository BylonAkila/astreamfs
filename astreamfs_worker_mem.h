/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**************************************************************************
 * _MEMCOUNT would typically be defined when using debug or stats.
 */

#ifndef ASTREAMFS_WORKER_MEM_H
#define ASTREAMFS_WORKER_MEM_H

#define _GNU_SOURCE
#include <stdlib.h>
#include <malloc.h>
#include <stdalign.h>
#include <stdatomic.h>

#ifdef _MEMCOUNT
struct aw_mem_stats {
			/* Note: all 4 memory counters are managed only when
			 * the defaults memory function are used. Those default
			 * functions are also exposed for convenience if
			 * counting other allocations is needed. When the
			 * user provided his own functions, the 4 counters
			 * have undefined values. */
	unsigned long long n_alloc;
	unsigned long long n_free;
	unsigned long long mem_alloced;
	unsigned long long mem_freed;
			/* This counts contentions on push/pop of messages
			 * and a few atomics exchanges (like stream map) */
	unsigned long long contentions;
	unsigned int 	   max_spins;
};

/**
 * @brief same as the standard function aligned_alloc + statitics
 *
 * NOTE: this function and the following are only available when stats are
 *       'on'. Othewise just use the standard function (use a macro if
 *	 convenient).
 * There are used by default (or the standard functions when no STATS) when
 * no "memory management" function is provided for aw_operations in aw_init.
 *
 * The caller must consistenly either provide the 3 functions, or provide none.
 *
 * These default functions will count allocations in the 4 counters returned
 * above in aw_glob_stats.
 *
 * @param alignment for the returned buffer
 * @param size of the block to be allocated
 * @return pointer on the allocated buffer
 */
void *aw_aligned_alloc(size_t alignment, size_t size);

/**
 * @brief same as the standard function free + statitics
 *
 * NOTE: see above
 *
 * @param pointer to be freed
 * @return none
 */
void aw_free(void *ptr);

/**
 * @brief same as the standard function realloc + statitics
 *
 * NOTE: this function is not needed by astreamfs_worker but is provided
 *       for complete statistics when used elsewhere
 *
 * @param alignment for the returned buffer
 * @param size of the block to be allocated
 * @return pointer on the allocated buffer
 */
void *aw_realloc(void *ptr, size_t size);

/**
 * @brief just do statitics as if freed but do not free.
 *
 * NOTE: see above + notes in aw_operations structure comments
 *
 * @param pointer to be released
 * @return none
 */
void aw_release(void *ptr);


/**
 * @brief count contentions.
 *
 * For each atomic exchange loop, a 'spin' counter is taken. When the atomic
 * immediately succeeds, this shall be called with spin = 0, meaning there
 * were no contention.
 * When spin is not 0, there was a contention (the atomic exchange failed at
 * least once) and this counts the contention and the "highest spin".
 * Since update the "highest spin" is also done with an exchange... this
 * function can in turn also create a contention and give a higher spin count,
 * this is managed too.
 *
 * @param number of "spins"
 * @return none
 */
void aw_count_contentions(unsigned int spin);

/**
 * @brief retrieve memeory statistics
 *
 * @param pointer on statistics to return for memory
 * @return 0 or error
 */
void aw_get_mem_stats(struct aw_mem_stats *m_stats, memory_order order);

#else

void aw_release(void *unused_ptr);
#define aw_count_contentions(spin)
#define aw_get_mem_stats(m,o)

#endif
#endif
