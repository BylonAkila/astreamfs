/*
 * 1fichierfs: deals with statistics
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include "1fichierfs.h"
#ifdef _STATS

/*******************************************************
 *  These are the things you can change at compile time.
 */
static const char *stat_api_label[] = {
/*	FOLDER_LS		= 0, */ "folder/ls.cgi         ",
/*	DOWNLOAD_GET_TOKEN	= 1, */ "download/get_token.cgi",
/*	USER_INFO		= 2, */ "user/info.cgi         ",
/*	FILE_MV			= 3, */ "file/mv.cgi           ",
/*	FILE_CHATTR		= 4, */ "file/chattr.cgi       ",
/*	FILE_RM			= 5, */ "file/rm.cgi           ",
/*	FILE_CP			= 6, */ "file/cp.cgi           ",
/*	FOLDER_MV		= 7, */ "folder/mv.cgi         ",
/*	FOLDER_RM		= 8, */ "folder/rm.cgi         ",
/*	FOLDER_MKDIR		= 9, */ "folder/mkdir.cgi      ",
/*	FTP_USER_LS		=10, */ "ftp/users/ls.cgi      ",
/*	FTP_USER_RM		=11, */ "ftp/users/rm.cgi      ",
/*	FTP_USER_ADD		=12, */ "ftp/users/add.cgi     ",
/*	FTP_PROCESS		=13, */ "ftp/process.cgi       ",
/*	DOWNLOAD_GET_TOKEN	=14, */ "fallback/get_token    ",
};

static const char *stat_step_label[] = {
/*	STEP_WRITING	*/ "Writing ",
/*	STEP_WAIT_FTP	*/ "Wait_FTP",
/*	STEP_WAIT_MV	*/ "Wait_mv ",
/*	STEP_WAIT_RM	*/ "Wait_rm ",
/*	STEP_DONE	*/ "  Done  ",
};

/********************************************************
 *  Do NOT change the following definitions or variables.
 *  Things would break!
 *  These definitions and variables are either system dependent,
 *  or depend on how 1fichier works.
 */
#define N_API_ROUTES (sizeof(stat_api_label)/sizeof(stat_api_label[0]))

typedef unsigned long long tm_max_t;

struct stat_times {
	_Atomic tm_max_t	   max;
	_Atomic unsigned long long sec;
	_Atomic unsigned long      nsec;
};


struct sr_stat_times {
	struct timespec	t_sum;	/* Sum     of all times */
	struct timespec	t_max;	/* Maximum of all times */
};

struct sr_stats {
	unsigned long		active;
		/* Number of active readers for stats of streamers.
		 * Number of streamers that were once active then closed and
		 *        added to global statistics, for "global".
		 */
	unsigned long		cur_speed;	/* Current speed in bytes (for a
						 * streamer, zero for global) */

	unsigned long long	n_ticks;
		/* Number of ticks (as defined by SPEED_RES above) with data
		 * transfer. This should be used to compute "average speed".  */


	unsigned long long	n_streams; 	/* Number streams used so far */
	unsigned long long	contentions;	/* Nb of lock contentions     */
	unsigned long long	n_rq_in;	/* Nb of requests received    */
	unsigned long long	n_rq_out;	/* Nb ot requests responded   */
	unsigned long long	fuse_sz;	/* Sum of fuse requests size  */
	unsigned long long	network_sz;	/* Sum of network reads       */
	unsigned long long	move_sz;	/* Sum additional 'move'      */
		/* Fuse and network size can be different due to anticipation
		 * and requests using 'gaps'.
		 */

	struct sr_stat_times	rq_tm;		/* Times for normal requests  */
		/* 'Solo' is when no reader is available and a single range
		 * is done to handle an incoming request. This is quite
		 * under-optimised and hopefully a pretty uncommon read
		 * pattern */
	unsigned long long	solo_rq;	/* Nb of 'solo' requests      */
	unsigned long long	solo_sz;	/* Total size of 'solo' req.  */
	struct sr_stat_times	solo_tm;	/* Times for 'solo' requests  */
};


static const unsigned int refresh_category[] = {
/*	REFRESH_TRIGGER		= 0, */ 0,
/*	REFRESH_HIDDEN		= 1, */ 0,
/*	REFRESH_TIMER		= 2, */ 1,
/*	REFRESH_POLLING		= 3, */ 2,
/*	REFRESH_WRITE		= 4, */ 3,
/*	REFRESH_MV		= 5, */ 4,
/*	REFRESH_LINK		= 6, */ 4,
/*	REFRESH_UNLINK		= 7, */ 4,
/*	REFRESH_MKDIR		= 8, */ 4,
/*	REFRESH_RMDIR		= 9, */ 4,
/*	REFRESH_404		=10, */ 5,
/*	REFRESH_EXIT		=11, */ 6,
/*	REFRESH_INIT_ROOT	=12, */ 6,
};

static const char *refresh_category_label[] = {
			"Trig.", "Timer", "Poll.", "Write", "  API", "Error", " Auto" };

#define NB_REFRESH_CAT (sizeof(refresh_category_label) / \
			sizeof(refresh_category_label[0]))

#define MAX_ITEMS MAX_WRITERS

struct spd2 {
		uint32_t	   speed    [MAX_ITEMS][SPEED_RES*SPEED_MEM];
	_Atomic	tm_index_t	   tm_last  [MAX_ITEMS];
		unsigned long	   sec_dl   [MAX_ITEMS];
		unsigned long long size     [MAX_ITEMS];
	_Atomic	tm_index_t	   tot_last ;
	_Atomic	unsigned long	   tot_s_dl ;
};

static struct {
		struct spd2	   speed_write;
		unsigned long long write_nrq[MAX_WRITERS];
		unsigned long	   write_err[MAX_WRITERS];
		struct stat_times  write_tm [MAX_WRITERS];
	_Atomic	unsigned long long solo_sz  ;
	_Atomic	unsigned long	   solo_nrq ;
	_Atomic	unsigned long	   solo_err ;
		struct stat_times  solo_tm  ;
	_Atomic unsigned long      api_calls[N_API_ROUTES];
	_Atomic unsigned long      api_retry[N_API_ROUTES];
	_Atomic unsigned long      api_err  [N_API_ROUTES];
		struct stat_times  api_tm   [N_API_ROUTES];
	_Atomic unsigned long	   refresh[NB_REFRESH_CAT];
} all_stats;


/*
 * @brief returns the time max from delay
 *
 *  Compute the index from the delay shifting sec and nsec specified precision
 *
 * @param timespec pointer where the current timespec will be stored
 * @return time index.
 */
static tm_max_t tm_to_tm_max(struct timespec *delay)
{
	return (tm_max_t)delay->tv_sec  *  CLOCK_PREC +
			 delay->tv_nsec / (CLOCK_RES / CLOCK_PREC);
}


/*
static char *format_tm(unsigned long long num, char *buf)
{
	return raw_format_hr(num, buf, 1000, sec);
}
*/


/*
 * @brief compute the time average and formats it in the buffer
 *
 * @param seconds
 * @param nano seconds
 * @param dividend (for the average)
 * @param buffer to store the result (if needed, must be SZ_BUF_TIME size min)
 * @return none.
 */
static const char *tm_avg_to_str(unsigned long long sec, unsigned long nsec,
				 unsigned long long n, char *buf)
{
	static const char undef_avg[]= "  0.0000";

	if (0 == n)
		return undef_avg;

	nsec = ((sec % n) * CLOCK_RES + nsec) / n;
	sec /= n;
	sprintf(buf, "%3lu.%.04lu"
		   , (unsigned long)sec
		   , nsec / (CLOCK_RES / CLOCK_PREC));
	return buf;
}


/*
 * @brief formats the max time in the buffer
 *
 * Shift the max time appropriately according to the precision (CLOCK_PREC)
 *
 * @param max time
 * @param buffer to store the result (must be SZ_BUF_TIME size min)
 * @return none.
 */
static const char *tm_max_to_str(tm_max_t max, char *buf)
{
	sprintf(buf, "%3lu.%.04lu"
		   , (unsigned long)(max / CLOCK_PREC)
		   , (unsigned long)(max % CLOCK_PREC));
	return buf;
}


/*
 * @brief computes the current speed from the tick measures collected
 *
 * Algorithm: for tm_index_t, see: tm_to_tm_index
 *
 * 	Donwload speed is measured in ticks: SPEED_RES by second.
 *	It accumulates SPEED_MEM seconds.
 *	The first half second has a weight of 1
 *	Each subsequent half second is weighted +1 of the previous second.
 *	The total is then divide by the sum of weights: n * (n + 1) / 2,
 *	where 'n' is the number of half seconds, to normalize it to
 * 	a weight of 1. Finally multiplied by 2 to get speed by second.
 *
 *	Practical example SPEED_RES = 8 and SPEED_MEM = 2
 *
 *     -2s    -1.5s    -1s    -0.5s     0 ('last')
 *	|               |               |
 * 	|A.B.C.D|E.F.G.H|I.J.K.L|M.N.O.P|
 *
 * 	The compute, assuming we are in the same 'tm_index' as 'last' will be:
 *	(1 * (A+B+C+D) + 2 * (E+F+G+H) + 3 * (I+J+K+L) + 4 * (M+N+O+P)) / 5
 *
 *	When 'now' is ahead of 'last' the non-existent tick measures are zeroes,
 *	because it means no data was read in those ticks.
 *
 * 	This computation avoids abnormal "spikes" and smoothes the numbers.
 *
 * @param time index for 'now'
 * @param time index of the last tick measure
 * @param pointer on the collection of measures
 * @return the average speed.
 */
static unsigned long long compute_speed(tm_index_t now,
					tm_index_t last,
					uint32_t spds[])
{
	unsigned int i, j;
	unsigned long long speed = 0;

	if (now - last >= SPEED_RES*SPEED_MEM)
		return 0;

	for (i = SPEED_MEM * 2; i > 0; i--)
		for (j = 0; j < SPEED_RES / 2; j++, now--)
			if (now <= last)
				speed += spds[now % (SPEED_RES*SPEED_MEM)] * i;
	return speed / (2 * SPEED_MEM + 1);
}



/*
 * @brief retrieves the reference stream for the reader within live streams
 *
 * Can also return 'idle' when appropriate or '(???)' if the reference is not
 * found (should not happen!)
 * Note: as usual, we skip 'special' (shouldn't be) and 'del' streams.
 *
 * @param max time
 * @param buffer to store the result (must be SZ_BUF_TIME size min)
 * @return none.
 */
static const char *reader_ref(uint64_t fh, char *buf, void *userdata)
{
	struct lfiles *head = (struct lfiles *)userdata;
	static const char unkown[]   = "(???\?)";
	static const char overflow[] = "(****)" ;
	unsigned long i, cnt;

	for (i = 0; i < head->n_lfiles && i < 9999; i++) {
		cnt = LOAD(head->a_lfiles[i]->counter);
		if (0 != (cnt & FL_DEL))
			continue;
		if ((struct lfile *)((uintptr_t)(fh)) == head->a_lfiles[i]) {
			snprintf(buf, SZ_BUF_REF, "(%4lu)", i + 1);
			return buf;
		}
	}
	return (9999 == i) ? overflow : unkown;
}

/*
 * @brief conversly find the fh (if any) in stats to grab n_streams
 *
 * @param max time
 * @param buffer to store the result (must be SZ_BUF_TIME size min)
 * @return none.
 */
static const char *file_streams(struct lfile *lf, struct aw_stats *f_stats
						 , char *buf)
{
	static const char unkown[] = "    ?";
	unsigned int i;

	for (i = 0; i < engine_info.n_streams; i++) {
		if ((struct lfile *)((uintptr_t)(f_stats[i].u.f.fh)) == lf) {
			format_hr(f_stats[i].n_streams, buf);
			return buf;
		}
	}
	return unkown;
}


/*
 * @brief prepare the content of the stats file
 *
 * This function is called when opening the pseudo-file for statistics.
 * The pointer is then returned 1fichierfs DEBUG=1by unfichier_open, and is unique to the
 * process that opened this pseudo-file. Should other processes ask for
 * statistics in parallel, they will get a different buffer.
 * Subsequent read from that unfichier_open will return the data that is in
 * the buffer. unfichier_release will free the buffer (no rcu needed here).
 *
 * Note: since this is called by unfichier_open_wrapped, it is under rcu_lock.
 *       It is then safe to get the live (streams) pointer.
 *
 * IMPORTANT (and TODO): the formatting of the 1000000000data will be deprecated soon
 *	to be replaced by a more "linux-like" feature, same as data presented
 *	by the kernel under /sys. Formatting should not be made here, but
 *	by the (other) user-space tool that needs the statistics.
 *	This deprecation concerns only the formatting part (this function),
 *	the statistics computation stays.
 *
 * Global note:
 * 	Numbers: are displayed "human readable" à la 'curl'.
 * 		4 digits are displays plus a multiplier (K, M, G, etc...)
 *		When a decimal point is used, only 3 digits are displayed.
 *		Examples: 7248M, 1.04G, 12.7K, 940
 *	Time:	for avg and max the precision is 100 millisec: eg 999.1234
 *		for uptime and time left to stream: 2d hh:mm:ss
 *
 * Parts of statistics:
 *   Uptime: time since the program started (eg: 2d 14:23:05)
 *   Readers: for each reader the display shows
 *		- Index: [01] to MAX_READERS
 *		- Down.: bytes read
 *		- N.Req: Number of read requests completed
 *		- Avg Time: average time to respond to a read request
 *		- Max Time: maximum time a read request took
 *		- Ref: either 'idle' or the index of the stream (eg (   7) )
 *		- Qu: number of queued read requests
 *		- N.Err: number of errors
 *		- Speed: current read speed
 *		- Av.Sp: average speed. The average is computed only on seconds
 *			where some data was read. It does not change when
 *			the reader is idle or no request are being served.
 *			The average on the lifespan of the program can be
 *			computed with the uptime.
 *  	Solo:	This shown the same statistics as the readers when single ranges
 *		must be used. Now shown if no single range was ever used.
 *		Note: when this is shown, the global performance starts to drop.
 *		      To fix that, either do less parallel read or recompile
 *		      the program with more readers.
 *		Not displayed 'Ref', 'Qu', 'Speed': it would not make sense.
 *  	Total:	Total for all readers (including Solo if present)
 *   Writers: since writers are started only when needed, this part is shown
 *	      only if a writer have been started. Only started writers are shown
 *		- Index: [01] to MAX_WRITERS
 *		- Upload: bytes written
 *		- N.Req: Number of write requests completed
 *		- Avg Time: average time to respond to a write request
 *		- Max Time: maximum time a write request took
 *		- Ref: either 'idle' or the index of the file (eg (   7) )
 *		- N.Err: number of errors
 *		- Speed: current write speed
 *		- Av.Sp: average speed. The average is computed only on seconds
 *			where some data was written. It does not change when
 *			the writer is idle. The average on the lifespan of the
 *			program can be computed with the uptime.
 *   Total: Total for all started writers
 *
 * 	NOTE: what is measured is the read of write request received (through
 * 	      the fuse callbacks), not the network speed. This can lead to
 *	      noticeable differences, for example when doing a "pv" to copy
 *	      a file trough encfs stacked on top of 1fichierfs, "pv" will
 *	      measure the bytes written (to encfs), whereas 1fichierfs will
 *	      report the bytes sent by encfs. Because encfs sends some data
 *	      and then overwrites it, the data written by encfs is noticeable
 *	      bigger than the "useful" write (around 10 to 15% more data).
 *
 *   API: for each of the API routes where at least 1 request occurred, shows:
 * 		-      : name of the route (eg. folder/ls.cgi)
 *		- N.Req: Number of times this API was called.
 *		- Avg Time: Average time of all calls on this routes
 *		- Max Time: Maximum time it took for a request on this route
 *		- Retry: Number of retries on this route. This happens when the
 *			 server responds HTTP 429 (too many requests). In such
 *			 case, the user should do less parallel tasks to avoid
 *			 throttling.
 *		- N.Err: Number of errors on this route (429 not counted as err)
 *  	Total:	Total for all calls to any API route.
 *   Refresh: show count with cause of refresh. Causes are:
 *		"Trig." : triggered by the user (eg opening trigger file)
 *		"Timer" : timer elapsed (option --refresh-time)
 *		"Write" : when a write action completes it refreshes the target
 *			  directory where to store the file
 *		"  API" : some API calls trigger diectory refreshes (eg rename)
 *		"Error" : some errors do a full refresh (eg cache not synced)
 *		" Auto" : on init (when root is not /) and exit (always).
 *   Memory: show count and memory alloced/freed.
 * 	     plus contentions: number and max spin in the "memory" column.
 *   "Write files": if any, files being written are shown.
 *		- Ref: the reference of this file (links with writers Ref)
 *		- Size: size written so far
 *		- Left: time estimated to next step
 *		- Lnk: number of links on this file
 *		- Er: 'E' if an error was detected on this stream
 *		- Step: step in the writing process, can be: "Writing",
 * 			"Wait_FTP" (before FTP process is triggered),
 *			"Wait_mv" or "Wait_rm" when waiting for file to show in
 *			the upload directory to move or delete it, or "Done".
 *			The step can be prefixed with "rm/" if the user
 * 			requested the deletion of the file.
 *		- Path: target of the file.
 *   Live streams: for each 'live stream' (opened or recently closed files):
 *		- Ref: the reference of this file (links with readers Ref)
 *		- Size: size of the file
 *		- Left: time left before we consider this stream 'dead'
 *		- Lnk: number of links on this stream (opened + readers)
 *		- Er: 'E' if an error was detected on this stream
 *		- N.Loc: number of times download/get_token was called for this
 *			 stream.
 *		- N.Stm: number of curl requests (streams) done for this entry.
 *		- Path: the initial path of this entry (when first opened).
 *			Note: the key to the files is in fact the URL. So if
 *			the file was renamed/moved after the first open, the
 *			entry will still be used for streaming, but the path
 *			is not changed and keeps the value it had when first
 *			opened.
 * @param none
 * @return a pointer on the stat structure populated with the current stats.
 */
size_t out_stats(struct stats *stts)
{
	struct lfiles *lfls;
	unsigned long  n_lfiles;
	tm_max_t read_max, api_max, sum_max = 0;
	unsigned long long api_sec, read_sz, read_sec, read_nrq;
	unsigned long read_err, read_nsec;
	unsigned long api_calls, api_retry, api_err, api_nsec;
	unsigned long long sum_sz  = 0, sum_nrq  = 0;
	unsigned long long sum_sec = 0, sum_nsec = 0;
	unsigned long sum_err = 0, sum_calls = 0, sum_retry = 0;
	char avg[SZ_BUF_TIME], max[SZ_BUF_TIME];
	char buf1[SZ_BUF_HR], buf2[SZ_BUF_HR], buf3[SZ_BUF_HR];
	char buf4[SZ_BUF_HR], buf5[SZ_BUF_HR];
	uint32_t speed[SPEED_RES*SPEED_MEM];
	unsigned long long cur_speed, avg_speed, sum_speed = 0;
	tm_index_t tm_last, tm_tmp, tm_now;
	unsigned long sec_dl, sum_sec_dl = 0;
	struct timespec now;
	int i;
	unsigned long ii, n_loc, cnt;
	time_t t;
	struct aw_stats f_stats[engine_info.n_streams];

	struct wfile_list *wfl;
	bool *keep;
	unsigned long wn_files;

	if (0 != stts->size)
		return stts->size;

	/****************************
	 * Part: Uptime
	 */
	out_stat_header(stts, &now, &tm_now);

	/****************************
	 * Part: Readers (all)
	 */
	lfls = LOAD(live);
	if (NULL == lfls)
		n_lfiles = 0;
	else
		n_lfiles = lfls->n_lfiles;

	if (STAT_MAX_SIZE - 1 == out_stat_read(stts, f_stats, reader_ref, lfls))
		return STAT_MAX_SIZE - 1;


	/****************************
	 * Part: Writers (all)
	 */
	wfl = LOAD(wflist);
	wn_files = get_wfl_n_files(wfl, &keep);

	for (i = 0; i < MAX_WRITERS; i++) {
		char ref[] = " idle ";
		if (!get_writer_ref(wfl, keep, i, ref, sizeof(ref)))
			break;
		/* Don't display anything if no writers are started */
		if (0 == i) {
			SNPRINTF("Writers:\n%s",
				 "    Upload N.Req  Avg Time  Max Time   Ref     N.Err Speed Av.Sp\n");
			sum_speed = 0;
			sum_max   = 0;
			sum_sz    = 0;
			sum_nrq   = 0;
			sum_err   = 0;
			sum_sec   = 0;
			sum_nsec  = 0;
		}
		tm_last   = LOAD(all_stats.speed_write.tm_last[i]);
		/* This loop ensures that the speed measures and the tm_last
		 * are in sync. It is not an issue if this function has to
		 * loop a few times since statistics are not time-critical.
		 */
		do {
			tm_tmp   = tm_last;
			memcpy(speed, &all_stats.speed_write.speed[i][0],
			       sizeof(speed));
			tm_last  = LOAD(all_stats.speed_write.tm_last[i]);
		} while (tm_tmp != tm_last);
		sec_dl   = all_stats.speed_write.sec_dl[i];
		read_sz  = all_stats.speed_write.size[i];

		read_nrq = all_stats.write_nrq[i];
		read_err = all_stats.write_err[i];
		read_sec = LOAD(all_stats.write_tm[i].sec , relaxed);
		read_nsec= LOAD(all_stats.write_tm[i].nsec, relaxed);
		read_max = LOAD(all_stats.write_tm[i].max , relaxed);
		cur_speed = compute_speed(tm_now, tm_last, speed);
		sum_speed += cur_speed;
		if (read_max > sum_max)
			sum_max = read_max;
		sum_sz  += read_sz;
		sum_nrq += read_nrq;
		sum_err += read_err;
		sum_sec += read_sec;
		sum_nsec+= read_nsec;
		if (0 == sec_dl)
			avg_speed = 0;
		else
			avg_speed = read_sz / (unsigned long long)sec_dl;

		SNPRINTF("[%.02d] %s %s  %s  %s %s    %s %s %s\n",
			 i + 1,
			 format_hr(read_sz , buf1),
			 format_hr(read_nrq, buf2),
			 tm_avg_to_str(read_sec, read_nsec, read_nrq, avg),
			 tm_max_to_str(read_max, max),
			 ref,
			 format_hr(read_err , buf3),
			 format_hr(cur_speed, buf4),
			 format_hr(avg_speed, buf5));
	}
	/****************************
	 * Part: Writers (Total)
	 */
	if (i > 0) {
		SNPRINTF("%s","----------------------------------------------------------------\n");
		sum_sec   += sum_nsec / CLOCK_RES;
		sum_nsec   = sum_nsec % CLOCK_RES;
		sum_sec_dl = LOAD(all_stats.speed_write.tot_s_dl, relaxed);
		if (0 == sum_sec_dl)
			avg_speed = 0;
		else
			avg_speed = sum_sz / (unsigned long long)sum_sec_dl;
		SNPRINTF("Tot. %s %s  %s  %s           %s %s %s\n\n",
			format_hr(sum_sz   , buf1),
			format_hr(sum_nrq  , buf2),
			tm_avg_to_str(sum_sec, sum_nsec, sum_nrq, avg),
			tm_max_to_str(sum_max, max),
			format_hr(sum_err  , buf3),
			format_hr(sum_speed, buf4),
			format_hr(avg_speed, buf5));

	}


	/****************************
	 * Part: API (all)
	 */
	SNPRINTF("API:\n%s",
		 "                        N.Req  Avg Time  Max Time Retry N.Err\n");
	sum_sec = 0;
	sum_nsec= 0;
	sum_err = 0;
	sum_max = 0;
	for (i=0; i<N_API_ROUTES; i++) {
		api_calls = LOAD(all_stats.api_calls[i]  , relaxed);
		if (0 == api_calls)
			continue;
		api_retry = LOAD(all_stats.api_retry[i]  , relaxed);
		api_err   = LOAD(all_stats.api_err[i]	 , relaxed);
		api_sec   = LOAD(all_stats.api_tm[i].sec , relaxed);
		api_nsec  = LOAD(all_stats.api_tm[i].nsec, relaxed);
		api_max   = LOAD(all_stats.api_tm[i].max , relaxed);
		sum_sec   += api_sec;
		sum_nsec  += api_nsec;
		sum_calls += api_calls;
		sum_retry += api_retry;
		sum_err   += api_err;
		if (api_max > sum_max)
			sum_max = api_max;
		SNPRINTF("%s: %s  %s  %s %s %s\n",
			 stat_api_label[i],
			 format_hr(api_calls, buf1),
			 tm_avg_to_str(api_sec, api_nsec, api_calls, avg),
			 tm_max_to_str(api_max, max),
			 format_hr(api_retry, buf2),
			 format_hr(api_err  , buf3));
	}

	/****************************
	 * Part: API (Total)
	 */
	SNPRINTF("%s","--------------------------------------------------------------\n");
	sum_sec += sum_nsec / CLOCK_RES;
	sum_nsec = sum_nsec % CLOCK_RES;
	SNPRINTF("Total                 : %s  %s  %s %s %s\n\n",
		 format_hr(sum_calls, buf1),
		 tm_avg_to_str(sum_sec, sum_nsec, sum_calls, avg),
		 tm_max_to_str(sum_max, max),
		 format_hr(sum_retry, buf2),
		 format_hr(sum_err  , buf3));

	/****************************
	 * Part: Refresh
	 */
	 SNPRINTF("%s", "        ");
	 for (i = 0; i < NB_REFRESH_CAT; i++)
		 SNPRINTF(" %s", refresh_category_label[i]);
	 SNPRINTF("%s", "   Total\nRefresh:");
	 sum_nrq  = 0;
	 for (i = 0; i < NB_REFRESH_CAT; i++) {
		ii = LOAD(all_stats.refresh[i]);
		sum_nrq += ii;
		SNPRINTF(" %s", format_hr((unsigned long long)ii, buf1));
	}
	SNPRINTF("   %s\n\n", format_hr(sum_nrq, buf1));

	/****************************
	 * Part: Memory
	 */

	stts->size = out_stats_mem(stts->buf, stts->size, STAT_MAX_SIZE);
	if (STAT_MAX_SIZE - 1 == stts->size)
		return STAT_MAX_SIZE - 1;

	/****************************
	 * Part: Live streams
	 */
	if (0 == wn_files && (NULL == lfls || 0 == lfls->n_lfiles))
		return stts->size;

	/* Sub-part 'title'
	 */
	SNPRINTF("Live streams: |Write| %lu    (Read) %lu\n",
		 wn_files,
		 ((NULL == lfls) ? 0 : lfls->n_lfiles));
	SNPRINTF("%s", "  Ref   Size  Left Lnk Er N.Loc N.Stm Path\n");

	/* Sub-part for 'write' streams
	 */
	for (ii = 0; ii < wn_files; ii++) {
 		uint64_t size;
		time_t   eta;
		unsigned long counter, step;
		unsigned int m;
		const char *path;
		char chron[] = "--:--";

		get_wf_info(wfl, keep, ii, &size, &eta, &counter, &step, &path);
		if (0 < eta) {
			if (eta >= now.tv_sec + main_start.tv_sec) {
				eta -= (now.tv_sec + main_start.tv_sec);
				m = eta / 60;
				if (m > 99)
					snprintf(chron, sizeof(chron),
						 "**:**");
				else
					snprintf(chron, sizeof(chron),
						 "%.02u:%.02lu",
						 m, (unsigned long)eta % 60);
			}
			else {
				eta = (now.tv_sec + main_start.tv_sec) - eta;
				m = eta / 60;
				if (m > 9)
					snprintf(chron, sizeof(chron),
						 "-*:**");
				else
					snprintf(chron, sizeof(chron),
						 "-%.01u:%.02lu",
						 m, (unsigned long)eta % 60);
			}
		}
		SNPRINTF("|%4lu| %s %s %3lu  %c %s%s %s\n",
			 ii + 1,
			 format_hr(size, buf1),
			 chron,
			 (counter & OPEN_MASK) / OPEN_INC,
			 (counter & FL_ERROR) != 0 ? 'E'   : ' '  ,
			 (counter & FL_DEL  ) != 0 ? "rm/" : "   ",
			 stat_step_label[step],
			 path);
	}
	if (0 != wn_files) {
		free(keep);
	/* Separation line when both parts are present
	 */
		if (NULL != lfls) {
			SNPRINTF("%s", "------\n");
		}
	}

	/* Sub-part for 'read' streams
	 */

	if (NULL == lfls)
		return stts->size;
	n_loc = get_stats_glob_locations();
	for (ii = 0; ii < n_lfiles; ii++) {
		n_loc += lfls->a_lfiles[ii]->nb_loc;
		cnt = LOAD(lfls->a_lfiles[ii]->counter, relaxed);
		if (0 != (cnt & FL_DEL))
			continue;

		t = lfls->a_lfiles[ii]->loc_until;
		if (t > now.tv_sec + main_start.tv_sec)
			t -= now.tv_sec + main_start.tv_sec;
		else
			t = 0;
		SNPRINTF("(%4lu) %s %.02lu:%.02lu %3lu  %c %s %s %s\n",
			 ii + 1,
			 format_hr(lfls->a_lfiles[ii]->size, buf1),
			 t / 60, t % 60,
			 (cnt & OPEN_MASK) / OPEN_INC,
			 (cnt & FL_ERROR) != 0 ? 'E' : ' ',
			 format_hr(lfls->a_lfiles[ii]->nb_loc, buf2),
			 file_streams(lfls->a_lfiles[ii], f_stats, buf3),
			 lfls->a_lfiles[ii]->path);
	}


	return stts->size;
}

/*
 * @brief time increment
 *
 * The statistic times are update so:
 * 	- Add the delay between now and start param.
 * 		If nsec is above 1000000000, normalise it (atomic exchange).
 *	- Save the 'max' if needed.
 *
 * @param time statistics pointer to update
 * @param start time
 * @return none
 */
static void inc_time(struct stat_times *t, struct timespec *start )
{
	unsigned long spin = 0;
	unsigned long cur_nsec, nsec, sec;
	tm_max_t max, cur_t;
	struct timespec now;

	clock_gettime(CLOCK_MONOTONIC, &now);
	if (now.tv_nsec < start->tv_nsec) {
		now.tv_nsec += CLOCK_RES;
		now.tv_sec  -= 1;
	}
	now.tv_nsec -= start->tv_nsec;
	now.tv_sec  -= start->tv_sec;
	cur_nsec = LOAD(t->nsec);

	do {
		spin++;
		nsec = cur_nsec + now.tv_nsec;
		if (nsec >= CLOCK_RES) {
			nsec -= CLOCK_RES;
			sec  = now.tv_sec + 1;
		}
		else {
			sec  = now.tv_sec;
		}
	} while(!XCHG(t->nsec, cur_nsec, nsec));
	spin_add(spin -1, "inc_time(1)");
	/* Atomic operations on 64 bits implies a lock on 32 bits systems, so
	 * avoiding it when not necessary should be beneficial in spite of
	 * the test and jump cost */
	if (0 != sec)
		ADD(t->sec, (unsigned long long)sec, relaxed);

	cur_t = tm_to_tm_max(&now);
	max = LOAD(t->max);
	spin = 0;
	while (cur_t > max && !XCHG(t->max, max, cur_t))
		spin++;
	spin_add(spin, "inc_time(2)");
}

/*
 * @brief update speed statistics
 *
 * This is called by the "engine", precisely prune_queue when full buffers
 * are sent back. Calling it there avoids locking.
 *
 * @param number of bytes written (if negative = error)
 * @param index of the reader (from 0 to MAX_READERS - 1)
 * @return none
 */
void update_speed_stats(bool read, int written, unsigned int i)
{
	unsigned int j;
	struct timespec now;
	tm_index_t tm_now, tot_last;
	struct spd2 *sp;

	if (NULL == params.stat_file)
		return;

	sp = &all_stats.speed_write;
	sp->size[i] += (unsigned long long)written;

	tm_now = tm_to_tm_index(&now);
	if (tm_now == sp->tm_last[i]) {
		j = tm_now % (SPEED_RES * SPEED_MEM);
		if (likely(sp->speed[i][j] < UINT32_MAX - (long)written))
			sp->speed[i][j] += written;
		else
			sp->speed[i][j]  = UINT32_MAX;
	}
	else {
		if (tm_now - sp->tm_last[i] >= (SPEED_RES*SPEED_MEM)) {
			sp->sec_dl[i]++;
			memset(sp->speed[i],
			       '\0',
			       sizeof(sp->speed[0]));
		}
		else {
			if (sp->tm_last[i] / SPEED_RES !=
			    tm_now / SPEED_RES)
				sp->sec_dl[i]++;
			for (j = 1; sp->tm_last[i] + j < tm_now; j++)
				sp->speed[i]
					[(sp->tm_last[i] + j) %
						(SPEED_RES*SPEED_MEM)] = 0;
		}
		sp->speed[i][tm_now % (SPEED_RES*SPEED_MEM)] = written;
		STORE(sp->tm_last[i], tm_now);
		tot_last = LOAD(sp->tot_last);
		if (tot_last / SPEED_RES != tm_now / SPEED_RES) {
			if (XCHG(sp->tot_last, tot_last, tm_now))
				ADD(sp->tot_s_dl, 1UL, relaxed);
		}
	}
}


/*
 * @brief update write statistics
 *
 * Times and request count.
 *
 * @param number of bytes written (not called on error, is always >0)
 * @param index of the writer
 * @param timespec when the write was received
 * @return none
 */
void update_write_stats(int written, unsigned int i, struct timespec *start)
{
	if (i >= MAX_WRITERS)	/* This can happen when racing with a DEL     */
		return;		/* In this case, do nothing avoids mem corrup */

	inc_time(&all_stats.write_tm[i], start);

	all_stats.write_nrq[i] += 1UL;
}

/*
 * @brief update write errors
 *
 * Error detected by async writers are counted here.
 *
 * @param index of the writer
 * @param timespec when the write was received
 * @return none
 */
void update_write_err(unsigned int i)
{
	all_stats.write_err[i]++;
}

/*
 * @brief update read statistics when doing a single range
 *
 *
 * @param number of bytes written (if negative = error)
 * @param timespec when the read was received
 * @return none
 */
void update_single_stats(int written, struct timespec *start)
{
	if (written >= 0)
		ADD(all_stats.solo_sz, (unsigned long long)written, relaxed);
	else
		ADD(all_stats.solo_err, 1L, relaxed);
	ADD(all_stats.solo_nrq, 1UL, relaxed);

	inc_time(&all_stats.solo_tm, start);
}


/*
 * @brief update api statistics
 *
 *
 * @param index of the API route
 * @param timespec when the api call started
 * @param http_code of the response
 * @param number of retries
 * @return none
 */
void update_api_stats(enum api_routes rt, struct timespec *start,
		      enum hCode hcode, unsigned int retry)
{
	ADD(all_stats.api_calls[rt], 1UL, relaxed);
	if (0 != retry)
		ADD(all_stats.api_retry[rt], retry, relaxed);
	if (HTTP_OK != hcode && hBusy != hcode)
		ADD(all_stats.api_err[rt], 1UL, relaxed);
	inc_time(&all_stats.api_tm[rt], start);
}


/*
 * @brief update refresh statistics
 *
 * @param cause of the refresh
 * @return none
 */
void update_refresh_stats(enum refresh_cause cause)
{
	ADD(all_stats.refresh[refresh_category[cause]], 1UL, relaxed);

}

#endif /* _STATS, if no stats, this is an empty code. */
