/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**************************************************************************
 * This is the statistic part of astreamfs_reader.c
 * It is meant to be as fast as possible, especially when not needed.
 * - There is an option not to build statictics in (define NOSTATS at build)
 *   which replaces stats function call with empty macros so that there
 *   is not even a call in the executable.
 * - When stats are built in but not yet initialised, internal functions that
 *   could take time (such as getting the precise time) are replaced by
 *   a function that does only 'return'
 *
 * Speeds
 * ------
 *	What the user sees is how fast read requests are completed.
 *	So, the best we can measure is at the fuse level, the time it takes
 *	to complete a read: from when the request hits the read or read_buf
 *	fuse callback, to when the engine is about to return from that callback.
 *	This might be different from the network speed at some moments but
 *	should be almost the same on average.
 *
 * 	Two kinds of speed are measured:
 *	- Current speed
 *	- Average speed
 *
 * 	Each of these speeds is measured:
 *	- globally (1 value)
 *	- for each thread (1 value per thread)
 *	- for each file (1 value for each active file)
 *
 * 	(Not measured: for each stream inside a file!)
 *
 * 	The speed measure resolution is given by SPEED_RES
 *	(in  astreamfs_worker_stats.h)
 *
 * 	Current speed is computed by accumulating complete read requests in the
 *	last SPEED_MEM (in  astreamfs_worker_stats.h) seconds.
 *	Then a weighted average is done, giving more importance to recent
 *	ticks. Note: a tick = (1 / SPEED_RES) second.
 *
 *	The average is computed by accumlating byte transferred (complete read
 *	requests with no error) and getting the number of ticks when there
 *	was an active transfer. This is the number of ticks between the
 *	start and end of a fuse request. An algorithm (speed_add function)
 *	adds those ticks without counting duplicates since requests can happen
 *	in parallel. In the end the average is computed from that.
 *
 *	To summarize, once a read request of B bytes is complete, it is counted
 *	inside the tick when it finished for current speed. If it took N ticks
 *	to complete, it is as if each tick saw B/N bytes for the average.
 *
 * Statistic limits
 * ----------------
 *	Because of parallelism, and the fact that a thread can be sleeping for
 *	an arbitrary amount of time, some numbers might be dropped.
 *
 * 	Current speed: if a request is more than SPEED_MEM seconds late
 *	when arriving at the thread that should count it, it is dropped from
 *	being accumulated into the transferred bytes.
 *
 * 	Average speed: if a request is more than the number of ticks of the
 *	averaging algorithm late, it is assumed the late ticks of this request
 *	were already counted. The number of ticks is 32/64 in 32/64bits O.S.
 *
 * 	These situations are unlikely... but anything can happen in parallelism.
 *	Since this is about statistics, it is not a blocking issue that in
 *	extremely rare cases current speed might be underestimated.
 *
 * 	For the average speed, the consequence is that it can become inaccurate
 *	when the speed falls beyond a given threshold (~16kB/s in 64bits with
 *	current constants). In such case, the average speed might be
 *	over-estimated to the threshold.
 *	Should that happen, the most important issue is that the server is
 *	irresponsive (at least for some files). Overestimated average statistics
 *	are of lesser importance than irresponsiveness of the server!
 *
 *	Because of parallelism and speed that can be extremely low, there is
 *	no solution to have perfect numbers without consuming an unbounded
 *	amount of memmory for the upkeep of statistics.
 */


#ifdef _STATS

static struct aw_stats *stats_glob  = NULL;
static struct aw_stats *stats_files = NULL;
static struct spd      *spd_glob    = NULL;
static struct spd      *spd_files   = NULL;

/* "Solo" statistics being done in the "fuse thread" using a standard
 * mutex is fine as is does not at all block anything in the async worker.
 * Hence the "solo" global variables do not need _Atomic */
static pthread_mutex_t  solo_stats_mutex = PTHREAD_MUTEX_INITIALIZER;

static struct aw_solo_stats solo_stats = {0, 0, {{0,0}, {0,0}}};


/**
 * Utility functions that are called within this source only do not need
 * the indirection since they are not called with dummies!
 */

/*
 * @brief utility: "adds" stats
 *
 * The "addition" is a real addition for the "t_sum" part of the structure
 * and is a "max" for the max part.
 *
 * @param target of the "addition".
 * @param what to add
 * @return none
 */
static void add_stats_tm(struct aw_stat_times *dest,
                         const struct timespec *to_add)
{
        if (to_add->tv_sec >= dest->t_max.tv_sec) {
                if (to_add->tv_sec > dest->t_max.tv_sec) {
                        dest->t_max.tv_sec  = to_add->tv_sec ;
                        dest->t_max.tv_nsec = to_add->tv_nsec;
                }
                else if (to_add->tv_nsec > dest->t_max.tv_nsec) {
                        dest->t_max.tv_nsec = to_add->tv_nsec;
                }
        }
        dest->t_sum.tv_sec  += to_add->tv_sec;
        dest->t_sum.tv_nsec += to_add->tv_nsec;
        if (CLOCK_RES <= dest->t_sum.tv_nsec) {
                dest->t_sum.tv_nsec -= CLOCK_RES;
                dest->t_sum.tv_sec  += 1;
        }
}
#undef add_stats_tm

/*
 * @brief utility: timespec difference
 *
 * @param where to store the difference = end - start
 * @param timespec start
 * @param timespec end
 * @return none
 */
static void diff_tm(struct timespec *diff, const struct timespec *start
                                         , const struct timespec *end)
{
        diff->tv_sec = end->tv_sec - start->tv_sec;
        if (end->tv_nsec >= start->tv_nsec) {
                diff->tv_nsec = end->tv_nsec - start->tv_nsec;
        }
        else {
                diff->tv_nsec = CLOCK_RES + end->tv_nsec - start->tv_nsec;
                diff->tv_sec -= 1;
        }
}
#undef diff_tm

#define tm_to_tick(tm) (((tick_t)(tm)->tv_sec ) * SPEED_RES + \
                        ((tick_t)(tm)->tv_nsec) / (CLOCK_RES / SPEED_RES))

/*
 * @brief utility: add the read size to the relevant "speed" index
 *
 * @param index of the speed structure
 * @param tick of current time
 * @param tick of the real start of the read request
 * @param timespec end of read request
 * @param bytes written
 * @return none
 */
static void speed_add(struct spd *speed, tick_t now, tick_t start0,
                      tick_t end, ssize_t written)
{
        unsigned int i;

	#define TICK_MATRIX_SZ (8 * sizeof(unsigned long))

	AW_ASSERT(end <= now);
	AW_ASSERT(start0 <= end);

	/* Number of active ticks to count (tick_dl) */
	/* Shift the bit matrix if needed*/
	if (now > speed->tick_now) {
		/* Shift more than width of the operand = undefined behaviour */
		if (now - speed->tick_now < TICK_MATRIX_SZ)
			speed->tick_bits >>= (now - speed->tick_now);
		else
			speed->tick_bits = 0;
	}

	if (end + TICK_MATRIX_SZ > now) {
		unsigned long bmask;
		bmask = ULONG_MAX >> (now - end);
		if (start0 + TICK_MATRIX_SZ <= now)
			aw_ops.log(LOG_INFO,
				   "late or slow request dropped for average ticks count.\n");
		else
			if (now - start0 + 1 < TICK_MATRIX_SZ)
				bmask ^= (ULONG_MAX >> (now - start0 + 1));
		/* Now xmask has 1 from start0 to end ticks relative to now */
		if (speed->tick_bits !=  (speed->tick_bits | bmask)) {
			/* Some new bits to count! */
			int add;
			add = __builtin_popcountl(speed->tick_bits | bmask) -
			      __builtin_popcountl(speed->tick_bits);
			speed->tick_dl   += add;
			speed->tick_bits |= bmask;
		}
	}
	else {
		aw_ops.log(LOG_INFO,
			   "late request dropped for average ticks count.\n");
	}

        /* When now is ahead of tick_now, insert zeroes
         * A maximum of SPEED_RES * SPEED_MEM to insert. */
        for (i = 0; speed->tick_now < now && i < SPEED_SZ; i++) {
                speed->tick_now++;
                speed->speed[speed->tick_now % SPEED_SZ] = 0;
        }
        /* In case tick_now was more than SPEED_SZ below, reset it now. */
        if (speed->tick_now < now)
                speed->tick_now = now;

        /* Add the written size in the right tick if not too old*/
        if (now - end < SPEED_SZ)
                speed->speed[end % SPEED_SZ] += written;
}
#undef speed_add

/*
 * @brief add thread stats to global
 *
 * @param pointer on statistics to return
 * @param index of the thread to be added
 * @return none
 */
static void sum_stats(struct aw_stats *t_stats, tid_t t)
{
        t_stats[0].u.g.active_files += t_stats[t].u.g.active_files;
        t_stats[0].active_streams   += t_stats[t].active_streams;
        t_stats[0].u.g.n_files      += t_stats[t].u.g.n_files;
        t_stats[0].n_streams        += t_stats[t].n_streams;
        t_stats[0].overread         += t_stats[t].overread;

        t_stats[0].start_times.t_sum.tv_sec
                                    += t_stats[t].start_times.t_sum.tv_sec;
        if (t_stats[0].start_times.t_sum.tv_nsec +
            t_stats[t].start_times.t_sum.tv_nsec  > CLOCK_RES) {
                t_stats[0].start_times.t_sum.tv_nsec
                        -= (CLOCK_RES - t_stats[t].start_times.t_sum.tv_nsec);
                t_stats[0].start_times.t_sum.tv_sec += 1;
        }
        else {
                t_stats[0].start_times.t_sum.tv_nsec
                                    += t_stats[t].start_times.t_sum.tv_nsec;
        }

        if ( t_stats[0].start_times.t_max.tv_sec <
                                        t_stats[t].start_times.t_max.tv_sec ||
            (t_stats[0].start_times.t_max.tv_sec ==
                                        t_stats[t].start_times.t_max.tv_sec &&
             t_stats[0].start_times.t_max.tv_nsec <
                                        t_stats[t].start_times.t_max.tv_nsec)) {
                t_stats[0].start_times.t_max.tv_sec =
                                        t_stats[t].start_times.t_max.tv_sec;
                t_stats[0].start_times.t_max.tv_nsec =
                                        t_stats[t].start_times.t_max.tv_nsec;
        }
}
#undef sum_stats

/*
 * @brief compute speed from raw spd values
 *
 * @param pointer on statistics to return
 * @param pointer on the raw "speed" structure for computation
 * @param index of the thread to be added
 * @param tick for speed computation
 * @return none
 */
static void compute_speeds(struct aw_stats *t_stats, struct spd *t_spd,
                           tick_t now)
{
        unsigned int i, j, k;
        unsigned long long speed;
        uint32_t speeds[SPEED_RES*SPEED_MEM];

        if (now >= t_spd->tick_now + SPEED_SZ) {
                t_stats->cur_speed = 0;
        }
        else {
                for (i = SPEED_SZ; 0 < i; now--) {
                        i--;
                        if (now > t_spd->tick_now)
                                speeds[i] = 0;
                        else
                                speeds[i] =
                                        t_spd->speed[now % SPEED_SZ];
                }

                speed = speeds[0] + speeds[1] + speeds[2];
                k = 3;
                for (i = 2; i < 5; i++)
                        for (j = 0; j < 4; j++, k++)
                                speed += i * speeds[k];

                speed /= 39;
                t_stats->cur_speed = speed * SPEED_RES;
        }
        if (0 == t_spd->tick_dl)
                t_stats->avg_speed = 0;
        else
                t_stats->avg_speed = t_stats->size /
                                     t_spd->tick_dl * SPEED_RES;
}
#undef compute_speeds

/*
 * @brief: move timespec
 *
 * @param dest timespec
 * @param src timespec
 * @return none
 */
static void real_move_tm(struct timespec *dest, const struct timespec *src)
{
        dest->tv_sec  = src->tv_sec;
        dest->tv_nsec = src->tv_nsec;
}
#undef real_move_tm
static void dummy_move_tm(struct timespec *dest_unused,
                          const struct timespec  *src_unused)
                           { (void)dest_unused; (void) src_unused;}

/*
 * @brief raw copy of statistics
 *
 * This part of getting statistics is running inside the main async loop
 * it tries to do the minimum necessary and let other computations to the
 * caller in the fuse thread.
 *
 * @param private thread data
 * @param stat get action
 * @return none
 */
static void real_raw_copy(const struct private_data *pdata, struct action *a)
{
        sid_t i, j, k;
        bool done;
        struct action *r;
        uint64_t fhlist[N_FILES];

        memcpy(a->a.sg.t_stats + tidx + 1, stats_glob + tidx + 1
                                         , sizeof(struct aw_stats));

        memcpy(a->a.sg.t_spd + tidx + 1, spd_glob + tidx + 1
                                       , sizeof(struct spd));

        /* files */
        k = 0;
        for (i = pdata->owned_streams; N_STREAMS != i; i = pdata->list[i]) {
                done = false;
                /* Count streams for "unique" files */
                for (j = 0; j < k; j++)
                        if (strms[i].fh == fhlist[j]) {
                                a->a.sg.f_stats[strms[i].fid].active_streams++;
                                done = true;
                                break;
                        }
                if (0 == strms[i].fh)   /* Skip files not opened or error */
                        continue;
                if (!done) {
                /* Statistics for the new "unique file" they have all been
                 * collected in the "mapped" slot. */
                        fhlist[k++] = strms[i].fh;
                        memcpy( a->a.sg.f_stats + strms[i].fid,
                                stats_files + strms[i].fid,
                                sizeof(struct aw_stats));
                        a->a.sg.f_stats[strms[i].fid].active_streams = 1;

                        memcpy( a->a.sg.f_spd  + strms[i].fid,
                                spd_files + strms[i].fid,
                                sizeof(struct spd));
                }
                /* Count pending requests and buffers */
                for (r = strms[i].queue; NULL != r; r = r->next)
                        if (ACTION_GAP == r->type)
                                a->a.sg.f_stats[strms[i].fid].u.f.n_buf += 1;
                        else
                                a->a.sg.f_stats[strms[i].fid].u.f.n_rq += 1;
        }
        if (O_THREAD == tidx) {
                /* Copy the idle file stats */
                for (i = o_free_head; i != N_STREAMS; i = o_list[i])
                        if (0 == a->a.sg.f_stats[i].u.f.fh &&
                            0 !=     stats_files[i].u.f.fh) {
                                memcpy( a->a.sg.f_stats + i,
                                        stats_files + i,
                                        sizeof(struct aw_stats));
                                a->a.sg.f_stats[i].u.f.tid = max_threads;

                                memcpy( a->a.sg.f_spd  + i,
                                        spd_files + i,
                                        sizeof(struct spd));
                        }

                /* Global stats */
                a->a.sg.t_stats[0].n_reads  = stats_glob[0].n_reads;
                a->a.sg.t_stats[0].size     = stats_glob[0].size;
                memcpy(&a->a.sg.t_stats[0].read_times,
                       &stats_glob[0].read_times,
                       sizeof(struct aw_stat_times));
                memcpy(a->a.sg.t_spd, spd_glob, sizeof(struct spd));
        }
        a->type = ACTION_GAP;   /* Needed by aw_get_stats for optim. loop */
        finish_action(a);
}
#undef real_raw_copy
static void dummy_raw_copy(const struct private_data    *pdata_unused,
                           struct action        *a_unused) {
                                           (void)pdata_unused;
                                           (void)a_unused; }

/*
 * @brief count retries
 *
 * @param the index of the stream being retried
 * @return none
 */
static void real_count_retries(sid_t sid)
{
        stats_glob[tidx + 1].n_retries += 1;
        stats_files[strms[sid].fid].n_retries += 1;
}
#undef real_count_retries
static void dummy_count_retries(sid_t sid_unused) { (void)sid_unused; }

/*
 * @brief count the new stream
 *
 * @param Increment if true, decrement otherwise
 * @param the index of this new stream
 * @return none
 */
static void real_count_streams(bool inc, sid_t sid)
{
        if (inc) {
                stats_glob[tidx + 1].active_streams   += 1;
                stats_glob[tidx + 1].n_streams        += 1;
                stats_files[strms[sid].fid].n_streams += 1;
        }
        else {
                AW_ASSERT(0 != stats_glob[tidx + 1].active_streams);
                stats_glob[tidx + 1].active_streams   -= 1;
        }
}
#undef real_count_streams
static void dummy_count_streams(bool inc_unsuned,  sid_t sid_unused) {
                              (void) inc_unsuned;  (void)sid_unused; }

/*
 * @brief count the over read in a stream
 *
 * @param add or substract to over
 * @param the index where it happens
 * @return none
 */
static void real_count_over(ssize_t written,  sid_t sid)
{
        stats_glob[tidx + 1].overread += written;
        stats_files[strms[sid].fid].overread += written;
}
#undef real_count_over
static void dummy_count_over(ssize_t written_unused,  sid_t sid_unused) {
                               (void)written_unused;  (void)sid_unused; }


/*
 * @brief count the new file
 *
 * @param Increment if true, decrement otherwise
 * @param the index of the first stream when this file is initialised
 * @return none
 */
static void real_count_files(bool inc)
{
        if (inc) {
                stats_glob[tidx + 1].u.g.active_files += 1;
                stats_glob[tidx + 1].u.g.n_files      += 1;
        }
        else {
                AW_ASSERT(0 != stats_glob[tidx + 1].u.g.active_files);
                stats_glob[tidx + 1].u.g.active_files -= 1;
        }
}
#undef real_count_files
static void dummy_count_files(bool inc_unsuned) { (void)inc_unsuned; }


/*
 * @brief update start time
 *
 * Unlike read stats, this is initiated within one of the async threads because
 * 'start' already has the times as measured 'fuse side'.
 *
 * @param start action
 * @return none
 */
static void real_update_start_time(const struct action *t)
{
        struct timespec diff;

        diff_tm(&diff, &t->a.t.t_start, &t->a.t.t_end);
        add_stats_tm(&stats_glob[tidx + 1].start_times, &diff);
        add_stats_tm(&stats_files[strms[t->a.t.sid].fid].start_times, &diff);
        stats_files[strms[t->a.t.sid].fid].u.f.fh  = strms[t->a.t.sid].fh;
        stats_files[strms[t->a.t.sid].fid].u.f.tid = tidx;
}
#undef real_update_start_time
static void dummy_update_start_time(const struct action *a_unused)
                                                {  (void)a_unused; }

/*
 * @brief gets precise time
 *
 * @param where to store the timespec
 * @return none
 */
void real_get_time(struct timespec *now)
{
        clock_gettime(CLOCK_MONOTONIC, now);
}
#undef real_get_time
static void dummy_get_time(struct timespec *tm_unused) { (void)tm_unused; }


/*
 * @brief handle the incoming stat_set
 *
 *      Donwload speed is measured in ticks: SPEED_RES by second.
 *      It accumulates SPEED_MEM seconds.
 *
 *      When 'now' is ahead of 'last' the non-existent tick measures are zeroes,
 *      because it means no data was read in those ticks.
 *
 *      This part of the algorithm is only accumulating the sizes of complete
 *      read requests within the right 'tick'. Computation of the speed
 *      itself is done within aw_get_stats from these raw numbers.
 *
 * @param current action
 * @param earliest stat_set (if not NULL)
 * @return none
 */
static void real_set_time(struct private_data *pdata)
{
        struct timespec diff;
        ssize_t written;
        tick_t now = 0, start0, end;
        struct action *a, **where;
        bool post;
        fid_t fid;

        where = &pdata->actions;
        while (NULL != *where) {
                a = *where;

                if (ACTION_STATS_SET != a->type) {
                        where = &a->next;       /* Not a stat look next */
                        continue;
                }
                /* Stat: remove the action, next loop is on same *where */
                *where = a->next;

                if (ACTION_START == a->a.ss.r_type) {
                        real_update_start_time(a);
                        free(a);
                        continue;
                }

                if (0 != a->a.ss.t_now)
                        now = a->a.ss.t_now;
                else if (0 == now) {
                        /* Only O_THREAD will do this 'costly' operation, and
                         * pass the result (now) in t_now of the structure */
                        real_get_time(&diff);
                        now = tm_to_tick(&diff);
                }
                start0 = tm_to_tick(&a->a.ss.start0);
		end    = tm_to_tick(&a->a.ss.end);

                /* Save variables from 'a' that can't be derefenced after post*/
                written = a->a.ss.written;
                diff_tm(&diff, &a->a.ss.start, &a->a.ss.end);
                if (O_THREAD == tidx) {
                        /* Send the action as soon as possible, saving
                         * written and end and post that are needed */
                        post = (O_THREAD != a->a.ss.tid);
                        if (post) {
                                a->a.ss.t_now = now;
                                post_action(a , a->a.ss.tid);
                        }
                        /* Do global stats on O_THREAD*/
                        if (0 < written) {
                                speed_add(&spd_glob[0],
					  now, start0, end, written);
                                stats_glob[0].size    += written;
                        }
                        stats_glob[0].n_reads += 1;
                        add_stats_tm(&stats_glob[0].read_times, &diff);

                        /* Then skip the rest when action was posted */
                        if (post)
                                continue;
                }
                /* Do the local thread stats */
                if (0 < written) {
                        speed_add(&spd_glob[tidx + 1],
				  now, start0, end, written);
                        stats_glob[tidx + 1].size    += written;
                }
                stats_glob[tidx + 1].n_reads += 1;
                add_stats_tm(&stats_glob[tidx + 1].read_times, &diff);

                /* Do the file stats, unless flushed */
                if (N_STREAMS > a->a.ss.sid) {
                        fid = strms[a->a.ss.sid].fid;
                        if (0 < written) {
                                speed_add(&spd_files[fid],
					  now, start0, end, written);
                                stats_files[fid].size    += written;
                        }
                        stats_files[fid].n_reads += 1;
                        add_stats_tm(&stats_files[fid].read_times, &diff);
                }

                /* Read-set-time request are allocated */
                free(a);
        }
}
#undef real_set_time
static void dummy_set_time(struct private_data *pdata_unused) {
                                          (void)pdata_unused; }


/*
 * @brief update read time
 *
 * This is sent from a fuse thread to async reader O_THREAD.
 * O_THREAD will compute the global counters, then either its own if the
 * read was on O_THREAD already or push it to the right thread for counting.
 *
 * @param timespec when the read really started
 * @param timespec after start_stream (if any, otherwise same as above)
 * @param type (read or solo)
 * @param file handle
 * @param index of the stream
 * @return none
 */
static void real_update_read_time(struct timespec *start0,
				  struct timespec *start,
                                  const struct action *r)
{
        struct action *a;
        struct timespec diff, end;


        if (ACTION_SOLO == r->type) {
                /* For solo stats, there are done directly here in the fuse
                 * thread with a standard lock. It does not interfere/block
                 * the operations of the async worker. */
                AW_ERROR(pthread_mutex_lock(&solo_stats_mutex));
                solo_stats.solo_n_reads += 1;
                if (0 < r->a.r.written)
                        solo_stats.solo_size += r->a.r.written;

                real_get_time(&end);
                diff_tm(&diff, start, &end);
                add_stats_tm(&solo_stats.solo_times, &diff);
                AW_ERROR(pthread_mutex_unlock(&solo_stats_mutex));
        }
        else {
                a                     = malloc(sizeof(struct action));
                a->type               = ACTION_STATS_SET;
                a->sem_done           = NULL;
                a->a.ss.fh            = r->a.r.fh;
                a->a.ss.written       = r->a.r.written;
                a->a.ss.sid           = r->a.r.sid;
                a->a.ss.tid           = r->a.r.tid;
                a->a.ss.r_type        = r->type;
                a->a.ss.t_now         = 0;
                real_move_tm(&a->a.ss.start0, start0);
                real_move_tm(&a->a.ss.start , start);
                real_get_time(&a->a.ss.end);

                post_action(a , O_THREAD);
        }
}
#undef real_update_read_time
static void dummy_update_read_time(struct timespec *tm_unused0,
				   struct timespec *tm_unused,
                                   const struct action *r_unused) {
                                        (void)tm_unused0; (void)tm_unused;
                                        (void)r_unused;}

/*
 * @brief get a slot for allocation of a new file to a thread
 *
 * If there are old stats with the same fh, the slot will be returned, otherwise
 * a slot will be chosen and cleared of potential old stats.
 *
 * @param the fh to check
 * @return pointer on the slot found.
 */
static fid_t *real_get_slot(uint64_t fh)
{
        fid_t *p;

        if (NULL == stats_files)        /* stats built but not started yet */
                return &o_free_head;

        for (p = &o_free_head; *p != N_FILES; p = &o_list[*p])
                if (fh == stats_files[*p].u.f.fh)
                        return p;

        /* Not found, reset stats on that slot*/
        memset(&stats_files[o_free_head], '\0', sizeof(struct aw_stats));
        memset(  &spd_files[o_free_head], '\0', sizeof(struct spd));

        return &o_free_head;
}
#undef real_get_slot
static fid_t *dummy_get_slot(uint64_t fh_unused) { (void)fh_unused;
        return &o_free_head; /* No need "cleaning" as long as no stats!*/ }


/*
 * @brief release a slot
 *
 * Simply release at the end of the list when stats are on, which gives more
 * chance that a fh can be reused. With no stats, no loop: realease at head.
 *
 * @param none
 * @return pointer on where to release.
 */
static fid_t *real_release_slot()
{
        fid_t *p;

        if (NULL == stats_files)        /* stats built but not started yet */
                return &o_free_head;

        for (p = &o_free_head; *p != N_FILES; p = &o_list[*p]);
        return p;
}
#undef real_release_slot
static fid_t *dummy_release_slot() { return &o_free_head; }

/*
 * @brief clean old statistics when the caller explicitely calls "aw_close"
 *
 * @param the fh to check
 * @param the fid (file ID) to clean (or N_FILES to clean the free list)
 * @return pointer on the slot found.
 */
static void real_clean_old(uint64_t fh, fid_t fid)
{
        if (NULL == stats_files)        /* stats built but not started yet */
                return;

        if (N_FILES == fid) {
                /* When N_STREAMS is passed, this cleans the "free list" that
                 * could hold an old reference to the fh to check.
                 * This must run on thread 0 */
                for (fid = o_free_head; fid != N_FILES; fid = o_list[fid])
                        if (fh == stats_files[fid].u.f.fh) {
                                stats_files[fid].u.f.fh = 0;
                                return;
                        }
        }
        else {
                stats_files[fid].u.f.fh = 0;
        }
}
#undef real_clean_old
static void dummy_clean_old(uint64_t fh_unused, fid_t fid_unused) {
                               (void)fh_unused; (void)fid_unused;  }

/*
 * @brief called with aw_destroy to free allocations for statistics
 *
 * @param none
 * @return none
 */
static void real_destroy(void)
{
        free(stats_glob);
        free(stats_files);
        free(spd_glob);
        free(spd_files);
}
#undef real_destroy
static void dummy_destroy() {}

/**
 * Functions used outside of this file (in astreamfs_reader.c itself) are
 * accessed indirectly, so that they are replaced by dummies when stats
 * are built but not started (aw_init_stats not called) */

static struct aw_stats_operations real_stats_ops = {
        real_move_tm,
        real_raw_copy,
        real_count_retries,
        real_count_streams,
        real_count_over,
        real_count_files,
        real_update_start_time,
        real_set_time,
        real_get_time,
        real_update_read_time,
        real_get_slot,
        real_release_slot,
        real_clean_old,
        real_destroy,
};

static struct aw_stats_operations dummy_stats_ops = {
        dummy_move_tm,
        dummy_raw_copy,
        dummy_count_retries,
        dummy_count_streams,
        dummy_count_over,
        dummy_count_files,
        dummy_update_start_time,
        dummy_set_time,
        dummy_get_time,
        dummy_update_read_time,
        dummy_get_slot,
        dummy_release_slot,
        dummy_clean_old,
        dummy_destroy,
};

static struct aw_stats_operations *stats_ops = &dummy_stats_ops;

#define stats_move_tm(dest,src) stats_ops->move_tm(dest,src)
#define stats_raw_copy(pdata, a) stats_ops->raw_copy(pdata, a)
#define stats_count_retries(idx) stats_ops->count_retries(idx)
#define stats_count_streams(n,idx) stats_ops->count_streams(n,idx)
#define stats_count_over(written,idx) stats_ops->count_over(written,idx)
#define stats_count_files(n) stats_ops->count_files(n)
#define stats_inc_reads(a) stats_ops->inc_reads(a)
#define stats_update_start_time(a) stats_ops->update_start_time(a)
#define stats_set_time(pdata) stats_ops->set_time(pdata)
#define stats_get_time(tm) stats_ops->get_time(tm)
#define stats_update_read_time(tm0,tm,r) stats_ops->update_read_time(tm0,tm,r)
#define stats_get_slot(fh) stats_ops->get_slot(fh)
#define stats_release_slot(fh) stats_ops->release_slot(fh)
#define stats_clean_old(fh, idx) stats_ops->clean_old(fh,idx)
#define stats_destroy() stats_ops->destroy()

#define STAT_DECL(type,name)  type name

#else

#define stats_move_tm(dest,src)
#define stats_raw_copy(pdata, a)
#define stats_count_retries(idx)
#define stats_count_streams(n,idx)
#define stats_count_over(written,idx)
#define stats_count_files(n)
#define stats_inc_reads(a)
#define stats_update_start_time(a)
#define stats_set_time(pdata)
#define stats_get_time(tm)
#define stats_update_read_time(tm0,tm,r)
#define stats_get_slot(fh) (&o_free_head)
#define stats_release_slot(fh) (&o_free_head)
#define stats_clean_old(fh, idx)
#define stats_destroy()

#define STAT_DECL(type,name)  type name; (void)name

#endif

#ifdef _STATS

/**
 * @brief initialise statistics
 *
 * Same important note as above, it should be called at startup, otherwise
 * statistics could be completely incoherent.
 *
 * This should also be called AFTER aw_init so that is uses the custom
 * functions passed with aw_oper instead of the default ones.
 *
 * When built with NOSTATS, return ENOSYS (not implemented)
 *
 * @param None
 * @return 0 or error
 */
int aw_init_stats()
{
        if (NULL != stats_glob)
                return 0;       /* Nothing to do: already initialised. */

        stats_glob = aligned_alloc(LEVEL1_DCACHE_LINESIZE,
                                   (max_threads + 1) * sizeof(struct aw_stats));
        if (NULL == stats_glob)
                return ENOMEM;
        memset(stats_glob, '\0', (max_threads + 1) * sizeof(struct aw_stats));

        stats_files = aligned_alloc(LEVEL1_DCACHE_LINESIZE,
                                           N_STREAMS * sizeof(struct aw_stats));
        if (NULL == stats_files)
                return ENOMEM;
        memset(stats_files, '\0', N_STREAMS * sizeof(struct aw_stats));

        spd_glob = aligned_alloc(LEVEL1_DCACHE_LINESIZE,
                                        (max_threads + 1) * sizeof(struct spd));
        if (NULL == spd_glob)
                return ENOMEM;
        memset(spd_glob, '\0', (max_threads + 1) * sizeof(struct spd));

        spd_files = aligned_alloc(LEVEL1_DCACHE_LINESIZE,
                                                N_STREAMS * sizeof(struct spd));
        if (NULL == spd_files)
                return ENOMEM;
        memset(spd_files, '\0', N_STREAMS * sizeof(struct spd));

        stats_ops = &real_stats_ops;

        return 0;
}
#undef aw_init_stats

/**
 * @brief retrieve statistics
 *
 * First get the raw copy of statistics from the async reader then do
 * all the computations within the fuse stream.
 *
 * Computation:
 *      The first half second has a weight of 1
 *      Each subsequent half second is weighted +1 of the previous half second.
 *      Considering that last tick might not be "full", in fact the oldest
 *      half seconds has one tick less.
 *
 *      Practical example SPEED_RES = 8 and SPEED_MEM = 2
 *
 *     -2s    -1.5s    -1s    -0.5s     0 ('last')
 *      |               |               |
 *      |  B.C.D|E.F.G.H|I.J.K.L|M.N.O.P|   A
 *
 *      The compute is:
 *      (1 * (B+C+D) + 2 * (E+F+G+H) + 3 * (I+J+K+L) + 4 * (M+N+O+P)) / 39
 *
 *      This gives the weighted speed per tick, and should be multiplied by
 *      SPEED_RES to get the speed per second.
 *
 *      This computation avoids abnormal "spikes" and smoothes the numbers.
 *
 * @param pointer on statistics to return for threads (and total)
 * @param pointer on statistics to return for files
 * @param pointer on statistics to return for global (mem, contention, solo)
 * @return 0 or error
 */
void aw_get_stats(struct aw_stats *t_stats, struct aw_stats *f_stats,
                  struct aw_solo_stats *s_stats)
{
        struct action a[max_threads];
        sem_t sem_done;
        struct timespec tm;
        tick_t now;
        tid_t t, u;
        fid_t i;
        struct spd t_spd[max_threads + 1], f_spd[N_STREAMS];

        AW_ERROR(sem_init(&sem_done, 0, 0));

        for (i = 0; i < N_FILES; i++) {
                f_stats[i].u.f.fh    = 0;
                f_stats[i].u.f.n_rq  = 0;
                f_stats[i].u.f.n_buf = 0;
        }

        for (t = 0; t < max_threads; t++) {
                a[t].type     = ACTION_STATS_GET;
                a[t].sem_done = &sem_done;
                a[t].a.sg.t_stats = t_stats;
                a[t].a.sg.f_stats = f_stats;
                a[t].a.sg.t_spd = t_spd;
                a[t].a.sg.f_spd = f_spd;
                post_action(&a[t], t);
        }

        real_get_time(&tm);
        now = tm_to_tick(&tm);

        t_stats[0].u.g.active_files = 0;
        t_stats[0].active_streams   = 0;
        t_stats[0].u.g.n_files      = 0;
        t_stats[0].n_streams        = 0;
        t_stats[0].overread         = 0;
        t_stats[0].start_times.t_sum.tv_sec  = 0;
        t_stats[0].start_times.t_sum.tv_nsec = 0;
        t_stats[0].start_times.t_max.tv_sec  = 0;
        t_stats[0].start_times.t_max.tv_nsec = 0;

        /* SOLO: Note that the code in this function is running in a "fuse
         * thread" and is not slowing down any "async worker" once the responses
         * are back. So the "global" information (mem, contentions, solo) are
         * copied back here, between the request having been sent to workers
         * and the wait for response. */

        AW_ERROR(pthread_mutex_lock(&solo_stats_mutex));
        memcpy(s_stats, &solo_stats, sizeof(struct aw_solo_stats));
        AW_ERROR(pthread_mutex_unlock(&solo_stats_mutex));

        /* Now wait for the information to be copied by each async worker */
        for (t = 0; t < max_threads; t++) {
                AW_ERROR(sem_wait(&sem_done));
                /* Already compute sums between sem_waits */
                for (u = 0; u < max_threads; u++)
                        if (ACTION_STATS_GET != a[u].type) {
                                sum_stats(t_stats, u + 1);
                                compute_speeds(&t_stats[u + 1],
                                               &t_spd[u + 1], now);
                                /* Restore type to avoid multiple counting */
                                a[u].type = ACTION_STATS_GET;
                        }
        }

        AW_ERROR(sem_destroy(&sem_done));

        compute_speeds(&t_stats[0], &t_spd[0], now);

        for (i = 0; i < N_STREAMS; i++)
                if (0 != f_stats[i].u.f.fh)
                        compute_speeds(&f_stats[i], &f_spd[i], now);

}
#undef aw_get_stats

#endif /* _STATS */
