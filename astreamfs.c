/*
 * AStreamFS: stands for Asynchronous Stream Filesystem (fuse).
 *	      It mounts a set of URLs so that they can be accessed/streamed
 *	      as if it were local read-only local files.
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Feature:
 *	This filesystem mounts URLs as readonly files in the mount directory.
 *	The only thing it needs, is for the server to handle ranges.
 *	Note that there was already httpfs/httpfs2
 *	The implementation of it was simple an straightforward: it did a
 *	http GET range offset-(offset+size) for each read request.
 *	It works, but has a lot of overhead, even with kernel buffering.
 *	The trouble with that simple approach comes with the generalisation
 *	of https these days (2018), when the server closes the	connection,
 *	not only you have the mentioned overhead, but you also	get to do
 *	the TLS handshake. On some test servers I use, this comes close
 *	to 1 second overhead for each read, which made httpfs unusable
 *	for more than very small tasks.
 *
 *	What this implementation does instead, is that it tries its best to
 *	"stream" access to the http(s) resource. This correspond in fact to
 *	a majority of cases like video, music, uncompressing a rar/zip, etc...
 *	Even the use case quoted by https: reading files in a iso, generally
 *	involves random access to locate that file followed by (hopefully)
 *	sequential access on the file.
 *
 *	On the case you really need random access, there is little thing that
 *	this implementation can do better than httpfs (it could actually be
 *      a little worse because closing streams takes time) and should that be
 *	your case, you would probably be better of downloading the file to
 *	access it locally for more than small tasks.
 *
 *	On top of this "stream" orientation, a few useful options are also
 *	implemented, borrowing from the curl CLI tool when it made sense, like
 *	following locations, etc...
 *	See the help for more details.
 *
 * Architecture:
 *	It works via workers or "reader threads" (readers for short).
 *
 *	An active reader has current buffers it is filling from a stream. These
 *      buffers are consecutive with at most 128k "holes", thus min/max offset.
 *
 *	When a reader has no more things to do it waits at the offset of the
 *	last buffer that was responded.
 *
 * 	After some time the reader stops waiting and goes back to idle state.
 *
 * 	When a request arrives astreamfs looks in order of preference:
 *	- if the buffer received goes inside a (min/)max or just after max
 *	  with a possible "hole" of 128k.
 *	- if there is an idle worker.
 *	If none of that is found, it does a single range request (not streamed).
 *
 *	Locking:
 *	Fuse incoming reads lock the file so that they read a consistent state.
 *	Readers lock only when they send back full buffers.
 *	The buffer exchange between fuse threads and readers is done via a pair
 *	symetric semaphores.
 *
 *	The 128k gap mentioned above is to take into account parallelism from
 *	the kernel that can send streamed blocks in "reversed" order. If a
 *	reader goes into a "hole" it uses its internal 128k buffer, which can
 *	be used to fill late reversed blocks.
 *
 *
 *	Worker nodes that encapsulate the read requests are not allocated,
 *	since we need only one buffer for each fuse thread, and one buffer for
 *	each async reader, we use a thread local variable.
 *
 * Usage:
 *	Either
 *		read the help provided by: `astreamfs -h` or
 *		look at the help section below, under astreamfs_opt_proc()
 *
 * Build:
 *      Use the provided Makefile. astreamfs is the default target.
 *
 * Compile dependancy:
 *	curl and fuse library
 *
 * Version: 1.0.0
 *
 * History:
 * 	2022/03/05: 1.0.0 Now using atreamfs_worker (+ stats included!)
 * 	2020/03/17: 0.9.1.1 Fix: buf queue (prv_ins) + Copyright to 2020
 * 	2019/06/22: 0.9.1 backport cleaner get http_code (hd_chk) & check size
 * 	2019/06/08: 0.9.0 New: added curl options --insecure and --cacert
 * 	2019/04/28: 0.8.6 Fix stat's st_block, subtype param, algo simplified.
 * 	2019/02/14: 0.8.5 Backport from 1fichierfs: fix 32bits + common code.
 * 	2018/12/29: 0.8.4 Better logging + replaced GCC atomics by C11 standard.
 * 	2018/12/16: 0.8.3 Cleaning: moved common code to astreamfs_util.c/h
 *	2018/12/01: 0.8.2 Fix some bugs with parameters handling
 *	2018/11/24: 0.8.1 Support user:password basic authentication (-u --user)
 *	2018/03/25: 0.8.0 Full path support (incl. relative) + minor optim.
 *	2018/03/24: 0.7.9 Canonicalize and deduplicate paths
 *	2018/03/24: 0.7.8 Log to a file option (prevent syslog spamming!)
 *	2018/03/17: 0.7.7 Bump for packaging
 *	2018/03/12: 0.7.5 First path implementation
 *	2018/03/10: 0.7.4 More work on argument management
 *	2018/03/08: 0.7.3 Project renamed: avoid confusion with existing httpfs2
 *	2018/03/06: 0.7.2 Better multi-file implementation
 *	2018/03/04: 0.7.1 Added options passed by /etc/fstab to click-mount!
 *	2018/03/03: 0.7.0 First incomplete implementation of multiple files
 *	2018/02/18: 0.6.2 Bug fix on close requests
 *	2018/02/17: 0.6.1 File modification time, single ranges, more comments.
 *	2018/02/16: 0.6.0 Version with locks, it became too complex and bug
 *			  prone without locking! We might look into it later.
 *	2018/02/11: 0.5.5 Simplified algorithm: removed wait state.
 *	2018/02/10: 0.5.4 Several bug fix
 * 	2018/02/10: 0.5.1 Implemented timeouts, better streaming algorithm
 * 	2018/01/31: 0.5.0 Initial version
 *
 *
 */

#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stddef.h>
#include <strings.h>

#define STREAM_STRUCT struct filespec
#include "astreamfs_worker.h"
#include "astreamfs_util.h"

#define PROG_NAME	"astreamfs"
#define PROG_VERSION	"1.0.0"

#define REASONABLE_RETRIES (5)

#define ST_BLK_SZ 512 /* This is the block size stat's st_blocks */

/* Global immutable constant strings for various use */
       const char log_prefix[]		= PROG_NAME;
static const char default_user_agent[]	= PROG_NAME"/"PROG_VERSION;
static const char default_filename[]	= "file";
static const char ro_opt[]		= "-oro,fsname=";
static const char subtype[]		= ",subtype=";
static const char def_type[]		= PROG_NAME;


/*
 *  Global variables set during the initialisation phase then immutable.
 */

static unsigned long count_URL = 0;
static unsigned long count_path = 1;
static unsigned long not_found = 0;

static mode_t st_mode;

/*****************************************/

#define FL_GOT_NAME		    1
#define FL_GOT_SIZE		    2
#define FL_GOT_TIME		    4
#define FL_LOCATION		    8
#define FL_KEEP_LOCATION	   16
#define FL_REMOTE_NAME		   32
#define FL_REMOTE_HEADER_NAME	   64
#define FL_REMOTE_TIME		  128
#define FL_INSECURE		  256

#define FL_IS_INIT		  512
#define FL_NO_RANGE		 1024
#define FL_NOT_FOUND		 2048

#define F_GOT_NAME(fs)		 (0 != (LOAD((fs)->flags) & FL_GOT_NAME))
#define F_GOT_SIZE(fs)		 (0 != (LOAD((fs)->flags) & FL_GOT_SIZE))
#define F_GOT_TIME(fs)		 (0 != (LOAD((fs)->flags) & FL_GOT_TIME))
#define F_LOCATION(fs)		 (0 != (LOAD((fs)->flags) & FL_LOCATION))
#define F_KEEP_LOCATION(fs)	 (0 != (LOAD((fs)->flags) & FL_KEEP_LOCATION))
#define F_REMOTE_NAME(fs)	 (0 != (LOAD((fs)->flags) & FL_REMOTE_NAME))
#define F_REMOTE_HEADER_NAME(fs) (0 != (LOAD((fs)->flags) & \
							FL_REMOTE_HEADER_NAME))
#define F_REMOTE_TIME(fs)	 (0 != (LOAD((fs)->flags) & FL_REMOTE_TIME))
#define F_INSECURE(fs)		 (0 != (LOAD((fs)->flags) & FL_INSECURE))

#define F_IS_INIT(fs)		 (0 != (LOAD((fs)->flags) & FL_IS_INIT))
#define F_NO_RANGE(fs)		 (0 != (LOAD((fs)->flags) & FL_NO_RANGE))
#define F_NOT_FOUND(fs)		 (0 != (LOAD((fs)->flags) & FL_NOT_FOUND))

struct filespec {
	/* File characteristics gathered at startup */
	char        *URL;
	_Atomic unsigned int flags;
	unsigned int max_retries;
	time_t	     curl_opt_keepalive_time;
	time_t	     idle_time;
	time_t	     keep_location_time;
	long         curl_IP_resolve;
	char        *filename;
	char        *path;
	char	    *userpwd;

	/* Used when file becomes live (at least opened once) */
	_Atomic unsigned int nb_open;
	char	    *location;
#ifdef _STATS
	unsigned int location_count;
#endif
	struct stat  st;
	time_t	     keep_until;	/* For keeping locations */
	pthread_mutex_t curlmutex;
};


static struct filespec *fss = NULL;
static char **paths = NULL;

struct parse_data {
	unsigned long cur_URL;
	bool new_args;
	bool got_URL;
	const char *last_noopt;
	char *cur_abs_path;
	size_t path_sz;
};


static struct filespec *find_file(const char *path)
{
	unsigned int i;
	for (i = 0; i < count_URL; i++)
		if (0 == strncmp(path, fss[i].path, strlen(fss[i].path)) &&
		    0 == strcmp(path + strlen(fss[i].path), fss[i].filename))
			return &fss[i];
	return NULL;
}

static char *find_dir(const char *path)
{
	unsigned int i;
	size_t sz = strlen(path);
	int cmp;

	for (i = 0; i < count_path; i++) {
		cmp = strncmp(path, paths[i], sz);
		if (0 == cmp)
			return paths[i];
		if (cmp < 0)
			break;
	}
	return NULL;
}

static int astreamfs_getattr(const char *path, struct stat *stbuf)
{
	struct filespec *fs;
	char *p;

	DEBUG("getattr: `%s`\n", path);

	memset(stbuf, 0, sizeof(struct stat));
	p = find_dir(path);
	if (NULL != p) {
		memcpy(stbuf, &params.mount_st, sizeof(struct stat));
	}
	else {
		fs = find_file(path);
		if (NULL == fs) {
#ifdef _STATS
			if (NULL != params.stat_file &&
			    0 == strcmp(path + 1, params.stat_file)) {
				memcpy(stbuf, &params.mount_st
					    , sizeof(struct stat));
				stbuf->st_mode	 = st_mode & 
				       ~(S_IXUSR | S_IXGRP | S_IXOTH | S_ISUID);
				stbuf->st_size	 = KBUF;
				stbuf->st_blocks = KBUF / ST_BLK_SZ;
				return 0;
			}   
#endif
			return -ENOENT;
		}
		memcpy(stbuf, &fs->st, sizeof(struct stat));
	}

	return 0;
}

static int astreamfs_readdir(const char *path, void *buf,
			     fuse_fill_dir_t filler, off_t offset,
			     struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;
	unsigned int i, j;
	int cmp;
	size_t s, s_prev;
	char *p, *q, *prev = NULL;

	DEBUG("readdir: `%s` %u\n", path, offset);

	p = find_dir(path);
	if (NULL == p)
		return -ENOENT;

	if (0 != filler(buf, ".", NULL, 0))
		return 1;
	if (p == paths[0]) {
		if (0 != filler(buf, "..", NULL, 0))
			return 1;
#ifdef _STATS
		if (NULL != params.stat_file &&
		    0 != filler(buf, params.stat_file, NULL, 0))
			return 1;
#endif
	}
	else {
		if (0 != filler(buf, "..", NULL, 0))
			return 1;
	}

	s = strlen(path);
	j = (0 == strcmp(path, "/")) ? 0 : 1;
	s_prev = 0;
	for (i = 0; i < count_path; i++) {
		cmp = strncmp(path, paths[i], s);
		if (0 == cmp) {
			q = strchr(paths[i] + s + j, '/');
			if (NULL == q ||
			    (s_prev == q - paths[i] - s - j &&
			     0 == strncmp(prev, paths[i] + s + j, s_prev)))
				continue;
			s_prev = q - paths[i] - s - j;
			prev = paths[i] + s + j;
			char dir[s_prev + 1];
			memcpy(dir, prev, s_prev);
			dir[s_prev] = '\0';
			if (0 != filler(buf, dir, NULL, 0))
				return 1;
		}
		if (cmp < 0)
			break;
	}
	for (i = 0; i < count_URL; i++)
		if (!F_NOT_FOUND(&fss[i]) && fss[i].path == p) {
			if (0 != filler(buf, fss[i].filename, &fss[i].st, 0))
				return 1;
		}
	return	0;
}

static int astreamfs_open(const char *path, struct fuse_file_info *fi)
{
	unsigned int n;
	struct filespec *fs;
	struct fi_to_fh *fifh;

	if ((fi->flags & 3) != O_RDONLY)
		return -EACCES;

	fs = find_file(path);
	if (NULL == fs) {
#ifdef _STATS
		if (NULL != params.stat_file &&
		    0 == strcmp(path + 1, params.stat_file)) {
			struct stats *stts;
		
			stts = malloc(sizeof(struct stats));
			pthread_mutex_init(&stts->initmutex, NULL);
			stts->size = 0;
			fifh = malloc(sizeof(struct fi_to_fh));
			fifh->fh   = (uintptr_t)stts;
			fifh->type = TYPE_SPECIAL;
			fi->fh = (uintptr_t)fifh;
			/* Stats are computed at first read to avoid computing
			 * them if the stat file is only opened/closed. */
			return 0;
		}
#endif
		return -ENOENT;
	}

	if (F_NO_RANGE(fs))
		return -EACCES;

	n = atomic_fetch_add_explicit(&fs->nb_open, 1, memory_order_relaxed);

	fifh = malloc(sizeof(struct fi_to_fh));
	fifh->fh = (uintptr_t)fs;
	fifh->type = TYPE_READ;
	STORE(fifh->last_opt, UNKNOWN_OPT);
	fi->fh = (uintptr_t)fifh;

	fi->keep_cache = 1;

	lprintf(LOG_INFO, ">> Opened (%d). fifh=%p fifh->fh=%p\n"
			, n + 1, fifh, fs);
	return 0;
}

static int astreamfs_release(const char *path, struct fuse_file_info *fi)
{
	int res = 0;
	struct fi_to_fh *fifh;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));

#ifdef _STATS
	if (TYPE_SPECIAL == fifh->type) {
		/* This is the stat file */
		struct stats *stts = (struct stats *)((uintptr_t)(fifh->fh));

		DEBUG(">> Releasing stat file.\n");
		res = pthread_mutex_destroy(&stts->initmutex);
		if (0 != res)
			lprintf(LOG_CRIT,
				"error: %d on pthread_mutex_destroy.\n", res);
		free(stts);
	}
	else
#endif
	{
		unsigned int n = 1;
		struct filespec *fs;

		fs = (struct filespec *)((uintptr_t)(fifh->fh));
		n = atomic_fetch_sub_explicit(&fs->nb_open,
					      1,
					      memory_order_relaxed);
		lprintf(LOG_INFO, ">> Released (%d).\n", n - 1);
	}

	free(fifh);
	return res;
}


struct fs_loc {
	struct filespec *fs;
	int http_code;
	char *location;
	off_t sz;
};


static const char location_header[] = "Location:";
static const char content_disposition_header[] = "Content-Disposition:";
static const char attachment_part[] = "attachment";
static const char inline_part[] = "inline";
static const char multi_part[] = "form-data";
static const char filename_part[] = "filename";
static const char last_modified_header[] = "Last-Modified:";

#include <locale.h>


/*
 * @brief grab the remote name
 *
 * @param buffer where the header is stored
 * @param size of the buffer
 * @param filespec where to store the potential result
 * @return true if no error, false on error
 */
static bool get_remote_time(const char *buffer, const size_t sz
					      , struct filespec *fs)
{
	struct tm rt;
	time_t filetime;
	const char *p;

	if (lengthof(last_modified_header) >= sz)
		return true;	/* Too short to be "Last-Modified:" */

	if (0 != strncasecmp(buffer, last_modified_header
				   , lengthof(last_modified_header)))
		return true;	/* Not Last-Modified*/

	/* This is Last-Modified: header */
	/* Since buffer always finishes with CR-LF, \0 is not need, the
	 * string interpretation will either fail or succeed within buffer */
	/* Note: strptime is picky about space in front of strings */
	p = buffer + lengthof(last_modified_header);
	while (isblank(*p))
		p++;
	if (NULL == strptime(p,"%a,%d %b%Y%T", &rt))
		return false;

	rt.tm_isdst = -1;      /* Not set by strptime(); tells mktime()
				  to determine whether daylight saving time
				  is in effect */

	filetime = mktime(&rt);
	if (-1 == filetime)
		return false;
	
	fs->st.st_mtim.tv_sec  =
	fs->st.st_ctim.tv_sec  = filetime;
	fs->st.st_mtim.tv_nsec =
	fs->st.st_ctim.tv_nsec = 0;
	OR(fs->flags, FL_GOT_TIME);
	
	return true;
}

/*
 * @brief grab the remote name
 *
 * @param buffer where the header is stored
 * @param size of the buffer
 * @param filespec where to store the potential result
 * @return true if no error, false on error
 */
static bool get_remote_name(const char *buffer, const size_t sz
					      , struct filespec *fs)
{
	unsigned int i, j, l;
	char *r;

	if (lengthof(content_disposition_header) >= sz)
		return true;	/* Too short to be "Content-Disposition:" */

	if (0 != strncasecmp(buffer, content_disposition_header
				   , lengthof(content_disposition_header)))
		return true;	/* Not Content-Disposition*/

	/* This is Content-Disposition: header */

	/* Skip possible spaces */
	for (i = lengthof(content_disposition_header);
	     i < sz && isblank(buffer[i]);
	     i++);

	/* Content-Disposition might be followed by
	 * - inline 
	 * - form-data
	 * - attachment
	 * The first two are correct according to the norm but do not
	 * provide a name that can be used (easily), they are just skipped.
	 * If none of the 3 options above is found: considered malformed. */
	if (sz - i > lengthof(inline_part) &&
	    0 == strncasecmp(buffer + i, inline_part, lengthof(inline_part)))
		return true;	/* inline is Ok but does not give a name!*/
		
	if (sz - i > lengthof(multi_part) &&
	    0 == strncasecmp(buffer + i, multi_part, lengthof(multi_part)))
		return true;	/* multipart is Ok but does not give a name!*/

	if (sz - i < lengthof(attachment_part) ||
	    0 != strncasecmp(buffer + i , attachment_part
					, lengthof(attachment_part)))
		return false;	/* malformed header! */

	/* Now there is: Content-Disposition: attachment
	 * Skip until the next ';' allowing only space, and spaces after ';' */
	for (i += lengthof(attachment_part); i < sz; i++) {
		if (';' == buffer[i])
			break;
		if (!isblank(buffer[i]))
			return false;	/* unexpected characters before ';' */
	};
	for (i++; i < sz && isblank(buffer[i]); i++);

	/* TODO, now filename is expected
	 * It could be followed by a further filename* specification that
	 * should be preferred, but is not implemented so far.
	 * If filename is not found as expected: consider malformed */
	if (sz - i < lengthof(filename_part) ||
	    0 != strncasecmp(buffer + i , filename_part
					, lengthof(filename_part)))
		return false;	/* Malformed */

	/* Now there is: Content-Disposition: attachment; filenmae
	 * Skip until the next '=' allowing only space, and spaces after '=' */
	for (i += lengthof(filename_part); i < sz; i++) {
		if ('=' == buffer[i])
			break;
		if (!isblank(buffer[i]))
			return false;	/* unexpected characters before '=' */
	};
	for (i++; i < sz && isblank(buffer[i]); i++);

	/* Some servers don't quote the name, this is considered malformed. */
	for (; i < sz; i++) {
		if ('"' == buffer[i])
			break;
		if (!isblank(buffer[i]))
			return false;	/* unexpected characters before '"' */
	};
	if (sz == i)
		return false; /* malformed " was not found */

	/* Count bytes for allocation */
	i++;
	for (j = i, l = 0; j < sz && buffer[j] !='"' ; j++, l++)
		if ( '\\' == buffer[j] )
			j++;
		else if ( '\0' == buffer[j] )
			return false;
	if (sz == j || l == 0)
		return false;	/* malformed: end " not found or empty name */

	/* Allocate, copy, and sanitize potential '/' */
	free((void *)fs->filename);
	r = malloc( l + 1);
	fs->filename = r;
	OR(fs->flags, FL_GOT_NAME);
	for (j = i; buffer[j] !='"'; j++, r++) {
		if ( '\\' == buffer[j] )
			j++;
		*r = ( '/' == buffer[j] ) ? '_' : buffer[j] ;
	}
	*r = '\0';
	return true;
}

/*
 * @brief callback for consume_headers, same as curl header callback
 *
 *
 * @param buffer where the header is stored
 * @param size of each item in the buffer
 * @param number of items in the buffer
 * @param opaque pointer to user data
 * @return size * nmemb if all ok, or anything else if error
 */

static size_t header_check(char *buffer, size_t size,
                           size_t nitems, void *userdata)
{
	struct fs_loc *fsl = (struct fs_loc *)userdata;
	struct filespec *fs = fsl->fs;
	size_t sz = size * nitems; 	/* Readbility */

	/*
	 * The first header MUST be the http status code otherwise this is
	 * a protocol error, and might be caught in astreamfs_util.c
	 */
	if (0 == fsl->http_code) {
		http_response_status_code(buffer, sz, &fsl->http_code);
		switch(fsl->http_code) {
			case HTTP_PARTIAL_CONTENT :
				break;	/* Ok! */
			case HTTP_MOVED_PERMANENTLY  :
			case HTTP_FOUND		     :
			case HTTP_SEE_OTHER	     :
			case HTTP_TEMPORARY_REDIRECT :
			case HTTP_PERMANENT_REDIRECT :
				if  (!F_LOCATION(fs)) {
					lprintf(LOG_WARNING,
						"the server returned %ld but -L was not specified for %s\n",
						fsl->http_code, fs->URL);
					return 0;
				}
				fsl->http_code = 300;
				break;
			case HTTP_OK :
				OR(fs->flags, FL_NO_RANGE);
				lprintf(LOG_ERR,
					"the server does not support ranges for %s\n",
					fs->URL);
				return 0;
			case HTTP_NOT_FOUND :
				/* Could be Ok when it is a redirect location */
				lprintf(LOG_WARNING, "URL: %s was not found\n"
						   , fs->URL);
				return 0;
			case 0 	:
				/* There was an error! */
				return 0;
			default :
				lprintf(LOG_ERR,
					"protocol error or unexpected return code (%ld) for %s\n",
					fsl->http_code, fs->URL);
				return 0;
		}
		return sz;	/* Ok breaks here, error cases returned 0 */
	}
	else if (300 == fsl->http_code) {
		char *p, *q;
		/* There was a redirection and this is expected (-L), wait for
		 * the 'Location:' header and no other header */
		if (lengthof(location_header) >= sz)   /* Too short */
			return sz;
		if (0 != strncasecmp(buffer, location_header
					   , lengthof(location_header)))
			return sz;
		p = buffer + lengthof(location_header);
		q = buffer + sz - 2;	/* -2 not counting CR+LF */
		while (p < q && isblank((unsigned char)*p))  /* Skip spaces */
			p++;
		free(fsl->location);
		fsl->location = malloc(q - p + 1);
		mempcpy(fsl->location, p, q - p);
		fsl->location[q - p] = '\0';
		return 0;	/* Location grabbed, now end request */
	}
	else {
		/* 206 Partial content as expected, otherwise returned at
		 * status code line. Grab other details: size, time, name... */

		/* Always get the size: either set it or check it */
		if (0 == fsl->sz) {
			if (!http_file_size(buffer, sz, &fsl->sz))
				return 0;	/* Error reading size, stop */
			if (0 != fsl->sz) {
				if (F_GOT_SIZE(fs)) {
					if (fs->st.st_size == fsl->sz)
						return sz;
					lprintf(LOG_ERR,
						"file size changed from %"PRIu64" to %"PRIu64" for %s\n",
						(uint64_t)fs->st.st_size,
						(uint64_t)fsl->sz,
						fs->URL);
					return 0;
				}
				else {
					fs->st.st_size = fsl->sz;
					fs->st.st_blocks = fsl->sz / ST_BLK_SZ;
					OR(fs->flags, FL_GOT_SIZE);
				}
				return sz;
			}
		}

		if (F_IS_INIT(fs))
			return sz;

		/* Get remote name and time, only when requested */
		if (F_REMOTE_HEADER_NAME(fs)) {
			if (!get_remote_name(buffer, sz, fs))
				/* malformed Content Dispo is ignored */
				lprintf(LOG_WARNING,
					"malformed Content-Disposition header: `%.*s` for URL: %s.\n",
					sz - 2, buffer, fs->URL);
		}
		if (F_REMOTE_TIME(fs)) {
			if (!get_remote_time(buffer, sz, fs))
				/* malformed Last-Modified is ignored */
				lprintf(LOG_WARNING,
					"malformed Last-Modified header: `%.*s` for URL: %s.\n",
					sz - 2, buffer, fs->URL);
		}
	}
	return sz;
}


/*
 * @brief utility to initialise a curl handle
 *
 * The handle is initialised and populated according to the global parameters
 *
 * @param none
 * @return the initialised curl handle
 */
static CURL *curl_init(struct filespec *fs)
{
	CURL *curl;

	curl = curl_easy_init();
	if (NULL == curl)
		lprintf( LOG_CRIT, "initializing curl easy handle.\n" );
	CURL_EASY_SETOPT(curl, CURLOPT_IPRESOLVE, fs->curl_IP_resolve, "%ld");
	if (F_INSECURE(fs))
		CURL_EASY_SETOPT(curl, CURLOPT_SSL_VERIFYPEER, 0, "%ld");
	if (NULL != params.ca_file)
		CURL_EASY_SETOPT(curl, CURLOPT_CAPATH, params.ca_file, "%s");
	if (NULL != params.user_agent)
		CURL_EASY_SETOPT(curl, CURLOPT_USERAGENT, params.user_agent,
				 "%s");
	if (0 == fs->curl_opt_keepalive_time) {
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPALIVE, 0L, "%ld");
	}
	else {
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPALIVE, 1L,"%ld");
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPIDLE,
				(long)fs->curl_opt_keepalive_time, "%ld");
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPINTVL,
				(long)fs->curl_opt_keepalive_time, "%ld");
	}

	if (NULL != fs->userpwd) {
		CURL_EASY_SETOPT(curl, CURLOPT_USERPWD, fs->userpwd, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC, "%l");
	}

	/* Do the "connect only" */
	CURL_EASY_SETOPT(curl, CURLOPT_CONNECT_ONLY, 1L, "%ld");

	return curl;
}

#ifdef _STATS
/*
 * @brief retrieves the reference stream for the reader array
 *
 * @param file handle
 * @param buffer to store the result (must be SZ_BUF_REF size min)
 * @param not used
 * @return the string.
 */
static const char *reader_ref(uint64_t fh, char *buf, void *userdata)
{
	(void)userdata;
	static const char unkown[] = "(???\?)";
	struct filespec *fs;
	size_t s;

	fs = (struct filespec *)((uintptr_t)fh);
	s = snprintf(buf, SZ_BUF_REF, "(%4lu)", (unsigned long)(fs - fss) + 1);
	if (s >= SZ_BUF_REF)
		return unkown;
	else
		return buf;
}

/*
 * @brief tells if a given file is currently "live"
 *
 * @param file stat information
 * @param the 'fh' to check
 * @param where to store the number of streams so far
 * @return true if streaming, false otherwise
 */
static bool is_live(const struct aw_stats *f_stats, uint64_t fh,
		    unsigned int *pidx)
{
	unsigned int idx;
	for (idx = 0; idx < engine_info.n_streams; idx++)
		if (f_stats[idx].u.f.fh  == fh) {
			 *pidx = idx;
			 return (f_stats[idx].u.f.tid != engine_info.n_threads);
		}
	return false;
}

static size_t out_stats(struct stats *stts)
{
	struct timespec now;
	tm_index_t tm_now;
	struct aw_stats f_stats[engine_info.n_streams];
	unsigned int i, n;
	bool live[count_URL];
	unsigned int idx[count_URL];
	time_t until[count_URL];
	char buf1[SZ_BUF_HR], buf2[SZ_BUF_HR], buf3[SZ_BUF_HR], buf4[SZ_BUF_HR];
static  const char blank[] = "     ";
static  const char overflow[] = "**:**";
	char *buf_until , buf5[SZ_BUF_HR];
	char *buf_loc_cnt;

	out_stat_header(stts, &now, &tm_now);
	
	/****************************
	 * Part: Readers (all)
	 */

	if (STAT_MAX_SIZE - 1 == out_stat_read(stts, f_stats, reader_ref, NULL))
		return STAT_MAX_SIZE - 1;


	/****************************
	 * Part: Memory
	 */
	stts->size = out_stats_mem(stts->buf, stts->size, STAT_MAX_SIZE);
	if (STAT_MAX_SIZE - 1 == stts->size)
		return STAT_MAX_SIZE - 1;

	/****************************
	 * Part: Live streams
	 */

	now.tv_sec += main_start.tv_sec; /* out_stat_header subed it */
	for (n = i = 0; i < count_URL; i++) {
		live[i] = is_live(f_stats, (uintptr_t)(&fss[i]), &idx[i]);
		until[i] = 0;
		if (F_KEEP_LOCATION(&fss[i])) {
			lock(&fss[i].curlmutex, NULL);
			if (0 != fss[i].keep_until &&
			    fss[i].keep_until > now.tv_sec) {
				until[i] = fss[i].keep_until - now.tv_sec;
			}
			unlock(&fss[i].curlmutex, NULL);	
		}
		if (live[i] || 0 != until[i])
			n++;
	}
			
	if (0 == n)
		return stts->size;

	/* Sub-part 'title'
	 */
	SNPRINTF("Live streams: %u\n", n);
	SNPRINTF("%s", "  Ref   Size  Left Lnk Retry E N.Loc N.Stm Path\n");
	for (n = i = 0; i < count_URL; i++) {
		if (!live[i] && 0 == until[i])
			continue;
		if (0 == until[i]) {
			buf_until   = (char *)blank;
			buf_loc_cnt = (char *)blank;
		}
		else {
			if (until[i] >= 3600 || until[i] < 0) {
				buf_until = (char *)overflow;
			}
			else {
				sprintf(buf5, "%.02ld:%.02ld"
					    , until[i] / 60, until[i] % 60);
				buf_until = buf5;
			}
			buf_loc_cnt = format_hr(fss[i].location_count, buf2);
		}
		SNPRINTF("(%4u) %s %s %3u %s %c %s %s %s%s\n",
			 i + 1,
			 format_hr(fss[i].st.st_size, buf1),
			 buf_until,
			 fss[i].nb_open,
			 format_hr(f_stats[idx[i]].n_retries,buf3),
			 (F_NO_RANGE(&fss[i])) ? '*' : ' ',
			 buf_loc_cnt,
			 format_hr(f_stats[idx[i]].n_streams, buf4),
			 fss[i].path, fss[i].filename);

	}
	return stts->size;
}

/*
 * @brief initialises stats buffer if needed and return the correct size
 *
 * @param the stats buffer
 * @param size of the buffer
 * @param offset from where to read
 * @return actual size considering real buffer length
 */
static size_t init_stat_buf(struct stats *s, size_t size, off_t offset)
{
	size_t stat_sz;
	
	if (0 == s->size) {
		/* Note that several read threads could lock, but only
		 * the first that locked will fill the buffer, the next
		 * one will read a non zero size and only return it.
		 * Also rcu lock since it is using the lfile head. */
		lock(&s->initmutex, NULL);
		
		/* Read size again since it will already be changed if this
		 * was not the first to lock. */
		if (0 == s->size)
			out_stats((struct stats *)s);

		unlock(&s->initmutex, NULL);
	}
	
	stat_sz = s->size;

	if (offset + size >= stat_sz) {
		if (offset >= stat_sz)
			return 0;
		else
			return stat_sz - offset;
	}
	return size;
}
#endif


/*
 * @brief fuse callback for read
 *
 *
 * @param path to the file being read
 * @param buffer where we store read data
 * @param size of the buffer
 * @param offset from where we read
 * @param fuse internal pointer with possible private data
 * @return number of bytes read (should be size unless error or end of file)
 */
static int astreamfs_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	struct fi_to_fh *fifh;
	struct filespec *fs;
	int res;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));

#ifdef _STATS
	if (TYPE_SPECIAL == fifh->type) {
		struct stats *stts = (struct stats *)((uintptr_t)(fifh->fh));
		size = init_stat_buf(stts, size, offset);
		memcpy(buf, stts->buf + offset, size);
		return size;
	}
#endif

	fs = (struct filespec *)((uintptr_t)(fifh->fh));

	DEBUG(	">> astreamfs_read: %p (%"PRIu64",%lu) --------\n",
		fs, offset, size);

	if (offset + size > fs->st.st_size) {
		if (offset >= fs->st.st_size)
			return 0;
		else
			size = fs->st.st_size - offset;
	}
	
	res = aw_read(path, buf, size, offset, fi);
	/* When error, it means retries have been exhausted, hence mark file */
	if (0 > res)
		OR(fs->flags, FL_NO_RANGE);
	return res;
}

/*
 * @brief this is the read_buf callback as fuse wants it!
 *
 *
 * @param path of the file to read.
 * @param buf vector to be allocated and populated.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
static int astreamfs_read_buf(const char *path, struct fuse_bufvec **bufp,
		size_t size, off_t offset, struct fuse_file_info *fi)
{
	struct fi_to_fh *fifh;
	struct filespec *fs;
	int res;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));

#ifdef _STATS
	if (TYPE_SPECIAL == fifh->type) {
		/* This is the stat file */
		struct stats *stts = (struct stats *)((uintptr_t)(fifh->fh));
		size = init_stat_buf(stts, size, offset);
		*bufp = init_bufp(size);
		memcpy((*bufp)->buf->mem, stts->buf + offset, size);
		return size;
	}
#endif

	fs = (struct filespec *)((uintptr_t)(fifh->fh));

	DEBUG(	">> astreamfs_read_buf: %p (%"PRIu64",%lu) --------\n",
		fs, offset, size);

	if (offset + size > fs->st.st_size)
		size = (offset >= fs->st.st_size) ? 0 : fs->st.st_size - offset;

	
	res = aw_read_buf(path, bufp, size, offset, fi);
	/* When error, it means retreis have been exhausted, hence mark file */
	if (0 > res)
		OR(fs->flags, FL_NO_RANGE);
	return res;
}

/**
 * Callback functions for the new common read engine
 **/

static bool astreamfs_close_if_idle(uint64_t fh, void *sh,
				    time_t last_read, time_t now, bool force)
{
	struct filespec *fs;
	struct curl_raw_handle *crh;

	fs = (struct filespec *)((uintptr_t)fh);
	crh = (struct curl_raw_handle *)sh;

	if (!force && last_read + fs->curl_opt_keepalive_time > now)
		return false;

	curl_easy_cleanup(crh->curl);
	free(crh);

	return true;
}

static unsigned int astreamfs_retry(uint64_t fh, off_t offset)
{
	struct filespec *fs;

	fs = (struct filespec *)((uintptr_t)fh);
	return (offset >= fs->st.st_size) ? 0 : fs->max_retries; 
}


/* @brief base64 encoding length
 *
 * @param length of the string to encode
 * @return length of the encoded string
 */
#define b64_encode_len(l) ((l + 2) / 3 * 4)

/* @brief base64 encoding
 *
 * The caller shall make sure encoded points to a large enough buffer.
 * b64_encode_len above can be used to get the necessary size.
 * 
 * @param where to store the encoded result
 * @param string to encode
 * @param length of the string to encode
 * @return points after the last encoded character.
 */
static char *b64_encode(char *encoded, const char *string, int len)
{
static _Alignas(64) const char b64[64] =
	{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	  'w', 'x', 'y', 'z', '0', '1', '2', '3',
	  '4', '5', '6', '7', '8', '9', '+', '/'  };
	int i;
	char *p;
	/* string is used as unsigned char to simplify arithmetic below */
	const unsigned char *s = (const unsigned char *)string;

	p = encoded;
	for (i = 0; i < len - 2; i += 3) {
		*p++ = b64[  s[i    ] >> 2];
		*p++ = b64[((s[i    ] & 0x3) << 4) | (s[i + 1] >> 4)];
		*p++ = b64[((s[i + 1] & 0xF) << 2) | (s[i + 2] >> 6)];
		*p++ = b64[  s[i + 2] & 0x3F];
	}
	if (i < len) {
		*p++ = b64[s[i] >> 2];
		if (len - 1 == i) {
			*p++ = b64[((s[i] & 0x3) << 4)];
			*p++ = '=';
		}
		else {
			*p++ = b64[((s[i    ] & 0x3) << 4) | (s[i + 1] >> 4)];
			*p++ = b64[((s[i + 1] & 0xF) << 2)];
		}
		*p++ = '=';
	}

	return p;
}

/* Function called to start a new 'stream' at a given offset
 *
 *	It is expected that start_stream will do a range request
 *	starting at offset and up to the end of file (like: offset-)
 *	The return value is zero if all Ok and the opaque stream pointer will
 *	be set to a non-NULL value. If an error occurs it will be returned
 *	to the read() client and the opaque stream pointer is irrelevant.
 *
 * @param the file specification
 * @param struct curl_raw_handle (points also to filespec)
 * @param size when doing "single read" for regular streams : 0
 * @param offset where to start reading
 * @param lock flag
 * @return Ok = 0, Error = negative number returned as read error.
 */
static int astreamfs_start_stream_wrapped(struct filespec *fs,
					  struct curl_raw_handle **crh,
					  size_t size, off_t start_offset,
					  bool *locked)
{
	struct timespec now;
	char *url = fs->URL;
	struct fs_loc fsl;
	unsigned int redirs = 0;
	bool timeout;
	CURL *curl;
static 	const char auth_hdr[] = "\r\nAuthorization: Basic ";

	#define MAX_REDIR (5)

	if (F_KEEP_LOCATION(fs)) {
		/* When location is followed, locks are used to avoid
		 * several streams starting at the same time each taking
		 * a different location. */
		lock(&fs->curlmutex, locked);
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		if (0 == fs->keep_until ||
		         fs->keep_until < now.tv_sec) {
			free(fs->location);
			fs->location = NULL;
			fs->keep_until = 0;
		}
		else {
			url = fs->location;
			/* Unlock hoping the location still works */
			unlock(&fs->curlmutex, locked);
		}
	}
	
	fsl.fs = fs;
	fsl.location  = NULL;

	do {
		/* Build and send the request */
		char upwd[(NULL == fs->userpwd || url != fs->URL) ?
				1 :
				b64_encode_len(strlen(fs->userpwd)) 
							    + sizeof(auth_hdr)];
		fsl.http_code = 0;
		fsl.sz = 0;
		curl = curl_init(fs);
		CURL_EASY_SETOPT(curl, CURLOPT_URL, url, "%s");

		/* encode potential user:passwd (only when using fs->URL) */
		if (NULL == fs->userpwd || url != fs->URL) {
			upwd[0] = '\0';
		}
		else {
			char *p;
			p  = strcpy(upwd, auth_hdr);
			p += lengthof(auth_hdr);
			p  = b64_encode(p, fs->userpwd, strlen(fs->userpwd));
			*p = '\0';
		}

		*crh = send_request(curl, url, upwd
					, header_check
					, &fsl
					, start_offset
					, size
					, &timeout);
		if (NULL == *crh) {
			curl_easy_cleanup(curl);
			if (timeout)
				return -EREMOTEIO;
			if (*locked && 
			    300 == fsl.http_code && NULL != fsl.location) {
				/* 3xx is expected only when it was locked */
				if (redirs >= MAX_REDIR) {
					lprintf(LOG_ERR,
						"Too many redirections for: `%s`\n",
						fs->URL);
					free(fsl.location);
					return -EIO;
				}
				redirs += 1;
				url = fsl.location;
				continue;
			}
			else if (!*locked && F_KEEP_LOCATION(fs) && 
				 (HTTP_NOT_FOUND == fsl.http_code ||
				  HTTP_GONE == fsl.http_code)) {
				/* Tried reusing location, but it's gone, need
				 * to lock again now */
				lock(&fs->curlmutex, locked);
				free(fs->location);
				fs->location = NULL;
				fs->keep_until = 0;
				url = fs->URL;
				continue;
			}
			else if (url == fs->URL &&
				 HTTP_NOT_FOUND == fsl.http_code) {
				/* Now this is the provided URL that is 404 */
				OR(fs->flags, FL_NO_RANGE);
				return -ENOENT;
			}
			free(fsl.location);
			if (HTTP_INTERNAL_SERVER_ERROR <= fsl.http_code &&
			    HTTP_GATEWAY_TIME_OUT >= fsl.http_code)
				return -EREMOTEIO;	/* Server errors */
			else
				return -EIO;		/* Other errors */
		}
	} while(NULL == *crh);

	if (NULL != fsl.location) {
#ifdef _STATS
		lock(&fs->curlmutex, locked);
		fs->location_count++;
#endif
		if (F_KEEP_LOCATION(fs)) {
			fs->location = fsl.location;
			fs->keep_until = now.tv_sec + fs->keep_location_time;
		}
		else {
			free(fsl.location);
		}
	} 
	return 0;
}

static int astreamfs_start_stream(const char *path, struct fuse_file_info *fi,
				  size_t size, off_t off, void **sh,
				  int *fd)
{
	int res;
	bool locked = false;
	struct fi_to_fh *fifh;
	struct filespec *fs;
	struct curl_raw_handle *crh;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));
	fs = (struct filespec *)((uintptr_t)(fifh->fh));
	if (0 > off || off >= fs->st.st_size)
		return -ENODATA;
	if (F_NO_RANGE(fs))
		return -EIO;

	res = astreamfs_start_stream_wrapped(fs, &crh, size, off, &locked);

	unlock(&fs->curlmutex, &locked);

	if (0 == res) {
		*sh = crh;
		*fd = crh->sockfd;
	}
	else if (-EREMOTEIO !=  res) {
	/* Server error can be temporary, don't mark as no-range if so. */
		OR(fs->flags, FL_NO_RANGE);
	}

	return res;
}

static const struct aw_operations astreamfs_aw_oper = {
	.aligned_alloc	= NULL,
	.free		= NULL,
	.release	= NULL,
	.log		= lprintf,
	.error		= astreamfs_error,
	.anticipation	= NULL,
	.retry		= astreamfs_retry,
	.fh_from_fi	= astreamfs_fh_from_fi,
	.close_if_idle  = astreamfs_close_if_idle,
	.store_last_opt	= astreamfs_store_last_opt,
	.load_last_opt	= astreamfs_load_last_opt,
	.start_stream	= astreamfs_start_stream,
	.read		= astreamfs_read_callback,
};

/*
 * @brief init callback for fuse
 *
 * Using astreamfs common code to initialise readers, plus starts the post_ftp
 * thread for writing if needed.
 *
 * @param see fuse documentation
 * @return none
 */
void *astreamfs_init(struct fuse_conn_info *conn_unused)
{
	(void)conn_unused;

	engine_info.n_threads = (0 == params.threads) ? count_URL - not_found
						      : params.threads;
	return worker_init(&astreamfs_aw_oper, &engine_info);
}
	
static struct fuse_operations astreamfs_oper = {
	.init           = astreamfs_init,
	.getattr	= astreamfs_getattr,
	.readdir	= astreamfs_readdir,
	.open		= astreamfs_open,
	.read		= astreamfs_read,
	.read_buf	= astreamfs_read_buf,
	.release	= astreamfs_release,
};


enum {
     KEY_HELP,
     KEY_VERSION,
     KEY_DEBUG,
     KEY_FOREGROUND,
     KEY_RW,
     KEY_NOATIME,
     KEY_NOEXEC,
     KEY_NOSUID,
     KEY_NO_SPLICE_READ,
     KEY_SPLICE_READ,
     KEY_FS_NAME,
     KEY_FS_SUBTYPE,
     KEY_LOG_LEVEL,
     KEY_LOG_FILE,
     KEY_SIZE,
     KEY_TIME,
     KEY_IDLE_TIME,
     KEY_PATH,
     KEY_KEEP_LOCATION_TIME,
     KEY_NO_KEEP_LOCATION,
     KEY_RETRY,
#ifdef _STATS
     KEY_STAT_FILE,
#endif
     KEY_THREADS,
     KEY_CURL_CACERT,
     KEY_CURL_CONFIG,
     KEY_CURL_INSECURE,
     KEY_CURL_IPV4,
     KEY_CURL_IPV6,
     KEY_CURL_KEEPALIVE_TIME,
     KEY_CURL_LOCATION,
     KEY_CURL_NEXT,
     KEY_CURL_NO_KEEPALIVE,
     KEY_CURL_OUTPUT,
     KEY_CURL_REMOTE_HEADER_NAME,
     KEY_CURL_REMOTE_NAME,
     KEY_CURL_REMOTE_TIME,
     KEY_CURL_URL,
     KEY_CURL_USER_AGENT,
     KEY_CURL_USERPWD,
};

static const char end_expand[] = "AilotPSTu";


static const char arg_log_level[] = "--log-level=%i";
static const char arg_log_file[]  = "--log-file=%s";
static const char arg_idle_time[] = "--idle-time=%i";
static const char arg_path[]      = "--path=%s";
static const char arg_size[]      = "--size=%li";
static const char arg_time[]      = "--time=%li";
static const char arg_keep_location_time[]
				  = "--keep-location-time=%i";
static const char arg_no_keep_location[]
				  = "--no-keep-location";
#ifdef _STATS
static const char arg_stat_file[] = "--stat-file=%s";
#endif
static const char arg_threads[]	  = "--threads=%i";
static const char arg_ipv4[]      = "--ipv4";
static const char arg_ipv6[]      = "--ipv6";
static const char arg_config[] 	  = "--config=%s";
static const char arg_insecure[]  = "--insecure";
static const char arg_next[]      = "--next";
static const char arg_user_agent[]= "--user-agent=%s";
static const char arg_remote_header_name[]
				  = "--remote-header-name";
static const char arg_location[]  = "--location";
static const char arg_remote_name[]
				  = "--remote-name";
static const char arg_output[]    = "--output=%s";
static const char arg_remote_time[]
				  = "--remote-time";
static const char arg_userpwd[]   = "--user=%s";
static const char arg_url[]	  = "--url=%s";
static const char arg_keepalive_time[]
				  = "--keepalive-time=%i";
static const char arg_no_keepalive[]
				  = "--no-keepalive";
static const char arg_debug[]	  = "--debug";
static const char arg_retry[]	  = "--retry=%i";

static const char *long_names[] = {
	arg_config		+ O_OFFSET,
	arg_idle_time		+ O_OFFSET,
	arg_keep_location_time	+ O_OFFSET,
	arg_keepalive_time	+ O_OFFSET,
	arg_log_file		+ O_OFFSET,
	arg_log_level		+ O_OFFSET,
	arg_output		+ O_OFFSET,
	arg_path		+ O_OFFSET,
	arg_retry		+ O_OFFSET,
	arg_size		+ O_OFFSET,
#ifdef _STATS
	arg_stat_file		+ O_OFFSET,
#endif
	arg_threads		+ O_OFFSET,
	arg_time		+ O_OFFSET,
	arg_url			+ O_OFFSET,
	arg_userpwd 		+ O_OFFSET,
	arg_user_agent		+ O_OFFSET,
};
#define S_LONG_NAMES (sizeof(long_names) / sizeof(long_names[0]))

#define SZ_LONG_ARG_PATH (lengthof(arg_path) - SZ_STD_ARG_TYPE)
#define SZ_LONG_ARG_USER_AGENT (lengthof(arg_user_agent) - SZ_STD_ARG_TYPE)
#define SZ_O_ARG_USER_AGENT (SZ_LONG_ARG_USER_AGENT - O_OFFSET)
#define SZ_LONG_ARG_USERPWD (lengthof(arg_userpwd) - SZ_STD_ARG_TYPE)

static const struct fuse_opt astreamfs_opts[] = {
  /* our specific option */
  FUSE_ALL_ARG( "-l %i"	, arg_log_level	 , (int)KEY_LOG_LEVEL		),
  FUSE_L_O_ARG(           arg_log_file	 , (int)KEY_LOG_FILE		),
  FUSE_ALL_ARG( "-i %i"	, arg_idle_time	 , (int)KEY_IDLE_TIME		),
  FUSE_ALL_ARG( "-P %s"	, arg_path	 , (int)KEY_PATH		),
  FUSE_ALL_ARG( "-S %li", arg_size	 , (int)KEY_SIZE		),
  FUSE_ALL_ARG( "-T %li", arg_time	 , (int)KEY_TIME		),
  FUSE_ALL_ARG( "-t %i"	,
			  arg_keep_location_time
					 , (int)KEY_KEEP_LOCATION_TIME	),
  FUSE_L_O_ARG(		  arg_no_keep_location
					 , (int)KEY_NO_KEEP_LOCATION	),
  FUSE_L_O_ARG(		  arg_retry	 , (int)KEY_RETRY		),
#ifdef _STATS
  FUSE_L_O_ARG(		  arg_stat_file	 , (int)KEY_STAT_FILE		),
#endif
  FUSE_L_O_ARG(		  arg_threads	 , (int)KEY_THREADS		),

  /* curl options we understand */
  FUSE_ALL_ARG( "-4"	, arg_ipv4	 , (int)KEY_CURL_IPV4		),
  FUSE_ALL_ARG( "-6"	, arg_ipv6	 , (int)KEY_CURL_IPV6		),
  FUSE_ALL_ARG( "-:"	, arg_next	 , (int)KEY_CURL_NEXT		),
  FUSE_ALL_ARG( "-A %s"	, arg_user_agent , (int)KEY_CURL_USER_AGENT	),
  FUSE_ALL_ARG( "-J"	,
			  arg_remote_header_name
					 , (int)KEY_CURL_REMOTE_HEADER_NAME),
  FUSE_ALL_ARG( "-k"	, arg_insecure	 , (int)KEY_CURL_INSECURE	),
  FUSE_ALL_ARG( "-K"	, arg_config	 , (int)KEY_CURL_CONFIG		),
  FUSE_ALL_ARG( "-L"	, arg_location	 , (int)KEY_CURL_LOCATION	),
  FUSE_ALL_ARG( "-O"	, arg_remote_name,(int)KEY_CURL_REMOTE_NAME	),
	/*
	 * We can't use -o for output since that is used by fuse.
	 * We have to use the long option instead --output
	 */
  FUSE_L_O_ARG( 	  arg_output	 , (int)KEY_CURL_OUTPUT		),
  FUSE_ALL_ARG( "-R"	, arg_remote_time, (int)KEY_CURL_REMOTE_TIME	),
  FUSE_ALL_ARG( "-u %s"	, arg_userpwd	 , (int)KEY_CURL_USERPWD	),
  FUSE_L_O_ARG( 	  arg_url	 , (int)KEY_CURL_URL		),
  FUSE_L_O_ARG(		  arg_keepalive_time
					 , (int)KEY_CURL_KEEPALIVE_TIME	),
  FUSE_L_O_ARG(		  arg_no_keepalive
					 , (int)KEY_CURL_NO_KEEPALIVE	),

  /*
   *  fuse standard options
   */
  FUSE_OPT_KEY( "-h"			 , (int)KEY_HELP		),
  FUSE_OPT_KEY( "--help"		 , (int)KEY_HELP		),
  FUSE_OPT_KEY( "-V"			 , (int)KEY_VERSION		),
  FUSE_OPT_KEY( "--version"		 , (int)KEY_VERSION		),
  FUSE_OPT_KEY( "rw"			 , (int)KEY_RW			),
  FUSE_OPT_KEY( "ro"			 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_ALL_ARG( "-d"	, arg_debug	 , (int)KEY_DEBUG		),
  FUSE_OPT_KEY( "-f"			 , (int)KEY_FOREGROUND		),
  FUSE_OPT_KEY( "-o "			 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "-s"			 , (int)FUSE_OPT_KEY_KEEP	),

  FUSE_OPT_KEY( "allow_other"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "allow_root"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "auto_unmount"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "nonempty"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "default_permissions"	 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "fsname=%s"		 , (int)KEY_FS_NAME		),
  FUSE_OPT_KEY( "subtype=%s"		 , (int)KEY_FS_SUBTYPE		),
  FUSE_OPT_KEY( "large_read"		 , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "max_read=%i"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "hard_remove"		 , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "noatime"		 , (int)KEY_NOATIME		),
  FUSE_OPT_KEY( "noexec"		 , (int)KEY_NOEXEC		),
  FUSE_OPT_KEY( "nosuid"		 , (int)KEY_NOSUID		),
  FUSE_OPT_KEY( "nodev"			 , (int)FUSE_OPT_KEY_KEEP	),

  FUSE_OPT_KEY( "use_ino"                , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "readdir_ino"            , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "direct_io"              , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "kernel_cache"           , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "auto_cache"             , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "noauto_cache"           , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "umask=%s"               , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "uid=%i"                 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "gid=%i"                 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "entry_timeout=%s"       , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "negative_timeout=%s"    , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "attr_timeout=%s"        , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "ac_attr_timeout=%s"     , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "noforget"               , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "remember=%s"            , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "nopath"                 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "modules=%s"             , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "max_write=%i"           , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "max_readahead=%i"       , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "max_background=%i"      , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "congestion_threshold=%i", (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "async_read"             , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "sync_read"              , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "atomic_o_trunc"         , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "big_writes"             , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_remote_lock"         , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "no_remote_flock"        , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_remote_posix_lock"   , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "splice_write"           , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_splice_write"        , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "splice_move"            , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_splice_move"         , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "splice_read"		 , (int)KEY_SPLICE_READ		),
  FUSE_OPT_KEY( "no_splice_read"	 , (int)KEY_NO_SPLICE_READ	),
  FUSE_OPT_END
};

static const char s_http[] = "http://" ;
static const char s_https[]= "https://";
/* static const char s_ftp[]  = "ftp://"  ; */

static const struct {
  const char *p;
  const unsigned int  s;
} scheme[]= { {s_http , lengthof(s_http)},
              {s_https, lengthof(s_https)},
/*            {s_ftp  , lengthof(s_ftp)}, */
            };

#define N_SCHEMES (sizeof(scheme)/sizeof(scheme[0]))
#define MSG_ERR_ADD_OPT_ARG "adding option argument (fuse_opt_add_arg).\n"

/*
 * @brief canonicalizes an absolute path
 *
 * Since we create paths from the arguments, we cannot use the standard
 * canonicalize_file_name function. These functions do the same but do not
 * resolve links. So they only remove multiple consecutive /, useless ./
 * and compute ../
 * Because we don't resolve links, the returned buffer's size is always
 * shorter or equal than the entry buffer, so the "ret" buffer should be
 * at least of same length as the "name".
 *
 * @param path argument passed to the program
 * @param return buffer
 * @return size of the result string
 */
static size_t canonicalize_path(const char *arg, char *res)
{
	const char *p;
	char *q;

	p = arg + (('-' == arg[1]) ? SZ_LONG_ARG_PATH : SZ_SHORT_ARG);
	if ('\0' == *p) {
		lprintf(LOG_ERR, "empty path.\n");
		return 0;
	}

	if ( '/' == *p ) {
		*res = '/';
		q = res;
		p++;
	}
	else {
		q = res + strlen(res) -1;
	}

	for (; '\0' != *p; p++)
	{
		if ('/' == *q) {
			if ( '/' == *p )
				continue;
			if ('.' == *p) {
				if ('/' == p[1]) {
					p++;
					continue;
				}
				if ('\0' == p[1])
					break;
				if ('.' == p[1] &&
				    ('/' == p[2] || '\0' == p[2])) {
					if (q != res)
						while ('/' != *(--q));
					p += 2;
					continue;
				}
			}
		}
		if (q - res > PATH_MAX - 3) {
			lprintf(LOG_ERR, "path is too long: %s.\n", arg);
			return 0;
		}
		q++;
		*q = *p;
	}
	if ('/' != *q)
		*(++q) = '/';
	*(++q) = '\0';
	return q -res;
}

/*
 * @brief qsort callback to compare on paths
 *
 * @param pointer on key
 * @param pointer on another key
 * @return <0 >0 or ==0 according to order of keys
 */
static int path_cmp(const void *k1, const void *k2)
{
	return strcmp(*((char **)k1), *((char **)k2));
}


/*
 * @brief update paths pointer while deduplicating paths
 *
 * @param old path pointer
 * @param new path pointer
 * @return none
 */
static void update_path(char *old, char *new)
{
	unsigned long i;
	for (i = 0; i < count_URL; i++)
		if ( fss[i].path == old )
			fss[i].path = new;
}

/*
 * @brief deduplicates paths
 *
 * @param none
 * @return none
 */
static void dedup_path()
{
	unsigned long i, j, k, uniq;
	char **new;
	size_t mem;

	qsort(paths, count_path, sizeof(char *), path_cmp);
	uniq = 1;
	mem = strlen(paths[0]) + 1;
	for (j = 0, i = 1; i < count_path; i++) {
		if (0 == strcmp(paths[i], paths[j])) {
			update_path(paths[i],  paths[j]);
		}
		else {
			uniq++;
			mem += strlen(paths[i]) + 1;
			j = i;
		}
	}
	if (uniq == count_path) {
		lprintf(LOG_INFO,"all paths are unique.\n");
		return;
	}

	new = malloc(uniq * sizeof(char *) + mem);
	new[0] = (char *)(&new[uniq]);
	strcpy(new[0], paths[0]);
	update_path(paths[0],  new[0]);
	for (k= 0, j = 0, i = 1; i < count_path; i++)
		if (0 != strcmp(paths[i], paths[j])) {
			new[k + 1] = new[k] + strlen(new[k]) + 1;
			k++;
			strcpy(new[k], paths[i]);
			update_path(paths[i],  new[k]);
			j = i;
		}
	count_path = uniq;
	free(paths);
	paths = new;
	lprintf(LOG_INFO,"there are %u unique paths.\n", uniq);
}

/*
 * @brief finalise a filesystem entry
 *
 * Final operations and checks to simplify further management
 *
 * @param index of the entry to initialise
 * @return none
 */
static void end_init(unsigned long cur_URL)
{
	char *p;

	if (!F_GOT_NAME(&fss[cur_URL])) {
		if (!F_REMOTE_NAME(&fss[cur_URL])) {
			fss[cur_URL].filename =
					malloc(sizeof(default_filename) + 6);
			sprintf(fss[cur_URL].filename, "%s%06lu",
				default_filename, cur_URL);
		}
		else {
			p = strrchr(fss[cur_URL].URL, '/');
			if ('\0' == p[1]) {
			/*
			 * We could have issued only a warning and used the
			 * default file value, but we choose to replicate
			 * the curl command line tool behaviour here!
			 */
				lprintf(LOG_ERR,
					"remote file name has no length: %s.\n",
					fss[cur_URL].URL);
			}
			fss[cur_URL].filename = malloc(strlen(p));
			strcpy(fss[cur_URL].filename, p + 1);
		}
	}
}

/*
 * @brief initialise a filesystem entry
 *
 * Depending on new_args boolean, either sets the entry to default options or
 * copy options from the previous entry.
 *
 * @param pointer to the parse_data structure
 * @return none
 */
static void init_url(struct parse_data *d)
{
	if (d->new_args) {
		STORE(fss[d->cur_URL].flags, 0, relaxed);
		fss[d->cur_URL].max_retries = 0;
		fss[d->cur_URL].curl_IP_resolve = CURL_IPRESOLVE_WHATEVER;
		fss[d->cur_URL].curl_opt_keepalive_time = 60;
		fss[d->cur_URL].idle_time = 300;
		fss[d->cur_URL].keep_location_time = 0;
		fss[d->cur_URL].keep_until = 0;
		fss[d->cur_URL].userpwd = NULL;
		fss[d->cur_URL].path = paths[0];
#ifdef _STATS
		fss[d->cur_URL].location_count = 0;
#endif
		fss[d->cur_URL].location = NULL;
		STORE(fss[d->cur_URL].nb_open , 0, relaxed);
		d->new_args = false;
	}
	else {
		memcpy( &fss[d->cur_URL],
			&fss[d->cur_URL - 1],
			sizeof(struct filespec));
		if (0 != strcmp(fss[d->cur_URL].path, d->cur_abs_path))
			fss[d->cur_URL].path = paths[count_path - 1];
		fss[d->cur_URL].flags &= ~(FL_GOT_NAME | FL_GOT_SIZE |
					   FL_GOT_TIME);		   
		fss[d->cur_URL].st.st_size   = 0;
		fss[d->cur_URL].st.st_blocks = 0;
	}
	fss[d->cur_URL].URL = NULL;
	fss[d->cur_URL].filename = NULL;
	pthread_mutex_init(&fss[d->cur_URL].curlmutex, NULL);
}


/*
 * @brief processing function called by fuse_opt_parse
 *
 * See fuse_opt.h for this function specifics.
 *
 * @param data is the user data passed to the fuse_opt_parse() function
 * @param arg is the whole argument or option
 * @param key determines why the processing function was called
 * @param outargs the current output argument list
 * @return -1 on error, 0 if arg is to be discarded, 1 if arg should be kept
 */
static int astreamfs_opt_proc(void *parse_data,
			      const char *arg,
			      int key,
			      struct fuse_args *outargs)
{
	struct parse_data *d = (struct parse_data *)parse_data;
	int i;
	uint64_t i64;
	unsigned long off, sz;
	char *p, *q;

	switch (key) {
	case KEY_CURL_URL :
		arg = strchr(arg, '=') + 1;
		if (NULL == arg)
			lprintf(LOG_ERR, "malformed argument: %s\n", arg);
		/* fall through to manage the URL */
	case FUSE_OPT_KEY_NONOPT :
		if (count_URL - 1 == d->cur_URL &&
		    NULL != fss[d->cur_URL].URL) {
			return 1;
		}

		if (d->got_URL) {
			end_init(d->cur_URL);
			d->cur_URL++;
			init_url(d);
		}
		d->got_URL = true;
		for (i = 0;
		     i < N_SCHEMES &&
		     0 != strncmp(arg, scheme[i].p, scheme[i].s);
		     i++);
		if (N_SCHEMES == i && NULL != strstr(arg, "://"))
			lprintf( LOG_ERR, "unknown scheme in: %s\n", arg);
		if (NULL != strchr(arg, '@'))
			lprintf( LOG_ERR, "user:pwd in URL is not supported, use -u: %s\n", arg);
		p = strchr(arg + (( N_SCHEMES == i ) ? 0 : scheme[i].s), '/');
		q = malloc(strlen(arg) +
			     ((NULL==p) ? 2 : 1) +
			     (( N_SCHEMES == i ) ? scheme[0].s : 0));
		fss[d->cur_URL].URL = q;
		if (N_SCHEMES == i) {
			memcpy(q, scheme[0].p, scheme[0].s);
			q += scheme[0].s;
		}
		strcpy(q, arg);
		if (NULL == p) {
			q += strlen(arg);
			q[0]= '/';
			q[1]= '\0';
		}
		break;

	case KEY_CURL_NEXT :
		if (count_URL - 1 == d->cur_URL || !d->got_URL) {
			lprintf(LOG_WARNING,
				"--next is ignored before we got a URL or after last URL.\n");
			break;
		}
		end_init(d->cur_URL);
		d->cur_URL++;
		d->new_args = true;
		d->got_URL = false;
		init_url(d);
		d->cur_abs_path[1] = '\0';
		break;

	case KEY_PATH:
		paths[count_path] = paths[0] + d->path_sz;
		sz = canonicalize_path(arg, d->cur_abs_path);
		strcpy(paths[count_path], d->cur_abs_path);
		d->path_sz += sz + 1;
		if (!d->got_URL)
			fss[d->cur_URL].path = paths[count_path];
		count_path++;
		break;

	case KEY_CURL_USERPWD:
		off = ('-' == arg[1])	? SZ_LONG_ARG_USERPWD
					: SZ_SHORT_ARG   ;
		if (NULL == strchr(arg + off, ':'))
			lprintf(LOG_ERR,
				"-u with no password (user:pwd) `%s`\n",
				arg + off);
		free(fss[d->cur_URL].userpwd);
		fss[d->cur_URL].userpwd = malloc(strlen(arg + off) + 1);
		strcpy(fss[d->cur_URL].userpwd, arg + off);
		break;
	case KEY_RW:
		lprintf(LOG_WARNING,
			"this filesystem will be mounted read-only, ignoring 'rw' option.\n");
		break;

	case KEY_DEBUG:
		params.debug = true;
                    /* debug implies foreground: fall through */
	case KEY_FOREGROUND:
		params.foreground = true;
		return 1;
	case KEY_NOATIME:
		params.noatime = true;
		return 1;
	case KEY_NOEXEC:
		params.noexec = true;
		return 1;
	case KEY_NOSUID:
		params.nosuid = true;
		return 1;
	case KEY_NO_SPLICE_READ:
		params.no_splice_read = true;
		return 1;
	case KEY_SPLICE_READ:
		params.no_splice_read = false;
		return 1;

	case KEY_CURL_CACERT:
		read_str(arg, &params.ca_file);
		break;
	case KEY_CURL_IPV4:
		fss[d->cur_URL].curl_IP_resolve = CURL_IPRESOLVE_V4;
		break;
	case KEY_CURL_IPV6:
		fss[d->cur_URL].curl_IP_resolve = CURL_IPRESOLVE_V6;
		break;
	case KEY_CURL_REMOTE_HEADER_NAME:
		fss[d->cur_URL].flags |= FL_REMOTE_HEADER_NAME;
		break;
	case KEY_CURL_REMOTE_NAME:
		fss[d->cur_URL].flags |= FL_REMOTE_NAME;
		break;
	case KEY_CURL_REMOTE_TIME:
		fss[d->cur_URL].flags |= FL_REMOTE_TIME;
		break;
	case KEY_CURL_LOCATION:
		fss[d->cur_URL].flags |= FL_LOCATION ;
		break;
	case KEY_NO_KEEP_LOCATION:
		fss[d->cur_URL].flags &= ~FL_KEEP_LOCATION;
		break;
	case KEY_CURL_NO_KEEPALIVE:
		fss[d->cur_URL].curl_opt_keepalive_time = 0;
		break;
	case KEY_CURL_OUTPUT:
		read_str(arg, &fss[d->cur_URL].filename);
		fss[d->cur_URL].flags |= FL_GOT_NAME;
		break;
	case KEY_RETRY:
		i64 = read_int(arg);
		if (i64 > REASONABLE_RETRIES) {
			lprintf(LOG_WARNING,
				"unreasonable retry specification: %"PRIu64", using %u.\n",
				i64, (unsigned int)REASONABLE_RETRIES);
			fss[d->cur_URL].max_retries = REASONABLE_RETRIES;
		}
		else {
			fss[d->cur_URL].max_retries = i64;
		}
		break;
	case KEY_SIZE:
		fss[d->cur_URL].flags |= FL_GOT_SIZE;
		fss[d->cur_URL].st.st_size    =
		fss[d->cur_URL].st.st_blocks  = (size_t)read_int(arg);
		fss[d->cur_URL].st.st_blocks /= ST_BLK_SZ;
		break;
	case KEY_TIME:
		fss[d->cur_URL].flags |= FL_GOT_TIME;
		fss[d->cur_URL].st.st_mtim.tv_sec  =
		fss[d->cur_URL].st.st_ctim.tv_sec  = read_int(arg);
		fss[d->cur_URL].st.st_mtim.tv_nsec =
		fss[d->cur_URL].st.st_ctim.tv_nsec = 0;
		break;
	case KEY_IDLE_TIME:
		fss[d->cur_URL].idle_time = (unsigned int)read_int(arg);
		break;
	case KEY_CURL_KEEPALIVE_TIME:
		fss[d->cur_URL].curl_opt_keepalive_time =
						(unsigned int)read_int(arg);
		break;
	case KEY_LOG_LEVEL:
		params.log_level = (unsigned int)read_int(arg);
		if (MAX_LEVEL < params.log_level) {
			lprintf(LOG_WARNING,
				"log level=%u must be between 0 and %u: assuming %u.\n",
				params.log_level, MAX_LEVEL, MAX_LEVEL);
			params.log_level = MAX_LEVEL;
		}
		break;
	case KEY_LOG_FILE:
		if (NULL != log_fh)
			fclose(log_fh);
		read_str(arg, &params.log_file);
		log_fh = fopen(params.log_file, "w");
		if (NULL == log_fh)
			lprintf(LOG_ERR, "cannot open %s.\n", params.log_file);
		break;
	case KEY_KEEP_LOCATION_TIME:
		fss[d->cur_URL].keep_location_time =
						(unsigned int)read_int(arg);
		fss[d->cur_URL].flags |= FL_KEEP_LOCATION;
		break;
	case KEY_CURL_INSECURE:
		fss[d->cur_URL].flags |= FL_INSECURE;
		break;
	case KEY_FS_NAME:
		read_str(arg, &params.filesystem_name);
		break;
	case KEY_FS_SUBTYPE:
		read_str(arg, &params.filesystem_subtype);
		break;
	case KEY_CURL_USER_AGENT:
		if (default_user_agent != params.user_agent)
			free(params.user_agent);
		off = ('-' == arg[0]) ? ( '-' == arg[1] ? SZ_LONG_ARG_USER_AGENT
							: SZ_SHORT_ARG )
				      :			  SZ_O_ARG_USER_AGENT;
		sz = strlen(arg + off);
		if (0 == sz) {
			params.user_agent = NULL;
		}
		else {
			params.user_agent = malloc(strlen(arg + off) + 1);
			strcpy(params.user_agent, arg + off);
		}
		break;
#ifdef _STATS
	case KEY_STAT_FILE:
		read_str(arg, &params.stat_file);
		if ('/' == params.stat_file[0])
			memmove(params.stat_file, 
				params.stat_file + 1,
				strlen(params.stat_file));
		if (NULL != strchr(params.stat_file, '/')) {
			lprintf(LOG_ERR,
				"forbidden '/' in name of stat file `%s`.\n",
				params.stat_file);
			free(params.stat_file);
			params.stat_file = NULL;
		}
		break;
#endif
	case KEY_THREADS:
		i64 = read_int(arg);
		if (i64 > UINT_MAX)
			lprintf(LOG_ERR, "threads overflow: %s\n", arg);
		params.threads = i64;
		break;

	case KEY_HELP :
	case KEY_VERSION :
		lprintf(LOG_WARNING,
			"--help and --versions options are ignored in a config file.\n");
		break;
	default:
		lprintf(LOG_ERR, "unknown option: %s.\n", arg);
		break;
	}
	return 0;
}

/*
 * @brief counting function called by fuse_opt_parse
 *
 * This callback only handles --help, --version, and counts the number of
 * URLs so that we can allocate memory for the filesystem entries.
 * It does also keep track of the last non-option argument which is the
 * mount point.
 *
 * See fuse_opt.h for this function specifics.
 *
 * @param data is the user data passed to the fuse_opt_parse() function
 * @param arg is the whole argument or option
 * @param key determines why the processing function was called
 * @param outargs the current output argument list
 * @return -1 on error, 0 if arg is to be discarded, 1 if arg should be kept
 */
 static int astreamfs_opt_count(void *parse_data,
				const char *arg,
				int key,
				struct fuse_args *outargs)
{
	struct parse_data *d = (struct parse_data *)parse_data;

	switch (key) {
	case KEY_HELP :
		fprintf(stderr,
			"Usage: astreamfs [options] URL [options] [[URL] [options]]* mountpoint\n"
			"\n"
			"astreamfs options:\n"
			"     -l --log-level=N  Verbosity level. The levels are those of syslog.\n"
			"                       The higher, the most verbose. Default: 4 (warning).\n"
			"        --log-file=filepath\n"
			"                       Log will be saved to that file instead of stderr\n"
			"                       (when foreground) of syslog. This is mostly useful\n"
			"                       to avoid spamming syslog when log-level is high and\n"
			"                       not running foreground.\n"
#ifdef _STATS
			"    --stat-file=filename\n"
			"                      Pseudo-file to get live statistics from the driver.\n"
#endif
			"     -i  --idle-time=T Time (sec) after which a stream will return to idle\n"
			"                       state if it receives no request. Default: 300s\n"
			"     -t --keep-location-time\n"
			"                       Combined with -L option, astreamfs will use the\n"
			"                       redirected location for the specified time (sec), when\n"
			"                       a new stream is needed. It can save a lot of overhead\n"
			"                       in situations where the redirected location is valid\n"
			"                       for some time.\n"
			"                       This option has no effect when -L is not specified.\n"
			"                       Default: --no-keep-location (see below)\n"
			"                       Both time option: 0 means forever.\n"
			"        --no-keep-location\n"
			"                       This is the default and cancels a previous -t.\n"
			"                       It means always restart from the original URL and never\n"
			"                       use twice a previous redirect location.\n"
			"     -P --path         Specifies a path for all subsequent URLs.\n"
			"                       Default (and after --next) is root: /\n"
			"     -S --size=N       Specify the file size (bytes).\n"
			"                       astreamfs won't try to get the file from the server.\n"
			"     -T --time=T       Specify the file modification date & time (timestamp).\n"
			"                       Default: same date & time as the mount directory.\n"
			"                       When specified, -R option is ignored.\n"
			"\n"
			"curl options used by astreamfs (`man curl` for detail):\n"
			"     -4 --ipv4\n"
			"     -6 --ipv6\n"
			"     -k --insecure\n"
			"        --cacert\n"
			"     -A --user-agent\n"
			"     -J --remote-header-name\n"
			"        --keepalive-time\n"
			"     -L --location\n"
			"        --no-keepalive\n"
			"        --output\n"
			"     -O --remote-name\n"
			"     -R --remote-time\n"
			"     -u --user (Note: authentication is set to basic as with --basic)\n"
			"     	 --url\n"
			"     -: --next\n"
			"\n"
			"     The default user agent sent in each request is: %s\n"
			"     You can change it with the -A option. To stop sending user agent use: -A ''\n"
			"     All fuse options, standard mount options, --user-agent, --log-level and\n"
			"     --log-file are global: eg. not specific to a single URL. They may all be\n"
			"     specified with the -o form of fuse, unlike options related to URLs.\n"
			"\n"
			"Multiple URLs:\n"
			"     All non global options applies to the 'current' URL.\n"
			"     When a new URL is specified, directly or with --url, it inherits all the\n"
			"     options set for the previous URL, except: --output, -S and -T\n"
			"     If you want options reset to default use: -: --next before the next URL.\n"
			"     Note: global options (see above) are not reset by --next.\n"
			"\n"
			"General options:\n"
			"    -o opt,[opt...]  mount options\n"
			"    -h   --help      print help\n"
			"    -V   --version   print version\n"
			"\n", default_user_agent);
		if (0 != fuse_opt_add_arg(outargs, "-ho"))
			lprintf(LOG_ERR, MSG_ERR_ADD_OPT_ARG);

		exit((0 == fuse_main(outargs->argc,
				     outargs->argv,
				     &astreamfs_oper,
				     NULL)) ? EXIT_SUCCESS : EXIT_FAILURE);


	case KEY_VERSION :
		fprintf(stderr, "Copyright (C) 2018-2025  Alain Bénédetti\n"
			"This program comes with ABSOLUTELY NO WARRANTY.\n"
			"This is free software, and you are welcome to redistribute it\n"
			"under the conditions of the GPLv3 or later, at your convenience.\n"
			"Full license text can be found here: https://www.gnu.org/licenses/gpl-3.0.html\n\n");
		fprintf(stderr, PROG_NAME": version "PROG_VERSION"\n");
		if (0 != fuse_opt_add_arg(outargs, "--version"))
			lprintf( LOG_ERR, MSG_ERR_ADD_OPT_ARG );

		exit((0 == fuse_main(outargs->argc,
				     outargs->argv,
				     &astreamfs_oper,
				     NULL)) ? EXIT_SUCCESS : EXIT_FAILURE);

	case KEY_PATH :
		count_path++;
		d->path_sz += canonicalize_path(arg, d->cur_abs_path) + 1;
		break;

	case KEY_CURL_NEXT :
		d->cur_abs_path[1] = '\0';
		break;

	case KEY_CURL_URL :
		d->last_noopt = NULL;
		count_URL++;
		break;
	case FUSE_OPT_KEY_NONOPT :
		d->last_noopt = arg;
		count_URL++;
		break;
	}
	return 0;
}


/*
 * @brief Tests the URL passed as parameter
 *
 * All elements have a name (be it by default) and a time (be it default).
 * When there is no size or when remote header information is requested
 * an access is made to possibly complete the information of each URL.
 *
 * @param pointer to structure specifying the details of the URL to check
 * @return none
 */
 static void test_URL()
{
	unsigned long i, tested = 0;
	int res;
	struct curl_raw_handle *crh;
	bool locked = false;

	lprintf(LOG_NOTICE, "checking the URLs.\n");

	for (i = 0; i < count_URL; i++) {
		/* Ignore KEEP_LOCATION when not following locations! (-L) */
		if (!F_LOCATION(&fss[i]))
			fss[i].flags &= ~FL_KEEP_LOCATION;
		if (!F_GOT_SIZE(&fss[i]) ||
		    F_REMOTE_HEADER_NAME(&fss[i]) ||
		    F_REMOTE_TIME(&fss[i])) {
			tested++;
			res = astreamfs_start_stream_wrapped(&fss[i], &crh,
							     0,0, &locked);
			unlock(&fss[i].curlmutex, &locked);

			if (0 != res) {
				fss[i].flags |= FL_NO_RANGE;
				if (-ENOENT == res) {
					not_found++;
					fss[i].flags |= FL_NOT_FOUND;
				}
			}
			else {
				curl_easy_cleanup(crh->curl);
				free(crh);
			}
		}
		fss[i].flags |= FL_IS_INIT;
	}
	if (0 != not_found) {
		lprintf(LOG_WARNING,
			"%lu URL(s) not found within the %lu entries tested.\n",
			not_found, tested);
		if (not_found == count_URL)
			lprintf(LOG_ERR, "No valid URL left: exiting.\n");
	}
}


/*
 * @brief consistency check on arguments passed to the command line
 *
 * Also prints out the parameters used if verbosity is >= INFO (6).
 *
 * @param none
 * @return none
 */
static void check_args(struct fuse_args *args)
{
	size_t s_name, s_type;
	char *fsname;

	lprintf(LOG_INFO, "log level is %d.\n",
	 params.log_level);
	if (NULL == params.user_agent)
		lprintf(LOG_INFO, "no user_agent string will be sent\n");
	else
		lprintf(LOG_INFO, "user_agent=%s\n", params.user_agent );
	if (params.foreground)
		lprintf(LOG_INFO, "foreground.\n");
	if (params.debug)
		lprintf(LOG_INFO, "debug.\n");
	fsname = (NULL == params.filesystem_name) ? fss[0].URL
						  : params.filesystem_name;
	s_name = strlen(fsname);
	if (NULL == params.filesystem_subtype) {
		params.filesystem_subtype= (char *)def_type;
		s_type= lengthof(def_type);
	}
	else {
		s_type= strlen(params.filesystem_subtype);
		lprintf(LOG_INFO, "filesystem_subtype=%s\n"
				, params.filesystem_subtype);
	}
	char add_opt[sizeof(ro_opt) + sizeof(subtype) + s_name + s_type];
	memcpy(add_opt, ro_opt, lengthof(ro_opt));
	memcpy(add_opt + lengthof(ro_opt), fsname, s_name);
	memcpy(add_opt + lengthof(ro_opt) + s_name, subtype, lengthof(subtype));
	memcpy(add_opt + lengthof(ro_opt) + s_name + lengthof(subtype),
	       params.filesystem_subtype, s_type + 1);
	lprintf(LOG_INFO, "adding options=%s\n",add_opt);
	fuse_opt_add_arg(args, add_opt);
}


/*
 *
 */
int main(int argc, char *argv[])
{
	unsigned int ret = 0, i;
	struct parse_data data = { 0, true, false, NULL, NULL, 2 };
	struct fuse_args exp_args = {argc, argv, false};
	struct fuse_args args;
	struct timespec t;

	clock_gettime(CLOCK_REALTIME, &t);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &main_start);

	expand_args(&exp_args, long_names, S_LONG_NAMES, end_expand);

	/* Count URLs, allocate memory for the FS and checks mount path */
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	args.allocated = false;
	{
		char buf[PATH_MAX];
		data.cur_abs_path = buf;
		buf[0] = '/';
		buf[1] = '\0';
		if (0 != fuse_opt_parse(&args,
					&data,
					astreamfs_opts,
					astreamfs_opt_count))
			lprintf(LOG_ERR, "parsing arguments.\n");
	}
	fuse_opt_free_args(&args);
	if (count_URL < 2)
		lprintf(LOG_ERR, "no URL or mount path specified.\n");
	count_URL--;

	check_mount_path(data.last_noopt);

	fss   = malloc(count_URL * sizeof(struct filespec));
	params.mount_st.st_mode &= ~( S_IWUSR |	S_IWGRP | S_IWOTH );
	for (i = 0; i < count_URL; i++) {
		memcpy( &fss[i].st, &params.mount_st, sizeof(struct stat));
		fss[i].st.st_nlink = 1;
		fss[i].st.st_size = 0;
	}

	/* Parse arguments */
	params.user_agent = (char *)default_user_agent;
	paths = malloc(count_path * sizeof(char *) + data.path_sz);
	paths[0] = (char *)(&paths[count_path]);
	paths[0][0] = '/';
	paths[0][1] = '\0';
	data.path_sz = 2;
	count_path = 1;
	init_url(&data);
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	{
		char buf[PATH_MAX];
		data.cur_abs_path = buf;
		buf[0] = '/';
		buf[1] = '\0';
		if (0 != fuse_opt_parse(&args,
					&data,
					astreamfs_opts,
					astreamfs_opt_proc))
			lprintf(LOG_ERR, "parsing arguments.\n");
	}
	end_init(count_URL - 1);

	start_message(&t.tv_sec);

	lprintf(LOG_INFO, "found %u URL.\n", count_URL);
	lprintf(LOG_INFO, "found %u paths\n", count_path);
	if (exp_args.allocated)
		free(exp_args.argv);

	/* Check arguments, test URLs and output debug information */
	check_args(&args);

	st_mode = params.mount_st.st_mode & ~(S_IFMT | S_ISVTX);
	st_mode |= S_IFREG;
	if (params.noexec)
		st_mode &= ~(S_IXUSR | S_IXGRP | S_IXOTH);
	if (params.nosuid)
		st_mode &= ~S_ISUID;
	for (i = 0; i < count_URL; i++) {
		fss[i].st.st_mode = st_mode;
	}

	if ( CURLE_OK != curl_global_init(CURL_GLOBAL_ALL) )
		lprintf( LOG_ERR, "initializing curl: curl_global_init().\n" );

	test_URL();

	dedup_path();

	if (params.log_level >= LOG_INFO)
		for (i= 0; i < count_URL; i++) {
			lprintf(LOG_INFO, "fss[%d].URL=%s\n", i, fss[i].URL);
			if (F_NOT_FOUND(&fss[i])) {
				lprintf(LOG_WARNING, "Entry removed\n");
				continue;
			}
			lprintf(LOG_INFO, "fss[%d].filename=%s%s\n"
					, i, fss[i].path, fss[i].filename);
			lprintf(LOG_INFO, "Remote file size=%"PRIu64"\n"
					, fss[i].st.st_size);
			lprintf(LOG_INFO, "flags=%x times: keepalive=%u idle=%u\n"
					, fss[i].flags
					, fss[i].curl_opt_keepalive_time
					, fss[i].idle_time);
			if (F_LOCATION(&fss[i]) && F_KEEP_LOCATION(&fss[i]))
				lprintf(LOG_INFO, "location=%s keep=%u\n"
						, fss[i].location
						, fss[i].keep_location_time);
			if (NULL != fss[i].userpwd) {
				lprintf(LOG_INFO, "User:pwd: (%p) `%.*s:***`\n"
						, fss[i].userpwd
						, strchr(fss[i].userpwd, ':') -
						  fss[i].userpwd
						, fss[i].userpwd );
			}
		}

	/*
	 * Starting the fuse mount now!
	*/
	lprintf(LOG_NOTICE,"Init done, starting fuse mount.\n");
	init_done= true;

	if (params.no_splice_read)
		astreamfs_oper.read_buf = NULL;

	ret = fuse_main(args.argc,
			args.argv,
			&astreamfs_oper,
			NULL);

	/* Cleaning */
#ifdef _STATS
	/* Do one last stats if STATS are on and needed */
	struct stats *last_stats = NULL;

	if (NULL != params.stat_file && 
	    (NULL != log_fh || params.foreground)) {
		last_stats = malloc(sizeof(struct stats));
		last_stats->size = 0;
		out_stats(last_stats);
	}
#endif
	aw_destroy();
	curl_global_cleanup();
	fuse_opt_free_args(&args);
	for (i= 0; i < count_URL; i++) {
		free(fss[i].URL);
		free(fss[i].location);
		free(fss[i].filename);
		free(fss[i].userpwd);
	}
	if (default_user_agent != params.user_agent)
		free(params.user_agent);
	if (def_type != params.filesystem_subtype)
		free(params.filesystem_subtype);
	free(params.filesystem_name);
	free(params.log_file);
	free(params.ca_file);
	free(fss);
	free(paths);
#ifdef _STATS
	if (NULL != params.stat_file) {
		if (NULL != last_stats) {
			if (NULL != log_fh)
				fputs(last_stats->buf, log_fh);
			else
				fprintf(stderr, "%s", last_stats->buf);
			free(last_stats);
		}
		free(params.stat_file);
	}
#endif
#ifdef _MEMCOUNT
	struct stats stts;
	stts.size = 0;
	out_stats_mem(stts.buf, stts.size, STAT_MAX_SIZE);
	lprintf(LOG_INFO, "Last memory status for leak check.\n");
	if (NULL != log_fh)
		fputs(stts.buf, log_fh);
	else if (params.foreground)
		fprintf(stderr, "%s", stts.buf);
#endif
	if (NULL != log_fh)
		fclose(log_fh);
	return ret;
}
