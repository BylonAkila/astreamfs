/*
 * 1fichierfs: simplified implementation of rcu
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/***********************************************************************
 *  Instead of pulling the liburcu dependancy, for the moment we do
 *  a simplified processing.
 *  The cleaning of structures is done when we observe zero thread in
 *  the read critical section. This is a stronger condition than what
 *  liburcu does, but as long as there is a "reasonable" usage of the
 *  mount, it will only defer from a "reasonable" time the freeing of buffers.
 *  We still use the liburcu function names, so that replacing it with the
 *  read stuff at some point should be easy.
 *  What also makes less efficient to use the real liburcu is that we have
 *  no control on fuse threads that have critical sections. The safest path is
 *  then to issue rcu_register_thread and rcu_read_lock at the same time, and
 *  same for unlock.
 */
#include "1fichierfs.h"

struct rcu_item {
	struct rcu_item  *next;
	void		(*func)(struct rcu_item *item);
	void		 *p;
};


#define STEP_COUNT (2) /* Must be power of 2, but 2 (2¹) is enough! */
#define MASK_DEFER (STEP_COUNT - 1)
static _Atomic (struct rcu_item *) rcu_heads[STEP_COUNT] = { NULL, };
static _Atomic unsigned long       rcu_count = 0 ;

/* When that is called, it is assured that there is no contention because
 * rcu_read_unlock can only call here on the only thread that succeeded the
 * XCHG. Hence no "atomic protection" is needed.
 */
static void rcu_free_queue(unsigned int index)
{
	struct rcu_item *prv, *next;
	prv = AND(rcu_heads[index], NULL);
	if (NULL != prv) {
		do {
			DEBUG("RCU Defered cleaning (%u) %p.\n", index, prv);
			next = prv->next;
			prv->func(prv);
			free(prv);
			prv = next;
		} while(NULL != prv);
	}
	SUB(rcu_count, STEP_COUNT);
	/* Note that at this point the "other" queue pointed to by rcu_count
	 * might also be at zero thread. To avoid complex loops, the freeing
	 * will be done next time it reaches zero. */
}

/* Just in case of multiple rcu_init/rcu_exit.
 */
void rcu_init(void)
{
	unsigned int i;

	for (i = 0; i < STEP_COUNT; i++)
		STORE(rcu_heads[i], NULL);
	STORE(rcu_count  , 0L);
}

void rcu_read_lock(void)
{
	ADD(rcu_count, STEP_COUNT);
}

/* Decrement and decision to rcu_free MUST be atomic!
 * The process it then to read the counter, extract the index of the current
 * queue, then test if this is the last thread:
 * - if so, the target value is 1 thread active (the rcu cleaner) + increment
 *   the index. This ensures the rcu cleaner can clean the queue  before it
 *   loops back to queuing on the same queue. The cleaner will then decrement
 *   itself once the cleaning is done, allowong for the next queue to now be
 *   also freed.
 * - otherwise the target is just one less thread active.
 * ... then we do the XCHG.
 * Once the XCHG has succeeded and this was the last thread, do the rcu_free
 *
 * So, when the rcu_free is called on an index, another index will gather new
 * concurrent rcu_free, so to avoid race conditions.
 * There could be 2^N indexes, but actually, 2 (2¹) indexes are enough.
 *
 * Inconvenient, if the thread that won the XCHG (unlikely) dies doing the
 * freeing of the queue, the other index will never reach zero again and thus
 * only rcu_exit will be able to free memory. But anyway, if any caller thread
 * mixes up with unpaired locks/unlocks, the same occurs!
 */
void rcu_read_unlock(void)
{
	unsigned long	spin = 0, count, target;
	unsigned int index;
	bool last_thread;

	count = LOAD(rcu_count);
	do {
		spin++;
		index = count & MASK_DEFER;
		last_thread = (STEP_COUNT == count - index);
		if (last_thread)
			target = STEP_COUNT + ((index + 1) & MASK_DEFER);
		else
			target = count - STEP_COUNT;
	} while(!XCHG(rcu_count, count, target));
	spin_add(spin - 1, "rcu");
	if (last_thread)
		rcu_free_queue(index);
}

static void call_rcu(struct rcu_item *item,
		     void (*func)(struct rcu_item *item))
{
	unsigned int spin = 0, i;

	i = LOAD(rcu_count) & MASK_DEFER; /* Index of the current head */
	DEBUG("call_rcu (%d): %p\n", i, item);

	item->func = func;
	item->next = atomic_load_explicit(&rcu_heads[i], memory_order_acquire);
	while(!atomic_compare_exchange_strong_explicit(  &rcu_heads[i],
							 &item->next, item,
							 memory_order_acq_rel,
							 memory_order_acquire))
		spin++;
	aw_count_contentions(spin);
}

/* This is done at exit in case the caller got mixed up and did
 * more locks than unlock!
 * ATTENTION:	this must NOT be called when several threads are still actively
 *		calling rcu clean functions, otherwise crashes can happen!
 *		It is typically meant to be called at program termination or
 *		at similar stages of a program.
 */
void rcu_exit(void) {
	unsigned int i;

	for (i = 0; i < STEP_COUNT; i++)
		rcu_free_queue(i);
}


static void rcu_ptr(struct rcu_item *item)
{
	DEBUG(">> rcu_free_ptr: pointer=%p\n", item->p);
	free(item->p);
}

/*
 * These two functions are used either to clean a specific structure, in case
 * the cleaning function is passed, or to clean a single pointer. For the
 * later a standard free() is used, implemented by rcu_ptr above.
 */

void rcu_free_struct(void *pstruct, void (*func)(struct rcu_item *item))
{
	struct rcu_item *item;

	if (NULL == pstruct)
		return;

	item = malloc(sizeof(struct rcu_item));
	item->p = pstruct;
	call_rcu(item, func);
}

void rcu_free_ptr(void *p)
{
	rcu_free_struct(p, rcu_ptr);
}

/*
 * These allow to hide the struct rcu_item exact layout
 */
void *rcu_get_p_struct_from_item(struct rcu_item *item)
{
	return item->p;
}

