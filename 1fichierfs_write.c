/*
 * 1fichierfs: write parts of 1fichierfs
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
 * November 2019 "writing" Architecture note.
 *
 * Writing limitations
 *	- Writing is limited to writing new files (creation) or writing on
 *	  zero byte size files.
 *	- "Append" would be possible (with time limits) but is not implemented
 *	  so far: the most desirable usecase is Firefox downloads, so that is
 *	  a /TODO. Indeed when downloading, firefox periodically flush/closes
 *	  the file and appends to continue the download.
 *	- "Truncate" (completely overwriting a file) is not implemented either,
 *	  because the user workaround is easy: delete the file first and it
 *	  becomes the first case (writing a new file).
 *	- Overwriting parts or random write is not implemented (and probably
 *	  won't be). This requires completely donwloading and re-uploading the
 *	  file. For random writes local storage would be required for efficiency
 *	  which is not 1fichierfs' direction: use rclone instead.
 *
 * Asynchronous writing strategy:
 *   A "direct" strategy could have been used: async_writer waits on the curl
 *	callback, when a write block is received, pass it the to async_writer's
 *	curl callback which copies it and responds to the fuse thread.
 *	This would minimize the number of memcpy (expensive operation) but
 *	has major drawbacks:
 *	- could do a lot of context switches (with the default 4K write block)
 *	- no leeway for rewinding the write buffer
 *
 *	Look at this article:
 *		https://eli.thegreenplace.net/2018/measuring-context-switching-and-memory-overheads-for-linux-threads/
 *
 *      	It shows that the cost of a context switch is about the same as
 *		the cost of a 25k memcpy.
 *		The standard write buffer is 4k, and thus that is very
 *		unoptimised. To make it better, the fuse option big_writes
 *		can send write buffers up to 128k. Tested on a Raspberry Pi 4
 *		the CPU difference is 100% between 4K and 128k buffers!
 *		The default value (see below) to trigger writes is 16k.
 *		For each 16k block, "direct" writing strategy costs 100k
 *		(equivalent of 4 x 25k memcpy)
 *		Buffered strategy costs 16k (buffering) + 25k (1 switch), which
 *		is 42% of the "direct" cost, and can be even less with smaller
 * 		fuse buffers.
 *
 *	encfs:  because the user's personal data is stored on a remote server
 *		and the user has no way of controlling what the remote server
 *		does with his data, it is a very good practice to use some
 *		kind of local encryption such as encfs.
 *		This way, personal files are encrypted on the fly when written
 *		and only an encrypted version is stored on the remote. When
 *		reading the reverse process is happening.
 *		With encfs, the part of the server where data is stored could be
 *		accessed in plain http. An observer will only see obfuscated
 *		filenames and encrypted data.
 *
 *		It is then desirable to make the driver work with encfs.
 *		encfs has 2 particularities:
 *		- when writing to a file, it first opens it in "w" mode to
 *		  create it, then closes it immediately. Then the file is
 *		  opened in "wr" mode and the writing happens.
 *		- a sequential write produces write blocks around 1K, plus
 *		  some very small blocks down to 8 bytes. 8 bytes blocks are
 *		  later overwritten (fseek) with bigger blocks. big_writes
 *		  does not change it because for an unknown reason encfs does
 *		  direct/IO, thus buffering here is indeed a huge performance
 *		  boost.
 *
 * 		With 1.9.6, 1fichierfs is friendlier with encfs for renames.
 *		Indeed, the key depending on the filename or the whole path
 *		needs being rewritten when the file is renamed/moved.
 *		The key is 8 bytes at the start of the file. Only that is
 *		rewritten.
 *		Because there is no way to do such a rewrite, a hack is used
 *		appending 12 characters to the filename when there was such
 *		a rewrite.
 *		encfs first does the rewrite, then does a rename.
 *		For files in the storage, the rewrite will trigger a first
 *		rename with the 12 characters of the filename appended (unless
 *		the filename will be too long, in which case an error is
 *		returned). Then the rename/move will propagate the added 12
 *		bytes on the filename. Those bytes are removed from the
 *		directory listing so that encfs continues to see the "right"
 *		names. Hence a user looking at the directory and at the raw
 *		storage on the standard 1fichier.com interface will see
 *		files with different names.
 * 		When a key rewrite happens in the upload cycle, either this
 *		is early (before 64K) and nothing happens -see leeway above-, or
 *		it is already too late then the write algorithm will have
 *		saved the first 4096 bytes of the file (a read is issued
 *		before the write of 8 bytes), the key is changed on those bytes.
 *		When the transfer ends, the FTP-filename is derived with that
 *		key the filename stays that of encfs.
 *		If there is a further rename before the file ends up in the
 *		storage, the new key is saved with the rewrite, and the rename
 *		happens on the FTP-filename only when the rename is triggered
 *		by encfs using the old and new keys. The filename stays that
 *		of encfs for the renamed file.
 *
 * The "buffered" approach is then preferred.
 * 	In all cases (except very small files <16k) if will be faster with the
 *	default 4K block due to less context switch... and much faster with
 *	encfs due to the small blocks used.
 *	The buffered approach also allows a leeway to rewind the write buffer
 *	as long as the write is in the window (64k by default with BIG_CHUNKS).
 *
 * 	The principle is that incoming writes are copied into the buffer.
 *	If there is enough space left in the buffer or enough free buffer, this
 *	can be done without any context switch, directly from the fuse thread.
 *	When he maximum buffers to keep (6) is reached and more space is needed
 *	to copy the received data: one buffer is sent to the async writer, and
 *	one buffer is taken from the free pool. At this point there can be
 *	some waiting if the async_writer did not finish writing buffers.
 *	This is done with a "pipe" algorithm on a ring buffer.
 *
 * create callback:
 *   1fichier.com storage does NOT support storing empty files.
 *   When a file is opened for writing a new entry is added to the currently
 *   being written files. If the handle is closed with 0 bytes written, the
 *   file will be kept there until the end of the session, or until it is
 *   explicitly deleted by the user.
 *   This allows programs like encfs that need "empty files" to work despite
 *   the fact that 1fichier.com does not support such files.
 *
 * Post Processing files.
 *   Writing is done through FTP.
 *   Compared to other programs like rclone that use HTTP, the biggest advantage
 *   of FTP is that the file size is not needed in advance to upload it.
 *   This means that, contrary to rclone, the user does NOT need any kind
 *   of local disk space to start uploading something. The standard 128k
 *   memory buffers for each writer is enough.
 *
 *   But then, same as uploading file from any FTP tool to 1fichier, there is
 *   a "post processing" of files.
 *
 *   This post-processing comes with different steps:
 *	-1) 5 minutes wait (same for "automatic" or "manual"). That is on
 *	    purpose, to allow "appends" in case of failed FTP uploads. Currently
 *	    this facility is not used by 1fichierfs.
 *	    I made a request for a more "API friendly" behaviour. It was
 *	    accepted in the todo list (with no commitment date so far)
 *	-2) Storage. The file is copied from the FTP server onto the storage.
 *	    The duration of this step depends mostly on the file size.
 *	-3) File checks on the storage. The file is visible on the storage
 *	    but not yet available for reading. It is assumed that on this
 *	    phase the server does things like checking the "whirlpool checksum"
 *	    to ensure that phase 2 was correct.
 *
 *   1fichierfs does all the "post processing" with the single thread
 *   writer_util. Every time it is activated (via semaphore) writer_util
 *   thread checks what is to be done to post-process the file once
 *   the writing is over.
 *
 *   Except to the case of the deletion of an empty file, writer_util is
 *   where the virtual "wfiles" (structures to manage files in the process
 *   of writing) are freed: when their processing is over and they become
 *   available as regular storage (or are deleted).
 *
 *   This post-processing brings some limitations with other features.
 *   -  Renaming/moving is not limited. Each rename/move is recorded in the
 *	wfile structure, and when the file finally appears in the storage,
 *	the last rename/move is used.
 *   -  Deleting is not limited either. Note that fuse has two ways of doing
 *	that for currently opened files. The standard way is to rename/hide
 *	the file instead of deleting it immediately, so that ongoing read/write
 *	operations can continue normally. The file is finally deleted with
 *	the last handle on that file is released. The second ways is through
 *	the option hard_remove which immediately removes the file sending
 *	-ENOENT to any subsequent read/write.
 *	When using the standard mode (rename/hide) 1fichierfs will detect and
 *	optimize it for writing. Since the uploaded file will in anycase be
 *	deleted, the writing becomes "faked" (no actual FTP writing happens
 *	anymore), and the FTP file is immediately removed. The final remove
 *	(unlink) sent by fuse does nothing (delete is already done!).
 *   -  Hard link is currently disabled when uploading a file. It is called
 *	"copy" by 1fichier.com, and it is true that "copying" a file while it
 *	is not completely uploaded makes little sense.
 *   -  READING: files being written to can also be opened "RW" or "R" but
 *	the reading is limited to the buffer leeway (64k by default).
 *	Once the file is on the "post processing" phase, it is not possible to
 *	open it for reading until the post processing is over. This is
 *	a server limitation (almost all remote storage have this limitation).
 *      Small files exception (from 1.7.2) for mechanisms such as Nautilus Trash
 *      small files (<= 4096B) that were written are kept in memory until the
 * 	upload process is over.
 *
 * October 2020 - Lazy initialisation.
 *   -  The main goal is to avoid messing with a remote storage for people that
 *      never intend to use the "write" part of 1fichierfs. Indeed, when write
 *      is never used, the directory /.upload.1fichierfs becomes unnecessary
 *      same as a sub-FTP-account.
 *   -  .upload.1fichierfs directory is still fetched at startup (foreground)
 *      after reading the root directory of the mount.
 * 	==> If the directory is NOT there: do nothing (being lazy) but still
 *          assuming write will be possible. The writer_util thread is NOT
 *          created, since no 'resume' is needed either.
 *      ==> if the directory IS present: writer_util starts, does the "resume"
 *          process, then checks the existence of the ftp account.
 *          If the ftp account exists, check dir_id, else do nothing: lazy.
 *   -  When ftp writing is needed, the parts of the initialisations that
 *      were not done are now completed before starting the actual ftp:
 *      - creation of upload target directory
 *      - start of writer_util if necessary
 *      - check of sub-ftp-account or creation if it did not exist
 *      Since the ftp accounts are not anymore deleted/created, it could
 *      happen that the password is incorrect when writing to ftp, in such
 *      case the sub-ftp-account would be deleted/created.
 *   -  Other benefits: several users on the same LAN (to have an identical
 *	ipv4 address) can use the same ftp_account. Specifying an ftp account
 *	starting with the default prefix becomes Ok. There is no strange
 *	behaviour of 'create' at startup (showing no space left before init),
 *	instead, the first file written might take longer to start because it
 *	might need to finish some global 'write initialisation'.
 *   -  New behaviour: the mount can transition from rw to ro anytime when
 *	there is an error that would block further uploads. This already
 *	happens on hard drives when errors: remounting -ro, so it should not
 *	disrupt any program badly.
 */


#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include "1fichierfs.h"

/*******************************************************
 *  These are the things you can change at compile time.
 */
// #define MAX_WRITERS 4		/* in 1fichierfs.h for stats          */
#define RING_BUF_SZ	KBUF		/* Size of ring buffer (must be 2^n)
					 */
#define RING_MASK  (RING_BUF_SZ - 1)	/* With 2^n this allows logical to
					 * physical address translation with
					 * a signle AND operation
					 */
#define PIPE_CHUNK	(32768)		/* Size of the chunks used by this
					 * "tolerant pipe" system. Must be
					 * 2^n and smaller than RING_BUF_SZ
					 */
#define W_BUF_SZ   (3 * PIPE_CHUNK)	/* Must be a multiple of PIPE_CHUNK
					 * and smaller than ring.
	 * Default write values:   buffer is 128k with 4 chunks of 32k
	 * 3 chunks at max for writing which gives a guaranteed write window
	 * of 64k. This is the "tolerance". As all pipe it can write at the
	 * end (append or stream) but also can do the equivalent of a
	 * lseek(fd, N, SEEK_END) where N is between -64k and 0.
	 * A "forward" tolerance could also be added if needed. At the moment
	 * the intended support is for sequential writes through tools like
	 * encfs, and they don't need "forward" writes. For encfs only
	 * a -1024 tolerance would be enough.
	 */


#define STRINGIFY2(x) #x
#define STRINGIFY(x) STRINGIFY2(x)
#define FTP_BASE_PREFIX_SZ (1)		/* Prefix length for FTP base: "F" = 1*/
#define FTP_BASE_RANDOM_SZ 16		/* Random (clock based) length        */
#define FTP_BASE_SZ (FTP_BASE_PREFIX_SZ + FTP_BASE_RANDOM_SZ)
					/* Base name for ftp upload is 17 chars:
					 * F + hexa representation of 64bits
					 *     compacted time with nanosec
					 *     (ie 16 chars)
					 */

#define FTP_NAME_SZ (FTP_BASE_SZ + 1 + 8 + 1)
					/* Remote name is 27 chars:
					 * ftp_base name (17 chars : above )
					 *    + '_' (1 char)
					 *    + 32 bit hex counter (ie 8 chars)
					 *    + F (files) D,G,H,I (dir) (1 char)
					 */

#define RES_TYPE(name) (name[FTP_NAME_SZ -1])

#define RES_DIR_SZ	16		/* Part of the "resume name" which
					 * is the parent dir ID in hex, hence
					 * 16 chars (parent dir ID is 64 bits)*/
#define RES_NAME_SEP_SZ (1)		/* Separator when the filename is too
					 * long, to avoid lead/trail spaces,
					 * this is '.' thus 1 char */

#define RES_NOT_INITIALIZED (LLONG_MIN)	/* Marks that resume is not yet
					 * initialized, which is when the file
					 * is in the "Writing" step */
#define RES_ERROR	(LLONG_MIN + 2)	/* Resume directory creation error.
					 */

#define FTP_TRIGGER_TIME	(305) 	/* This should not be set below 300
					 * because the 1fichier documentation
					 * says no file is processed unless
					 * its size didn't change for 5 minutes.
					 * Default 305 to be safe
					 */
#define CHK_UPLOAD_DIR_INTVL	(5)	/* This is how often the upload dir
					 * will be checked when waiting for
					 * a file to arrive on the storage
					 */

#define TIME_PROCESS_FTP	(30)	/* Estimated time the ftp to storage
					 * process takes.
					 * As fo Dec 2019, no file appears to
					 * be stored within less than 30 sec.
					 */
#define TIME_PER_GB 		(13)	/* Estimated time it takes for each
					 * GigaByte of storage. As of Dec 2019
					 * estimation show between 13 and 14 sec
					 * par GigaByte, in clear, a 101GB file
					 * will take about 1380 seconds
					 * (or 23 min) more to arrive on the
					 * storage than a 1GB file.
					 * This helps avoid un-necessary polling
					 * the upload directory.
					 * 13 sec is conservative.
					 */

#define RESUMABLE_AGE	((time_t)(CHK_UPLOAD_DIR_INTVL * 60))
					/* When an entry is "resumable" = both
					 * resume dirs + file, how "old" it must
					 * be so that it is resumed.
					 * Normally, the 1fichierfs instance
					 * that created it should have acted
					 * under CHK_UPLOAD_DIR_INTVL: give
					 * some leeway before acting upon it.
					 * Uncomplete entries and upload dir
					 * protection use resume_delay instead.
					 */
			/* These 2 constants could be passed as options       */
       const char upload_dir[]		= "/.upload." PROG_NAME;
static const char ftp_user_prefix[]    	= PROG_NAME"-" ;

#define MAX_UPLOAD_PATH_SZ (sizeof(upload_dir) + MAX_FILENAME_LENGTH)

/*******************************************************
 *  Structures and variables used for writing.
 */

#define GIGA (1024ULL * 1024ULL * 1024ULL)
#define SMALL_FILE_SIZE_MAX 4096

static char ftp_base[FTP_BASE_SZ + 1] = "F";
_Atomic uint32_t wf_counter = 0;

enum w_step {
	STEP_WRITING,
	STEP_WAIT_FTP,
	STEP_WAIT_MV,
	STEP_WAIT_RM,
	STEP_DONE,
};

struct resume {
	long long id1;
	long long id2;
};

struct wfile {
	_Atomic unsigned long	counter;
	_Atomic uint64_t	size;
	bool			no_ssl		:1,
				encfs		:1,
				has_encfs_key	:1,
				has_new_key	:1;
	_Atomic enum w_step	step;
	unsigned int		iwriter;
	char			ftp_name[FTP_NAME_SZ + 1];
	struct resume		r;
	time_t			cdate;
	time_t			eta;
	pthread_mutex_t		wmutex;
	char 			*data;	/* For "small" files or encfs */
	char			new_key[ENCFS_HEADER_SZ];
};

struct wfile_spec {
	struct wfile	*wf;
	long long	parent_id;
	char		*filename;
	char		path[];
};

struct wfile_list {
	unsigned long     n_files;
	struct wfile_spec *files[];
};


/**
 * SIGNALS
 *   since the pipe works with semaphore and no interlock, when one side wants
 *   to signal a condition to the other side, it raises a signal (atomic or).
 *
 *  EXIT is raised upon termination of the program by write_destroy.
 *	==> the async writer must exit, finishing any ongoing business first.
 *
 * Close and Error signal correspond to one end of the pipe being stopped.
 *  CLOSE is raised by write_release upon reception through fuse of the last
 *	close() on the current file handle. "Writing" part of the pipe closed.
 *	==> all received data must be flushed and the file sent to writer_util
 *	    for further waiting (FTP, storage). async_writer then becomes idle.
 * ERROR on the other hand, is raised only by the async_writer on an FTP error.
 *	"reading" part of the pipe closed.
 * 	==> async_writer does NOT become idle immediately, but has to wait for
 *	    the other side of the pipe to acknowledge the error.
 *	    unfichier_write must not continue writing and returns -EREMOTEIO
 *	    When write_release sends the close, the writer returns to idle.
 *	    The file is kept along in the list, in case the user can salvage
 *	    something looking at the FTP server directly. It is only removed
 *	    from the list if the user explicitely deletes it.
 *
 **/

#define P_SIG_EXIT	1
#define P_SIG_CLOSE	2
#define P_SIG_ERROR	4

#define IS_SIG_EXIT(sig)  (0 != (P_SIG_EXIT  & sig))
#define IS_SIG_CLOSE(sig) (0 != (P_SIG_CLOSE & sig))
#define IS_SIG_ERROR(sig) (0 != (P_SIG_ERROR & sig))


struct writer {
	char   *buf;
	_Atomic unsigned long p_signal;	/* touched by both to signal other */
	uint64_t  woff;		/* Touched only by fuse threads */
	uint64_t  wsz;		/* under lock = no need atomics */
	sem_t	  wsem;		/* Where fuse threads wait      */
	uint64_t  roff;		/* Touched only by curl callback*/
	uint64_t  rsz;		/* single thread = no atomics   */
	sem_t	  rsem;		/* Where curl callbacks wait    */
	_Atomic bool idle;
	struct wfile *wf;
	CURL *curl;
	pthread_t thread;
};
static struct writer writers[MAX_WRITERS];

static sem_t		sem_writer;

_Atomic (struct wfile_list *) wflist = NULL;

static pthread_mutex_t	init_write_mutex = PTHREAD_MUTEX_INITIALIZER;
static 	      	bool	writer_util_started = false;
static pthread_t	writer_util_thread, main_thread;
static sem_t		writer_util_sem;
static 		bool	lazy_init = true;
static _Atomic	bool	can_write = true;
static _Atomic	bool	ftp_pwd_fixed = false;
static _Atomic  bool	write_started = false;

#define FTP_USER_RANDOM_NAME_SZ (lengthof(ftp_user_prefix) + FTP_BASE_RANDOM_SZ)

static _Atomic (char *) ftp_user_name = NULL;
static		char *	ftp_valid_user= NULL;

/* CAN_WRITE and WRITE_INIT_DONE are always used at the start of some action.
 * It means there has been a thread switch or the like an relaxed is enough. */
#define WRITE_INIT_DONE	(NULL != LOAD(ftp_user_name, relaxed))
#define CAN_WRITE (LOAD(can_write, relaxed))

/************************************************************************/

static const char ftp_server[]= "ftp://ftp.1fichier.com/";
static const char ftp_dele_command[] = "DELE ";

CURL *curl_init_ftp()
{
	CURL *curl;

	curl = curl_init(false);
	CURL_EASY_SETOPT(curl, CURLOPT_USERNAME, LOAD(ftp_user_name), "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_PASSWORD, params1f.api_key, "%s");

	return curl;
}

static CURLcode ftp_dele_file(struct wfile *wf)
{
	CURL *curl;
	struct curl_slist *cmdlist = NULL;
	char cmd[sizeof(ftp_dele_command) + FTP_NAME_SZ];
	CURLcode res;

	curl = curl_init_ftp();
	CURL_EASY_SETOPT(curl, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS, "%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_SSL_ENABLE_ALPN, 0L, "%ld");
	/* NPN only valid between libcurl 7.36 and 7.86*/
	#if  LIBCURL_VERSION_MAJOR == 7 && \
	    (LIBCURL_VERSION_MINOR >= 36 && LIBCURL_VERSION_MINOR < 86)
		CURL_EASY_SETOPT(curl, CURLOPT_SSL_ENABLE_NPN , 0L, "%ld");
	#endif
	CURL_EASY_SETOPT(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL, "%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_NOBODY, 1L, "%ld");

	memcpy(cmd, ftp_dele_command, lengthof(ftp_dele_command));
	strcpy(cmd + lengthof(ftp_dele_command), wf->ftp_name);
	cmdlist = curl_slist_append(cmdlist, cmd);
	if (NULL == cmdlist)
		lprintf(LOG_CRIT,
			"building ftp command (curl_slist_append).\n");

	CURL_EASY_SETOPT(curl, CURLOPT_PREQUOTE, cmdlist, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_URL, ftp_server, "%p");

	res = curl_easy_perform(curl);

	if (0 == res)
		lprintf(LOG_NOTICE,
			"successfuly deleted file %s before storage.\n",
			wf->ftp_name);
	else
		lprintf(LOG_WARNING,
			"there was an error %d deleting file %s on the FTP server.\n",
			(int)res, wf->ftp_name);

	curl_slist_free_all(cmdlist);
	curl_easy_cleanup(curl);

	return res;
}

/*
 * @brief delete an FTP user
 *
 * @param name of the user to delete
 * @return none (can_write set to false if error tested)
 */
static void del_ftp_user(char *name, bool test_err)
{
	static const char ftp_rm_post[]= "{\"user\":\"%s\"}";
	char post[sizeof(ftp_rm_post) + strlen(name)];
	long http_code;
	struct json j;

	json_init(&j);
	sprintf(post, ftp_rm_post, name);
	http_code = curl_json_perform(post, &j, FTP_USER_RM, name);
	json_free(&j);
	if (HTTP_OK == http_code)
		DEBUG( "del_ftp_user: successfully deleted FTP user: %s\n",
			name);
	else
		if (test_err) {
			lprintf(LOG_WARNING,
				"write diabled! could not delete FTP user: %s, http_code=%ld\n",
				name, http_code);
			STORE(can_write, false, relaxed);
		}
}

/*
 * @brief create the FTP user
 *
 * The user is created with the name in ftp_user_name and pwd = API_Key
 *
 * @param none
 * @return none
 */
static void create_ftp_user(char *user)
{
static const char ftp_add_post[]= "{\"user\":\"%s\",\"pass\":\"%s\",\"folder_id\":%"PRId64"}";
	struct json j;
	long http_code;

	char post[sizeof(ftp_add_post)  + strlen(user)
					+ strlen(params1f.api_key)
					+ STRLEN_MAX_INT64];
	json_init(&j);

	sprintf(post, ftp_add_post
		    , user
		    , params1f.api_key
		    , (int64_t)get_upload_id());
	http_code = curl_json_perform(post, &j,
				      FTP_USER_ADD,
				      user);
	json_free(&j);
	if (HTTP_OK == http_code) {
		DEBUG( "create_ftp_user: successfully created FTP user: %s\n",
			user);
		STORE(ftp_user_name, user, relaxed);

	}
	else {
		lprintf(LOG_WARNING,
			"writing impossible: cannot create FTP user `%s`.\n",
			user);
		STORE(can_write, false, relaxed);
	}
}

/*
 * @brief fix FTP password in case of login denied
 *
 * @param curl_easy_perform return code
 * @param address of the boolean whether pwd was fixed prior to curl
 * @return true if perform must be done again (fixed pwd) false otherwise
 */
static bool fixed_ftp_pwd(CURLcode res, bool *pwd_fixed)
{
	bool cont = false;

	/* No error or another error, nothing to fix! */
	if (CURLE_LOGIN_DENIED != res)
		return false;

	if (*pwd_fixed) {
		STORE(can_write, false);
		lprintf(LOG_ERR,
			"ftp login denied and password was already changed. Blocking writes to avoid account temporary ban.\n");
	}
	else {
		/* There was a login error on FTP and is has not
		 * already been "fixed" when this curl-FTP started. */
		lock(&init_write_mutex, NULL);
		if (!LOAD(ftp_pwd_fixed, relaxed)) {
			/* And no other thread fixed it in the mean time */
			lprintf(LOG_WARNING,
				"ftp login denied: attempting to delete/create the ftp account.\n");
			/* del without testing error, because if the account
			 * was simply deleted that triggers a login denied */
			del_ftp_user(LOAD(ftp_user_name, relaxed), false);
			create_ftp_user(LOAD(ftp_user_name, relaxed));
		}
		if (CAN_WRITE) {
			/* All went fine in del/create or another thread fixed
			 * it: loop on perform */
			*pwd_fixed = true;
			STORE(ftp_pwd_fixed, true, relaxed);
			lprintf(LOG_NOTICE,
				"ftp account fixed successfully, trying curl again.\n");
			cont = true;
		}
		unlock(&init_write_mutex, NULL);
	}
	return cont;
}

static size_t pipe_read(char *dest, size_t dsz, size_t ssz, struct writer *wr)
{
	size_t   chunk2, chunk1 = 0;
	uint64_t rsz = wr->rsz & RING_MASK;

	if (ssz <= dsz) {
		chunk2 = ssz;
		wr->roff = wr->rsz + chunk2;
	}
	else {
		chunk2 = dsz;
	}
	wr->rsz += chunk2;
	if (rsz + chunk2 > RING_BUF_SZ) {
		chunk1 = RING_BUF_SZ - rsz;
		memcpy(dest, wr->buf +  rsz, chunk1);
		dest   += chunk1;
		chunk2 -= chunk1;
		rsz = 0;
	}
	memcpy(dest, wr->buf + rsz, chunk2);
	return chunk1 + chunk2;
}

/*
 * @brief curl callback function to read bytes and send them via ftp
 *
 * @param where to store the read data
 * @param size of data units in bytes (1)
 * @param number of data units
 * @param pointer on the relevant wfile structure
 * @return positive: data effectively read. zero: eof, negative: error
 */
static size_t read_data(char *ptr,
			 size_t size,
			 size_t nmemb,
			 struct wfile *wf)
{
	size_t sz = size * nmemb;
	struct writer *wr = &writers[wf->iwriter];

	DEBUG( "read_data[%u]: rsz=%"PRIu64" roff=%"PRIu64" wsz=%"PRIu64"\n",
		(unsigned int)(wr - writers), wr->rsz, wr->roff, wr->wsz);

	if (0 != (FL_DEL & LOAD(wf->counter))) {
					/* Delete = close without uploading   */
					/* more data: file will be deleted!   */
		DEBUG ( "read_data[%d]: received a DEL signal.\n",
			(unsigned int)(wr - writers));
		return 0;
	}

	/* Case close: move all what remains in the pipe buffer to curl */
	if (IS_SIG_CLOSE(LOAD(wr->p_signal))) {
		/* When the last piece of data is out, semaphores must
		 * be reset. It is possible that write sent several blocks
		 * then release happened, and this thread was stuck and now
		 * close pushes all data and is late on waiting on semaphores */
		if (wr->rsz == wr->wsz)
			return 0;
		/* Lock is necessary here. The file will be in STEP_FTP once
		 * curl exits it's perform, and write is still possible
		 * -and desirable for small files/encfs!- in the leeway buffer.
		 * If not locked, data could be corrupted by parallel writes. */
		lock(&wf->wmutex, NULL);
		sz = pipe_read(ptr, sz, wr->wsz - wr->rsz, wr);
		unlock(&wf->wmutex, NULL);
		DEBUG( "read_data[%d] closing: rsz=%"PRIu64" roff=%"PRIu64" wsz=%"PRIu64" sz=%u size*nmemb=%u\n",
			(unsigned int)(wr - writers),
			wr->rsz, wr->roff, wr->wsz,
			(unsigned int)sz, (unsigned int)(size * nmemb));
		return sz;
	}


//TODO: don't wait forever... FTP server closes sockets after 5 min!
	if (wr->rsz == wr->roff)	/* Otherwise unfinished, no need sem */
		wait(&wr->rsem);

	/* Normal case, move a single buffer to curl */
	sz = pipe_read(ptr, sz, PIPE_CHUNK - (wr->rsz % PIPE_CHUNK), wr);

	if (wr->rsz == wr->roff)	/* sem_post only when all data is out */
		post(&wr->wsem);
	return sz;
}

/*
 * @brief asynchronous writer
 *
 * This is the asynchronous writer that will start the reading part of the
 * pipe.
 *
 * It will normally receive a semaphore when the pipe is full to some extent
 * from writers. At this point the FTP upload will be started and the above
 * function (read_data) is the curl callback responsible for reading data and
 * moving it out of the pipe, so that writer have space to continue writing.
 *
 * This function might also receive and generate signals.
 * DEL and CLOSE signals can be received only for small files that do not
 * exceed the portion of the ring buffer allocated for writing (by default 96k).
 *
 * If such signals are received here, it means the FTP upload has not started
 * yet. For close, it just starts the FTP and the callback will flush all the
 * data in one go. For DEL there is nothing to do since nothing has yet been
 * written on the remote storage.
 *
 * It can also receive the exit signal, which simply means break the loop!
 *
 * @param opaque pointer from pthread_create (here points to the writer)
 * @return pointer to be returned for pthread_join
 */

void *
async_writer(void *arg) {
	struct writer *w = arg;
	struct timespec now;
	CURLcode res;
	bool last_ssl = true, first_time = true;
	unsigned long p_signal;
	unsigned int i;
	bool pwd_fixed;

	for(p_signal = 0; !IS_SIG_EXIT(p_signal); p_signal= LOAD(w->p_signal)) {
		if (!IS_SIG_ERROR(LOAD(w->p_signal))) {
			/* Writer becomes idle (unless error) reset it        */
			if (unlikely(first_time)) {
				first_time = false;
			}
			else {
				while ( 0 == trywait(&w->rsem));
				while ( 0 == trywait(&w->wsem));
				STORE(w->p_signal, 0);
				STORE(w->idle, true);
				post(&sem_writer); /* Must be AFTER idle! */
			}
			for (i = 0;
			     i < (RING_BUF_SZ - W_BUF_SZ) / PIPE_CHUNK;
			     i++)
				post(&w->wsem);
			DEBUG(	"async_writer[%u] loop: waiting.\n",
				(unsigned int)(w - writers));
		}
		if (!IS_SIG_CLOSE(LOAD(w->p_signal))) /* When error and close */
			wait(&w->rsem);	  /* would wait forever without test  */

		p_signal = LOAD(w->p_signal);	/* catch signal after wait    */

		/* When error, if no close is received writers didn't yet see
		 * the error: continue. Otherwise loop to reset unless there
		 * is also an exit!  */
		if (IS_SIG_ERROR(p_signal)) {
			if (IS_SIG_CLOSE(p_signal)) {
				STORE(w->wf->step, STEP_DONE);
				STORE(w->wf->size, w->roff);
				w->wf->iwriter =  MAX_WRITERS;
				if (IS_SIG_EXIT(p_signal))
					break;
				STORE(w->p_signal, 0);
			}
			continue;
		}

		/* Close can be seen here only when FTP upload didn't yet start
		 * If the file is to be deleted, there is nothing to do.      */
		if (IS_SIG_CLOSE(p_signal)) {
			if (0 != (FL_DEL & LOAD(w->wf->counter))) {
				STORE(w->wf->step, STEP_DONE);
				if (IS_SIG_EXIT(p_signal))
					break;
				else
					continue;
			}
		}
		else {
			if (IS_SIG_EXIT(p_signal))
				break;
		}

		/* From that point there is data to write, whether if it is
		 * a small file that fits in the ring buffer or not,
		 * the upload must be started
		 *
		 * EXIT Signal could be up, but there is something to upload
		 * first.
		 *
		 * FTP control connection is kept alive, but does not know how
		 * to switch between TLS and no TLS or reverse. So in this case
		 * end the connection and start a new one.
		 */
		if (NULL != w->curl && last_ssl != w->wf->no_ssl) {
			curl_easy_cleanup(w->curl);
			w->curl = NULL;
		}
		if (NULL == w->curl)
			w->curl = curl_init_ftp();

		/* The signal is either 0 or CLOSE for short file that fit
		 * in the ring buffer's write zone. */
		{
			char buf[sizeof(ftp_server) + FTP_NAME_SZ];
			memcpy(buf, ftp_server, lengthof(ftp_server));
			strcpy(buf + lengthof(ftp_server), w->wf->ftp_name);
			CURL_EASY_SETOPT(w->curl, CURLOPT_URL, buf, "%s");
		}
		CURL_EASY_SETOPT(w->curl, CURLOPT_FTPSSLAUTH
				        , CURLFTPAUTH_TLS, "%ld");
		CURL_EASY_SETOPT(w->curl, CURLOPT_SSL_ENABLE_ALPN, 0L, "%ld");
		/* NPN only valid between libcurl 7.36 and 7.86*/
		#if  LIBCURL_VERSION_MAJOR == 7 && \
		    (LIBCURL_VERSION_MINOR >= 36 && LIBCURL_VERSION_MINOR < 86)
			CURL_EASY_SETOPT(w->curl, CURLOPT_SSL_ENABLE_NPN
						, 0L, "%ld");
		#endif
		if (w->wf->no_ssl) {
		#if  LIBCURL_VERSION_MAJOR > 7 || \
		    (LIBCURL_VERSION_MAJOR == 7 && LIBCURL_VERSION_MINOR >= 52)
			CURL_EASY_SETOPT(w->curl, CURLOPT_SSLVERSION
						, CURL_SSLVERSION_MAX_TLSv1_3, "%ld");
		#endif
			CURL_EASY_SETOPT(w->curl, CURLOPT_USE_SSL
					        , CURLUSESSL_CONTROL, "%ld");
		}
		else {
			/****************************************************
			 * /!\ Workaround curl BUG! I reported it here:
			 * https://github.com/curl/curl/issues/6149
			 * A ftp ssl upload seems to be closing the socket (RST)
			 * too fast with data left to be written that is then
			 * lost at end of file. This has been narrowed down to
			 * happen only when using TLSv1.3.
			 * So, at the moment, when using TLS, the workaround is
			 * to limit to TLSv1.2
			 * Of course no issue in the "else" case where the
			 * data channel is "clear"
			 * For curl before 7.52 there was no TLSv1_3 (eg 16.04)
			 * ***************************************************/
		#if  LIBCURL_VERSION_MAJOR > 7 || \
		    (LIBCURL_VERSION_MAJOR == 7 && LIBCURL_VERSION_MINOR >= 52)
			CURL_EASY_SETOPT(w->curl, CURLOPT_SSLVERSION
						, CURL_SSLVERSION_MAX_TLSv1_2, "%ld");
		#endif
			CURL_EASY_SETOPT(w->curl, CURLOPT_USE_SSL
						, CURLUSESSL_ALL, "%ld");
		}
		last_ssl = w->wf->no_ssl;
		CURL_EASY_SETOPT(w->curl, CURLOPT_UPLOAD, 1L, "%ld");
		CURL_EASY_SETOPT(w->curl, CURLOPT_READFUNCTION, read_data,"%p");
		CURL_EASY_SETOPT(w->curl, CURLOPT_READDATA, w->wf, "%p");

		DEBUG(	"curling[%u:%s] ftp%s upload\n",
			(unsigned int)(w - writers),
			((LOAD(w->idle)) ? "idle" : "busy"),
			((w->wf->no_ssl) ? "" : "s"));
		post(&w->rsem);
		pwd_fixed = LOAD(ftp_pwd_fixed);
		do {
			/* Note that if the error is login denied, the
			 * read callback is not called, so the semaphore
			 * is not consumed, and perform can be tried again once
			 * account is fixed. */
			res = curl_easy_perform(w->curl);
		} while(fixed_ftp_pwd(res, &pwd_fixed));
		lprintf(((0 == res) ? LOG_NOTICE : LOG_ERR),
			"Transfer[%u] of size=%"PRIu64" ended with %d\n",
			(unsigned int)(w - writers),
			(uint64_t)LOAD(w->wf->size),
			(int)res);

		/* When there was an error, signal it and must loop to
		 * wait for the writers to send a CLOSE
		 */
		if (CURLE_OK != res) {
			OR(w->p_signal, P_SIG_ERROR);
			update_write_err(w - writers);
			post(&w->wsem);	/* In case unfichier_write waiting */
			continue;
		}

		/* Now sending to writer_util - same for deleted files */
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		w->wf->eta = now.tv_sec + FTP_TRIGGER_TIME;
		STORE(w->wf->step, STEP_WAIT_FTP);
		w->wf->iwriter = MAX_WRITERS;
		post(&writer_util_sem);
		DEBUG(	"async_writer[%u]: sent file to wait.\n",
		        (unsigned int)(w - writers));
	}

	curl_easy_cleanup(w->curl);
	return NULL;
}


/*
 * @brief actual release code for files being written, wrapped inside the lock.
 *
 * @param pointer to the wfile structure of the file to be released
 * @return none
 */

static void write_release_wrapped(struct wfile *wf)
{
	struct writer *wr;
	unsigned long counter;

	counter = SUB(wf->counter, OPEN_INC);

	DEBUG("write_release[%u]: %p (%lu).\n",
	      (unsigned int)(wf->iwriter), wf, counter - OPEN_INC);

	if (0 != ((counter - OPEN_INC) & OPEN_MASK))	/* File still opened  */
		return;


	if (0 != (counter & FL_DEL))	/* DEL while writing: don't send close*/
		return;			/* writer already detaching from wfile*/

	if (MAX_WRITERS == wf->iwriter)
		return;

	if (NULL == wf->data &&
	    ( wf->size <= SMALL_FILE_SIZE_MAX ||
	     (wf->encfs && wf->size < PIPE_CHUNK) )) {
		DEBUG( "write_release_wrapped[%u] temporarily storing file data %d bytes.\n",
			(unsigned int)(wf->iwriter), (int)wf->size);
		wf->data = malloc(wf->size);
		memcpy(wf->data, writers[wf->iwriter].buf, wf->size);
	}
	wr = &writers[wf->iwriter];
	OR(wr->p_signal, P_SIG_CLOSE);
	post(&wr->rsem);
}

/*
 * @brief fuse callback for release - write portion.
 *
 * Unless there are still writers, or the file is empty (not supported by
 * the remote) the release always go through the async_writer.
 *
 * Release is always successful because more work is done in background:
 * flushing buffers, waiting ftp trigger, moving file in place... all those
 * steps can have their own error and there would be no point waiting here.
 * Furthermore, when a fuse request is blocked (like a release) that can block
 * further fuse calls like read_dir or get_attr and make the mount unusable.
 *
 * @param pointer to the wfile structure of the file to be released
 * @return always 0 (success)
 */

int write_release(struct wfile *wf)
{
	lock(&wf->wmutex, NULL);
	write_release_wrapped(wf);
	unlock(&wf->wmutex, NULL);
	return 0;
}


/*
 * @brief whether a given wfile is to be kept or not.
 *
 * Only "create" will remove files from the list. It calls this function to
 * know whether a wfile is to be kept (or on the other hand discarded from
 * the list).
 *
 * @param pointer to the wfile structure to analyse
 * @return true to keep it / false to discard it.
 */
static bool keep_wf(struct wfile *wf)
{
	enum w_step step;
	unsigned long counter;

	counter = LOAD(wf->counter);
	step = LOAD(wf->step);
	if (STEP_DONE == step) {
		if (0 != (OPEN_MASK & counter)) /* Happens for rcu_del files */
			return true;
		if (0 == (FL_ERROR  & counter) || 0 != (FL_DEL    & counter))
			return false;
	}
	else {		/* For properly removing zero size files at rest */
		if (STEP_WRITING == step && 0 == LOAD(wf->size)
					 && FL_DEL == counter )
		    return false;
	}
	return true;
}

/*
 * @brief whether a given wfile is to be shown on the directory.
 *
 * Contrary to keep_wf, files that where "rcu_deleted" and are still opened
 * (although the file has been removed from FTP) are NOT to be shown.
 * The memory must NOT be freed for as long as the file is opened, thus the
 * different test in keep_wf.
 *
 * @param pointer to the wfile structure to analyse
 * @return true to keep it / false to discard it.
 */
 static bool show_wf(struct wfile *wf)
{
	unsigned long counter;

	counter = LOAD(wf->counter);

	if ( (STEP_DONE == LOAD(wf->step) && 0 == (FL_ERROR & counter)) ||
	      0 != (FL_DEL & counter))
		return false;
	else
		return true;
}

static struct wfile_spec *id_fname_find(struct wfile_list *wfl,
					long long id, const char *filename)
{
	unsigned int i;
	int cmp;

	for(i = 0; i < wfl->n_files && wfl->files[i]->parent_id < id; i++);
	for(; i < wfl->n_files && wfl->files[i]->parent_id == id; i++) {
		if (!show_wf(wfl->files[i]->wf)) /* Happens if a file is    */
			continue;		 /* deleted after STEP_DONE */
		cmp = strcmp(wfl->files[i]->filename, filename);
		if (0 == cmp) {
			if (0 != (LOAD(wfl->files[i]->wf->counter) & FL_DEL)
				&&
			    !is_fuse_hidden(filename))
				break;
			return wfl->files[i];
		}
		if (cmp > 0)
			break;
	}
	return NULL;
}

void write_find(const char *filename, struct walk *w)
{
	struct wfile_list *clist;
	struct wfile_spec *wps;

	clist = LOAD(wflist);
	if (NULL == clist)
		return;

	wps = id_fname_find(clist, w->parent->id, filename);
	if (NULL != wps) {
		DEBUG ("write_find(%s,..) %d %ld\n",
			filename, LOAD(wps->wf->step), LOAD(wps->wf->counter));
		w->is_wf  = true;
		w->res.wf = wps->wf;
	}
}

/*
 * @brief "Getter" for wfile_list n_files for statistics.
 *
 * To be useful, it returns only the wfiles that would be kept on subsequent
 * create.
 *
 * @param pointer to the wfile_list structure
 * @return number of useful wfiles
 */
unsigned long get_wfl_n_files(struct wfile_list *wfl, bool **keep)
{
	unsigned long i, j;

	if (NULL == wfl || LOAD(exiting))
		return 0;

	*keep = malloc(sizeof(bool) * wfl->n_files);

	for (i = j = 0; i < wfl->n_files; i++) {
		(*keep)[i] = keep_wf(wfl->files[i]->wf);
		if ((*keep)[i])
			j++;
	}

	if (0 == j)
		free(*keep);
	return j;
}

void get_wf_info(struct wfile_list *wfl, bool *keep, unsigned long ii,
		 uint64_t *size, time_t *eta, unsigned long *counter,
		 unsigned long *step, const char **path)
{
	unsigned long i, j;

	for (i = j = 0; i < wfl->n_files; i++) {
		if (keep[i]) {
			if (j == ii)
				break;
			j++;
		}
	}
	*size	= LOAD(wfl->files[i]->wf->size);
	*counter= LOAD(wfl->files[i]->wf->counter);
	*step	= (unsigned long)LOAD(wfl->files[i]->wf->step);
	*eta	= wfl->files[i]->wf->eta;
	*path	= wfl->files[i]->path;
}

bool get_writer_ref(struct wfile_list *wfl, bool *keep,
		    unsigned int i, char *ref, size_t len)
{
	unsigned long j, k;
	if (NULL == writers[i].buf)
		return false;

	if (LOAD(writers[i].idle) || NULL == wfl)
		return true;

	for (j = k = 0; j < wfl->n_files; j++) {
		if (keep[j]) {
			k++;
			if (wfl->files[j]->wf == writers[i].wf)
				break;
		}
	}
	if (j == wfl->n_files)
		snprintf(ref, len, "|???\?|");
	else
		snprintf(ref, len, "|%4lu|", k);

	return true;
}

int write_readdir(long long id, const char *path, void *buf,
		  fuse_fill_dir_t filler)
{
	struct wfile_list *clist;
	unsigned long i;

	clist = LOAD(wflist);
	DEBUG("write_readdir %s %p %lu\n", path
					 , clist
					 , (NULL==clist) ? 0L : clist->n_files);
	if (NULL == clist)
		return 0;
	for(i = 0; i < clist->n_files && clist->files[i]->parent_id < id; i++);
	for(; i < clist->n_files && clist->files[i]->parent_id == id; i++)
		if (show_wf(clist->files[i]->wf) &&
		    0 != filler(buf, clist->files[i]->filename, NULL, 0))
			return -EBADF;
	return	0;
}

void write_getattr(struct stat *stbuf, struct wfile *wf)
{
	stbuf->st_size		= LOAD(wf->size);
	stbuf->st_atim.tv_sec	=
	stbuf->st_ctim.tv_sec	=
	stbuf->st_mtim.tv_sec	= wf->cdate;
}

/* Unlinking a written file is tricky to mimic the RCU behaviour of Linux,
 * especially with fuse not trusting that the driver knows how to do that!
 * When the file is still opened, mark it as DEL. The write process will
 * 	continue normally but writes nothing from that point. The async_writer
 *	is instructed to delete what could have been already written.
 */
static int write_unlink_wrapped(struct wfile *wf)
{
	unsigned long counter;
	enum w_step step;

	step    = LOAD(wf->step);
	counter = LOAD(wf->counter);
	if (STEP_DONE == step && 0 == (counter & FL_ERROR))	/* Too late! */
		return -EAGAIN;

	if (0 != (counter & FL_DEL))	/* Already del... should not happen!  */
		return 0;

	OR(wf->counter, FL_DEL);
	if (STEP_WRITING != step && 0 != LOAD(wf->size))
		post(&writer_util_sem);	/* Trigger writer_util for action     */

	return 0;
}

int write_unlink(struct walk *w)
{
	int res;

	lock(&w->res.wf->wmutex, NULL);
	res = write_unlink_wrapped(w->res.wf);
	unlock(&w->res.wf->wmutex, NULL);

	return res;
}

/* freing wfile_spec needs to be made in the right order. Since rcu is called
 * with a stack, this function will ensure the order is right
 */
static void rcu_free_wfile_spec(struct wfile_spec *wfs)
{
	DEBUG(">> rcu_free_wps: %p->%p\n", wfs, wfs->wf);
	pthread_mutex_destroy(&wfs->wf->wmutex);
	if (NULL != wfs->wf->data)
		free(wfs->wf->data);
	free(wfs->wf);
	free(wfs);
}

static void rcu_free_wps(struct rcu_item *item)
{
	rcu_free_wfile_spec(rcu_get_p_struct_from_item(item));
}


static void free_wfiles(struct wfile_list *wfl, bool *keep)
{
	unsigned int i;

	for(i = 0;  i < wfl->n_files; i++) {
		if (!keep[i])
			rcu_free_struct(wfl->files[i], rcu_free_wps);
	}
}

/*
 * @brief resume directory creation
 *
 * Principle: once the file is closed, hence it's complete for storing this
 * 	starts the possible resume process.
 *	A directory is created with several parts in its name:
 * 	- The same FTPname as the file, but ending with G (instead of F)
 *      - 16 characters as the target directory id in hexa
 *      - The rest is the filename "translated" with ' " special-escaped
 * 	Should the name be too long to fit in the MAX_FILENAME_LENGTH, two
 *	directories are used, marked H and I instead of G. A '.' is added to
 *	the first part of the name to avoid trailing white space.
 *	The second directory does not have the target ID, but only the end
 *      of the name after the FTPname.
 *
 * This is done so because using directories is the only way to create, update
 * and delete "strings" on the server with an API.
 *
 * Should at some point the file be deleted, if the FTP delete does not succeed
 * the name of the directories is marked with D (instead of G).
 *
 * When the file is renamed during the upload process, directories are
 * renamed accordingly.
 *
 * When file are seen in the upload directory, they are either moved or
 * deleted, and these "resume directories" are also removed.
 *
 * Should the program be terminated while some files were in the process of
 * being stored, when the programm is started again, it can check the existence
 * of files in the FTP storage, existing "resume directories", files in the
 * upload directory (if FTP is auto) and act accordingly (move or delete).
 *
 * The only failure case is when the user removes the target directory before
 * the uploaded file can be stored there. This error can already happen when
 * the program is running, and there is nothing that can be done for the user
 * wrecking havoc this way!
 *
 * NOTE: this "resume" marking is not started when writing. The mount cannot be
 * 	stopped "normally" (ie fusermount -u) as long as there are opened files.
 *      So if it is stopped at that moment, it is either killed, crashed or the
 *	machine was powered down. In each case, the uploaded file would be
 *	incomplete and not worth saving it as a valid file.
 */

#define RES_UTF8_MAX	  (4)  /* UTF-8 max lenght */
#define RES_COUNT_START (0x20) /* Count from character space */
#define RES_COUNT_END   (0x7F) /* Count up to that character */

#define min(a,b) \
  ({ __auto_type _a = (a); \
      __auto_type _b = (b); \
    _a < _b ? _a : _b; })


static char *resume_init_path(char *path, const char *ftpname)
{
	sprintf(path, "%s/%.*s", upload_dir, (FTP_NAME_SZ - 1), ftpname);
	return path + sizeof(upload_dir) +   (FTP_NAME_SZ - 1);
}


static unsigned char res_smallest(unsigned char *c_table, char no1, char no2)
{
	unsigned int i, k = 255;
	unsigned char j;
	char buf[RES_UTF8_MAX];

	/* Do not consider white space, start at 1 */
	for (j = i = RES_COUNT_START + 1; i <= RES_COUNT_END && k > 0; i++) {
		if (c_table[i - RES_COUNT_START] < k &&
		    i != '/' && i != no1 && i != no2 &&
		    1 == translate(i, buf, TR_DIR)) {
			j = i;
			k = c_table[i - RES_COUNT_START];
		}
	}
	return j;
}

/*
 * @brief returns a "resume_name" from the filename.
 *
 * Because it is going to be used as directory name, and ' and " are
 * allowed in files but not in directory, those 2 characters must be replaced
 * too. Replacing them as for dirname would limit the available filename
 * length a lot, so another algorithm is used.
 * 3 characters among the least used UTF-8 0x21/0x7F (excluding forbidden) are
 * taken as replacement. 1st replaces ", second replaces ', and the 3rd one is
 * the 'escape' in case either of the 1st, 2nd or the 'escape' character itself
 * is used in the string.
 * Since max filelenght is 250 and there are 87 characters allowed, it is
 * mathematically proven that at worst 3 characters are repeated twice.
 * This means the longest resulting name is 259 characters:
 * - 3 chars (the replacement characters)
 * - 6 char max (escaping of those 3 x 2 characters)
 * - 250 the string itself.
 *
 * The name is prefixed by the FTP name (27 char) plus the target dir
 * in hexa (16) plus the 3 encoding char (3 char), total of 46 char.
 * This leaves only 204 characters for the name itself, which is why there
 * can be an "overflow" if the name is longer than 204.
 * The overflow will only have the FTP name as a prefix (last letter changed).
 * The "cut" cannot be made without precaution:
 *   - cannot cut inside an UTF-8 sequence
 *   - cannot cut inside an escape
 *   - must count the translation that will be made by "rename" to avoid
 *     the issue of a directory name too long to be created
 *   - keep some margin because when the first part of the name cannot end with
 *     'spaces'. To avoid that, a "." is added after the first part of the name
 *     when there is an overflow. This shortens the available length to 203.
 *
 * @param where to store the resulting name.
 * @param where to store the overflow name if any.
 * @param pointer to the wfile that needs the writer.
 * @return none
 */
		/* This threshold allows to apply the limits above to decide
		 * when to "cut" the name and move to overflow */
#define RES_CUT_THRESHOLD (MAX_UPLOAD_PATH_SZ - RES_UTF8_MAX)

static void resume_name(char *name, char *name_overflow, struct wfile_spec *wfs,
			bool old)
{

	char *q;
	const char *p;
	char r1, r2, esc;
	size_t sz, l;
	unsigned char c_table[RES_COUNT_END - RES_COUNT_START + 1];
	char buf[RES_UTF8_MAX];
	bool in2;
	char *filename, encfsname[MAX_FILENAME_LENGTH + 1];

	if (old && wfs->wf->has_encfs_key) {
		filename = encfs_appendb64_key(encfsname, wfs->filename,
					       wfs->wf->data);
	}
	else {
		if (!old && wfs->wf->has_new_key) {
			filename = encfs_appendb64_key(encfsname, wfs->filename,
						       wfs->wf->new_key);
			encfs_copy_key(wfs->wf->data, wfs->wf->new_key);
			wfs->wf->has_new_key   = false;
			wfs->wf->has_encfs_key = true;
		}
		else {
			filename = wfs->filename;
		}
	}

	/* Then, count UTF-8 1byte characters in the string */
		/* Init the table */
	memset(c_table, 0, sizeof(c_table));

		/* Count */
	for (p = filename; '\0' != *p; p++)
		if (0 == (0x80 & *p) && *p > RES_COUNT_START)
			c_table[(unsigned int)*p - RES_COUNT_START] += 1;

		/* Get the 3 fewest used characters */
	r1  = res_smallest(c_table, RES_COUNT_START, RES_COUNT_START);
	r2  = res_smallest(c_table, r1		   , RES_COUNT_START);
	esc = res_smallest(c_table, r1		   , r2		    );

		/* Initialize the name with that */
	q = resume_init_path(name, wfs->wf->ftp_name);
	q += sprintf(q, "G%0" STRINGIFY(RES_DIR_SZ) PRIX64 "%c%c%c"
		      , (uint64_t)(wfs->parent_id), r1, r2, esc);

	sz = q - name; /* All char so far in the string are non-special */

		/* Then escape the name with potential name overflow */
	in2   = false;
	name_overflow[0] = '\0';
	for (p = filename; '\0' != *p; q++) {
		l = 1;
		if (*p == '\x22') {
			*q = r1;
		}
		else {
			if (*p == '\x27') {
				*q = r2;
			}
			else {
				if (*p == r1 || *p == r2 || *p ==esc) {
					*q++ = esc;
					l = 2;
				}
				else {
					l = translate(*p, buf, TR_DIR);
				}
				*q = *p;
			}
		}
		sz += l;
		p++;
		if (!in2) {
			/* The next character must not be a "continuation
			 * character" to avoid cutting an UTF-8 sequence, nor
			 * zero because there is nothing left to overflow. */
			if (sz >= RES_CUT_THRESHOLD &&
			    0x80 != (0xC0 & *p)     && '\0' != *p) {
				q[1] = '.';
				q[2] = '\0';
				name[sizeof(upload_dir) + FTP_NAME_SZ -1] = 'H';
				q = resume_init_path(name_overflow,
						     wfs->wf->ftp_name);
				*q = 'I';
				in2 = true;
			}
		}
	}
	*q = '\0';
}


/*
 * @brief creates metadata directories
 *
 * Called once the file has been succesfully uploaded and "released".
 *
 * @param pointer to the wfs for which to create metadata.
 * @return none
 */

static void metadata_create(struct wfile_spec *wfs)
{
	char name	  [MAX_UPLOAD_PATH_SZ + 1];
	char name_overflow[MAX_UPLOAD_PATH_SZ + 1];
	int res;

	resume_name(name, name_overflow, wfs, true);

	lock(&wfs->wf->wmutex, NULL);
	res = mkdir_upload(name,
			   name + sizeof(upload_dir),
			   get_upload_id(),
			   &wfs->wf->r.id1);
	if (0 == res && '\0' != name_overflow[0])
		res = mkdir_upload(name_overflow,
				   name_overflow + sizeof(upload_dir),
				   get_upload_id(),
				   &wfs->wf->r.id2);
	if (0 != res) {
		lprintf(LOG_ERR,
			"%d creating metadata related to file FTP:%s name:%s parent:%lld\n",
			res, wfs->wf->ftp_name, wfs->filename, wfs->parent_id);
		wfs->wf->r.id1 = RES_ERROR;
	}
	unlock(&wfs->wf->wmutex, NULL);
}

/*
 * @brief creates the special resume 'D' name for files to be delete later
 *
 * @param pointer to the wfs for which to create metadata.
 * @param where to store the resulting name.
 * @param where to store the overflow name (for compatibility with resume_name).
 * @return none
 */
static void resume_delete_name(char *name, char *name_overflow,
			       struct wfile_spec *wfs)
{
	char *p;

	p = resume_init_path(name, wfs->wf->ftp_name);
	p[0] = 'D';
	p[1] = '\0';

	name_overflow[0] = '\0';
}


/*
 * @brief renames metadata directories
 *
 * If new_wfs is NULL, it means rename to the 'delete name'.
 *
 * @param pointer to the old wfs already having metadata.
 * @param pointer to the new wfs to rename metadata to.
 * @return none
 */

static void metadata_rename(struct wfile_spec *old_wfs,
			    struct wfile_spec *new_wfs )
{
	char old_name	      [MAX_UPLOAD_PATH_SZ + 1];
	char old_name_overflow[MAX_UPLOAD_PATH_SZ + 1];
	char new_name	      [MAX_UPLOAD_PATH_SZ + 1];
	char new_name_overflow[MAX_UPLOAD_PATH_SZ + 1];
	int res;

	/* Do not rename when there was no name! */
	if (RES_NOT_INITIALIZED == old_wfs->wf->r.id1)
		return;

	resume_name(old_name, old_name_overflow, old_wfs, true);
	if (NULL == new_wfs)
		resume_delete_name(new_name, new_name_overflow, old_wfs);
	else
		resume_name(new_name, new_name_overflow, new_wfs, false);

	res = rename_dir_upload(old_name,
				new_name,
				old_wfs->wf->r.id1,
				get_upload_id(),
				new_name + sizeof(upload_dir));
	if ( 0 == res &&
	    ('\0' != old_name_overflow[0]  || '\0' != new_name_overflow[0])) {
		if ('\0' != old_name_overflow[0]) {
			if ('\0' == new_name_overflow[0]) {
				/* Case delete old overflow */
				rmdir_upload(old_name_overflow,
					     old_wfs->wf->r.id2);
				old_wfs->wf->r.id2 = RES_NOT_INITIALIZED;
			}
			else {	/* Case rename the overflow */
				res = rename_dir_upload(old_name_overflow,
							new_name_overflow,
							old_wfs->wf->r.id2,
							get_upload_id(),
							new_name_overflow +
							    sizeof(upload_dir));
			}
		}
		else {		/* Case create the overflow */
			res = mkdir_upload(new_name_overflow,
					   new_name_overflow +
						sizeof(upload_dir),
					   get_upload_id(),
					   &new_wfs->wf->r.id2);
		}
	}
	if (0 != res) {
		lprintf(LOG_ERR,
			"%d renaming metadata related to file FTP:%s from:%s to:%s\n",
			res, old_wfs->wf->ftp_name, old_wfs->filename,
			(NULL != new_wfs) ? new_wfs->filename : NULL);
		if (NULL != new_wfs)
			new_wfs->wf->r.id1 = RES_ERROR;
	}
}

/*
 * @brief delete metadata directories
 *
 * @param pointer to the wfs for which to create metadata.
 * @param flag true if the dirname to delete is the special 'D' name
 * @return none
 */

static void metadata_delete(struct wfile_spec *wfs, bool del)
{
	char name	  [MAX_UPLOAD_PATH_SZ + 1];
	char name_overflow[MAX_UPLOAD_PATH_SZ + 1];
	int res1, res2 = 0;

	if (del)
		resume_delete_name(name, name_overflow, wfs);
	else
		resume_name(name, name_overflow, wfs, true);

	DEBUG("metadata_delete: %s\n", name);

	res1 = rmdir_upload(name, wfs->wf->r.id1);

	if ('\0' != name_overflow[0])
		res2 = rmdir_upload(name_overflow, wfs->wf->r.id2);

	if (0 != res1 || 0 != res2)
		lprintf(LOG_ERR,
			"%d %d deleting metadata related to file FTP:%s name:%s parent:%lld\n",
			res1, res2, wfs->wf->ftp_name, wfs->filename, wfs->parent_id);
	wfs->wf->r.id1 = RES_NOT_INITIALIZED;
	wfs->wf->r.id2 = RES_NOT_INITIALIZED;
}

static int write_rename_wrapped(struct walk *wf, struct walk *wt,
				const char *from, const char *to)
{
	char *rename_to;
	struct wfile_list *cur_flist, *new_flist;
	struct wfile_spec *cur_wfs, *new_wfs;
	unsigned long spin;
	unsigned int n_files, i, j;

	if (STEP_DONE == LOAD(wf->res.wf->step))
		return -EAGAIN;

	cur_flist = LOAD(wflist);

	cur_wfs = id_fname_find(cur_flist,
				wf->parent->id,
				strrchr(from, '/') + 1);
	if (NULL == cur_wfs)		/* Should not happen, blocked by fuse?*/
		return -ENOENT;

	rename_to = strrchr(to, '/') + 1;
	if (is_fuse_hidden(rename_to)) {
		DEBUG( "fuse hid a file being written to, delete instead.\n");
		write_unlink_wrapped(wf->res.wf);
		/* For file deleted before writing the first byte */
		if (MAX_WRITERS != wf->res.wf->iwriter)
			post(&writers[wf->res.wf->iwriter].rsem);
		return 0;
	}

	new_flist = NULL;
	new_wfs = malloc(sizeof(struct wfile_spec) + strlen(to) + 1);
	strcpy(new_wfs->path, to);
	new_wfs->wf	   = cur_wfs->wf;
	new_wfs->parent_id = wt->parent->id;
	new_wfs->filename  = strrchr(new_wfs->path, '/') + 1;

	spin = 0;
	do {
		spin++;

		if (NULL == cur_flist)
			n_files = 1;
		else
			n_files = cur_flist->n_files;
		new_flist = realloc(new_flist,
				    sizeof(struct wfile_list) +
				    sizeof(struct wfile) * (n_files));

		i = 0;
		j = 0;
		while( i < n_files &&
		       ( cur_flist->files[i]->parent_id <  new_wfs->parent_id ||
		        (cur_flist->files[i]->parent_id == new_wfs->parent_id &&
		         strcmp(cur_flist->files[i]->filename,
			        new_wfs->filename) < 0)
		        )
		      ) {
			if (cur_flist->files[i] != cur_wfs) {
				new_flist->files[j] = cur_flist->files[i];
				j++;
			}
			i++;
		}
		new_flist->files[j] = new_wfs;
		j++;
		while( i < n_files) {
			if (cur_flist->files[i] != cur_wfs) {
				new_flist->files[j] = cur_flist->files[i];
				j++;
			}
			i++;
		}
		new_flist->n_files  = j;
	} while(!XCHG(wflist, cur_flist, new_flist));

	spin_add(spin - 1, "write_rename");

	if (NULL != cur_flist) {
		DEBUG(	">> write_rename_wrapped: rcu wfile array pointer %p\n",
			cur_flist);
		rcu_free_ptr(cur_flist);
	}

	metadata_rename(cur_wfs, new_wfs);

	rcu_free_ptr(cur_wfs);

	return 0;
}

int write_rename(struct walk *wf, struct walk *wt,
		 const char *from, const char *to)
{
	int res;

	lock(&wf->res.wf->wmutex, NULL);
	res = write_rename_wrapped(wf, wt, from, to);
	unlock(&wf->res.wf->wmutex, NULL);
	return res;
}

/* Utility to check semaphore initialisation */
static const char err_sem_init[] = "sem_init: %d\n";
#define SEM_INIT(sem, pshared, value) 					   \
	do {	int err;						   \
		err = sem_init(sem, pshared, value);			   \
		if (0 != err)						   \
			lprintf(LOG_CRIT, err_sem_init, err);	} while(0)


/*
 * @brief associates a writer to a file
 *
 * This function waits until a writer becomes available.
 * The previous behaviour: returning EAGAIN after a short delay broke basic
 * utilities like cp.
 * This is temporary until 1fichier.com provides a "better" alternative and
 * all the code is replaced by using the fuse threads instead of dedicated
 * threads.
 * Before that, the driver might suffer some delays if all "writers" are
 * busy.
 *
 * @param pointer to the wfile that needs the writer.
 * @return none
 */

static int get_writer(struct wfile *wf)
{
	unsigned int i;
	unsigned long spin = 0;
	bool idle;

	errno = 0;

	wait(&sem_writer);

	/* When the semaphore is acquired there is at least 1 idle writer
	 * XCHG is still needed in case of racing to get an idle writer.
	 */

	for (i = 0; i < MAX_WRITERS; i++) {
		idle = LOAD(writers[i].idle);
		if (idle) {
			if (XCHG(writers[i].idle, idle, false))
				break;
			spin ++;
		}
	}
	spin_add(spin, "get_writer");

	DEBUG("get_writer: got writer [%u]\n", i);

	wf->iwriter = i;
	writers[i].wf   = wf;
	writers[i].woff = 0;
	writers[i].wsz  = 0;
	writers[i].roff = 0;
	writers[i].rsz  = 0;

	/* Starts that writer if necessary */
	if (NULL == writers[i].buf) {
		int err;

		writers[i].buf = malloc(RING_BUF_SZ);
		writers[i].curl= NULL;
		STORE(writers[i].p_signal, 0);
		SEM_INIT(&writers[i].rsem, 0, 0);
		SEM_INIT(&writers[i].wsem, 0, 0);
		err = pthread_create(&writers[i].thread,
				     NULL,
				     async_writer,
				     &writers[i]);
		if (0 != err)
			lprintf(LOG_CRIT, "creating writer thread[%u]: %d\n"
					, i, err);
	}

	return 0;
}


#ifdef _DEBUG
static char *steps[] = {"STEP_WRITING",
			"STEP_WAIT_FTP",
			"STEP_WAIT_MV",
			"STEP_WAIT_RM",
			"STEP_DONE" };

static void dump_wflist(struct wfile_list *wfl, const char *msg)
{
	unsigned long i;

	DEBUG("dump_wflist: %s.\n", msg);
	if (NULL == wfl) {
		DEBUG("dump_wflist: write file list is empty.\n");
		return;
	}
	for (i=0; i < wfl->n_files; i++) {
		DEBUG(	"dump_wflist %3u: %p-> [%u:%s] /%lld/ %s {%s} sz=%"PRIu64"\n",
			i,
			wfl->files[i],
			wfl->files[i]->wf->iwriter,
			(MAX_WRITERS == wfl->files[i]->wf->iwriter) ? "----" :
			  ((LOAD(writers[wfl->files[i]->wf->iwriter].idle)) ?
				"idle" : "busy"),
			wfl->files[i]->parent_id,
			wfl->files[i]->filename,
			steps[wfl->files[i]->wf->step],
			(uint64_t)LOAD(wfl->files[i]->wf->size));
	}
}
#else
#define dump_wflist(wfl, msg)
#endif

enum called_from {
	FROM_INIT,
	FROM_UTIL,
	FROM_CREATE,
};

static bool write_init_background(enum called_from from);

/*
 * @brief used for create
 *
 * @param path to the file being created + opened
 * @param fuse_file_info pointer
 * @return 0 if ok or error code
 */

static int unfichier_create_wrapped(const char *path,
				    struct fuse_file_info *fi)
{
	struct fi_to_fh *fifh;
	struct wfile *new_file;
	struct wfile_spec *new_file_spec;
	struct wfile_list *cur_flist, *new_flist = NULL;
	unsigned int n_files, i, j;
	bool *keep = NULL, special;
	unsigned long spin;
	struct timespec create_time;
	uint32_t cnt;
	struct walk w;
	int res, cmp;
	char *filename;

	res = forbidden_chars(path, TR_FILE);
	if (0 != res)
		return res;

	find_path(path, &w);
			/* This should NOT happen! Kernel/fuse is supposed  */
			/* supposed to call create only when file is opened */
	if (NULL != w.res.fs)	/* for write and does NOT exist     */
	{
		if (w.is_dir)
			return -EISDIR;
		else
			return -EEXIST;
	}

	if (reserved_names( path, w.parent->id))
		return -EACCES;

	/* Also forbids to write directly on the upload_directory since the
	 * end process excpects to "move" the file, not to chattr. */
	if (forbidden_write(w.parent, true, NULL) ||
	    w.parent->id == get_upload_id())
		return -EACCES;

	/* Write on shared root (id=0) not supported so far. This would require
	 * managing also the email of the destination. */
	if (w.parent->id == 0 && w.parent->shared)
		return -EACCES;

	/* encfs: cannot already be a "special name" */
	filename = strrchr(path, '/');
	if (NULL != filename)
		filename += 1;
	if (is_encfs_name(filename, NULL, &special) && special) {
		lprintf(LOG_WARNING,
			"creating the file: `%s` with the pattern or encfs-key-rewritten is forbidden\n",
			filename);
		return -EACCES;
	}

	/* From here, write really needs to be started */
	if (!write_init_background(FROM_CREATE))
		return -EROFS;

	/* Allocate and initialise the newly created file
	 */
	new_file = malloc(sizeof(struct wfile));
	STORE(new_file->step, STEP_WRITING);

	new_file_spec = malloc(sizeof(struct wfile_spec) + strlen(path) + 1);
	strcpy(new_file_spec->path, path);
	new_file_spec->filename  = strrchr(new_file_spec->path,'/') + 1;
	new_file_spec->parent_id = w.parent->id;
	new_file_spec->wf = new_file;

	STORE(new_file->counter, OPEN_INC);
	STORE(new_file->size, 0);
	clock_gettime(CLOCK_REALTIME, &create_time);
	new_file->cdate = create_time.tv_sec;
	new_file->eta = 0;
	new_file->r.id1 = RES_NOT_INITIALIZED;
	new_file->r.id2 = RES_NOT_INITIALIZED;
	new_file->iwriter = MAX_WRITERS;
	new_file->data = NULL;
	cnt = ADD(wf_counter, 1, relaxed);
	snprintf(new_file->ftp_name, sizeof(new_file->ftp_name)
				   , "%s_%08"PRIX32"F", ftp_base, cnt + 1);
	pthread_mutex_init(&new_file->wmutex, NULL);
	new_file->no_ssl = w.parent->no_ssl;
	new_file->encfs  = w.parent->encfs;
	new_file->has_encfs_key = false;
	new_file->has_new_key   = false;

	/* Add it (sorted) to the global list.
	 * The sort is by parent id / filename to be faster for finding.
	 */

	cur_flist = LOAD(wflist);

	dump_wflist(cur_flist, "before create");

	spin = 0;
	do {
		spin++;
		if (NULL == cur_flist) {
			n_files = 1;
		}
		else {
			n_files = cur_flist->n_files + 1;
			keep = realloc(	keep,
					cur_flist->n_files * sizeof(keep[0]));
		}
		new_flist = realloc(new_flist,
				    sizeof(struct wfile_list) +
				    sizeof(struct wfile) * (n_files));

		i = 0;
		j = 0;
		cmp = -1;
		while( i < n_files - 1 &&
		       ( cur_flist->files[i]->parent_id <  w.parent->id ||
		        (cur_flist->files[i]->parent_id == w.parent->id &&
		         (cmp = strcmp( cur_flist->files[i]->filename,
					new_file_spec->filename)      ) < 0)
		        )
		      ) {
			keep[i] = keep_wf(cur_flist->files[i]->wf);
			if (keep[i]) {
				new_flist->files[j] = cur_flist->files[i];
				j++;
			}
			i++;
		}
		/* Unlikely because kernel/fuse library should avoid doing so,
		 * but it is undocumented: created twice the same file at the
		 * same time. It's Ok if it's kept after rcu_del though,
		 * in this case FL_DEL flag will be on  */
		if (0 == cmp &&
		    keep_wf(cur_flist->files[i]->wf) &&
		    0 == (LOAD(cur_flist->files[i]->wf->counter) & FL_DEL)) {
			free(new_file);
			free(new_flist);
			free(keep);
			return -EEXIST;
		}
		new_flist->files[j] = new_file_spec;
		j++;
		while(i < n_files - 1) {
			keep[i] = keep_wf(cur_flist->files[i]->wf);
			if (keep[i]) {
				new_flist->files[j] = cur_flist->files[i];
				j++;
			}
			i++;
		}
		new_flist->n_files  = j;
	} while(!XCHG(wflist, cur_flist, new_flist));

	spin_add(spin - 1, "unfichier_create");

	dump_wflist(new_flist, "after create");

	/* To avoid double free, rcu-freeing wfiles must be done only when
	 * the exchange was successul. That way, only 1 thread (the one that
	 * succeded the exchange) will attempt to free from cur_flist, and
	 * potential parallel create will work on new_flist from the "winner",
	 * with deleted entries not present.
	 * The freeing cannot call again keep_wf because the situation can have
	 * changed, but uses the recorded positions to free.
	 */
	if (NULL != cur_flist && new_flist->n_files != cur_flist->n_files + 1)
		free_wfiles(cur_flist, keep);
	free(keep);

	if (NULL != cur_flist) {
		DEBUG(	">> unfichier_create: rcu wfile array pointer %p\n",
			cur_flist);
		rcu_free_ptr(cur_flist);
	}

	fifh = malloc(sizeof(struct fi_to_fh));
	fifh->type = TYPE_WRITE;
	STORE(fifh->last_opt, UNKNOWN_OPT);	/* In case there are reads */
	fifh->fh = (uintptr_t)new_file;

	fi->fh = (uintptr_t)fifh;

	DEBUG(	"created file[%u]: %s (fh=%p) r=%s\n",
		new_file->iwriter, path, new_file, new_file->ftp_name);

	return 0;
}

/*
 * @brief fuse callback for create
 *
 * Same as opening a file, but for writing and when if does not exist.
 *
 * IMPORTANT: "virtual" files that are being written are NOT translated.
 *   The check is made for forbidden characters in --raw-names, and for
 *   max length and returned accordingly. If the creation is possible, the
 *   name is kept in memory un-translated to avoid complexity and double
 *   translations when searching, renaming, etc...
 *   The name is only translated at the last moment, which is when doing the
 *   final rename, and also for the resume algorithm (see below).
 *
 * @param path to the file being created + opened
 * @param mode for the file to be created (not support by the remote)
 * @param fuse_file_info pointer
 * @return 0 if ok or error code
 */

int unfichier_create(const char *path, mode_t mode,
		     struct fuse_file_info *fi)
{
	int res;

	DEBUG( "<<in: create(%s, 0x%X, 0x%X)\n", path, mode, fi->flags);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_create_wrapped(path, fi);

	rcu_read_unlock();
	rcu_unregister_thread();

	DEBUG( ">>out: create(%s,...) res=%d\n", path, res);

	return res;
}


/*
 * @brief write part of open
 *
 * This is called when re-opening an existing file for write.
 *
 * There is no need checking if write is ready because it is always called
 * after create that tested it (since truncate not supported).
 *
 * @param pointer on the wfile structure for that file
 * @param counter of the wfile
 * @param file info pointer passed by fuse
 * @return 0 or error code
 */

int write_open_wrapped(struct wfile *wf, unsigned long counter,
			struct fuse_file_info *fi)
{
	if (STEP_WRITING != LOAD(wf->step)) {
		if ((fi->flags & O_ACCMODE) == O_WRONLY || NULL == wf->data) {
		/* Writing is not possible anymore */
			return -EBUSY;
		}
		else {
		/* Reading is still possible in the "small files' buffer" */
			if ((fi->flags & O_ACCMODE) == O_RDONLY &&
			    wf->size > SMALL_FILE_SIZE_MAX)
			/* This is encfs file, forbid incoherent readonly */
				return -EBUSY;
			else
			/* RW mode must granted access to allow changing the
			 * key header, whatever the size of the file */
				return 0;
		}
	}

	if (0 != (counter & FL_DEL))
		return -ENOENT;
	if (0 != (counter & FL_ERROR))
		return -EREMOTEIO;

	return 0;
}

int write_open(const char *path, struct walk *w, struct fuse_file_info *fi)
{
	int res;
	unsigned long counter;

	lock(&w->res.wf->wmutex, NULL);

	counter = LOAD(w->res.wf->counter);

	res = write_open_wrapped(w->res.wf, counter, fi);
	if (0 == res) {
		struct fi_to_fh *fifh;

		fifh = malloc(sizeof(struct fi_to_fh));
		fifh->fh   = (uintptr_t)w->res.wf;
		fifh->type = TYPE_WRITE;
		if ((fi->flags & O_ACCMODE) == O_WRONLY)
			STORE(fifh->last_opt, UNKNOWN_OPT);

		fi->fh = (uintptr_t)fifh;

		counter += OPEN_INC;
		STORE(w->res.wf->counter, counter);
		DEBUG(	">> write_open[%u] handle: %p (0x%lX).\n",
			w->res.wf->iwriter, w->res.wf, counter);
	}
	else {
		DEBUG(	">> write_open failed with %d handle: %p (0x%lX).\n",
			res, w->res.wf, counter);
	}
	unlock(&w->res.wf->wmutex, NULL);

	return res;
}


/*
 * @brief fuse callback for flush and fsync
 *
 * Only for debug purpose, does nothing so far.
 *
 * @param path to the file to flush
 * @param fuse_file_info pointer
 * @return 0 if ok or error code
 */

int unfichier_flush(const char *path, struct fuse_file_info *fi)
{
	DEBUG( "flush(%s, ...)\n", path);

	return 0;
}

int unfichier_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
	DEBUG( "fsync(%s, ...)\n", path);

	return 0;
}


static void pipe_write( const char *buf, size_t size, off_t offset,
			struct writer *wr)
{
	if (offset + size > wr->wsz)
		wr->wsz = offset + size;
	offset &= RING_MASK;
	if (offset + size > RING_BUF_SZ) {
		size_t chunk;
		chunk = RING_BUF_SZ - offset;
		memcpy(wr->buf + offset, buf, chunk);
		buf  += chunk;
		size -= chunk;
		offset = 0;
	}
	memcpy(wr->buf + offset, buf, size);
}

static int write_read_wrapped(char *buf, size_t size, off_t offset,
			      struct wfile  *wf)
{
	struct writer *wr;
	size_t chunk;
	unsigned long counter;

	counter = LOAD(wf->counter);
	if (0 != (counter & FL_ERROR))
		return -EREMOTEIO;
	if (0 != (counter & FL_DEL))
		return -ENOENT;

	/* This is a closed "small file" not yet stored  */
	if (NULL != wf->data) {
		size_t sz, dsz;

		dsz = (wf->size > SMALL_FILE_SIZE_MAX) ? SMALL_FILE_SIZE_MAX
						       : wf->size;
		if (offset >= dsz)
			return 0;
		sz = dsz - offset;
		if (sz > size)
			sz = size;
		DEBUG( "write_read_wrapped: reading %d bytes from temporarily saved file data.\n",
			sz);
		memcpy(buf, wf->data, sz);
		return sz;
	}

	wr = &writers[wf->iwriter];
	/* Check that offset received is acceptable for the pipe's tolerance  */
	if (offset < wr->woff || offset > wr->wsz) {
		lprintf(LOG_ERR,
			"write_read_wrapped[%u]: read out of bounds %"PRIu64":%u bounds=%"PRIu64":%"PRIu64".\n",
			wf->iwriter,
			(uint64_t)offset,
			(unsigned int)size,
			(uint64_t)(wr->woff),
			(uint64_t)(wr->wsz));
		return -EREMOTEIO;
	}

	/* Ok to answer that read request */
	if (offset + size > wr->wsz)
		size = wr->wsz - offset;
	if (0 == size)
		return 0;

	offset &= RING_MASK;
	if (offset + size > RING_BUF_SZ) {
		chunk = RING_BUF_SZ - offset;
		memcpy(buf, wr->buf + offset, chunk);
		buf  += chunk;
		chunk = size - chunk;
		offset = 0;
	}
	else {
		chunk = size;
	}
	memcpy(buf, wr->buf + offset, chunk);
	return size;
}

int write_read(char *buf, size_t size, off_t offset, struct wfile *wf)
{
	lock(&wf->wmutex, NULL);

	size = write_read_wrapped(buf, size, offset, wf);

	unlock(&wf->wmutex, NULL);

	return size;
}

static int write_append()
{
	//TODO
	return -EACCES;
}

/*
 * @brief test special case of rewriting encfs header
 *
 * @param path of the file
 * @param buffer to write
 * @param size of the data to write
 * @param (absolute) offset of the data
 * @param pointer to control structure of the written file
 * @param where to copy the key if it is a rewrite case
 * @return true if this was a rewrite key, false otherwise
 */

static bool is_encfs_rewrite_key(const char *path, const char *buf,
				 size_t size, off_t offset,
				 struct wfile *wf, char *where)
{
	const char *filename;

	if (0 != offset || ENCFS_HEADER_SZ != size || !(wf->encfs))
		return false;

	filename = strrchr(path, '/') + 1;
	if (!is_encfs_name(filename, NULL, NULL))
		return false;

	if (!wf->has_encfs_key &&
	    strlen(filename) + ENCFS_HEADER_B64_SZ > MAX_FILENAME_LENGTH)
		return false;

	encfs_copy_key(where, buf);
	wf->has_encfs_key = true;
	return true;
}

/*
 * @brief special writing case
 * 
 * Due to the deferred pipe buffer writing, when the file is released (last
 * close on a file handle), ftp writing continues to flush what remains before
 * the file goes fo "WAIT_FTP" step. This is even a more important time for
 * small files because the ftp writing might not have started yet.
 * When the file is re-opened for writing at that moment, it is desirable to
 * still populate the "leeway buffer" if any, especially to avoid as much
 * as possible encfs "rename-hacks".
 * 
 * This function takes care of this special writing case.
 *
 * @param path of the file
 * @param buffer to write
 * @param size of the data to write
 * @param (absolute) offset of the data
 * @param pointer to control structure of the written file
 * @return same as fuse write callback: written size or error
 */
static int write_after_close(const char *path, const char *buf,
			     size_t size, off_t offset,
			     struct wfile *wf)
{
	/* Writing must be in the leeway: between rsz and size of file (wsz). */
	if (offset >= writers[wf->iwriter].rsz && offset + size <= wf->size) {
		pipe_write(buf, size, offset, &writers[wf->iwriter]);

		/* Also update saved data if any */
		if (NULL != wf->data  && SMALL_FILE_SIZE_MAX > offset) {
			size_t chunk;
			if (offset + size >=SMALL_FILE_SIZE_MAX)
				chunk = SMALL_FILE_SIZE_MAX - offset;
			else
				chunk = size;
			memmove(wf->data + offset, buf, chunk);
		}					
		return size;
	}
	/* Even if not in leeway, it can still be a rewrite_key case, resume
	 * dir not yet created at this step, copying in wf->data is enough */
	if (is_encfs_rewrite_key(path, buf, size, offset, wf, wf->data))
		return ENCFS_HEADER_SZ;
	lprintf(LOG_ERR,
		"write_after_close[%u]: out of bounds off=%"PRIu64":%"PRIu64" bounds=%"PRIu64":%"PRIu64".\n",
			(unsigned int)(wf->iwriter),
			(uint64_t)offset, (uint64_t)size,
			(uint64_t)writers[wf->iwriter].rsz,
			(uint64_t)wf->size);
	return -EREMOTEIO;
}

static int unfichier_write_wrapped(const char *path, const char *buf,
				   size_t size, off_t offset,
				   struct wfile *wf, unsigned long counter)
{
	size_t chunk;
	struct writer *wr;
	unsigned long signal;
	int err;

	/* Beware, file has been marked as DEL, so do like /dev/null here
	 * writer for that file is not valid anymore because it has been made
	 * idle and could have been reused to write another file
	 * It is an implementation choice to free immediately the async_writer,
	 * the drawback is that potential read will fail since there is no
	 * leeway buffer anymore. */
	if (0 != (counter & FL_DEL)) {
		return size;
	}

	switch (wf->step) {
		case STEP_WRITING  :
			if (MAX_WRITERS == wf->iwriter)
				break;
			/* When there is already a writer, look to signals */
			signal = LOAD(writers[wf->iwriter].p_signal);
			if (IS_SIG_ERROR(signal)) {
				OR(wf->counter, FL_ERROR);
				return -EREMOTEIO;
			}
			if (IS_SIG_CLOSE(signal))
				return write_after_close(path, buf, size,
							 offset, wf);
			break;
		case STEP_DONE	   : 	/* Should not happen, just in case! */
			return -EACCES;
		case STEP_WAIT_FTP :	/* Append or encfs rewrite key */
			err = write_append();
			if (0 <= err)
				return err;	/* Append succesful */
			/** falltrhough to test encfs rewrite for both cases */
		case STEP_WAIT_MV  :	/* At this stage Append is no longer
					   possible, only rewrite key */
			if (is_encfs_rewrite_key(path, buf, size, offset, wf,
						 wf->new_key))
				return ENCFS_HEADER_SZ;
			return -EACCES; //TODO
		case STEP_WAIT_RM  :
			return -ENOENT;	/* Should not happen DEL handled above*/
	}

	/* But when file is not marked DEL, get a writer if none */
	if (MAX_WRITERS == wf->iwriter) {
		err = get_writer(wf);
		if (0 != err) {
			DEBUG("unfichier_write_wrapped[4]: no writer available, returning %d\n", err);
			return err;
		}
	}
	wr = &writers[wf->iwriter];

	/* Check that offset received is acceptable for the pipe's tolerance  */
	if (offset < wr->woff || offset > wr->woff + W_BUF_SZ) {
		if (is_encfs_rewrite_key(path, buf, size, offset, wf, wf->data))
			return ENCFS_HEADER_SZ;
		lprintf(LOG_ERR,
			"unfichier_write_wrapped[%u]: write out of bounds off=%"PRIu64":%"PRIu64" bounds=%"PRIu64":%"PRIu64".\n",
			(unsigned int)(writers - wr),
			(uint64_t)offset, (uint64_t)size,
			(uint64_t)(wr->woff),
			(uint64_t)(wr->woff + W_BUF_SZ));
		return -EREMOTEIO;
	}

	/* When everything fits in, directly write it */
	if (offset + size <= wr->woff + W_BUF_SZ) {
		pipe_write(buf, size, offset, wr);
		return size;
	}


	/* Otherwise it means new 1 or + blocks are needed, so iterate. */
	chunk = wr->woff + W_BUF_SZ - offset;
	if (0 != chunk)
		pipe_write(buf, chunk, offset, wr);
	if (0 == wr->woff && wf->encfs) {
		wf->data = malloc(SMALL_FILE_SIZE_MAX);
		memmove(wf->data, wr->buf, SMALL_FILE_SIZE_MAX);
	}
	post(&wr->rsem);
	wr->woff += PIPE_CHUNK;
	wait(&wr->wsem);

	return unfichier_write_wrapped(path, buf    + chunk,
				       size   - chunk,
				       offset + chunk,
				       wf	     ,
				       counter        ) + chunk;
}

/*
 * @brief fuse callback for write
 *
 * Note on locks: unlike reading, writing order is important, especially when
 *		  writes can overlap. Thus locking is needed to be sure
 *		  writes are processed in the order they are received.
 *	For the pipe algorithm, there can be several processes writing,
 *	incoming from fuse client writes. Thus locking here also avoids
 *	having to use atomics.
 *	There is no such problem on the other side of the pipe since only
 *	the single process of the curl callback is removing data from the pipe.
 *
 * @param path to the file being read
 * @param buffer where we store read data
 * @param size of the buffer
 * @param offset from where we read
 * @param fuse internal pointer with possible private data
 * @return number of bytes read (should be size unless error or end of file)
 */

int unfichier_write(const char *path, const char *buf, size_t size,
		    off_t offset, struct fuse_file_info *fi)
{
	struct fi_to_fh *fifh;
	struct wfile *wf;
	unsigned long counter;
	int sz;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));
	if (TYPE_READ == fifh->type)
		/* The storage file was opened RW and now trying to write */
		return encfs_rewrite_key(path, buf, size, offset, fi);

	wf = (struct wfile *)((uintptr_t)(fifh->fh));


#ifdef _STATS
	/* Note: _COARSE clock does not have enough resolution here */
	struct timespec start;

	if (NULL != params.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);
#endif

	DEBUG(	"unfichier_write[%u]: fh=%p (%"PRIu64":%ld)\n",
		(unsigned int)wf->iwriter,
		wf,
		(uint64_t)offset,
		(long)size);

	lock(&wf->wmutex, NULL);

	counter = LOAD(wf->counter);
	sz = unfichier_write_wrapped(path, buf, size, offset, wf, counter);
	if (sz > 0) {
		if (offset + sz > LOAD(wf->size))
			STORE(wf->size, offset + sz);
		if (0 == (counter & FL_DEL) && MAX_WRITERS > wf->iwriter)
			update_speed_stats(false, sz, wf->iwriter);
	}
	unlock(&wf->wmutex, NULL);

#ifdef _STATS
	if (NULL != params.stat_file && sz > 0 && 0 == (counter & FL_DEL))
		update_write_stats(sz, wf->iwriter, &start);
#endif

	return sz;
}

/* This is for the callbacks below that do nothing but return 0 or -EACCES if
 * writing is disabled.
 */

#define FAKE_CALLBACK (CAN_WRITE ? 0 : -EACCES)

/*
 * @brief fuse callback for utimens
 *
 * This is only to avoid commands like touch, rsync, etc... to complain about
 * not being able to change times.
 * It doesn't do anything because there is nothing the driver can do to
 * alter times: they are set by the server when the file is stored.
 *
 * @param path to the file
 * @param timespecs to set
 * @return 0 or error code
 */

int unfichier_utimens(const char *path, const struct timespec tv[2])
{
	return FAKE_CALLBACK;
}

/*
 * @brief fuse callback for chmod
 *
 * This is only to avoid commands like cp, chmod, etc... to complain about
 * not being able to change times.
 * It doesn't do anything because there is nothing the driver can do to
 * alter modes since the server does not handle them.
 *
 * @param path to the file
 * @param mode to set
 * @return 0 or error code
 */

int unfichier_chmod(const char *path, mode_t mode)
{
	return FAKE_CALLBACK;
}

/*
 * @brief fuse callback for chown
 *
 * This is only to avoid commands like cp, chown, etc... to complain about
 * not being able to change times.
 * It doesn't do anything because there is nothing the driver can do to
 * alter owners since the server does not handle them.
 *
 * @param path to the file
 * @param uid to set
 * @param gid to set
 * @return 0 or error code
 */

int unfichier_chown(const char *path, uid_t uid, gid_t gid)
{
	return FAKE_CALLBACK;
}

static bool is_valid_common_name(const char *name)
{
	int i;

	for (i=0; i < FTP_BASE_PREFIX_SZ; i++)
		if (ftp_base[i] != name[i])
			return false;
	for (i=0; i < FTP_BASE_RANDOM_SZ; i++)
		if (!isxdigit(name[i + FTP_BASE_PREFIX_SZ]))
			return false;
	if ( '_' != name[FTP_BASE_SZ])
			return false;
	for (i= FTP_BASE_SZ + 1; i < FTP_NAME_SZ - 2; i++)
		if (!isxdigit(name[i]))
			return false;
	return true;
}

static bool is_valid_ftp_name(const char *name)
{
	if (is_valid_common_name(name) && 'F'  == RES_TYPE(name))
		return ('\0'== name[FTP_NAME_SZ]);
	return false;
}

static bool is_valid_resume_name(const char *name)
{
	int i;

	if (!is_valid_common_name(name))
		return false;
	switch(RES_TYPE(name)) {
		case 'D' :	return ('\0'== name[FTP_NAME_SZ]);
		case 'H' :	if ('.' != name[strlen(name) - 1])
					return false;
				/* fallthrough */
		case 'G' :	for (i=0; i < RES_DIR_SZ; i++)
					if (!isxdigit(name[FTP_NAME_SZ + i]))
						return false;
		case 'I' :	return ('\0' != name[FTP_NAME_SZ]);
		default  :	return false;
	}
}



static const char * next_file(struct dentries *dent, unsigned long *i,
			      const char **URL, time_t *cdate)
{
	const char *ftpname;

	do {
		ftpname = list_file_upload(dent, i, URL, cdate);
	} while (NULL != ftpname && !is_valid_ftp_name(ftpname));
	return ftpname;
}


/*
 * @brief decodes ' and " encoded in "resume names" directories
 *
 *
 */

static char *decode_resume_name(const char r1, const char r2, const char esc,
				const char *from, char *to)
{
	const char *p;
	char *q;

	for (p = from, q = to; '\0' != *p; p++, q++) {
		if (esc == *p)
			*q = *++p;
		else if (r1 == *p)
			*q = '\x22';
		else if (r2 == *p)
			*q = '\x27';
		else
			*q = *p;
	}
	*q = '\0';
	return q;
}

/*
 * @brief add the upload directory in front of a file/dir name.
 *
 * Path must be as least: MAX_UPLOAD_PATH_SZ + 1
 *
 */

static char *upload_path(char *path, const char *name)
{
	memcpy(path, upload_dir, lengthof(upload_dir));
	path[lengthof(upload_dir)] = '/';
	strcpy(path + sizeof(upload_dir), name);
	return path;
}

static void rm_old_resume_dir(time_t cdate, struct timespec *now,
			      const char *dirname, const long long dir_id,
			      const char *msg, struct wfile_spec *wfs)
{
	char path[MAX_UPLOAD_PATH_SZ + 1];

	if (now->tv_sec < cdate + params1f.resume_delay) {
		lprintf(LOG_WARNING,
			"resume dir too recent to be deleted: %s\n",
			dirname);
	}
	else {
		lprintf(LOG_WARNING,
			"%sdeleting old resume dir(s): %s\n",
			msg, dirname);
		if (!params1f.dry_run) {
			if (NULL == wfs)
				rmdir_upload(upload_path(path, dirname),
					     dir_id);
			else
				metadata_delete(wfs,
						('D' == RES_TYPE(dirname)));
		}
	}
}


/*
 * @brief gets next directory (ie metadata) to resume
 *
 * It also reverse translates the ' and " from the directory name to the
 * filename and manages case where the name overflow in 2 directories.
 * Also the error cases where one of those 2 directories is missing.
 *
 */

static const char * next_subdir(struct dentries *dent, unsigned long *i,
				time_t *cdate, struct timespec *now,
				struct wfile_spec *wfs, const char *msg)
{
	const char *dirname, *supname, *q;
	char *p;
	char r1, r2, esc;
	char path[MAX_UPLOAD_PATH_SZ + 1];
	time_t cdate2;

	while (1) {
		wfs->wf->r.id2 = RES_NOT_INITIALIZED;
		dirname = list_subdir_upload(dent, i, &wfs->wf->r.id1, cdate);
		if (NULL == dirname)
			return NULL;
		if (is_valid_resume_name(dirname)) {
			if ('D' == RES_TYPE(dirname)) {
				wfs->filename[0] = '\0';
				break;
			}
			if ('I' == RES_TYPE(dirname)) {
				/* No need to wait for resume_delay, it is
				 * always garbage to have 2nd part with no
				 * first part: immediately delete it */
				lprintf(LOG_WARNING,
					"%sresume second part with no first part, deleting: %s\n",
					msg, dirname);
				if (!params1f.dry_run) {
					rmdir_upload(upload_path(path, dirname),
						     wfs->wf->r.id1);
				}
				continue;
			}
			sscanf(dirname + FTP_NAME_SZ,
			       "%" STRINGIFY(RES_DIR_SZ) "Lx",
			       &wfs->parent_id);

			q = dirname + FTP_NAME_SZ + RES_DIR_SZ;
			r1  = q[0];
			r2  = q[1];
			esc = q[2];

			p = decode_resume_name(r1, r2, esc,
					       q + 3, wfs->filename );

			if ('H' == RES_TYPE(dirname)) {
				supname = list_subdir_upload(dent,
							    i,
							    &wfs->wf->r.id2,
							    &cdate2);
				if (NULL == supname ||
				    0 != strncmp(dirname,
						 supname,
						 FTP_NAME_SZ - 1 ) ||
				    'I' != RES_TYPE(supname)) {
					lprintf(LOG_WARNING,
						"resume first part without second part: %s\n",
						dirname);
					rm_old_resume_dir(*cdate,
							  now,
							  dirname,
							  wfs->wf->r.id1,
							  msg,
							  NULL);
					if (NULL == supname)
						return NULL;
					/* Backtrack i to continue with same */
					*i -= 1;
					continue;
				}
				/* Bactrack one character on p that is
				 * pointing after the '.' separator */
				if (cdate2 < *cdate)
					*cdate = cdate2;
				decode_resume_name(r1, r2, esc,
					           supname + FTP_NAME_SZ, p -1);
			}
			break;
		}
		lprintf(LOG_WARNING,
			"upload directory cluttered with unexpected subdir: %s\n",
			dirname);
	}
	return dirname;
}

static void msg_after_rename(int res, const char *from, long long id
				    , const char *to)
{
	if (0 == res)
		lprintf(LOG_NOTICE,
			"successfuly moved `%s` to /%lld/ `%s`\n",
			from, id, to);
	else
		lprintf(LOG_WARNING,
			"failed with %d to move `%s` to /%lld/ `%s`\n",
			res, from, id, to);
}

static void msg_after_del(int res, const char *name)
{
	if (0 == res )
		lprintf(LOG_NOTICE, "successfuly deleted `%s`\n", name);
	else
		lprintf(LOG_ERR, "failed with %d to delete `%s`\n", res, name);
}

/*
 * @brief resume process - blacklisting
 *
 * When the target directory of a resume has been deleted, it is blacklisted
 * to avoid a cascade of HTTP 403 that could lead to temporary account ban.
 *
 * In that case, the files are renamed in place inside the upload directory.
 * (which can lead to duplicate names, but that is ok with 1fichier.com, they
 * will simply not show in 1fichierfs -for now).
 *
 * blacklist_replace changes the target directory to that of the upload
 *	directory when the parent_id is on the blacklist
 * blacklist_add adds an entry in the blacklist when the rename_upload
 * 	returned with -EACCES which happens only when the API returned HTTP 403.
 *
 * Le blacklist pointer points on NULL when empty.
 * When not empty, blist[0] is the number of directory ID in the list.
 *
 * The search is simply sequential since it is not expected to have thousands of
 * blacklisted target directories to the point it is a performance issue!
 */

static int blacklist_replace(struct wfile_spec *wfs, long long *blist)
{
	long long i;

	if (NULL != blist && get_upload_id() != wfs->parent_id)
		for (i = 1; i <= blist[0]; i++)
			if (blist[i] == wfs->parent_id) {
				wfs->parent_id = get_upload_id();
				return 1;
			}
	return 0;
}

static long long * blacklist_add(int res, struct wfile_spec *wfs,
				 long long *blist)
{
	long long *new_list;

	if (-EACCES != res)
		return blist;

	if (get_upload_id() == wfs->parent_id)
		lprintf(LOG_CRIT, "upload directory unexpectedly removed!\n");

	lprintf(LOG_WARNING, "directory id: %"PRId64" is now blacklisted.\n"
			   , wfs->parent_id);

	if (NULL == blist) {
		new_list = malloc(2 * sizeof(long long));
		new_list[0] = 1;
	}
	else {
		new_list = malloc((blist[0] + 2) * sizeof(long long));
		new_list[0] = blist[0] + 1;
		memcpy(&new_list[2], &blist[1], blist[0] * sizeof(long long));
		free(blist);
	}
	new_list[1] = wfs->parent_id;
	return new_list;
}

/*
 * @brief resume process
 *
 * This is triggered in the background at startup to resume possible
 * interrupted upload process and finish the job.
 *
 * In some situation is also done before daemonising, see write_init_foreground.
 *
 */

static void resume_upload()
{
static  const char dr_msg[] = "(Dry run) ";
	const char *msg = dr_msg + ((params1f.dry_run) ? 0 : lengthof(dr_msg));
	struct dir *d;
	struct dentries *dent;
	unsigned long idir = 0, ifile = 0;
	const char *ftpname;
	const char *dirname = NULL; /* Non necessary init, but otherwise gcc
					complains about "possibly not init" */
	const char *URL;
	time_t cdate, ddate;
	int cmp, res;
	unsigned int n = 0, r = 0, f = 0, f_no_r = 0, r_no_f = 0, b = 0;
	char nameto[MAX_FILENAME_LENGTH + 1];
	struct wfile wf;
	struct wfile_spec wfs;
	struct timespec now;
	long long *blist = NULL, dest_id;


	if (lazy_init) {
		lprintf(LOG_NOTICE,
			"nothing to resume: upload directory did not exist at startup!\n");
		return;
	}

	wfs.wf = &wf;
	wfs.filename = nameto;

	d = refresh_upload(upload_dir, &dent);
	if (NULL == d) {
		lprintf(LOG_ERR, "problem getting listing of upload dir.\n");
		return;
	}

	clock_gettime(CLOCK_REALTIME_COARSE, &now);
	cmp = 0;
	while (1) {
		if (cmp <= 0)
			ftpname = next_file(dent, &ifile, &URL, &cdate);
		if (cmp >= 0)
			dirname = next_subdir(dent, &idir, &ddate, &now,
					      &wfs, msg);
		if (NULL == ftpname) {
			if (NULL == dirname)
				break;
			cmp = 1;
		}
		else {
			if (NULL == dirname)
				cmp = -1;
			else
				cmp = strncmp(ftpname, dirname, FTP_NAME_SZ -1);
		}
		if (cmp < 0) {
			lprintf(LOG_WARNING,
				"file with no resume information (clutter, uploading, killed, power off, crash, etc...): %s\n",
				ftpname);
			f_no_r ++;
			continue;
		}
		if (cmp > 0) {
			lprintf(LOG_WARNING,
				"resume information with no file (ftp waiting, storing, other instance running, etc...): %s\n",
				nameto);
			/* Files are never deleted automatically but resume
			 * directory are if they are useless and old enough. */
			memcpy(wf.ftp_name, dirname, FTP_NAME_SZ - 1);
			wf.ftp_name[FTP_NAME_SZ - 1] = '\0';
			rm_old_resume_dir(ddate, &now, dirname, 0, msg, &wfs);
			r_no_f ++;
			continue;
		}

		if (now.tv_sec  < cdate + RESUMABLE_AGE) {
			time_t wait;
			wait = cdate + RESUMABLE_AGE - now.tv_sec;
			r++;
			if ('D' == RES_TYPE(dirname))
				lprintf(LOG_WARNING,
					"%sfile %s too recent (%lus) to be deleted\n",
					msg, ftpname, (unsigned long)wait);
			else
				lprintf(LOG_WARNING,
					"%sfile %s too recent (%lus) to be renamed to /%lld/ %s\n",
					msg, ftpname, (unsigned long)wait
					   , wfs.parent_id, nameto);
		}
		else {
			n++;
			strcpy(wf.ftp_name, ftpname);
			wf.encfs	 = false;
			wf.has_encfs_key = false;
			if ('D' == RES_TYPE(dirname)) {
				lprintf(LOG_NOTICE,
					"%sresuming delete of file %s\n",
					msg, ftpname);
				if (params1f.dry_run)
					continue;
				res = unlink_upload(URL, ftpname);
				msg_after_del(res, ftpname);
				metadata_delete(&wfs, true);
				if (0 != res)
					f++;
			}
			else {
				lprintf(LOG_NOTICE,
					"%sresuming rename of file %s to /%lld/ %s\n",
					msg, ftpname, wfs.parent_id, nameto);
				if (params1f.dry_run)
					continue;
				/* Save id in case will be blacklisted */
				dest_id = wfs.parent_id;
				do {
					b += blacklist_replace(&wfs, blist);
					res = rename_upload(NULL, wfs.parent_id,
							    URL, nameto);
					blist = blacklist_add(res, &wfs, blist);
				} while (-EACCES == res);
				msg_after_rename(res, ftpname
						    , wfs.parent_id, nameto);
				if (0 == res) {
					/* Restore id in case was blacklisted */
					wfs.parent_id = dest_id;
					metadata_delete(&wfs, false);
				}
				else {
					f++;
				}
			}
		}
	}
	if (0 == n && 0 == r && 0 == f_no_r && 0 == r_no_f)
		lprintf(LOG_NOTICE,
			"last shutdown was clean, nothing to resume.\n");
	else {
		if (0 != n - f)
			lprintf(LOG_NOTICE,
				"%u file(s) %shave been resumed.\n",
				n - f, (params1f.dry_run) ? "would " : "");
		if (0 != f)
			lprintf(LOG_NOTICE,
				"%u file(s) failed to be resumed.\n", f);
		if (0 != r)
			lprintf(LOG_NOTICE,
				"%u file(s) too recent to be resumed.\n", r);
		if (0 != b)
			lprintf(LOG_NOTICE,
				"%u file(s) renamed in the upload dir because the target dir is gone!\n",
				b);
	}
	free(blist);
}

static bool is_valid_ftp_random_user(const char *name)
{
	unsigned int i;
	const char *p;

	if (NULL == name)
		return false;

	if (0 != strncmp(name, ftp_user_prefix, lengthof(ftp_user_prefix)))
		return false;
	p = name + lengthof(ftp_user_prefix);
	for (i = 0; i < FTP_BASE_RANDOM_SZ; i++)
		if (!isxdigit(p[i]))
			return false;
	return ('\0' == p[i]);
}


/*
 * @brief initialises the writing process before going to daemon
 *
 * Because this is done before going to daemon and starting to serve
 * read and list requests, it must be as quick as possible.
 * The rest of the write initialisation is done in the next function.
 *
 * Although it can take dome time, the resume process is done foreground when:
 *   - dry-run specified (that is expected and what dry-run does!)
 *   - -o ro or --no-upload specified: to avoid having to start the writer_util
 *     thread for resume only. This situation should not happen often, only
 *     when 1fichierfs was used with "write" enabled then used with write
 *     disabled. The overhead is only fetching a directory, unless the
 *     'write' run was ended with files in the process of being uploaded.
 *
 * Also the function is cut in two steps because when there is a remote root
 * and the remote root is a readonly share, this is the third case where
 * the resume_upload is done foreground. This is because the remote root must
 * be fectched after the upload dir, otherwise upload dir would not be found.
 */
void write_init_foreground1(const char *remote_root)
{
	struct walk w;

	if (params1f.readonly || params1f.no_upload)
		STORE(can_write, false, relaxed);

	if (NULL != remote_root && 0 == strcmp(remote_root, upload_dir))
		lprintf(LOG_ERR,
			"Mounting the upload directory: `%s` as remote root is not allowed.\n",
			upload_dir);

	find_path(upload_dir, &w);

	if (NULL != w.res.dir) {
		if (w.is_dir) {
			set_upload_id(w.res.dir->id);
			lazy_init = false;
		}
		else {
			lprintf(LOG_WARNING,
				"writing impossible: `%s` exists as a file.\n",
				upload_dir);
			STORE(can_write, false, relaxed);
		}
	}
}

void write_init_foreground2()
{
	struct timespec t;
	uint64_t x;

	if (params1f.readonly || params1f.no_upload)
		STORE(can_write, false, relaxed);

	if (params1f.dry_run || !CAN_WRITE) {
		/* Even when write is impossible, a resume_upload makes sense!*/
		lprintf(LOG_NOTICE, "dry-run or read-only resume.\n");

		rcu_read_lock();
		resume_upload();
		rcu_read_unlock();
	}
	else {
		ftp_valid_user = params1f.ftp_user;

		if (lazy_init)
			lprintf(LOG_NOTICE,
				"being lazy: no upload directory found, write initialisation will be done only when and if necessary.\n");

		/* The base name of the FTP user and files is computed here from
		 * seconds + nano seconds to make it reasonably unique.
		 * Seconds is not enough because 1fichierfs could be started
		 * several times (eg with different remote roots) in a shell,
		 * and because it is optimised to do minimal work before going
		 * to daemon, it takes less than a second thus it is possible
		 * that two instances are started the same second.  */

		clock_gettime(CLOCK_REALTIME, &t);
		x = (((uint64_t)t.tv_sec) << 30) + (uint32_t)t.tv_nsec;
		snprintf(ftp_base	  + FTP_BASE_PREFIX_SZ,
			 sizeof(ftp_base) - FTP_BASE_PREFIX_SZ,
			 "%0" STRINGIFY(FTP_BASE_RANDOM_SZ) PRIx64, x);
	}
}

/*
 * @brief logs a notice message when write initialisation is done.
 */
static void log_notice_write_ready()
{
	lprintf(LOG_NOTICE, "writing with FTP user `%s` in directory `%s`.\n"
			  , LOAD(ftp_user_name), upload_dir);
}

/*
 * @brief check an ftp account to point to the write upload directory
 *
 * If it checks Ok, the flag ftp_account_checked is set to true.
 * If the account does not point to the right directory, or there is an
 * error, the account is deleted only when full mode.
 *
 * @param item to search.
 * @param ftp_user string in that item.
 * @param whether it is "full" init or partial
 * @return none
 */
static void check_ftp_folder_id(char *item, char *ftp_user,
				enum called_from from)
{
	char *p;
	long long folder_id;

	p = json_find_key(item, json_folder_id);
	if (NULL == p) {
		lprintf(LOG_WARNING,
			"could not find folder_id for ftp_user: `%s` [%s]\n",
			ftp_user, item);
	}
	else {
		if ( 1 == sscanf(p, "%Ld", &folder_id)) {
			if (get_upload_id() == folder_id) {
				if (NULL == ftp_valid_user)
					ftp_valid_user = ftp_user;
				STORE(ftp_user_name, ftp_user, relaxed);
				log_notice_write_ready();
				return;
			}
		}
		else {
			lprintf(LOG_WARNING,
				"error reading folder_id for ftp_user: `%s` [%s]\n",
				ftp_user, item);
		}
	}
	/* From here, check failed or there was an error: delete ftp account */
	if (FROM_CREATE == from)
		del_ftp_user(ftp_user, true);
}

/*
 * @brief try to find a valid ftp account
 *
 * Valid as in it points to the right upload directory.
 * - If ftp-user is specified, only a match will be selected
 * - Otherwise:
 *	- the first valid ftp-user will be used
 * 	- if none and a user matching the selected production is found it is
 *	  selected and reset
 *	- otherwise a pseudo-random ftp_user is created
 *
 * @param whether it is "full" init or partial
 * @return none
 */

void find_valid_ftp_account(enum called_from from)
{
static const char json_user[]= "\"user\"";
static const char json_users[]= "\"users\"";
	struct json j;
	long http_code;
	char *p, *q, *start, *pv;
	char *user, *valid = NULL;
	size_t len;

	if (WRITE_INIT_DONE)
		return;

	DEBUG("Reading the list of existing FTP users.\n");

	json_init(&j);
	http_code = curl_json_perform("", &j, FTP_USER_LS, "");
	if (HTTP_OK != http_code) {
		lprintf(LOG_ERR,
			"write disabled: could not read the list of existing ftp users, http_code=%ld\n",
			http_code);
		STORE(can_write, false, relaxed);
		json_free(&j);
		return;
	}

	for (start = json_find_key(j.pb, json_users);
	     NULL != start && NULL != (p = strstr(start, json_item_start));
	     start = q) {
		q = json_find_key(p, json_user);
		if (NULL == q) {
			lprintf(LOG_ERR,
				"write disabled: unexpected; could not find \"user\" in the json response: %s\n",
				j.pb);
			STORE(can_write, false, relaxed);
			break;
		}
		if (NULL == ftp_valid_user) {
			user= malloc(json_strlen(q, TR_NONE) + 1);
			json_strcpy(q, user, TR_NONE);
			check_ftp_folder_id(p, user, FROM_INIT);
			if (WRITE_INIT_DONE)
				break;
			if (NULL == valid && is_valid_ftp_random_user(user)) {
				pv = p;
				valid = user;
			}
			else {
				free(user);
			}
		}
		else {
			len = strlen(ftp_valid_user);
			if (0 == strncmp(ftp_valid_user, q + 1, len) &&
			    '"' == q[1 + len]) {
				check_ftp_folder_id(p, ftp_valid_user, from);
				break;
			}
		}
	}
	if (NULL != valid) {
		if (NULL == ftp_valid_user) {
			ftp_valid_user = valid;
			if (FROM_CREATE == from && CAN_WRITE
						&& !WRITE_INIT_DONE)
				check_ftp_folder_id(pv, valid, from);
		}
		else {
			free(valid);
		}
	}
	json_free(&j);
}

/* Recursive refercence writer_util / write_init_background_wrapped, so
 * declare writer_util here */
static void *writer_util(void *unused);

/*
 * @brief initialises the writing process in background
 *
 *  Called from 3 places:
 * 	- (false) unfichier_init (critical path, must be quick)
 *	- (false) writer_util
 *      - (true)  each create
 *
 *  It will never be called when can_write is false because writing is
 *  disabled and no initialisation is necessary beside resume_upload which is
 *  done in init_foreground in this case.
 *
 * The writers threads are not started here, but only when needed (get_writer)
 *
 * Note that the FTP account name has to be unique: must not already have
 * been used by someone else. Thus it is imposible to use a constant name.
 * By default, a constant prefix is used, appended with a variable string based
 * on the seconds/nanoseconds at start.
 *
 * All that is done after the program is already a daemon, to avoid
 * delays at startup. Also, at this stage, errors are ignored. If there
 * is an error, it will simply log and disable writing, but the driver
 * will stil be usable for reading and other APIs features.
 *
 * @param boolean whether to do a full init (true) or a lazy one (false).
 * @return none
 */

static void write_init_background_wrapped(enum called_from from)
{
	int err;
	long long upload_dir_id;

	/* Need to start writer_init thread? */
	if (!writer_util_started) {

		/* This happens only when 'undo' sneaked before this function
		 * was called from writer_util. Calling from it now must return
		 */
		if (FROM_UTIL == from)
			return;
		/* When calling again (from create then), must join + semaphore
		 * destroy to avoid sending posts to the wrong thread!
		 * This can't be done in init_undo as it creates a deadlock */
		if (!pthread_equal(writer_util_thread, main_thread)) {
			pthread_join(writer_util_thread, NULL);
			writer_util_thread = main_thread;
			sem_destroy(&writer_util_sem);
		}

		/* Called from create when init writer_util was un-necessary, it
		 * means the upload directory was not there, hence create it. */
		if (FROM_CREATE == from && lazy_init) {
			if (lazy_init) {
				if (0 == mkdir_upload(upload_dir,
						      upload_dir + 1,
						      0,
						      &upload_dir_id )) {
					lprintf(LOG_NOTICE,
						"upload directory: `%s` successfully created, id=%lld\n",
						upload_dir, upload_dir_id);
					set_upload_id(upload_dir_id);
				}
				else {
					/* Error creating directory = no write! */
					lprintf(LOG_WARNING,
						"write diabled! could not create upload directory: `%s`\n",
						upload_dir);
					STORE(can_write, false, relaxed);
					return;
				}
			}
		}

		/* From here upload dir exists */
		if (FROM_CREATE == from || !lazy_init) {
			SEM_INIT(&writer_util_sem, 0, 0);
			writer_util_started = true;
			err = pthread_create(&writer_util_thread,
					     NULL,
					     writer_util,
					     &from);
			if (0 != err)
				lprintf(LOG_CRIT, "creating writer_util thread: %d\n"
						, err);
		}

		/* Called from unfichier_init: rest of the initialisation done
		 * in background since it is not needed immediately */
		if (FROM_INIT == from)
			return;
	}

	/* Check if there is a valid ftp_account */
	find_valid_ftp_account(from);

	/* From there, job is done when
	 *  - not full init (whether a valid ftp was found or not!)
	 *  - error (whether full init or not!)
	 *  - a valid ftp account is found */
	if (!CAN_WRITE || WRITE_INIT_DONE)
		return;
	if (FROM_CREATE != from) {
		lprintf(LOG_NOTICE,
			"being lazy: no valid ftp account found, will finish write initialisation when and if necessary.\n");
		return;
	}

	/* Then here a full init is requested, there was no error (so far!) and
	 * no valid ftp account exists, which means create it! */
	if (NULL == ftp_valid_user) {
		ftp_valid_user = malloc(FTP_USER_RANDOM_NAME_SZ + 1);
		memcpy(ftp_valid_user,
		       ftp_user_prefix,
		       lengthof(ftp_user_prefix));
		strcpy(ftp_valid_user + lengthof(ftp_user_prefix),
		       ftp_base + FTP_BASE_PREFIX_SZ);
	}
	create_ftp_user(ftp_valid_user);
	if (CAN_WRITE)
		log_notice_write_ready();
}

static bool write_init_background(enum called_from from)
{
	if (!CAN_WRITE)
		return false;

	/* Fast return for create when init is already done + started marked */
	if (FROM_CREATE == from && LOAD(write_started, relaxed)
				&& WRITE_INIT_DONE)
		return true;

	DEBUG("write_init_background: %s\n", (from == FROM_INIT) ? "init" :
					     ((from == FROM_UTIL)? "util" : "create"));

	/* So here can_write is true and init is not already done: finish init*/
	lock(&init_write_mutex, NULL);
	rcu_read_lock();

	/* Because another thread might have done the init while waiting
	 * here on the lock, must read again CAN_WRITE and WRITE_INIT_DONE. */
	if (CAN_WRITE) {
		if (FROM_CREATE == from)
			STORE(write_started, true, relaxed);

		if (!WRITE_INIT_DONE)
			write_init_background_wrapped(from);
	}

	rcu_read_unlock();
	unlock(&init_write_mutex, NULL);

	return CAN_WRITE;
}

/*
 * @brief allows delete of the upload directory
 *
 * The upload directory can be deleted if no write operation was ever
 * done since the program started. In such case, if they were a suitable
 * ftp account, it is deleted.
 *
 * Note that is called only from unfichier_rmdir and all other tests like:
 * existence and not empty are already ok. Thus it is called only when:
 * root is / (otherwise the user can't access the upload dir) and the
 * write initialisations already found the directory.
 *
 * This is done in 2 phases, so that the stopping the writer_util thread and
 * removing the ftp user is re-created if the directory could not be removed
 * successfully. It is also necessary to include the rmdir in the lock
 * so that the state is coherent even if "create" is called just after rmdir.
 *
 * @param none.
 * @return true is delete can be done, false otherwise
 */

bool write_init_undo_1()
{
	if (LOAD(write_started))
		return false;

	lock(&init_write_mutex, NULL);
	/* As always, if another thread started writing with this is locked */
	if (LOAD(write_started)) {
		unlock(&init_write_mutex, NULL);
		return false;
	}
	else {
		if (!writer_util_started) { /* Should not happen...to be safe */
			lprintf(LOG_ERR,
				"incoherent state: when upload directory exists writer_util thread should be started.\n");
			unlock(&init_write_mutex, NULL);
			return false;
		}
		/* In case undo is called before util, ftp user is not yet
		 * found, so do it now. */
		find_valid_ftp_account(FROM_UTIL);
		if (WRITE_INIT_DONE) {
			del_ftp_user(LOAD(ftp_user_name, relaxed), false);
		}
	}
	return true;
}

void write_init_undo_2(int res)
{
	if (0 == res) {
		writer_util_started = false;
		post(&writer_util_sem);
		DEBUG("write_init_undo: pushed exit to writer_util.\n");
		STORE(ftp_user_name, NULL, relaxed);
		lazy_init = true;
		set_upload_id(UPLOAD_DIR_ID_NOT_INITIALISED);
	}
	else {
		/* rmdir failed: create the previous ftp user again if removed*/
		if (WRITE_INIT_DONE)
			create_ftp_user(ftp_valid_user);
	}
	unlock(&init_write_mutex, NULL);
}


static void post_rename(struct wfile_spec *wfs, const char *url)
{
	int res;
	char *filename, encfs_name[MAX_FILENAME_LENGTH + 1];

	lock(&wfs->wf->wmutex, NULL);

	if (0 != (LOAD(wfs->wf->counter) & FL_DEL)) {
		/* Found a file that should be deleted and ftp_dele_file could
		 * not do it: the request came too late, file is now in the
		 * upload directory */
		res = unlink_upload(url, wfs->wf->ftp_name);
		msg_after_del(res, wfs->wf->ftp_name);
		/* NOTE: nothing can realistically be done if there is
		 *       an error deleting at that stage */
		metadata_delete(wfs, true);
	}
	else {
		if (wfs->wf->has_encfs_key)
			filename = encfs_appendb64_key(encfs_name,
						       wfs->filename,
						       wfs->wf->data);
		else
			filename = wfs->filename;
		res = rename_upload(wfs->path, wfs->parent_id,
				    url, filename);
		msg_after_rename(res,
				 wfs->wf->ftp_name,
				 wfs->parent_id,
				 filename);

		if (0 != res)
			OR(wfs->wf->counter, FL_ERROR);
		else
			metadata_delete(wfs, false);
	}

	STORE(wfs->wf->step, STEP_DONE);

	unlock(&wfs->wf->wmutex, NULL);
}

static void check_upload_dir(struct wfile_list *wfl)
{
	struct dir *d;
	struct dentries *dent;
	const char *url;
	unsigned long i;

	d = refresh_upload(upload_dir, &dent);
	if (NULL == d) {
		lprintf(LOG_ERR, "problem getting listing of upload dir.\n");
		return;
	}

	DEBUG("check_upload_dir: d=%p\n", d);

	for (i = 0; i < wfl->n_files; i++) {
		url = find_file_upload(d, wfl->files[i]->wf->ftp_name);
		if (NULL != url)
			post_rename(wfl->files[i], url);
	}
}

/*
 * @brief "manual" trigger of FTP uploaded files
 *
 * Trigger is un-necessary and returns an error when the account's FTP is set
 * to "automatic". The function calls it anyway, because it would be the
 * same (in the best situation) to get the account details to check whether
 * the call is needed or not. And in case the call is needed, two calls would
 * have been made instead of one. Also, at anytime the user can switch his
 * account from "manual" to "automatic" or the opposite.
 *
 * @param none
 * @return none
 */
static void trigger_ftp()
{
	struct json j;

	DEBUG("Performing a manual FTP trigger.\n");
	json_init(&j);
	curl_json_perform("", &j, FTP_PROCESS, "");
	json_free(&j);
}


static void delay_util(struct wfile *wf, time_t now_s, time_t *delay)
{
	time_t intvl;
	if (wf->eta > now_s + CHK_UPLOAD_DIR_INTVL)
		intvl = wf->eta - now_s;
	else
		intvl = CHK_UPLOAD_DIR_INTVL;
	if (0 == *delay || *delay > intvl)
		*delay = intvl;
}

static void test_chk_up(struct wfile *wf, time_t now_s, bool *fchk_up) {
	if (now_s >= wf->eta)
		*fchk_up = true;
}

static void test_trig(struct wfile *wf, time_t now_s, bool *ftrig) {
	if (now_s < wf->eta)
		return;

	STORE(wf->step, STEP_WAIT_MV);
	wf->eta  = now_s + TIME_PROCESS_FTP + (wf->size / GIGA) * TIME_PER_GB;
	*ftrig = true;
}

static void test_del(struct wfile_spec *wfs) {
	if (0 == (FL_DEL & LOAD(wfs->wf->counter)))
		return;

	if (0 == ftp_dele_file(wfs->wf)) {
		/* Since this is called first in the loop, it is possible
		 * that the resume name is not even created yet, if so
		 * there is no need deleting it! */
		if (RES_NOT_INITIALIZED != wfs->wf->r.id1)
			metadata_delete(wfs, false);
		STORE(wfs->wf->step, STEP_DONE);
	}
	else {
		metadata_rename(wfs, NULL);
		STORE(wfs->wf->step, STEP_WAIT_RM);
	}
}

static void create_resume_dir(struct wfile_spec *wfs) {
	if (0 != (FL_DEL & LOAD(wfs->wf->counter)))
		return;

	metadata_create(wfs);
}

static time_t post_actions(void)
{
	struct wfile_list *wfl;
	bool   ftrig, fchk_up;
	struct timespec now;
	time_t delay;
	unsigned long i;

	wfl = LOAD(wflist);
	if (NULL == wfl)
		return 0;

	/*
	 * Start looking if resume directories need to be created
	 */
	for (i = 0; i < wfl->n_files; i++)
		if (STEP_WAIT_FTP == LOAD(wfl->files[i]->wf->step) &&
		    RES_NOT_INITIALIZED == wfl->files[i]->wf->r.id1)
			create_resume_dir(wfl->files[i]);

	/*
	 * First, look if some files need be deleted
	 */
	for (i = 0; i < wfl->n_files; i++)
		switch (LOAD(wfl->files[i]->wf->step)) {
			case STEP_WRITING:
			case STEP_DONE:		break;
			case STEP_WAIT_FTP:
			case STEP_WAIT_MV:	test_del(wfl->files[i]);
			case STEP_WAIT_RM:	break;
		}

	/*
	 * Second, look if need: trigger FTP, and check upload directory
	 */
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	ftrig = fchk_up = false;
	for (i = 0; i < wfl->n_files; i++)
		switch (LOAD(wfl->files[i]->wf->step)) {
			case STEP_WRITING:
			case STEP_DONE:		break;
			case STEP_WAIT_FTP:	test_trig(wfl->files[i]->wf,
							  now.tv_sec,
							  &ftrig);
			case STEP_WAIT_MV:
			case STEP_WAIT_RM:	test_chk_up(wfl->files[i]->wf,
							    now.tv_sec,
							    &fchk_up);
						break;
		}

	/*
	 * Accordingly, do trigger FTP, and check upload directory if necessary
	 */
	if (ftrig)
		trigger_ftp();
	if (fchk_up)
		check_upload_dir(wfl);

	/*
	 * Last compute time to wait before next iteration of this function
	 * Get the clock again in case a lot of move/delete where done.
	 */
	delay = 0;
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	for (i = 0; i < wfl->n_files; i++)
		switch (LOAD(wfl->files[i]->wf->step)) {
			case STEP_WRITING:
			case STEP_DONE:		break;
			case STEP_WAIT_FTP:
			case STEP_WAIT_MV:
			case STEP_WAIT_RM:	delay_util(wfl->files[i]->wf,
							  now.tv_sec,
							  &delay);
						break;
		}
	return delay;
}


static void exit_util_loop_msgs()
{
	struct wfile_list *wfl;
	unsigned long i;
	enum w_step step;
	unsigned long counter;

	wfl = LOAD(wflist);
	if (NULL == wfl)
		return;

	for (i = 0; i < wfl->n_files; i++) {
		step = LOAD(wfl->files[i]->wf->step);
		if (STEP_DONE == step)
			continue;
		counter = LOAD(wfl->files[i]->wf->counter);
		/* Zero size file on the list are either empty files copied
		 * (they stay until the driver is closed) or files deleted
		 * before a byte was written. Don't warn on that since there
		 * was no writing on the server */
		if (0 !=  LOAD(wfl->files[i]->wf->size)) {
			if (0 != (FL_DEL & counter))
				lprintf(LOG_WARNING,
					"ATTENTION: FTP file %s%s to be deleted /%lld/ `%s` was not processed completely!\n",
					(0 == (FL_ERROR & counter)) ? "": "with ERROR ",
					wfl->files[i]->wf->ftp_name,
					wfl->files[i]->parent_id,
					wfl->files[i]->path);
			else
				lprintf(LOG_WARNING,
					"ATTENTION: FTP file %s%s to be renamed to /%lld/ `%s` was not processed completely and could be lost!\n",
					(0 == (FL_ERROR & counter)) ? "": "with ERROR ",
					wfl->files[i]->wf->ftp_name,
					wfl->files[i]->parent_id,
					wfl->files[i]->path);
		}
	}
}


/*
 * @brief utility thread for writing: does the "post processing" (see head note)
 *
 * First it initialises the rest of what is necessary to write. It is not
 * done before starting the daemon to avoid start delays.
 *
 * Here API errors are ignores. The worst effect would then be to disable
 * writing, but the program will continue to work for reading.
 *
 * @param none
 * @return none
 */

static void *writer_util(void *unused)
{
	struct timespec until;
	time_t delay = 0;

	rcu_register_thread();

	rcu_read_lock();
	resume_upload();
	rcu_read_unlock();

	write_init_background(FROM_UTIL);

	do {
		errno = 0;
		if (0 == delay) {
			wait(&writer_util_sem);
		}
		else {
			clock_gettime(CLOCK_REALTIME, &until);
			until.tv_sec += delay;
			timedwait(&writer_util_sem, &until);
		}

		if (!writer_util_started) /* write_init_undo triggered that */
			return NULL;

		rcu_read_lock();
		delay = post_actions();
		rcu_read_unlock();

		DEBUG(	"writer_util: delay=%lu.\n",
			(unsigned long)delay);
	} while(!LOAD(exiting));

	rcu_read_lock();
	/* Exit might have been received when in the loop, so more actions
	 * could be needed now. */
	if (0 == trywait(&writer_util_sem)) {
		DEBUG("writer_util: additional actions after exit.\n");
		post_actions();
	}
	exit_util_loop_msgs();
	rcu_read_unlock();

	rcu_unregister_thread();
	return NULL;
}

/*
 * @brief destroy callback for fuse: "write part"
 *
 * Ends the writing threads (if any) and the utility threads, and free memory.
 *
 * @param none
 * @return none
 */
void write_destroy()
{
	void * ret;
	unsigned int i;
	struct wfile_list *wfl;

	DEBUG("write_destroy: starts.\n");

	sem_destroy(&sem_writer);

	/* Write can be Ok then broken, hence writers + util can always exist */
	for (i=0; i< MAX_WRITERS && NULL != writers[i].buf; i++) {
		OR(writers[i].p_signal, P_SIG_EXIT);
		post(&writers[i].rsem);
		pthread_join(writers[i].thread, &ret);
		sem_destroy(&writers[i].wsem);
		sem_destroy(&writers[i].rsem);
		free(writers[i].buf);
		lprintf(LOG_NOTICE, "<< thread writer[%u] ended.\n", i);
	}
	if (!pthread_equal(writer_util_thread, main_thread)) {
		if (writer_util_started) {
			post(&writer_util_sem);
			DEBUG("write_destroy: pushed exit to write_util.\n");
		}
		pthread_join(writer_util_thread, &ret);
		sem_destroy(&writer_util_sem);
	}
	wfl = LOAD(wflist);
	DEBUG("write_destroy: free wfiles: %p %lu.\n", wfl,
		(unsigned long)((NULL == wfl) ? 0 : wfl->n_files));
	if (NULL != wfl) {
		for (i = 0; i < wfl->n_files; i++) {
			pthread_mutex_destroy(&wfl->files[i]->wf->wmutex);
			if (NULL != wfl->files[i]->wf->data)
				free(wfl->files[i]->wf->data);
			free(wfl->files[i]->wf);
			free(wfl->files[i]);
		}
		free(wfl);
		STORE(wflist, NULL);
	}
	if (ftp_valid_user != params1f.ftp_user)
		free(ftp_valid_user);
}

#include "1fichierfs_read.h"
static const struct aw_operations unfichier_aw_oper = {
	.aligned_alloc	= NULL,
	.free		= NULL,
	.release	= NULL,
	.log		= lprintf,
	.error		= astreamfs_error,
	.anticipation	= NULL,
	.retry		= NULL,
	.close_if_idle  = unfichier_close_if_idle,
	.store_last_opt	= astreamfs_store_last_opt,
	.load_last_opt	= astreamfs_load_last_opt,
	.fh_from_fi	= astreamfs_fh_from_fi,
	.start_stream	= unfichier_start_stream,
	.read		= astreamfs_read_callback,
};

/*
 * @brief init callback for fuse
 *
 * Using astreamfs common code to initialise readers, plus starts the post_ftp
 * thread for writing if needed.
 *
 * @param see fuse documentation
 * @return none
 */
void *unfichier_init(struct fuse_conn_info *conn_unused)
{
	int  i;
	(void)conn_unused;

	/* To be sure write_destroy will have initialized values, these
	 * initialisations are done even if not CAN_WRITE */
	SEM_INIT(&sem_writer, 0, MAX_WRITERS);
	for (i = 0; i < MAX_WRITERS; i++) {
		writers[i].buf  = NULL;
		STORE(writers[i].idle, true);
	}
	writer_util_thread =
	main_thread	   = pthread_self();

	/* can_write is true unless set to false by write_init_foreground */
	if (CAN_WRITE)
		write_init_background(FROM_INIT);

	engine_info.n_threads = params.threads;
	return worker_init(&unfichier_aw_oper, &engine_info);
}
