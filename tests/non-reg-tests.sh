#
# 1fichierfs: non regression tests
#
# Copyright (C) 2018-2021  Alain BENEDETTI <alainb06@free.fr>
#
# License:
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#! /bin/sh

#====================
# Sourcing parameters
# They are searched in a directory having the same name as this shell
# but replacing .sh with .config (actually replaces the last extension with
# config or appends .config if no extension)
#
# All the files in that directory will be sourced.
# Look at the example provided to define your own parameters.

CONFIG_DIR="$(echo "${0}" | sed 's/\(.*\)\(\.\|$\).*/\1\.config/')"

SOURCES="$( find "${CONFIG_DIR}" -maxdepth 1 -type f 2>/dev/null )"
if [ "${?}" -ne 0 ]; then
	echo "Did not find the parameter directory: ${CONFIG_DIR}"
	exit 1
fi
if [ -z "${SOURCES}" ]; then
	echo "Empty parameter directory: ${CONFIG_DIR}"
	exit 1
fi

for file in "${CONFIG_DIR}/"* ; do
  if [ -f "$file" ] ; then
    . "$file"
  fi
done

# Test if mandatory variable were provided
if [ -z "${TEST_ROOT}" ]	||
   [ -z "${TEST_SMALL_FILE}" ]	||
   [ -z "${TEST_PATH}" ]	;  then
	echo "Variables TEST_ROOT=\"${TEST_ROOT}\", TEST_SMALL_FILE=\"${TEST_SMALL_FILE}\", TEST_PATH=\"${TEST_PATH}\" must be defined, look at the documentation"
	exit 1
fi
#====================


if [ -n "${WORKING_DIR}" ]; then
	cd "${WORKING_DIR}" 2>/dev/null || {
		echo "Cannot change to working directory: ${WORKING_DIR} (WORKING_DIR=\"${WORKING_DIR}\")"
		exit 1
	}
fi


#====================
# Computing parts of the command line

if [ -n "${API_KEY}" ]; then
	API_KEY_OPT="--api-key='${API_KEY}'"
else
	API_KEY_OPT=''
fi

RUN_VALGRIND="$( which valgrind )"
if [ $? -eq 0 ] && [ -z "${USE_VALGRIND}" ]; then
	RUN_VALGRIND="${RUN_VALGRIND} --leak-check=full"
else
	RUN_VALGRIND=''
fi

if [ -n "${STAT_FILE}" ]; then
	STAT_OPT="--stat-file='${STAT_FILE}'"
else
	STAT_OPT=''
fi

if [ -n "${REFRESH_FILE}" ]; then
	REFRESH_OPT="--refresh-file='${REFRESH_FILE}'"
else
	REFRESH_OPT=''
fi

CMD="${RUN_VALGRIND} ${EXEC} ${API_KEY_OPT} --log-level=${LOG_LEVEL} ${STAT_OPT} ${REFRESH_OPT} ${OTHER_OPTIONS}"
#====================

#=======================
# Expected error strings
EACCES='Permission denied'
EBUSY='Device or resource busy'
EEXIST='File exists'
ENAMETOOLONG='File name too long'
EILSEQ='Invalid or incomplete multibyte or wide character'
ENOTEMPTY='Directory not empty'

#====================
# Other misceallaneous variables

UPLOAD_DIR="${MOUNT_PATH}/.upload.1fichierfs"

TMP_1="$(mktemp -d)"
TMP_ND1="$(printf '%s' "${TMP_1}" | sed 's|.*/||')"
TMP_ND2="$(mktemp -u | sed 's|.*/||')"
TMP_ND3="$(mktemp -u | sed 's|.*/||')"
TMP_ND4="$(mktemp -u | sed 's|.*/||')"
TMP_NF1="$(mktemp -u | sed 's|.*/||')"
TMP_NF2="$(mktemp -u | sed 's|.*/||')"
TMP_NF3="$(mktemp -u | sed 's|.*/||')"

LONGEST_NAME="123456789112345678921234567893123456789412345678951234567896123456789712345678981234567899123456789a123456789b123456789c123456789d123456789e123456789f123456789g123456789h123456789i123456789j123456789k123456789l123456789m123456789n123456789o123456789p"
LONG1="123456789a123456789b123456789b123456789d123456789e123456789f123456789g123456789h123456789i123456789j123456789k123456789l123456789m123456789n123456789o123456789p123456789q123456789r123456789s12345"
LONG2="123456789u123456789v123456789w123456789x123456"
UTF8_4="桒"
ALL_OK_CHARS="$( printf " \x21_#_%%&_()*+,-._0123456789:;_=_?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[_]^__abcdefghijklmnopqrstuvwxyz{|}~\x7f")"
ALL_CHK="$( printf " \x21_#_%%&_...+,-._0123456789:;_=_?@ABCDEFGHIJKLMNOPQRSTUVWXYZ._.^__abcdefghijklmnopqrstuvwxyz{|}~\x7f")"
ESCAPES="$( printf "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F" )"
RAW_NAME="I＜3＄"
TR_NAME="I<3$"
SMALL_FILE_CONTENT="This is a test writing a 'small file'."
WAIT_TIME=$(( WAIT_TIME + BIG_FILE_SIZE / 1024 * 27 ))

BOLD="$(tput bold)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
STD="$(tput sgr 0)"

TEST_NUM=1
#====================

cleanup()
{
#	echo "Cleaning up the test files"
#	rm -r "${TMP_1}"
#	rm -r "${TEST_DIR}${TMP_ND1}"
	echo "TMP_ND1=${TMP_ND1}"
	echo "TMP_ND2=${TMP_ND2}"
	echo "TMP_ND3=${TMP_ND3}"
	echo "TMP_ND4=${TMP_ND4}"
	echo "TMP_NF1=${TMP_NF1}"
	echo "TMP_NF2=${TMP_NF2}"
	echo "TMP_NF3=${TMP_NF3}"
}

errcmd()
{
	ERR=$?
	printf "${BOLD}${RED}[Error]${STD} %d on command %s during: Test %03d - %s\n" $ERR "${1}" $TEST_NUM "${2}"
	cleanup
	exit $ERR
}

errchk()
{
	printf "${BOLD}${RED}[Error]${STD} checking result of command %s during: Test %03d - %s\n" "${1}" $TEST_NUM "${2}"
	cleanup
	exit
}

failchk()
{
	printf "${BOLD}${RED}[Error]${STD} command %s should have failed and didn't during: Test %03d - %s\n" "${1}" $TEST_NUM "${2}"
	cleanup
	exit
}

chkmsg()
{
	printf '%s' "${3}" | grep -q "${4}" ||
		printf "Expected error: %s.\nGot: %s, during: %10s   Test %03d - %s\n" "${4}"  "${3}" "${1}" $TEST_NUM "${2}"
}

oktest()
{
	printf "${BOLD}${GREEN}[OK]${STD} %10s   Test %03d - %s\n" "${1}" $TEST_NUM "${2}"
	TEST_NUM=$(( TEST_NUM + 1 ))
}

skiptest()
{
	printf "${BOLD}[--]${STD} %10s   Test %03d - %s\n" "${1}" $TEST_NUM "${2}"
	TEST_NUM=$(( TEST_NUM + 1 ))
}

# @brief Do the standard test, it is expected to succeed
#
# - Issue the command with parameters
# - Test the return code
# - Test the result looking at the directory
# - Display the OK message if all is correct or previous step would have caught
#   an error, displayed it and exited.
#
# @param 1: the command name
# @param 2: the message to display for this test
# @param 3: -n or -z according to what is needed to test (existence/absence)
# @param 4: directory (not prefixed with TEST_DIR) to test
# @param 5: file/dir to test in that directory
# @param 6+: command specific extra parameters.
std_test()
{
	case "${1}" in
		mkdir)
			mkdir "${TEST_DIR}${4}/${5}"
			DIR="^d.*"
			;;
		rmdir)
			rmdir "${TEST_DIR}${4}/${5}"
			DIR="^d.*"
			;;
		rm)
			rm "${TEST_DIR}${4}/${5}"
			DIR=""
			;;
		ln)
			ln  "${6}" "${TEST_DIR}${4}/${5}"
			DIR=""
			;;
		mv)
			mv "${TEST_DIR}${6}"  "${TEST_DIR}${4}/${5}"
			DIR="${7}"
			;;
	esac
	[ $? = 0 ] ||	errcmd "${1}" "${2}"
	[ ${3} "$(ls -l  "${TEST_DIR}${4}" | sed -n "/${DIR}${5}/p")" ] || \
			errchk "${1}" "${2}"
	oktest "${1}" "${2}"
}


MSG_MKDIR="mkdir simple directory"
MSG_RM="delete file"
MSG_RMDIR="rmdir simple directory"

#--- status of upload: lazy or ftp user is there
test_00()
{
	THIS_TEST="get upload status"
	THIS_CMD="grep"
	LOOP=0
	while [ ${LOOP} -lt 10 ]; do
		grep -q 'writing with FTP user\|being lazy' "${LOG_FILE}" && break
		LOOP=$(( LOOP + 1 ))
		sleep 2
	done
	grep -q 'writing with FTP user\|being lazy' "${LOG_FILE}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"

	FTP_ACCOUNT="$(sed  -n '/writing with FTP user/{s/.*user `//;s/` .*//p}' "${LOG_FILE}" )"
	THIS_CMD="sed"
	if [ -z "${FTP_ACCOUNT}" ]; then
		THIS_TEST="No upload environment (lazy)."
	else
		THIS_TEST="FTP account: ${FTP_ACCOUNT}"
	fi
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_01()
{
	std_test "mkdir" "${MSG_MKDIR}" "-n" "" "${TMP_ND1}"
}

#----------
test_02()
{
	std_test "ln" "link a file: different name, different directory" \
		 "-n" "${TMP_ND1}" "${TMP_NF1}" "${TEST_FILE}"
}
#----------
test_02_1()
{
	std_test "ln" "link a file: different name, different directory" \
		 "-n" "${TMP_ND1}" "${TMP_NF2}" "${TEST_FILE}"
}

#----------
# From 1.8.3 rename to existing file should now work instead of failing.
test_02_2()
{
	std_test "mv" "rename file: to existing file" \
		 "-n" "${TMP_ND1}" "${TMP_NF1}" "${TMP_ND1}/${TMP_NF2}" ""

}

#----------
test_02_3()
{
	std_test "rm" "${MSG_RM}" "-z" "${TMP_ND1}" "${TMP_NF2}"
}

#----------
test_03()
{
	std_test "ln" "link a file: different name, same directory" \
		 "-n" "${TMP_ND1}" "${TMP_NF2}" "${TEST_DIR}${TMP_ND1}/${TMP_NF1}"
}

#----------
test_04()
{
	std_test "ln" "link a file: longest name, same directory" \
		 "-n" "${TMP_ND1}" "${LONGEST_NAME}" "${TEST_DIR}${TMP_ND1}/${TMP_NF1}"
}

#----------
test_05()
{
	THIS_TEST="link a file: name too long, same directory"
	THIS_CMD="ln"
	MSG="$(LC_ALL=C ln "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TEST_DIR}${TMP_ND1}/A${LONGEST_NAME}" 2>&1)"  && \
					failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${ENAMETOOLONG}"
	[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/A${LONGEST_NAME}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_06()
{
	THIS_TEST="link a file: name with escapes, same directory"
	THIS_CMD="ln"
	ln "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TEST_DIR}${TMP_ND1}/${ESCAPES}${ALL_OK_CHARS}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${ALL_CHK}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}


#----------
test_07()
{
	THIS_TEST="delete files"
	THIS_CMD="rm"

	std_test "rm" "${THIS_TEST}" "-z" "${TMP_ND1}" "${LONGEST_NAME}"

	rm "${TEST_DIR}${TMP_ND1}/${ESCAPES}${ALL_OK_CHARS}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${ALL_CHK}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_08()
{
	THIS_TEST="mkdir with leading white space"
	THIS_CMD="mkdir"

	if [ "${1}" = "fail" ]; then
		MSG="$(LC_ALL=C mkdir "${TEST_DIR}${TMP_ND1}/  ${TMP_ND2}" 2>&1)" && \
					failchk "${THIS_CMD}" "${THIS_TEST}"
		chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EILSEQ}"
		[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/^d.*${TMP_ND2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	else
		mkdir "${TEST_DIR}${TMP_ND1}/  ${TMP_ND2}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
		[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/^d.*${TMP_ND2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"

	fi
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_08_1()
{
	THIS_TEST="${MSG_RMDIR}"
	THIS_CMD="rmdir"
	rmdir "${TEST_DIR}${TMP_ND1}/  ${TMP_ND2}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/^d.*${TMP_ND2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}


#----------
test_09()
{
	std_test "mkdir" "${MSG_MKDIR}" "-n" "${TMP_ND1}" "${TMP_ND2}"
}

#----------
test_10()
{
	std_test "mkdir" "${MSG_MKDIR}" "-n" "${TMP_ND1}" "${TMP_ND3}"
}

#----------
test_11()
{
	std_test "mv" "mv directory different name different parent directory" \
		 "-n" "${TMP_ND1}/${TMP_ND3}" "${TMP_ND4}" "${TMP_ND1}/${TMP_ND2}" "^d.*"
}

#----------
test_12()
{
	std_test "mv" "mv directory different name same parent directory" \
		 "-n" "${TMP_ND1}/${TMP_ND3}" "${TMP_ND2}" "${TMP_ND1}/${TMP_ND3}/${TMP_ND4}" "^d.*"
}

#----------
test_13()
{
	std_test "mv" "mv directory same name different parent directory" \
		 "-n" "${TMP_ND1}" "${TMP_ND2}" "${TMP_ND1}/${TMP_ND3}/${TMP_ND2}" "^d.*"
}

#----------
test_14()
{
	std_test "rmdir" "${MSG_RMDIR}" "-z" "${TMP_ND1}" "${TMP_ND3}"
}

#----------
test_15()
{
	std_test "ln" "link a file: same name, different directory" \
		 "-n" "${TMP_ND1}/${TMP_ND2}" "${TMP_NF1}" "${TEST_DIR}${TMP_ND1}/${TMP_NF1}"
}

#----------
test_16()
{
	THIS_TEST="read files"
	THIS_CMD="diff"
	diff -q "${TEST_DIR}${TMP_ND1}/${TMP_ND2}/${TMP_NF1}" "${TEST_FILE}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_17()
{
	std_test "rm" "${MSG_RM}" "-z" "${TMP_ND1}" "${TMP_NF1}"
	std_test "rm" "${MSG_RM}" "-z" "${TMP_ND1}" "${TMP_NF2}"
}

#----------
test_18()
{
	std_test "mv" "rename file: same name, different directory" \
		 "-n" "${TMP_ND1}" "${TMP_NF1}" "${TMP_ND1}/${TMP_ND2}/${TMP_NF1}" ""
}

#----------
test_19()
{
	THIS_TEST="rename file: special characters"
	THIS_CMD="mv"
	if [ "${1}" = "fail" ]; then
		MSG="$(LC_ALL=C mv "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TEST_DIR}${TMP_ND1}/ab\\c${TMP_NF2})"  2>&1)" && \
					failchk "${THIS_CMD}" "${THIS_TEST}"
		chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EILSEQ}"
		[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/ab\\c${TMP_NF2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	else
		mv "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TEST_DIR}${TMP_ND1}/ab\\c${TMP_NF2}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
		[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/ab\\\\c${TMP_NF2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	fi
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_19_1()
{
	std_test "mv" "rename file: removing special characters" \
		 "-n" "${TMP_ND1}" "${TMP_NF1}" "${TMP_ND1}/ab\\c${TMP_NF2}" ""
}

#----------
test_20()
{
	std_test "mv" "rename file: different name, same directory" \
		 "-n" "${TMP_ND1}" "${TMP_NF2}" "${TMP_ND1}/${TMP_NF1}" ""
}

#----------
test_21()
{
	std_test "mv" "rename file: different name, different directory" \
		 "-n" "${TMP_ND1}/${TMP_ND2}" "${TMP_NF1}" "${TMP_ND1}/${TMP_NF2}" ""
}

#----------
test_22()
{
	THIS_TEST="delete directory and files"
	THIS_CMD="rm -r"
	rm -r "${TEST_DIR}${TMP_ND1}/${TMP_ND2}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/^d.*${TMP_ND2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_23()
{
	THIS_TEST="touch file"
	THIS_CMD="touch"
	touch "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${TMP_NF1}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_24()
{
	std_test "rm" "rm empty file" "-z" "${TMP_ND1}" "${TMP_NF1}"
}


#----------
test_25()
{
	THIS_TEST="echo > file"
	THIS_CMD="echo"
	printf "%s" "${SMALL_FILE_CONTENT}" > "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${TMP_NF1}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_25_0()
{
	THIS_TEST="small file upload exception"
	THIS_CMD="grep"
	grep -q "${SMALL_FILE_CONTENT}" "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_25_1()
{
	THIS_TEST="ls: wait for resume info"
	THIS_CMD="ls"
	LOOP=0
	while [ -z "$(ls -l  "${UPLOAD_DIR}" | sed -n "/${1}/p")" ]; do
		if [ $LOOP = 60 ]; then
			errchk "${THIS_CMD}" "${THIS_TEST}"
		fi
		LOOP=$(( LOOP + 1 ))
		sleep 1
	done
	oktest  "${THIS_CMD}" "${THIS_TEST} ($LOOP)"
}

#----------
test_25_2()
{
	THIS_TEST="check small write"
	THIS_CMD="cat"
	CONTENT="$(cat "${TEST_DIR}${TMP_ND1}/${TMP_NF1}")" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ "${SMALL_FILE_CONTENT}" =  "${CONTENT}" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_26()
{
	THIS_TEST="mv created file"
	THIS_CMD="mv"
	mv "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${TMP_NF2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_27()
{
	std_test "rm" "rm created file" "-z" "${TMP_ND1}" "${TMP_NF2}"
}

#----------
test_27_1()
{
	THIS_TEST="ls: wait deletion of resume info"
	THIS_CMD="ls"
	LOOP=0
	while [ -n "$(ls -l  "${UPLOAD_DIR}" | sed -n "/${1}/p")" ]; do
		if [ $LOOP = 20 ]; then
			errchk "${THIS_CMD}" "${THIS_TEST}"
		fi
		LOOP=$(( LOOP + 1 ))
		sleep 1
	done
	oktest  "${THIS_CMD}" "${THIS_TEST} ($LOOP)"
}

#----------
test_28()
{
	THIS_TEST="echo > file + mv"
	THIS_CMD="echo; mv"
	echo "This is a test" > "${TEST_DIR}${TMP_ND1}/${TMP_NF1}"; mv "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${TMP_NF2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_29()
{
	THIS_TEST="echo > file + rm"
	THIS_CMD="echo; rm"
	echo "This is a test" > "${TEST_DIR}${TMP_ND1}/${TMP_NF1}"; rm "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${TMP_NF1}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_30()
{
	THIS_TEST="echo | tee: double creation"
	THIS_CMD="echo | tee"
	echo "This is a test" | tee "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" > "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${TMP_NF1}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${TMP_NF2}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_31()
{
	std_test "rm" "rm created files" "-z" "${TMP_ND1}" "${TMP_NF1}"
	std_test "rm" "rm created files" "-z" "${TMP_ND1}" "${TMP_NF2}"
}

#----------
test_32()
{
	THIS_TEST="echo: long name with UTF8-4"
	THIS_CMD="echo"
	NAME="${LONG1}6789${UTF8_4}${LONG2}"
	echo "This is a test" > "${TEST_DIR}${TMP_ND1}/${NAME}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${NAME}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_33()
{
	NAME1="${LONG1}6789${UTF8_4}${LONG2}"
	std_test "mv" "mv: long name to short name" \
		 "-n" "${TMP_ND1}" "${TMP_NF1}" "${TMP_ND1}/${NAME1}" ""
}

#----------
test_34()
{
	NAME1="${LONG1}6${UTF8_4}${LONG2}"
	std_test "mv" "mv: short name to long name" \
		 "-n" "${TMP_ND1}" "${NAME1}" "${TMP_ND1}/${TMP_NF1}" ""
}


test_35_0()
{
	std_test "mv" "${1}" "-n" \
		 "${TMP_ND1}" "${NAME2}" "${TMP_ND1}/${NAME1}" ""
}

#----------
test_35()
{
	NAME2="${LONG1}67${UTF8_4}${LONG2}"
	test_35_0 "mv: long name change part 1"
}

#----------
test_36()
{
	NAME1="${NAME2}"
	NAME2="${LONG1}67${UTF8_4}${LONG2}7"
	test_35_0 "mv: long name change part 2"
}

#----------
test_37()
{
	NAME1="${NAME2}"
	NAME2="${LONG1}6789r${UTF8_4}${LONG2}"
	test_35_0 "mv: long name change both parts"
}

#----------
test_38()
{
	std_test "rm" "rm: delete both parts" "-z" "${TMP_ND1}" "${NAME2}"
}

#----------
test_39()
{
	THIS_TEST="create big file: raw name"
	THIS_CMD="cp"
	if [ "${1}" = "fail" ]; then
		MSG="$(LC_ALL=C cp "${TMP_1}/${TMP_NF3}" "${TEST_DIR}${TMP_ND1}/${RAW_NAME}" 2>&1)" && \
					failchk "${THIS_CMD}" "${THIS_TEST}"
		chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EILSEQ}"
		[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${RAW_NAME}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	else
		cp "${TMP_1}/${TMP_NF3}" "${TEST_DIR}${TMP_ND1}/${RAW_NAME}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
		[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${RAW_NAME}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	fi
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_40()
{
	THIS_TEST="create big file: translated name"
	THIS_CMD="cp"
	if [ "${1}" = "fail" ]; then
		MSG="$(LC_ALL=C cp "${TMP_1}/${TMP_NF3}" "${TEST_DIR}${TMP_ND1}/${TR_NAME}" 2>&1)" && \
					failchk "${THIS_CMD}" "${THIS_TEST}"
		chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EILSEQ}"
		[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n '/I<3\$/p')" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	else
		cp "${TMP_1}/${TMP_NF3}" "${TEST_DIR}${TMP_ND1}/${TR_NAME}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
		[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n '/I<3\$/p')" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	fi
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_41()
{
	T_END="$( date +"%s" )"
	DELTA=$(( T_END - T_START ))
	if [ $DELTA -lt $WAIT_TIME ]; then
		WAIT=$(( WAIT_TIME - DELTA ))
	else
		WAIT=0
	fi
	printf "It took %d seconds, need to wait another %d seconds\n" ${DELTA} ${WAIT}
	if [ $WAIT -ne 0 ]; then
		printf "Sleeping"
		T0=0;
		while [ $T0 -lt $WAIT ]; do
			sleep 10
			T0=$(( T0 + 10 ))
			printf "."
		done
		printf "\n"
	fi
	THIS_TEST="checking uploaded file"
	THIS_CMD="diff"
	diff -q "${TMP_1}/${TMP_NF3}" "${1}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_42()
{
	THIS_TEST="raw name to translated"
	THIS_CMD="ls"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n '/I<3\$/p')" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_43()
{
	THIS_TEST="delete big uploaded file"
	THIS_CMD="rm"
	rm "${TEST_DIR}${TMP_ND1}/${TR_NAME}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -z "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n '/I<3\$/p')" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_44()
{
	THIS_TEST="translated to raw name"
	THIS_CMD="ls"
	[ -n "$(ls -l  "${TEST_DIR}${TMP_ND1}" | sed -n "/${RAW_NAME}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_45()
{
	std_test "rm" "delete big uploaded file" "-z" "${TMP_ND1}" "${RAW_NAME}"
}

#----------
test_46()
{
	THIS_TEST="${MSG_RMDIR}"
	THIS_CMD="rmdir"
	rmdir "${TEST_DIR}${TMP_ND1}" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ -z "$(ls -l  "${TEST_DIR}" | sed -n "/^d.*${TMP_ND1}/p")" ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_49()
{
	THIS_TEST="check memory leak"
	THIS_CMD="grep ${LOG_FILE}"
	if [ -z "${STAT_FILE}" ] || [ -z "${LOG_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	tail -n3 "${LOG_FILE}" | grep -q 'Difference :      0      0' || \
				errcmd "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_50()
{
	THIS_TEST="cat stat file"
	THIS_CMD="cat"
	if [ -z "${STAT_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	cat "${MOUNT_PATH}/${STAT_FILE}" 2>/dev/null | grep -q 'Uptime:' || \
				errcmd "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_51()
{
	THIS_TEST="remove stat file"
	THIS_CMD="rm"
	if [ -z "${STAT_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C rm "${MOUNT_PATH}/${STAT_FILE}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_52_0()
{
	THIS_TEST="${1} stat file as source"
	THIS_CMD="${1}"
	if [ -z "${STAT_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C ${1} "${MOUNT_PATH}/${STAT_FILE}" "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_52()
{
	test_52_0 'ln'
	test_52_0 'mv'
}


#----------
test_60()
{
	THIS_TEST="existence of refresh file"
	THIS_CMD="test"
	if [ -z "${REFRESH_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	if [ "${1}" = "fail" ]; then
		[ ! -e "${MOUNT_PATH}/${REFRESH_FILE}" ] || \
				errchk "${THIS_CMD}" "${THIS_TEST}"
	else
		[ -f "${MOUNT_PATH}/${REFRESH_FILE}" -a ! -s "${MOUNT_PATH}/${REFRESH_FILE}" ] || \
				errcmd "${THIS_CMD}" "${THIS_TEST}"
	fi
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_61()
{
	THIS_TEST="remove refresh file"
	THIS_CMD="rm"
	if [ -z "${REFRESH_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C rm "${MOUNT_PATH}/${REFRESH_FILE}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_62_0()
{
	THIS_TEST="${1} refresh file as source"
	THIS_CMD="${1}"
	if [ -z "${REFRESH_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C ${1} "${MOUNT_PATH}/${REFRESH_FILE}" "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_62()
{
	test_62_0 'ln'
	test_62_0 'mv'
}

#----------
test_64_0()
{
	THIS_TEST="${1} refresh file as target"
	THIS_CMD="${1}"
	if [ -z "${REFRESH_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C "${1}" "${2}" "${MOUNT_PATH}/${REFRESH_FILE}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${3}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_64()
{
	test_64_0 'ln' "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" "${EACCESS}"
	test_64_0 'ln' "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${EBUSY}"
	test_64_0 'mv' "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" "${EACCESS}"
	test_64_0 'mv' "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${EACCESS}"
}

test_65()
{
	test_64_0 'mv' "${TEST_DIR}${TMP_ND1}" "${EACCESS}"
}

#----------
test_66()
{
	THIS_TEST="mkdir refresh file"
	THIS_CMD="mkdir"
	if [ -z "${REFRESH_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C mkdir "${MOUNT_PATH}/${REFRESH_FILE}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_67()
{
	THIS_TEST="create refresh file"
	THIS_CMD="printf"
	if [ -z "${REFRESH_FILE}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C sh -c "printf '%s' '${SMALL_FILE_CONTENT}' >'${MOUNT_PATH}/${REFRESH_FILE}'" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_70()
{
	THIS_TEST="removing upload directory"
	THIS_CMD="rmdir"
	UP_CLEAN='N'
	if [ -z "${RM_UPLOAD_DIR_TEST}" ] && [ -n "${FTP_ACCOUNT}" ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	if [ -d "${UPLOAD_DIR}" ]; then
		MSG="$(LC_ALL=C rmdir "${UPLOAD_DIR}" 2>&1)"
		if [ ${?} -eq 0 ]; then
			[ -e "${UPLOAD_DIR}" ] && \
				errchk "${THIS_CMD}" "${THIS_TEST}"
			UP_CLEAN='Y'
		else
			printf '%s' "${MSG}" | grep -q "${ENOTEMPTY}" ||
				failchk "${THIS_CMD}" "${THIS_TEST}"
			THIS_TEST="removing upload directory: not empty"
		fi
	else
		THIS_TEST="removing upload directory: already not present"
		UP_CLEAN='Y'
	fi
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_71_0()
{
	THIS_TEST="${1} upload directory as target"
	THIS_CMD="${1}"
	if [ "${UP_CLEAN}" = 'N' ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C "${1}" "${2}" "${UPLOAD_DIR}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${3}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_71()
{
	test_71_0 'ln' "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" "${EACCESS}"
	test_71_0 'mv' "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" "${EACCESS}"
	test_71_0 'mv' "${TEST_DIR}${TMP_ND1}" "${EACCESS}"
}

#----------
test_72()
{
	THIS_TEST="mkdir upload directory"
	THIS_CMD="mkdir"
	if [ "${UP_CLEAN}" = 'N' ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C mkdir "${UPLOAD_DIR}" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_73()
{
	THIS_TEST="create upload directory"
	THIS_CMD="printf"
	if [ "${UP_CLEAN}" = 'N' ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	MSG="$(LC_ALL=C sh -c "printf '%s' '${SMALL_FILE_CONTENT}' >'${UPLOAD_DIR}'" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_74()
{
	THIS_TEST="existence of upload directory"
	THIS_CMD="test"

	[ -d "${UPLOAD_DIR}" ] || errcmd "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_75()
{
	THIS_TEST="removing upload directory"
	THIS_CMD="rmdir"

	MSG="$(LC_ALL=C rmdir "${UPLOAD_DIR}" 2>&1)" && \
			failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${EACCESS}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

#----------
test_76_0()
{
	THIS_TEST="${1} inside upload directory"
	THIS_CMD="${1}"
	MSG="$(LC_ALL=C "${1}" "${2}" "${UPLOAD_DIR}/"${3}"" 2>&1)" && \
				failchk "${THIS_CMD}" "${THIS_TEST}"
	chkmsg 	"${THIS_CMD}" "${THIS_TEST}" "${MSG}" "${4}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_76()
{
	test_76_0 'ln' "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" "${TMP_NF2}" "${EACCESS}"
	test_76_0 'ln' "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TMP_NF1}" "${EBUSY}"
	test_76_0 'mv' "${TEST_DIR}${TMP_ND1}/${TMP_NF2}" "${TMP_NF2}" "${EACCESS}"
	test_76_0 'mv' "${TEST_DIR}${TMP_ND1}/${TMP_NF1}" "${TMP_NF1}" "${EACCESS}"
	test_76_0 'mv' "${TEST_DIR}${TMP_ND1}" "${TMP_ND1}" "${EACCESS}"
	test_76_0 'mkdir' '' "${TMP_ND1}" "${EACCESS}"
	test_76_0 'touch' '' "${TMP_NF1}" "${EACCESS}"
}

#----------
test_80()
{
	THIS_TEST="rename writing to writing"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	
	dd if="${TMP_1}/${TMP_NF3}" of="${TEST_DIR}${TMP_ND1}/w1" bs=1M count=512 2>/dev/null &
	dd if="${TMP_1}/${TMP_NF3}" of="${TEST_DIR}${TMP_ND1}/w2" bs=1M count=128 2>/dev/null &
	sleep 2
	mv "${TEST_DIR}${TMP_ND1}/w2" "${TEST_DIR}${TMP_ND1}/w1" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	wait
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w1") -eq 134217728 ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_81()
{
	THIS_TEST="rename writing to written"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi
	
	dd if="${TMP_1}/${TMP_NF3}" of="${TEST_DIR}${TMP_ND1}/w2" bs=1M count=256 2>/dev/null &
	sleep 1
	mv "${TEST_DIR}${TMP_ND1}/w2" "${TEST_DIR}${TMP_ND1}/w1" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	wait
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w1") -eq 268435456 ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_82()
{
	THIS_TEST="rename writing to existing"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	std_test "rm" "${THIS_TEST} (rm)" "-z" "${TMP_ND1}" 'w1'
	std_test "ln" "${THIS_TEST} (ln)" \
		 "-n" "${TMP_ND1}" 'w1' "${TEST_FILE}"
	
	dd if="${TMP_1}/${TMP_NF3}" of="${TEST_DIR}${TMP_ND1}/w2" bs=1M count=256 2>/dev/null &
	sleep 1
	mv "${TEST_DIR}${TMP_ND1}/w2" "${TEST_DIR}${TMP_ND1}/w1" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	wait
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w1") -eq 268435456 ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_83()
{
	THIS_TEST="rename written to writing"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	dd if="${TMP_1}/${TMP_NF3}" of="${TEST_DIR}${TMP_ND1}/w2" bs=1M count=128 2>/dev/null &
	sleep 1
	mv "${TEST_DIR}${TMP_ND1}/w1" "${TEST_DIR}${TMP_ND1}/w2" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	wait
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w2") -eq 268435456 ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_84()
{
	THIS_TEST="rename written to written"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	dd if="${TMP_1}/${TMP_NF3}" of="${TEST_DIR}${TMP_ND1}/w1" bs=1M count=128 2>/dev/null
	sleep 1
	mv "${TEST_DIR}${TMP_ND1}/w2" "${TEST_DIR}${TMP_ND1}/w1" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w1") -eq 268435456 ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_85()
{
	THIS_TEST="rename written to existing"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	std_test "ln" "${THIS_TEST} (ln)" \
		 "-n" "${TMP_ND1}" 'w2' "${TEST_FILE}"

	mv "${TEST_DIR}${TMP_ND1}/w1" "${TEST_DIR}${TMP_ND1}/w2" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w2") -eq 268435456 ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_86()
{
	THIS_TEST="rename existing to written"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	std_test "ln" "${THIS_TEST} (ln)" \
		 "-n" "${TMP_ND1}" 'w1' "${TEST_FILE}"

	mv "${TEST_DIR}${TMP_ND1}/w1" "${TEST_DIR}${TMP_ND1}/w2" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w2") -eq $(stat -c %s "${TEST_FILE}") ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_87()
{
	THIS_TEST="rename existing to writing"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	dd if="${TMP_1}/${TMP_NF3}" of="${TEST_DIR}${TMP_ND1}/w1" bs=1M count=128 2>/dev/null &
	sleep 1
	mv "${TEST_DIR}${TMP_ND1}/w2" "${TEST_DIR}${TMP_ND1}/w1" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w1") -eq $(stat -c %s "${TEST_FILE}") ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_88()
{
	THIS_TEST="rename existing to existing"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	std_test "ln" "${THIS_TEST} (ln)" \
		 "-n" "${TMP_ND1}" 'w2' "${TEST_FILE}"
	mv "${TEST_DIR}${TMP_ND1}/w2" "${TEST_DIR}${TMP_ND1}/w1" || \
					errcmd "${THIS_CMD}" "${THIS_TEST}"
	[ $(stat -c %s "${TEST_DIR}${TMP_ND1}/w1") -eq $(stat -c %s "${TEST_FILE}") ] || \
					errchk "${THIS_CMD}" "${THIS_TEST}"
	oktest  "${THIS_CMD}" "${THIS_TEST}"
}

test_89()
{
	THIS_TEST="rename 9 cases cleaning"
	THIS_CMD="mv"
	if [ ${BIG_FILE_SIZE} -lt 512 ]; then
		skiptest "${THIS_CMD}" "${THIS_TEST}"
		return
	fi

	std_test "rm" "${THIS_TEST} (rm)" "-z" "${TMP_ND1}" 'w1'
}

#__________________
# Start of tests
#__________________

set_log_file()
{
	if [ -n "${LOG_FILE_PATTERN}" ]; then
		LOG_FILE="$(printf "$LOG_FILE_PATTERN" ${1})"
		LOG_CMD="--log-file=${LOG_FILE}"
	else
		LOG_FILE=''
		LOG_CMD=''
	fi
}


mkdir -p "${MOUNT_PATH}" || errcmd "Creating mount path"

echo "Creating a random file"
dd of="${TMP_1}/${TMP_NF3}" if=/dev/urandom bs=1M count=${BIG_FILE_SIZE}


TEST_DIR="${MOUNT_PATH}/${TEST_ROOT}/${TEST_PATH}/"
TEST_FILE="${MOUNT_PATH}/${TEST_ROOT}/${TEST_SMALL_FILE}"

TEST_KIND="With option --raw_names"
set_log_file ${TEST_NUM}

COMMAND="${CMD} $(pwd)/${MOUNT_PATH} ${LOG_CMD} --raw-names"
eval "${COMMAND}" || errcmd "mount" "${TEST_KIND}"
oktest  "mount" "${TEST_KIND}"

test_00

test_01
test_02_1
test_70
test_71
test_72
test_73

test_50
test_51
test_52

test_60
test_61
test_62

test_39
T_START="$( date +"%s" )"
test_40 "fail"

test_80
test_81
test_82
test_83
test_84
test_85
test_86
test_87
test_88
test_89

test_23

test_74
test_75
test_76

test_24
test_02_3

test_25
test_25_0
test_25_1 "${TMP_NF1}"
test_25_2
test_26
test_25_1 "${TMP_NF2}"
test_27
test_27_1 "${TMP_NF2}"
test_28
test_25_1 "${TMP_NF2}"
test_27
test_29

test_02
test_02_1
test_02_2
test_03
test_04
test_05
test_06
test_07
test_08 "fail"
test_09
test_10
test_11
test_12
test_13
test_14
test_15
test_16
test_17
test_18
test_19 "fail"
test_20
test_21
test_22

test_32
test_25_1 "${LONG1}6789${UTF8_4}"
test_25_1 "${LONG2}"
test_33
test_25_1 "${TMP_NF1}"
test_27_1 "${LONG2}"
test_34
test_25_1 "${LONG1}6${UTF8_4}"
test_25_1 "${LONG2}"
test_35
test_25_1 "${LONG2}"
test_25_1 "${LONG1}67${UTF8_4}"
test_36
test_25_1 "${LONG1}67${UTF8_4}"
test_25_1 "${LONG2}7"
test_37
test_25_1 "${LONG1}6789r"
test_25_1 "${UTF8_4}${LONG2}"
test_38
test_27_1 "${LONG1}6789r"
test_27_1 "${UTF8_4}${LONG2}"
test_30
test_25_1 "${TMP_NF1}"
test_25_1 "${TMP_NF2}"
test_31
test_41 "${TEST_DIR}${TMP_ND1}/${RAW_NAME}"

fusermount -u "$(pwd)/${MOUNT_PATH}" || errcmd "fusermount" "${TEST_KIND}"
oktest  "fusermount" "${TEST_KIND}"
sleep 3
test_49
echo
echo
echo


TEST_KIND="With option --refresh-hidden --no-ssl='${TEST_DIR}'"
set_log_file ${TEST_NUM}

COMMAND="${CMD} $(pwd)/${MOUNT_PATH} ${LOG_CMD} --refresh-hidden --no-ssl='/${TEST_ROOT}/${TEST_PATH}/'"
eval "${COMMAND}" || errcmd "mount" "${TEST_KIND}"
oktest  "mount" "${TEST_KIND}"

test_23

test_60 "fail"
test_02_1
test_64
test_02_3
test_24
test_65
test_66
test_67

test_42
test_43
test_39 "fail"
test_40
T_START="$( date +"%s" )"

test_80
test_81
test_82
test_83
test_84
test_85
test_86
test_87
test_88
test_89

test_23
test_24
test_25
test_25_0
test_25_1 "${TMP_NF1}"
test_25_2
test_26
test_25_1 "${TMP_NF2}"
test_27
test_27_1 "${TMP_NF2}"
test_28
test_25_1 "${TMP_NF2}"
test_27
test_29

test_02
test_03
test_04
test_05
test_06
test_07
test_08
test_08_1
test_09
test_10
test_11
test_12
test_13
test_14
test_15
test_16
test_17
test_18
test_19
test_19_1
test_20
test_21
test_22

test_32
test_25_1 "${LONG1}6789${UTF8_4}"
test_25_1 "${LONG2}"
test_33
test_25_1 "${TMP_NF1}"
test_27_1 "${LONG2}"
test_34
test_25_1 "${LONG1}6${UTF8_4}"
test_25_1 "${LONG2}"
test_35
test_25_1 "${LONG2}"
test_25_1 "${LONG1}67${UTF8_4}"
test_36
test_25_1 "${LONG1}67${UTF8_4}"
test_25_1 "${LONG2}7"
test_37
test_25_1 "${LONG1}6789r"
test_25_1 "${UTF8_4}${LONG2}"
test_38
test_27_1 "${LONG1}6789r"
test_27_1 "${UTF8_4}${LONG2}"
test_30
test_25_1 "${TMP_NF1}"
test_25_1 "${TMP_NF2}""${EACCESS}"
test_31
test_41 "${TEST_DIR}${TMP_ND1}/${TR_NAME}"

fusermount -u "$(pwd)/${MOUNT_PATH}" || errcmd "fusermount" "${TEST_KIND}"
oktest  "fusermount" "${TEST_KIND}"
sleep 3
test_49
echo
echo
echo

TEST_KIND="With option --root=/${TEST_ROOT} --raw-names"
set_log_file ${TEST_NUM}

COMMAND="${CMD} $(pwd)/${MOUNT_PATH} ${LOG_CMD} --root=/${TEST_ROOT} --raw-names"
eval "${COMMAND}" || errcmd "mount" "${TEST_KIND}"
oktest  "mount" "${TEST_KIND}"

TEST_DIR="${MOUNT_PATH}/${TEST_PATH}/"
TEST_FILE="${MOUNT_PATH}/${TEST_SMALL_FILE}"

test_23
test_24

test_50
test_51
test_52

test_60
test_61
test_62

test_44
test_45
test_39
T_START="$( date +"%s" )"
test_40 "fail"

test_80
test_81
test_82
test_83
test_84
test_85
test_86
test_87
test_88
test_89

test_23
test_24
test_25
test_25_0
test_25_2
test_26
test_27
test_28
test_27
test_29

test_02
test_02_1
test_02_2
test_03
test_04
test_05
test_06
test_07
test_08 "fail"
test_09
test_10
test_11
test_12
test_13
test_14
test_15
test_16
test_17
test_18
test_19 "fail"
test_20
test_21
test_22

test_32
test_33
test_34
test_35
test_36
test_37
test_38
test_30
test_31
test_41 "${TEST_DIR}${TMP_ND1}/${RAW_NAME}"
test_45
test_46

fusermount -u "$(pwd)/${MOUNT_PATH}" || errcmd "fusermount" "${TEST_KIND}"
oktest  "fusermount" "${TEST_KIND}"
sleep 3
test_49


# When upload directory did not exist at test start, this will delete it
# to leave the same situation.
# Note that the test where directory exists but not the ftp account (same
# result for the variable) cannot be reproduced with this test. It has to be
# tested by using the web interface create the situation where upload directory
# exists and the ftp account does not.
if [ -z "${FTP_ACCOUNT}" ]; then
	TEST_KIND="With no option"
	set_log_file ${TEST_NUM}

	COMMAND="${CMD} $(pwd)/${MOUNT_PATH} ${LOG_CMD}"
	eval "${COMMAND}" || errcmd "mount" "${TEST_KIND}"
	oktest  "mount" "${TEST_KIND}"

	RM_UPLOAD_DIR_TEST='Y'
	test_70

	sleep 3 # Let background init complete.
	fusermount -u "$(pwd)/${MOUNT_PATH}" || errcmd "fusermount" "${TEST_KIND}"
	oktest  "fusermount" "${TEST_KIND}"
	sleep 3
	test_49
fi

echo "Cleaning up the test files"
rm "${TMP_1}/${TMP_NF3}"
rmdir "${TMP_1}"
