/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**************************************************************************
 * This instruments memory management and contention with counters.
 * It is useful to be built in either for debug or for stats.
 */

#include "astreamfs_worker_mem.h"

#ifdef _MEMCOUNT
/* Global stats counters: memory, contentions and "solo" */

static _Atomic (unsigned long long) n_alloc	= 0;
static _Atomic (unsigned long long) n_free	= 0;
static _Atomic (unsigned long long) mem_alloced	= 0;
static _Atomic (unsigned long long) mem_freed	= 0;
static _Atomic (unsigned long long) contentions = 0;
static _Atomic (unsigned int)	    max_spins	= 0;

/*
 * @brief count an alloc.
 *
 * @param number of increment
 * @param size to increment
 * @return none
 */
static void inc_alloc(int i, size_t s)
{
	atomic_fetch_add_explicit(&n_alloc    , i, memory_order_relaxed);
	atomic_fetch_add_explicit(&mem_alloced, s, memory_order_relaxed);
}

/**
 * @brief same as the standard function aligned_alloc + statitics
 *
 * NOTE: this function and the following are only available when _MEMCOUNT is
 *       'on' (typically when using debug or stats).
 * There are used by default (or the standard functions when no _MEMCOUNT) when
 * no "memory management" function is provided for aw_operations in aw_init.
 *
 * The caller must consistenly either provide the 3 functions, or provide none.
 *
 * These default functions will count allocations in the 6 counters returned
 * by in aw_get_mem_stats.
 *
 * @param alignment for the returned buffer
 * @param size of the block to be allocated
 * @return pointer on the allocated buffer
 */
void *aw_aligned_alloc(size_t alignment, size_t size)
{
	void *p;
	if (0 == size || 0 == alignment)
		return NULL;
	p = aligned_alloc(alignment,
			  alignment * (1 + ((size - 1) / alignment)) 
			 );
	if ( NULL != p )
		inc_alloc(1, malloc_usable_size(p));
	return p;
}

/*
 * @brief count a free.
 *
 * @param size to increment
 * @return none
 */
static void inc_free(size_t s)
{
	atomic_fetch_add_explicit(&n_free   , 1, memory_order_relaxed);
	atomic_fetch_add_explicit(&mem_freed, s, memory_order_relaxed);
}

/**
 * @brief just do statitics as if freed but do not free.
 *
 * NOTE: see above + notes in aw_operations structure comments
 *
 * @param pointer to be released
 * @return none
 */
void aw_release(void *p)
{
	if (NULL == p)
		return;
	inc_free(malloc_usable_size(p));
}


/**
 * @brief same as the standard function free + statitics
 *
 * NOTE: see above
 *
 * @param pointer to be freed
 * @return none
 */
void aw_free(void *p)
{
	if (NULL == p)
		return;
	aw_release(p);
	free(p);
}

/**
 * @brief same as the standard function realloc + statitics
 *
 * NOTE: this function is not needed by astreamfs_worker but is provided
 *       for complete statistics when used elsewhere
 *
 * @param alignment for the returned buffer
 * @param size of the block to be allocated
 * @return pointer on the allocated buffer
 */
void *aw_realloc(void *ptr, size_t size)
{
	size_t old_alloced, new_alloced;
	void *new;

	if (0 == size) {
		aw_free(ptr);
		return NULL;
	}
	/* Note: it is safe to call aligned_alloc without checking size, linux
	 * version will not fail if size is not a multiple of alignment, and
	 * will return a suitable buffer */
	if (NULL == ptr)
		return aw_aligned_alloc(alignof(max_align_t), size);

	old_alloced = malloc_usable_size(ptr);
	new = realloc(ptr, size);
	new_alloced = malloc_usable_size(new);
	if (ptr != new || new_alloced < old_alloced ) {
		/* Because of unsigned size_t and counter which is only
		 * incremented, in the unlikely case new_alloced is smaller than
		 * old_alloced, it is counted as if it was freed/alloced */
		inc_free(old_alloced);
		inc_alloc(1, new_alloced);
	}
	else if (new_alloced > old_alloced) {
		inc_alloc(0, new_alloced - old_alloced);
	}
	return new;
}

/**
 * @brief count contentions.
 *
 * For each atomic exchange loop, a 'spin' counter is taken. When the atomic
 * immediately succeeds, this shall be called with spin = 0, meaning there
 * were no contention.
 * When spin is not 0, there was a contention (the atomic exchange failed at
 * least once) and this counts the contention and the "highest spin".
 * Since update the "highest spin" is also done with an exchange... this
 * function can in turn also create a contention and give a higher spin count,
 * this is managed too.
 *
 * @param number of "spins"
 * @return none
 */
void aw_count_contentions(unsigned int spin)
{
	unsigned int spin2 = 0;
	unsigned int max;

	if (0 == spin)
		return;

	max = atomic_load_explicit(&max_spins, memory_order_acquire);

	while ( spin > max &&
		!atomic_compare_exchange_strong_explicit(&max_spins, &max, spin,
							 memory_order_acq_rel,
							 memory_order_acquire)){
		spin2++;
		if (spin2 > spin)
			spin = spin2;
	}

	atomic_fetch_add_explicit(&contentions,
				  spin + spin2,
				  memory_order_relaxed);
}


/**
 * @brief retrieve memeory statistics
 *
 * @param pointer on statistics to return for memory
 * @return 0 or error
 */
void aw_get_mem_stats(struct aw_mem_stats *m_stats, memory_order order)
{
	m_stats->n_alloc     = atomic_load_explicit(&n_alloc,     order);
	m_stats->n_free      = atomic_load_explicit(&n_free,      order);
	m_stats->mem_alloced = atomic_load_explicit(&mem_alloced, order);
	m_stats->mem_freed   = atomic_load_explicit(&mem_freed,   order);
	m_stats->contentions = atomic_load_explicit(&contentions, order);
	m_stats->max_spins   = atomic_load_explicit(&max_spins,   order);
}

#else

/**
 * @brief default release when compiled with no stats: does nothing!
 *
 * A macro cannot be used because a function is needed to fill the
 * aw_operations structure.
 *
 * @param pointer to release
 * @return none
 */
void aw_release(void *unused_ptr)
{
	(void)unused_ptr;
}

#endif
