/*
 * 1fichierfs: common header for 1fichierfs modules.
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define STREAM_STRUCT struct lfile
#include "astreamfs_worker.h"
#include "astreamfs_util.h"

#define PROG_NAME	"1fichierfs"
#define PROG_VERSION	"1.9.6"


#ifdef __GNUC__
#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)
#else
#define likely(x)	(x)
#define unlikely(x)	(x)
#endif

struct pbcb {
	char  *pb;
	size_t cb;
};

struct parse_data {
	bool got_API_key;
	const char *mount_path;
	unsigned long sz_no_ssl;
	unsigned long sz_encfs;
	struct fuse_operations const *unfichier_oper;
};

struct paths {
	unsigned long	n;
	char		**a;
};
struct params1f {
	long  curl_IP_resolve;
	char *api_key;
	struct paths no_ssl;
	struct paths encfs;
	char *refresh_file;
	char *ftp_user;
	char *remote_root;
	unsigned long refresh_time;
	unsigned long resume_delay;
	bool refresh_hidden;
	bool readonly;
	bool insecure;
	bool no_upload;
	bool dry_run;
	bool raw_names;
	uid_t	uid;
	gid_t	gid;
	unsigned int network_wait;
};

enum api_routes {
	FOLDER_LS		= 0,
	DOWNLOAD_GET_TOKEN	= 1,
	USER_INFO		= 2,
	FILE_MV			= 3,
	FILE_CHATTR		= 4,
	FILE_RM			= 5,
	FILE_CP			= 6,
	FOLDER_MV		= 7,
	FOLDER_RM		= 8,
	FOLDER_MKDIR		= 9,
	FTP_USER_LS		=10,
	FTP_USER_RM		=11,
	FTP_USER_ADD		=12,
	FTP_PROCESS		=13,
	FALLBACK_GET_TOKEN	=14,
};

enum refresh_cause {
	REFRESH_TRIGGER		= 0,
	REFRESH_HIDDEN		= 1,
	REFRESH_TIMER		= 2,
	REFRESH_POLLING		= 3,
	REFRESH_WRITE		= 4,
	REFRESH_MV		= 5,
	REFRESH_LINK		= 6,
	REFRESH_UNLINK		= 7,
	REFRESH_MKDIR		= 8,
	REFRESH_RMDIR		= 9,
	REFRESH_404		=10,
	REFRESH_EXIT		=11,
	REFRESH_INIT_ROOT	=12,
};

/* When using curl, the program considers several things:
 * - http status code
 * - curl errors
 * - "functional" errors reported in the JSON (for APIs) although curl and
 *   http status could have been all Ok.
 * This enum serves to have a unique set of values to summarize the error codes.
 */
enum hCode
{
	hOk	   = HTTP_OK,

	hBusy	   = -EBUSY,
	hEIO	   = -EIO,
	hCnxErr	   = -CURLE_COULDNT_CONNECT,
	hTooManyRq = HTTP_TOO_MANY_REQUESTS,
	hNotFound  = HTTP_NOT_FOUND,
	hForbidden = HTTP_FORBIDDEN,
};

enum translation_type {
	TR_NONE,
	TR_FILE,
	TR_DIR,
};

	/* Live stream flags for counter */
#define FL_ERROR	(1)
/* 1.0.9/1.2.0 This is to mark files as deleted on the tree and as deleted on
 *       the live list. It is thread safe (thanks to RCU) and does not require
 *       locks. It saves reading again the sub_dir for the tree management.   */
#define FL_DEL		(2)
#define OPEN_INC	(4)
#define OPEN_MASK	(~(FL_ERROR | FL_DEL))

#define ENCFS_HEADER_SZ (8)	    /* Size of the header encfs rewrites */
#define ENCFS_HEADER_B64_SZ (12)    /* The same header base64 encoded */

struct dir;
struct lfile {
	_Atomic unsigned long	counter; /* nb_open +(1b:SP)+(1b:DEL)+(1b:ERR)*/
	bool		no_ssl		:1,
			encfs		:1,
			has_encfs_key	:1,
			hidden		:1;
	char 			encfs_key[ENCFS_HEADER_SZ];
	long long	 	id;      /* in this case URL is the name      */
	char 			*email;
	unsigned long long	size;
	char			*URL;
	char			*location;
	_Atomic time_t		loc_until; /* Avoids locks for keep_live */
	pthread_mutex_t		loc_mutex;

	/* From there this is for statistics */
	unsigned long		nb_loc;
	char			path[];
};

struct lfiles {
	unsigned long	n_lfiles;
	struct lfile	*a_lfiles[];
};



/* Variable and functions defined in 1fichierfs.c */
/*******************************************************
 *  These are the things you can change at compile time.
 */
						/* Default initial size (on
						 * stack) of the JSON buffer  */
#define DEF_JSON_BUF_SIZE (16 * 1024 + 1)       /* This is not a random number!
						 * By default, curl callback
			bool			 * read buffers are 16k max.
						 * So when a json response fits
						 * in a single curl callback
						 * no malloc is needed. +1 for
						 * the '\0' end string.
						 * That should be the case of
						 * almost all requests except
						 * long directory listings (50+
						 * files + sub-directories).  */

/*******************************************************/

#define MAX_FILENAME_LENGTH (250)   /* Max lenght supported by 1fichier's API
				      for filenames                           */

#define MAX_ESCAPED_LENGTH (6 * MAX_FILENAME_LENGTH)  /* Worst = all \unnnn */


struct json {
	char  *pb;
	size_t cb;
	char   buf[DEF_JSON_BUF_SIZE];
};
struct file;
struct wfile;		/* in 1fichier_write.c */
struct wfile_spec;	/* in 1fichier_write.c */
struct dentries;
struct dir {
	_Atomic (struct dentries *) dent;
	pthread_mutex_t	lookupmutex;
	struct dir 	*parent; /* Parent directory of this directory */
	char		*name;
	long long	id;
	time_t		cdate;
				/* Flags for encfs, no_ssl, share access,...  */
	bool		test_encfs  :1,
			encfs	    :1,
			test_no_ssl :1,
			no_ssl	    :1,
			shared	    :1,
			readonly    :1,	/* readonly and hidden are always     */
			hidden	    :1;	/* false when dir is not shared       */
					/* hidden implies readonly because it
					 * is not possible to share with the
					 * hidden attribute + allow writing!  */
	unsigned long	email_off; /* offset of email in name for shared  dir */
};
extern struct dir root;	/* In 1fichierfs.c */

/* The access field is ACCESS_RW (0) when not on a shared directory. */
#define ACCESS_RW	(0)

struct walk {
	bool		is_dir;
	bool		is_wf;
	union {
		struct dir	*dir;
		struct file	*fs;
		struct wfile	*wf;
	} res;
	struct dir	*parent;
};

extern _Atomic bool exiting;
extern _Atomic (struct lfiles *)live;
extern struct params1f params1f;
extern mode_t st_mode;

#define JSON_ITEM_START "{\""
extern const char json_item_start[];
extern const char json_items[];
extern const char json_filename[];
extern const char json_url[];
extern const char json_folder_id[];

/* This is used to refresh upload directory when remote root is not / */
#define UPLOAD_DIR_ID_NOT_INITIALISED (-1LL)
extern long long  get_upload_id(void );
extern void	  set_upload_id(long long id);

extern bool is_fuse_hidden(const char *filename);
extern CURL *curl_init(bool is_http);
extern void json_init(struct json *j);
extern void json_free(struct json *j);
extern char *json_find_key(const char *json, const char *str);

/*
 * @brief length of the json string once translated as specified
 *
 * @param json string (points on the leading ")
 * @param type of translation needed
 * @return length of the translated string
 */
extern size_t json_strlen(const char *json, enum translation_type tr);

/*
 * @brief copies the json string to destination with translation as specified
 *
 * @param json string (points on the leading ")
 * @param destination of the translation
 * @param type of translation needed
 * @return length of the translated string
 */
extern char *json_strcpy(const char *json, char *dest,
			 enum translation_type tr);


/*
 * @brief codes the key into a characters base64 string
 *
 * @param pointer to the key to code
 * @param destination buffer, must be at least ENCFS_HEADER_B64_SZ chars big.
 * @return none
 */
extern void code64(const char *key, char *dest);

/*
 * @brief copy an encfs key
 *
 * @param destination of the key
 * @param source of the key
 * @return destination
 */
extern char *encfs_copy_key(char *key_dest, const char *key_src);

/*
 * @brief copy an encfs key
 *
 * ATTENTION, destination string size must be at least:
 *   	      strlen(filename) + ENCFS_HEADERB64_SZ + 1
 *
 * @param destination string,
 * @param raw file name
 * @param raw key to decode and append
 * @return destination
 */
extern char *encfs_appendb64_key(char *dest, const char *filename,
				 const char *key);


/*
 * @brief called by unfichier_write to check if it is an encfs rewrite key
 *
 * @param path of the file
 * @param buffer to write
 * @param size to write
 * @param offset in the file
 * @param fuse file info handle
 * @return error (-EACCESS) or OK
 */
extern int encfs_rewrite_key(const char *path, const char *buf, size_t size,
			     off_t offset, struct fuse_file_info *fi);

/*
 * @brief check if this is an encfs name, a special case, and get size
 *
 * @param filename, either zero terminated or a json string
 * @param where to return the size (if not NULL)
 * @param whether this is a special case (rewritten) (if not NULL)
 * @return true if it matches encfs or encfs+special pattern, false otherwise
 */
extern bool is_encfs_name(const char *name, size_t *size, bool *special);

extern void refresh_root(enum refresh_cause cause);
extern bool is_hidden(struct lfile *s);

/*
 * @brief performs a translation to a compatible name for the server
 *
 * @param character to translate
 * @param (output) where to store the translation
 * @param type of translation to perform (TR_FILE or TR_DIR)
 * @return number of characters outputed
 */
extern size_t translate(char c, char *trans, enum translation_type tr);

/*
 * @brief check if the name (file or dir) is reserved
 *
 * @param path to be checked
 * @param id of parent directory
 * @return true if this is a reserved name.
 */
extern bool reserved_names( const char* path, long long parent_id);

extern int rmdir_upload(const char *dirname, long long id);
extern int mkdir_upload(const char *path, const char *dirname,
			long long parent_id, long long *created_id);
extern int rename_dir_upload(const char *from,  const char *to,
			     long long dir_id, long long dest_id,
			     const char *dirname);
/*
 * @brief rename file from upload directory to the target.
 *
 * @param full path of target (if not NULL, used if rename with ID/file failed)
 * @param directory ID of the target where to rename
 * @param URL of the file in the upload directory
 * @param filename part of the full path to rename to.
 * @return O if success, othewise negative error code.
 */
extern int rename_upload(const char *to, const long long dest_id,
			 const char *URL , const char *filename);
/*
 * @brief delete file from upload directory to the target.
 *
 * @param URL of the file to delete
 * @param name of the file to be deleted in the upload directory
 * @return O if success, othewise negative error code.
 */
extern int unlink_upload(const char * URL, const char *ftpname);

extern bool ok_json_get(const char *json_str, const char *pattern, off_t *val);
extern enum hCode curl_json_perform(char *postdata, struct json *j,
				    enum api_routes rt, const char *name_msg);
/*
 * @brief check if write is forbidden in the directory.
 *
 * @param dir struct pointer of the directory to check
 * @param whether to log a message
 * @param file found to check age and parent.
 * @return true is write is forbidden (false otherwise)
 */
extern bool forbidden_write(struct dir * d, bool msg, struct walk *w);
extern bool forbidden_path(const char* path, const char *msg);
/*
 * @brief check filename illegal spaces, chars, and length
 *
 * @param string to check
 * @param type of translation needed (TR_FILE or TR_DIR)
 * @return O or code ENAMETOOLONG (speaks by itself) or illegal spaces: EILSEQ
 */
extern int forbidden_chars(const char *str, enum translation_type tr);
extern void find_path(const char *path, struct walk *w);
struct dir *refresh_upload(const char *path, struct dentries **dent);
extern const char *find_file_upload(struct dir *d, const char *filename);
const char * list_subdir_upload(struct dentries *dent, unsigned long *i,
				long long *subdir_id, time_t *cdate);
const char * list_file_upload(struct dentries *dent, unsigned long *i,
			      const char **URL, time_t *cdate);

/*
 * Getter of the global location counter of freed streamer (historical stat)
 * "relaxed" is ok for statistics!
 */
extern unsigned long get_stats_glob_locations();

/* These 2 functions are stats-related but needed when _MEMCOUNT is on */
/*
 * @brief prepare the content of the memory stats.
 *
 */

extern const char expo[];
char *format_hr(unsigned long long num, char *buf);

/*_______________________________________*/
/* Functions defined in 1fichierfs_rcu.c */
struct rcu_item;

#define rcu_register_thread()
#define rcu_unregister_thread()
extern void rcu_init(void);
extern void rcu_read_lock(void);
extern void rcu_read_unlock(void);
extern void rcu_exit(void);

	/*
	* Specific rcu cleaner functions for pointers, and structures
	*/
extern void rcu_free_ptr(void *p);
extern void rcu_free_struct(void *pstruct, void (*func)(struct rcu_item *item));
extern void *rcu_get_p_struct_from_item(struct rcu_item *item);

/*_________________________________________________________*/
/* Variables and Functions defined in 1fichierfs_options.c */
extern const struct fuse_opt  unfichier_opts[];

extern int  unfichier_opt_count(void *, const char *, int, struct fuse_args *);
extern int  unfichier_opt_proc(void *,	const char *, int, struct fuse_args *);
extern void check_args_first_pass(struct parse_data *);
extern void check_args_second_pass(struct timespec *);
extern void check_args_third_pass(struct fuse_args *);


/*_________________________________________*/
/* Elements declared in 1fichierfs_write.c */
/*******************************************************
 *  These are the things you can change at compile time.
 */
#define MAX_WRITERS 4

/**/
extern const char upload_dir[];
struct wfile_list;
extern _Atomic (struct wfile_list *) wflist;

extern bool is_write_possible(void);
extern unsigned long get_wfl_n_files(struct wfile_list *wfl, bool **keep);
extern void get_wf_info(struct wfile_list *wfl, bool *keep, unsigned long i,
			uint64_t *size, time_t *eta, unsigned long *counter,
			unsigned long *step, const char **path);
extern bool get_writer_ref(struct wfile_list *wfl, bool *keep,
			   unsigned int i, char *ref, size_t len);
extern void write_destroy();
extern void *unfichier_init(struct fuse_conn_info *conn);
extern void write_init_foreground1(const char *remote_root);
extern void write_init_foreground2();
extern bool write_init_undo_1();
extern void write_init_undo_2(int res);
extern int unfichier_write(const char *path, const char *buf, size_t size,
			   off_t offset, struct fuse_file_info *fi);
extern int unfichier_create(const char *path, mode_t unused,
			    struct fuse_file_info *fi);
extern int unfichier_utimens(const char *path, const struct timespec tv[2]);
extern int unfichier_chmod(const char *path, mode_t mode);
extern int unfichier_chown(const char *path, uid_t uid, gid_t gid);
extern int unfichier_flush(const char *path, struct fuse_file_info *fi);
extern int unfichier_fsync(const char *path, int datasync,
			   struct fuse_file_info *fi);
extern int unfichier_fallocate( const char *path_not_used,
				int mode, off_t offset, off_t len,
				struct fuse_file_info *fi_not_used);

extern int write_open(const char *path, struct walk *w,
		      struct fuse_file_info *fi);
extern int write_release(struct wfile *wf);
extern int write_readdir(long long id, const char *path, void *buf,
			 fuse_fill_dir_t filler);
extern int write_unlink(struct walk *w);
extern void write_getattr(struct stat *stbuf, struct wfile *wf);
extern void write_find(const char *filename, struct walk *w);
extern int write_rename(struct walk *wf, struct walk *wt,
			const char *from, const char *to);
extern int write_read(char *buf, size_t size, off_t offset, struct wfile *wf);

/*_________________________________________*/
/* Functions defined in 1fichierfs_stats.c */
#ifdef _STATS
size_t out_stats(struct stats *out);
void update_write_stats(int written, unsigned int i, struct timespec *start);
void update_write_err(unsigned int i);
void update_single_stats(int, struct timespec *);
void update_api_stats(enum api_routes, struct timespec *, enum hCode
				     , unsigned int);
void update_refresh_stats(enum refresh_cause);
void update_speed_stats(bool read, int written, unsigned int i);

#else
#define update_refresh_stats(cause)
#define update_write_err(i)
#define update_speed_stats(read,written,i)
#endif


/*________________________________________*/
/* Elements declared in 1fichierfs_read.c */
/**
 * Callback functions for the new common read engine
 **/

extern const struct sr_operations sr_ops, sr_ops2, sr_ops3;
