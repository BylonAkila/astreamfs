/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef ASTREAMFS_WORKER_H
#define ASTREAMFS_WORKER_H

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26
#define _FILE_OFFSET_BITS 64
#include <fuse.h>
#include <stdalign.h>
#include <stdbool.h>
#include <limits.h>
#include <semaphore.h>

#if LEVEL1_DCACHE_LINESIZE == 0
	#undef LEVEL1_DCACHE_LINESIZE
#endif

#ifndef LEVEL1_DCACHE_LINESIZE
	#define LEVEL1_DCACHE_LINESIZE 64
#endif

/** Conditional compilation options and convenience macros */

	/* By default, DEBUG code is absent, pass DEBUG=1 to make to have it. */
#ifdef DEBUG
	#define _DEBUG
	#ifndef _MEMCOUNT
		#define _MEMCOUNT	/* Also debug memory with counters */
	#endif
#else
	#undef _DEBUG
#endif

	/* By default, stats code is present, pass NOSTAT=1 remove it. */
#ifdef NOSTATS
	#undef _STATS
	#define STATS_AVAILABLE false
#else
	#define _STATS
	#define STATS_AVAILABLE true
	#ifndef _MEMCOUNT
		#define _MEMCOUNT	/* Also give stats for memory */
	#endif
#endif

/* Note that memory counting is by default only included when either debug
 * or statistics are included, and is otherwise absent.
 * Nevertheless, it can be forced by defining _MEMCOUNT before including
 * this header. */
#include "astreamfs_worker_mem.h"

/**************************************************************************
 * Typical use by the application (the fuse driver).
 *
 * The application has 5 function calls to use:
 * -1) aw_init: to call once when the application starts
 * -2) aw_destroy: to call in the fuse 'destroy' handler
 * -3) aw_close: optional, but to call at 'open' if the application re-uses
 *		 the file reference 'fh' (see below) values to be sure there
 * 		 are not 'bits and pieces' of an old stream using the same 'fh'.
 * -4) aw_read
 * -5) aw_read_buf: are as fuse expects them. The application can put them
 * 		 directly in the fuse_operation structure, or provide its own
 *		 that will do some stuff then call aw_read/aw_read_buf
 *
 * With aw_init the application has to provide a valid aw_operations structure
 * (see below). Those are callbacks that will be used by the asynchronous
 * workers threads when needed, same as fuse_operations works.
 * Only two MUST be provided: start_stream() (which starts a 'stream' aka a
 * sequential 'range' request from a position to EOF), and read(), to read on
 * that 'stream'.
 * It is highly recommended that the application provides close_if_idle() too.
 * This callback's purpose would typically free resources allocated to a
 * 'stream' (by start_stream) when the 'stream' is detected as idle or
 * forced-close.
 * Providing store_last_idx() and load_last_idx() is recommended
 * for optimisation purpose (and is easy to do!).
 * Providing fh_from_fi() is needed if the application does not directly
 * use 'fh' in the fuse_file_info structure as the file reference (see below).
 *
 * All other callbacks have sensible defaults and should be provided only
 * when needed.
 *
 * Unique remote file reference ('fh')
 * -----------------------------------
 *  When an user's program opens a file belonging to a fuse mount, fuse
 *  will call the open() driver's callback passing the path (as the fuse driver
 *  shows it) an a struct fuse_file_info *fi.
 *  As explained in the fuse documentation, the driver can make use of the
 *  'fh' field in the 'fi' structure to store private information (typically
 *  a pointer) and use it subsequent calls (read, close, etc...)
 *  The driver can also be stateless and not use 'fh' in which case you do
 *  not need this worker framework at all!
 *
 *  This worker framework explicitly needs a reference to know when this is
 *  the 'same remote file'. It cannot use the 'path' for that since the driver
 *  might represent several 'paths' pointing to the same remote file.
 *  So, to reliably get a 'unique remote file reference' the fh_from_fi()
 *  callback is used. If the caller does not provide such a callback, the
 *  default behaviour is to directly use the 'fh' field in the incoming 'fi'
 *  structure fuse_file_info.
 *  Providing this callback is then only necessary when the 'unique reference'
 *  to remote file is not stored in this 'fh' field but somewhere else.
 *
 *  Example when that helps:
 *  Let us consider the user has a remote content (file) of 8GB
 *  The user now issues 4 dd commands in parallel that would each read a
 *  2GB block from that file each starting at a different offset: 0, 2, 4, 6GB
 *  The end result is that the user has the full remote content in 4 chunks of
 *  2GB copied locally. The reads on the chunks were done in parallel.
 *  If the application used 'fh' directly in the fuse_file_info structure, the
 *  workers would still know this is the same remote content and proceed
 *  accordingly. Nevertheless, since the 'opt' opaque 32bit counter is
 *  stored/loaded at the 'fi' level, the application would have to provide a
 *  mechanism to store this 'opt' at the 'fi' level for maximum optimisation.
 *  Having used 'fh' from the fuse_file_info structure directly to point to
 *  the unique content, this becomes impossible because fuse does not provide
 *  another field in the structure for that: there is only 'fh' for private
 *  user data. So 'opt' would need to be stored at 'fh' level (not at 'fi'
 *  level!) which lowers the level of optimisation for this kind of "parallel
 *  sequential read".
 *
 *  On the other hand, if the user's program itself is doing this kind of
 *  parallelism ("download accelerators") with the same 'file handle' this will
 *  appear to be from a single 'fi' from fuse point of view, and there is no
 *  possible better optimisation similar to the one explained above.
 *
 *  So, indeed, using another level of indirection to get to 'fh' is coping
 *  only for those possible "parallel sequential" reads from different fuse
 *  file handles. Even if this optimisation is not done, the overhead is not
 *  at the level of inter-thread communication, it is only some atomical
 *  operations and a loop to retrieve the right 'stream' on each incoming read.
 *
 *  In conclusion the user should provide this fh_from_fi() feature:
 *  - if he already needs it and is not using 'fh' directly as the reference to
 *    the unique remote content.
 *  - for maximum optimisation in case of parallel reads from different 'fi'.
 *
 *
 * Statistics:
 * -----------
 * 	Statistics are provided, when they are builtin (by default).
 * 	To build without statistics define NOSTATS at build.
 *
 * 	Macro _STATS can be used for conditional code that needs statistics.
 *	Macro STATS_AVAILABLE (true or false) can be used at runtime.
 *
 * 9) aw_init_stats must be used to initialise stats. It is defined as
 *	a macro returning ENOSYS (not implemented) when built without stats.
 *
 * 	If the calling program uses statistics conditionnally according to
 *	some options passed, just don't call aw_init_stats when not needed.
 *	In such case, all statistics calls are replaced by dummy functions
 *	that do nothing but return.
 *	"Real" statistics code is then run only
 * 	- when built in
 *	- when statistics have been initiliased with aw_init_stat.
 *
 * Writing:
 * --------
 * Writing cannot be done in parallel because order matters when write blocks
 * overlap. Thus it cannot benefit the same from this architecture, and the
 * only benefit would be, when buffering, to do asynchronous writes.
 * It is a //TODO to provide that cleanly.
 **************************************************************************/

/* Constants that can be changed, but there is no reason why! */
#define UNKNOWN_OPT (UINT32_MAX)
struct aw_operations {

	/** aligned_alloc() replacement.
	 *  Default (if NULL): use aligned_alloc()
	 */
	void * (*aligned_alloc) (size_t alignment, size_t size);

	/** free() replacement.
	 *  Default (if NULL): use free()
	 */
	void   (*free)   (void *ptr);

	/** For read_buf, buffers are allocated by the driver and freed
	 *  elsewhere. If the provided functions where counting how much
	 *  memory is allocated/free, that would look as a leak.
	 *  So, in such case, release() will be called to compute memory that
	 *  'will be freed', but does not actually free it.
	 *  Default (if NULL): no operation
	 */
	void   (*release)   (void *ptr);


	/** Function called to write log messages.
	 *
	 *	This has the same semantics as the standard syslog function.
	 *
	 *	LOG_DEBUG levels are only used when this is compiled with
	 *	DEBUG as a defined symbol. By default no debug logging code
	 *	will be generated.
	 *
	 *  Default (if NULL): no logging
	 */
	void    (*log)  (int priority, const char *format, ...);

	/** Function called on system error (apart from malloc).
	 *
	 *	System errors are typically semaphore error codes or the like.
	 *	The result of the system call is passed to the function.
	 *	The function should either exit the program, considering
	 *	such errors are critical thus never return.
	 *	Returning to the caller after such errors will probably
	 *	trigger undefined behaviour.
	 *
	 *  Default (if NULL):	a function that simply exits with the error code
	 *
	 * @param the error (0 = no error)
	 * @param line where the error happened
	 * @return the error (but see comment, it should not return!)
	 */
	int    (*error)  (int res, int line);

	/** Function called when starting a stream to "anticipate" before what
	 *  is requested as 'offset' for reading.
	 *
	 *	This is useful considering the kernel might send read requests
	 *	in parallel, and they can arrive "in reverse order".
	 *	If such thing happens at the start of the stream and there was
	 *	no anticipation, that would mean opening another stream/solo.
	 *
	 * 	This is called in the main loop, and should be fast: typically
	 *	some arithmetic computation and tests.
	 *
	 *	It is ok to leave the default function, it does a good job.
	 *
	 *  Default (if NULL):	custom algorithm (look at the code!).
	 *
	 * @param reference to the file (as in fh from fuse_file_info)
	 * @param a clue given when close reads exist (or NO_ANTICIPATION_CLUE)
	 * @param size of the first read request that triggered the new stream
	 * @param offset where to start reading for that new stream
	 * @return how much 'anticipation' to apply. Actual start of stream
	 *	   would then be (offset - anticipation) to cope with the
	 *	   case where the first 2 read requests of a new stream are
	 *	   in reverse order (which happens!).
	 */
	size_t (*anticipation)(uint64_t fh, size_t anticipation_clue,
			       size_t size, off_t offset);

	/** Function called when the engine gets a recoverable error
	 *
	 *	Recoverable errors is when a read retry could work, it happens
	 *	- when the socket reports an error, times out, or is
	 *	  unexpectedly closed by the server.
	 *	- when the read callback returns 0 (which means either end of
	 *	  of file or connection closed by server)
	 *	- when -EREMOTEIO is returned by either read or start_stream
	 *	  callbacks.
	 *
	 * 	In such case, the retry callback is invoked with fh and the
	 *	offset where the retry would happen. Since the engine has no
	 *	notion of when the file is supposed to end, the callback should
	 *	make use of offset to determine whether a retry is necessary.
	 *
	 *	The callback should return the number of retries or zero if
	 *	no retry is expected, in which case the error will be returned
	 *	to the read function (or zero if end of file/connection closed).
	 *
	 * 	IMPORTANT: it is essential, when retries are needed, that the
	 *	read and start_stream functions return -EREMOTEIO when they
	 *	think the condition is not final and read can be retried.
	 *
	 *	NOTE: anticipation is not used when retrying.
	 *
	 *  Default (if NULL):	no retry (i.e. returns 0).
	 *
	 * @param reference to the file (as in fh from fuse_file_info)
	 * @param offset where to retry reading for that stream
	 * @return how many retries are expected (zero not to retry).
	 */
	unsigned int (*retry)(uint64_t fh, off_t offset);

	/** Function called to check if a given stream is considered idle, and
	 *  close it.
	 *
	 *	This is used regularly to check whether the application
	 *	considers this stream idle and then will close it.
	 *
	 *	Checking and closing are supposed to be fast because they
	 *	are called from the main loop.
	 *
	 * 	When the force flag is set (true) closing is forced to happen.
	 *	Times has then no importance and should not be tested.
	 *
	 *	Using the default function will probably be very unoptimised
	 *	and would require the application to clean sh somewhere else.
	 *	So it is highly recommended that this function be provided!
	 *
 	 *  Default (if NULL):	assumes the handle is never idle (returns false)
	 *
	 * @param reference to the file (as in fh from fuse_file_info)
	 * @param stream handle
	 * @param time when the last read request was responded
	 * @param time when the current read request arrived
	 * @param flag to force closing even if not considered idle
	 * @return if stream handle is considered idle (true) or busy (false)
	 */
	bool   (*close_if_idle)(uint64_t fh, void *sh,
				time_t last_read, time_t now, bool force);

	/** Optimisation function called to store an opaque optimisation data
	 *  Works with the next one. It is called only when needed.
	 *
	 *  Using the default works but is less optimised and each read
	 *  request might incur thread bouncing and delays.
	 *
	 *  Typically this function would do an atomic store of data in
	 *  a location linked to 'fi', and the load would do the atomic load.
	 *  Since it is an optimisation, relaxed load/store are fine.
	 *
	 *  Default (if NULL):	does nothing.
	 *
	 * @param struct fi from fuse (returned at open)
	 * @param the last opaque optimisation data to store
	 * @return none
	 */
	void (*store_last_opt)(struct fuse_file_info *fi, uint32_t opt);

	/** Optimisation function called to get the opaque optimisation data
	 *  When streaming, the optimisation avoid a potentially costly
	 *  search on all the streams to find the right one.
	 *
	 *  Using the default works but is less optimised.
	 *
	 * NOTE: either provide both functions or none. If only one is
	 *	 provided, an error (EINVAL) will be returned.
	 *
	 *  Default (if NULL): returns UNKNOWN_OPT.
	 *
	 * @param struct fi from fuse (returned at open)
	 * @return the last opaque optimisation data
	 */
	uint32_t (*load_last_opt)(struct fuse_file_info *fi);

	/** Optimisation function called to get the unique remote file reference
	 *  from the struct fuse_file_info pointer 'fi'.
	 *
	 *  See detailed comments above about the unique remote file reference.
	 *
	 *  Default (if NULL): uses the 'fh' field in the 'fi' structure.
	 *
	 * @param struct fi from fuse (returned at open)
	 * @return the 'fh' private user data
	 */
	uint64_t (*fh_from_fi)(struct fuse_file_info *fi);

       /* Network real reading.
        * IMPORTANT: these functions have no default and MUST be provided! */
	/** Function called to start a new 'stream' at a given offset
	 *
	 *	When the 'size' argument is zero, it is expected that
	 *	start_stream will do a range request starting at offset and up
	 *	to the end of file (like: offset-)
	 *	When 'size' is not zero (used for "solo" requests) the range
	 *	must start at offset and be of the specified size.
	 *
	 * 	IMPORTANT: for consistency, error return codes follow the
	 *		   fuse policy to be negative. That allows read/write to
	 *		   distinguish between number of bytes and errors.
	 *		   This call does not need the distinction but still
	 *		   follows the rule, same as open callbacks for fuse.
	 *		   Thus to returning meaningful system codes examples
	 *		   would be: -ENOENT, -EACCESS, or -ENODATA as below.
	 *
	 *	When an 'impossible range' (i.e. past the end of file) is
	 *	requested, the callback is expected to return the
	 *	standard error: -ENODATA.
	 *
	 *	'out' parameters should be populated on success (return is zero)
	 *	- sh: a stream handle opaque pointer passed to subsequent calls
	 *	      to the 'read' and 'close_if_idle' callbacks
	 *	- fd: the file descriptor (used for polling)
	 *
	 *      When return code is an error (negative), the 'out' parameters
	 *	are irrelevant. -EREMOTEIO error see 'retry' callback above.
	 *
	 * 	This is called out of the main loop, in the fuse thread, because
	 *	the time to start a stream can be long (TLS handhake, etc...)
	 *
	 *  Default: none, MUST be provided.
	 *
	 * @param path of the file
	 * @param struct fi from fuse (returned at open)
	 * @param size when doing "single read" for regular streams : 0
	 * @param offset where to start reading
	 * @param where to store the opaque stream handle (passed to read/close)
	 * @param where to store the file descriptor (for polling)
	 * @return Ok = 0, Error = negative number returned as read error.
	 */
       int (*start_stream)(const char *path, struct fuse_file_info *fi,
			   size_t size, off_t start_offset, void **sh,
			   int *fd);

	/** Function called to read a chunk of a stream
	 *
	 *	This has the same semantics as the system read function,
	 *	except for return value on error. Instead of returning -1
	 *	and setting errno, the error must be returned as a negative
	 *	value, same as expected by fuse (eg return -errno)
	 *
	 *	Since polling is used by the main loop of the asynchronous
	 *	reader, it is perfectly fine if this read callback does not
	 *	retrieve as much data as expected. It can also happen that
	 *	read returns no data although the socket was ready (eg openssl
	 *	buffering), in such case it should return -EAGAIN. This
	 *	discriminates from returning no data with no error, which
	 *	means End of File: only case this function should return 0.
	 *
	 *  Default: none, MUST be provided.
	 *
 	 * @param opaque stream handle (returned by start_stream) to read from
 	 * @param buffer where to store read data
 	 * @param count max bytes expected
 	 * @return Ok: number of bytes read, KO: negative error
	 */
       ssize_t  (*read)(void *sh, void *buf, size_t count);
};

/**
 * @brief initialise operations for the asynchonous reader
 *
 * The aw_operations structure passed must have non NULL functions for
 * start_stream and read, otherwise EINVAL is returned.
 * All the other members can be NULL, and in this case the statically
 * initialised defaulf will be used.
 *
 * On top of initialising the aw_operations structures, this functions also
 * initialises various semi-constants, and starts the asynchronous reader
 * thread. Should that fail, an error is returned.
 *
 * The default number of threads which is MIN( 1, available_procs / 2 ).
 * The maximum number of threads really configured is always limited to the
 * number of available procs, and to N_STREAMS / MIN_STREAMS_BY_FILE,
 * whichever the lowest.
 *
 * If the user specifies min/max_thrds, the function will try to find a
 * suitable number of threads inside the boundaries explained above.
 * Specifying 0 for either min or max_thrds means that the caller does not care
 * respectively about min or max.
 *
 * @param pointer to aw_operations to use.
 * @param minimum number of threads (0 = unspecified, use default).
 * @param maximum number of threads (0 = unspecified, use default).
 * @return 0 or error
 */
/* IMPORTANT: this function is not thread-safe and should be called only once.
 * ---------  Also bear in mind that fuse will kill all threads started
 *	      before the main loop. So, don't call it before calling fuse_main
 *	      otherwise the asynchronous thread will be terminated!..
 *	      The ideal place to call this function is within the .init of the
 * 	      fuse operations. If it is called later (for instance at the first
 *	      read), be sure to have the right synchronisation primitives!
 */
int aw_init(struct aw_operations const *a,
	    unsigned int min_thrds, unsigned int max_thrds);

/**
 * @brief terminates the asynchonous reader
 *
 * This will typically be called with the 'destroy' handler of the fuse
 * application.
 *
 * There must be no pending request at that moment, but normally the fuse
 * library checks that and won't allow 'destroy' if resources are still used,
 * instead fusermount -u will return a busy error message.
 *
 * @param none
 * @return none
 */
void aw_destroy(void);

/**
 * @brief get information of main internal constants of the async reader
 *
 * If this is called before aw_init, n_threads will be set to 1.
 *
 * @param pointer to aw_info structure to fill out.
 * @return none
 */
struct aw_info {
	unsigned int n_streams;	/* Number of streams configured  */
	unsigned int n_threads; /* Number of threads initialised */
	unsigned int block_size;/* Block size used for big reads */
	unsigned int max_gap;	/* Read ahead, multiple of block_size */
};
void aw_get_info(struct aw_info *info);

/**
 * @brief optional, when the caller wants to force close all streams of a 'fi'
 *
 * It is not necessary for the application to call this function at fuse
 * 'release' since there is a 'background cleaner' triggered when the
 * asynchronous reader has been idle for long enough (CLEAN_WAIT seconds) that
 * will close individual streams when they become idle. Streams will also be
 * reclaimed when needed by other files.
 *
 * Nevertheless, the streams association to a file ('fi') is based on the value
 * of 'fh' in the struct fuse_file_info. Should the application want to reuse
 * those 'fh', it should first call aw_close with that 'fh' to be sure there are
 * no bits and pieces remaining of the 'old' file using the same 'fh'.
 *
 * Note that, to be fast, it just sends a one-way message to the async-reader
 * and does not wait for the response, which means there is no return code.
 * If there were streams with read pendings with the same 'fh', it would
 * probably be a coding error of the caller, those pending reads will be
 * terminated with EINTR, and the streams will be closed and released.
 *
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return none
 */
void aw_close(uint64_t fh);

/**
 * @brief this is the read callback as fuse wants it!
 *
 *
 * @param path of the file to read.
 * @param buffer where data is to be copied.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
int aw_read(const char *path, char *buf, size_t size, off_t offset,
	    struct fuse_file_info *fi);

/**
 * @brief this is the read_buf callback as fuse wants it!
 *
 * It does also support zero sized reads, which can be handy to have the
 * bufp vector correctly initialised.
 *
 * @param path of the file to read.
 * @param buf vector to be allocated and populated.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
int aw_read_buf(const char *path, struct fuse_bufvec **bufp,
		size_t size, off_t offset, struct fuse_file_info *fi);

/**
 * @brief returns a suitable anticipation buffer given buffer size & offset
 *
 * This default method is exposed to that it can be called by a user
 * provided anticipation function
 *
 * @param file reference: 'fh' as in the struct fuse_file_info pointer
 * @param a clue given when close reads exist (or NO_ANTICIPATION_CLUE)
 * @param size of the requested read
 * @param offset of the requested read
 * @return size of suitable pre-buffer
 */
size_t aw_default_anticipation(uint64_t fh_unused, size_t anticipation_clue,
			       size_t sz, off_t offset);

/* When there is no anticipation clue, the anticipation function will be called
 * with NO_ANTICIPATION_CLUE as second parameter. Zero is then a valid
 * anticipation clue. */
#define NO_ANTICIPATION_CLUE \
		(unsigned long)((4 == sizeof(size_t)) ? UINT32_MAX : UINT64_MAX)


/* Statistic related functions */

#ifdef _STATS

struct aw_stat_times {
	struct timespec	t_sum;	/* Sum     of all times */
	struct timespec	t_max;	/* Maximum of all times */
};

struct aw_threads {
	unsigned long		n_files;
	unsigned int 		active_files;
};
struct aw_files {
	uint64_t		fh;	/* File handle (fuse_file_info.fh)    */
	uint8_t			tid;	/* Thread id where file is attached   */
	uint8_t			n_rq;	/* Number of pending read requests    */
	uint8_t			n_buf;  /* Number of buffers 		      */
};

struct aw_stats {
_Alignas(LEVEL1_DCACHE_LINESIZE)
	union	{
		struct aw_threads g;
		struct aw_files	  f;
	}			u;
	unsigned int		active_streams;
	unsigned long		cur_speed;	/* Current speed in bytes/s   */
	unsigned long		avg_speed;	/* Average speed in bytes/s   */

	unsigned long		n_streams; 	/* Number of streams so far   */
	unsigned long		n_retries; 	/* Number of retries so far   */
	unsigned long long	n_reads; 	/* Number of read received    */
	unsigned long long 	size;		/* Total size of the reads    */
		 long long	overread;	/* Overread is due to
		   anticipation and gaps being freed without having been used */
	struct aw_stat_times	read_times;	/* Times for normal requests  */
	struct aw_stat_times	start_times;	/* Times for normal requests  */
};

struct aw_solo_stats {
			/* This is for solo statistics. Average speed can
			 * be computed by the caller. */
	unsigned long long	solo_n_reads;
	unsigned long long	solo_size;
	struct aw_stat_times	solo_times;
};

/**
 * @brief initialise statistics
 *
 * Same important note as above, it should be called at startup, otherwise
 * statistics could be completely incoherent.
 *
 * This should also be called AFTER aw_init so that is uses the custom
 * functions passed with aw_oper instead of the default ones.
 *
 * When built with NOSTATS:
 * 		replaced by a macro evaluating to ENOSYS (not implemented)
 *
 * @param None
 * @return 0 or error
 */
int aw_init_stats();

/**
 * @brief retrieve statistics
 *
 * First get the raw copy of statistics from the async reader then do
 * all the computations within the fuse stream.
 *
 * IMPORTANT NOTE: the t_stats structure returns must be big enough to hold
 * 	global and threads statistics.
 *	One should then call aw_info() to get the real number of threads of
 *	the async reader, and provide a memory buffer at least:
 *      	(n_threads + 1) * sizeof(struct aw_glob_stats)
 *
 * 	The first (t_stats[0]) aw_glob_stats is the total of all threads.
 *	Then each successive one represent each thread stats.
 *
 * 	Same for f_stats, the buffer should be at least of size:
 *      	(n_streams) * sizeof(struct aw_glob_stats)
 *	n_streams is also returned by the call to aw_info.
 *
 * @param pointer on statistics to return for threads (and total)
 * @param pointer on statistics to return for files
 * @param pointer on statistics to return for solo
 * @return 0 or error
 */
void aw_get_stats(struct aw_stats *t_stats, struct aw_stats *f_stats,
		  struct aw_solo_stats *s_stats);

#else
#define aw_init_stats() (ENOSYS)
#define aw_get_stats(t,f,s)
#endif // _STATS

#endif // ASTREAMFS_WORKER_H
