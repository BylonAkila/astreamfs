/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**************************************************************************
 * Header to be included only in astreamfs_reader.c, not into "client code",
 * so that structure definitions are always visible
 */

#define CLOCK_RES (1000000000UL) /* Clock resolution is nano second           */
	/* Look at the algorithm to change it. SPEED_RES must be even	      */
#define SPEED_RES (8UL)	/* 8 ticks per second to compute transfer speed       */
#define SPEED_MEM (2UL)	/* 2 seconds of speed memory to avoid spike artefacts */

#define SPEED_SZ (SPEED_RES * SPEED_MEM) /* Convenience macro */

typedef unsigned long long tick_t;

struct spd {
_Alignas(LEVEL1_DCACHE_LINESIZE)
	uint32_t   speed    [SPEED_SZ];	/* For current speed */
	unsigned long long tick_dl;	/* Divider for average: busy ticks */
	unsigned long	   tick_bits;	/* Bit matrix of busy ticks */
	tick_t		   tick_now;	/* When the bit matrix starts */
};

#ifdef _STATS

/**
 * Functions used outside of this file (in astreamfs_reader.c itself) are
 * accessed indirectly, so that they are replaced by dummies when stats
 * are built but not started (aw_init_stats not called) */

struct aw_stats_operations {
	void   (*move_tm)(struct timespec *dest, const struct timespec *src);
	void   (*raw_copy)(const struct private_data *pdata, struct action *a);
	void   (*count_retries)(sid_t sid);
	void   (*count_streams)(bool inc, sid_t sid);
	void   (*count_over)(ssize_t written,  sid_t sid);
	void   (*count_files)(bool inc);
	void   (*update_start_time)(const struct action *a);
	void   (*set_time) (struct private_data *pdata);
	void   (*get_time) (struct timespec *tm);
	void   (*update_read_time)(struct timespec *tm0, struct timespec *tm,
				   const struct action *r);
	fid_t *(*get_slot)(uint64_t fh);
	fid_t *(*release_slot)(void);
	void   (*clean_old)(uint64_t fh, fid_t fid);
	void   (*destroy) (void);
};

static struct aw_stats_operations *stats_ops;

#endif
