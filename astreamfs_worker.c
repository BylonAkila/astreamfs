/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**************************************************************************
 * Feature: this intends to implement a generic "streaming" engine
 *	    that would be suitable to use with any "network" fuse driver.
 *
 * The specific features from each file systems are implemented with the
 *	same principle used by fuse with the "operation" structure
 *
 * Principles and vocabulary:
 * --------------------------
 *	As tested with the "new_engine" branch, performing the read task
 *	within the incoming fuse read thread is not ideal:
 *	- locking is complex
 *	- it is not optimised due to cache bouncing
 *	- depending on the governor used, it can have side effect not to
 *	  trigger the frequency stepping because the load on each core is
 *	  low, resulting in a slow throughput.
 *
 *	The principle is then the same as used in the original code with
 *	improvements and allowing generic read.
 *
 *	First, reader threads can "multiplex" the reads with a poll-like
 *	mechanism.
 *	"Slow" actions like starting a thread are delegated back to the
 *	incoming read thread.
 *
 * 	The first reader thread also works as orchestrator. It handles
 *	distribution of files to the other threads and statistics.
 *
 *	This architecture has many advantages:
 *	- "read" is straightforward, it simply delegates to the reader thread.
 *	- no locks are necessary at the exception of statistics for "solo" reads
 * 	  which do not interfere with main operations and are anyway slow.
 *	  Only semaphore/eventfd between the readers and the read thread and
 *	  some _Atomics are needed.
 *
 *	This has a single inconvenient (same as the original architecture):
 *	- each read "costs" the equivalent of 2 context switch with semaphores.
 *	  Due to the readahead technique used by kernel/fuse, this "cost" should
 *	  be almost invisible on sequential reads, because it is in parallel
 *	  with the actual client reads. On non-sequential, the cost of the
 *	  context switches is nothing compared to opening a new stream!
 *
 *-------------
 * Synchronisations:
 *	There are two different algorithms running.
 *	Files.
 *	  When the first read occurs on a new remote file, a thread must be
 *	  selected to handle this file for efficiency reason: it allows a better
 *	  and easier determination of sequentiality. The read request is then
 *	  sent to the orchestrator that selects the relevant thread. Note that
 *	  when this is a new file handle on the same remote file, the thread
 *	  that is already handling the remote file is selected.
 *	  This worker thread receives the file index in the orchestrator's table
 *	  (fid) where this file was stored, so that statistics on this file can
 *	  be done by the selected worker thread with absolutely zero lock.
 *	  Then the worker thread marks the read request with it's own thread id
 *	  (tid) and the stream used for that read (sid), so that subsequent
 *	  reads on the same file handle can arrive directly on the thread
 *	  bypassing the orchestrator. This optimisation happens if the user of
 *	  the library has made available encode/decode functions, which is
 *	  highly recommended, otherwise all subsequent read traffic will
 *	  continue to go through the orchestrator an suffer from one more
 *	  context switch (on top of more local computations).
 *
 *	  Now at some point the reader thread will determine that there is no
 *	  more activity on that remote file (no new requests after the
 *	  specified time-out, see is_idle() callback), releases it, and inform
 *	  the orchestrator (free).
 *
 * 	  The corner case that must be protected is when the 'free' message
 *	  travels to the orchestrator, if at the exact same time new requests
 *	  on the same file were orchestrated to the worker thread, there could
 *	  be a discrepancy of states, orchestrator freing the association from
 *	  the file to the worker, but worker having that file active.
 *	  The protection with NO atomics or locks is that the orchestrator
 *	  counts how many requests have been sent to a given worker. When a
 *	  worker frees a file it sends back how many requests have arrived from
 *	  the orchestrator. So when the orchestrator receives a "free file"
 *	  information, if the requests arrived matches the requests sent, it
 *	  knows there are no requests in transit, and the freeing can happen
 *	  safely. Otherwise, it means there are still some requests in transit
 *	  and the orchestrator simply ignores the freeing information since
 *	  the worker will reopen the slot for this file once any of the
 *	  request in transit finally arrives.
 *
 * 	  Note that for the forced 'close', to respect the closing order,
 *	  the orchestrator does not free the assotion right away but instead
 *	  forwards the close to the relevant worker. The 'free' that follows
 *	  will do the dis-association in the right order.
 *
 * Streams:
 *	  Now the reader thread has a read request to serve on a remote file.
 *	  It will first try to find an already running stream on that file that
 *	  would allow for sequential read, since this is the best case scenario.
 *	  encode/decode allow for that, and this is the first test made so
 *	  that it is hopefully very quick.
 *	  When no stream matches (or it is the first read ever on that remote
 *	  file), the reader thread has to get a new stream and start a range
 *	  request at the relevant position.
 *
 * 	  To avoid having to do a round trip with the orchestrator to reserve a
 *	  stream, this is done directly in the reader thread using bits of a
 *	  single word (64bits or 32bits according to the OS). Via a tight atomic
 *	  exchange loop the thread grabs one of the available bits in this word.
 *	  The stream at the grabbed index becomes exclusive for this thread.
 *	  A similar atomic exchange loop happens when the stream is released.
 *
 * 	  If there are no more available streams, "solo" operations are done.
 *	  That is extremely slow and what naive drivers can program easily.
 *	  In "solo" operation, each read requires a range request towards the
 *	  server. Since most traffic is now https, the performance can really
 *	  become ugly at this point... but the reads will still happen.
 *
 * 	  So, the synchronisation for streams is even more efficient, it
 *	  uses a single word. This is why there are "only" 64 streams (on 64bits
 *	  OS), but it should be sufficient in many situations!
 *	  If it were not enough, it is only a matter of changing the get/release
 *	  streams algorithm, with a lock any arbitrary size is possible.
 *	  There is no thread communication needed for that synchronisation.
 *
 * ------------------------------------------------------------------------
 * Gaps:
 *	It could be expected that a sequential read (as many standard operations
 *	do) would result is read requests arriving in clean sequential order
 *	in the driver. That would be too simple! :-)
 *	Not only requests can arrive in mixed order, but can vary in size.
 *	Kernel will only send multiple of 4K requests (unless Direct/IO?), and
 *	would, on "normal read-ahead" send blocks of 128k (32 x a page of 4k).
 *
 *	In most common situations, nice blocks of 128k arrive, and they can be
 *	in any order in a sequence of 4. Sequential would be 1-2-3-4, but there
 *	are number of chances the actual arrival is 3-1-4-2 or any.
 *	In this case the driver continues to read in sequence in self allocated
 *	"gaps", and will serve the buffered data whenever a "late" request
 *	arrives. If the "gap" is already full, there is even a chance the
 *	'snatch' optimisation can be applied, which will respond immediately
 *	to the requester.
 *
 *	This is why 'gaps' are chunked into pieces of 128k (or less), so that
 *	there is a chance of perfect optimisation if that gap was fully read and
 *	a request arrives late, in read_buf case it can be responded with no
 *	memcpy.
 *
 *	NOTE, 32bits kernels can suffer from "memory fragmentation" and might
 *	send some smaller (than 128k) requests even in sequential situation.
 *	Also, some requests might be very late, that is why the gaps are kept
 *	longer before being considered useless and freed in 32bits.
 *	It can also happen with 32bits kernels that the kernel sends a
 *	readahead, caches it, then drops that cached part and queries it again.
 *	In this case, there is nothing the driver can do to "fix" this kernel's
 *	behaviour, and the read will have to be done again on a new stream.
 *	Same can obviously happen with any kernel if the kernel caches are
 *	intentionnally dropped (e.g. echo 3 >/proc/sys/vm/drop_caches)
 *
 *
 **************************************************************************/

#include "astreamfs_worker.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <syslog.h>
#include <unistd.h>
#include <errno.h>
#include <sys/eventfd.h>
#include <pthread.h>
#include <stddef.h>
#include <poll.h>

/** CONSTANTS  - START */

/* Constants that can be changed */

#if UINTPTR_MAX == 0xffffffff
/* 32-bit */
	#define N_STREAMS (32)
	typedef uint32_t map_t;
#else
/* 64-bit */
	#define N_STREAMS (64)
	typedef uint64_t map_t;
	/* Number of concurrent streams for the asynchronous reader.
	 * Note each stream can consume by default 512k of "gap buffers",
	 * which makes in theory a maximum of 32MB of RAM (+ struct size) in
	 * 64bits. It should not be more than 255 (with current code since
	 * idx_t is currently uint8_t) Also, beware of the get/release algorithm
	 * that an uint64/32_t map of bits when changing that!.. A different
	 * algorithm would have to be provided for higher numbers */
#endif

#define N_FILES N_STREAMS	/* Max number of concurrent files, this is
				 * equal to streams, best case being 1 stream
				 * only per file */

#define MIN_STREAMS_BY_FILE (4)  /* Lowest max concurrent streams allowed */
#define MAX_STREAMS_BY_FILE (32) /* Highest max concurrent streams allowed */

#define POLL_WAIT    (1000) /* Standard time to wait when polling on reads in
			       milliseconds */
#define READ_TIMEOUT (10)   /* Timeout for reads (seconds) when polling. Note
			     * that since the main loop poll waits POLL_WAIT
			     * milliseconds for all possible sockets to poll,
			     * the timeout might be off by POLL_WAIT millisecs
			     * and is anyway truncated to seconds. */

#define CLEAN_WAIT (15)	/* Time with no read to trigger between occurrences
			   of the cleaning of old streams (seconds) */
#define CLEAN_MAX_WAIT (120)	/* Time to trigger cleaning, even if the
				   thread is busy. (seconds) */

#define O_THREAD (0)	/* Index of the orchestrator thread. There is no
			   obvious advantage to change that, but it is still
			   possible! */

/* DO NOT change the constants below unless you perfectly know what you
 * are doing! */

#define DEFAULT_PAGESIZE (4096) /* Default PAGESIZE used if the system call
				 * fails
				 */

#define MAX_PAGES_READ_AHEAD (32) /* Kernel constant: how much pages is the
				   * maximum possible readahead. That will
				   * determine the maximum gap size.
				   */

#define MAX_OUTSTANDING_REQUESTS (4) /* See comment above */

/** CONSTANTS - END */

typedef uint8_t fid_t; /* Type used to index files      */
typedef uint8_t sid_t; /* Type used to index streams    */
typedef uint8_t tid_t; /* Type used to index threads    */

/** Static variables - START */
/* Global semi-constants initialised at startup (aw_init) */
static long pagesize  = DEFAULT_PAGESIZE;
static size_t max_gap = \
	     DEFAULT_PAGESIZE * MAX_PAGES_READ_AHEAD * MAX_OUTSTANDING_REQUESTS;
static size_t rev_gap = \
	     DEFAULT_PAGESIZE * MAX_PAGES_READ_AHEAD * MAX_OUTSTANDING_REQUESTS;
static size_t max_gap_block_size = DEFAULT_PAGESIZE * MAX_PAGES_READ_AHEAD;

static tid_t max_threads = 1;
static sid_t share_of_streams = N_STREAMS; /* Streams by threads */
static _Thread_local tid_t tidx = 0; /* The index for each thread */

/** Thread related variables */
		/* This is semi-constants, they are set once when the threads
		 * are started during aw_init, then they never change. */
struct thread_consts {
	pthread_t aw_thread;
	int       event_fd;
};
static struct thread_consts *tc;

		/* This, on the contrary, are thread shared variables that
		 * change frequently. They must be atomic and aligned on a
		 * cacheline to avoid un-necessary cache bouncing. */
struct thread_shared {
 _Alignas(LEVEL1_DCACHE_LINESIZE)
	_Atomic (struct action *) actions_queue;
};
static struct thread_shared *ts;

		/* This, is private (as NOT shared) data of each thread,
		 * they are initialised on the stack of async_worker and
		 * passed down to each function that need them. */
struct private_data {
			/* The array of file descriptor has one more entry: the
			 * action eventfd as item 0, so that to be able to
			 * 'poll' both for actions and reads */
	struct pollfd fds[1 + N_STREAMS];
	time_t now;		/* Time for each main loop */
	time_t last_activity;	/* To trigger the cleaning of old streams */
	time_t last_clean;	/* To trigger the cleaning of old streams */
	bool   cleaned;		/* Flag if cleaned this "run" */
	nfds_t mfds;		/* Highest file descriptors to poll */
	unsigned int n_files;   /* Number of files on this thread */
	unsigned int nst;	/* Number of currently active streams */
	unsigned int pst;	/* Number of streams that need polling */
	struct action *actions;	/* Poped actions become private */
	uint64_t recv[N_FILES]; /* Number of orchestrated req. for this file */
	sid_t owned_streams;	/* Head of linked list of owned streams */
	sid_t list[N_STREAMS];	/* Linked list of owned stream */
};
#define NOT_OWNED (UINT8_MAX)
#define OWNED(idx) (NOT_OWNED != (idx))

struct start_data {	/* Transient data used to start each async_worker */
	tid_t tid;
	sem_t *sem_done;
	int res;
};

enum action_type {
	ACTION_READ	 = 0,	/* From read/read_buf sent to async workers */
	ACTION_GAP	 = 1,	/* Used internally by async worker for gaps */
	ACTION_START	 = 2,	/* From async to fuse and back */
	ACTION_SOLO	 = 3,	/* Return action when no streams avail*/
	ACTION_DESTROY	 = 4,	/* Called at fuse destroy normally! */
	ACTION_STATS_SET = 5,	/* It does not change the size of the enum */
	ACTION_STATS_GET = 6,	/* to always define values for stats! */

			/* Note: 8 and 16 for bitwise testing */
	ACTION_CLOSE	 = 8,	/* From caller to Thread 0 or inter threads */
	ACTION_FREE	 = 16,	/* From Thread !=0 to 0 to realease a fh    */
};

	/* Streams that are in use have one of those 3 states:
	 * ACTIVE_TRF: means that there are some read/write requests pending and
	 *		that polling is not necessary (although read might
	 *		still return 0 bytes, this is not an error nor EOF)
	 * ACTIVE_POLL: means that there are some read requests pending and
	 *		that polling is now necessary. That is because the
	 *		last read() returned -EGAIN.
	 * INACTIVE: when no read requests are pending. Polling is therefore
	 *	     not necessary either since no data is expected at the
	 *	     at the moment.
	 * STARTING: used to define the fd value at start of stream
	 * FREED: used to reset fd at -1 when the stream is closed
	 */
enum stream_state {
	ACTIVE_TRF  = 0,
	ACTIVE_POLL = 1,
	INACTIVE    = 2,
	STARTING    = 3,
	FREED       = 4,
};

struct gap {
	char	*buf;
	off_t	offset;
	size_t	size;
	size_t	written;
	uint8_t	retry;	/* 0 or retries left (1 = last retry) */
};

struct read_action {
	uint64_t 		fh;	/* From read_buf: fh in fuse_file_info*/
	struct fuse_bufvec	*bufp;	/*  ""	bufvec where to store bytes   */
	off_t			offset; /*  ""  offset of this block in file  */
	size_t			size;	/*  ""  requested size to read        */
	ssize_t			written;/* (out) how much was written to buf  */
	off_t			start;  /* (out) where to actually start read */
	uint64_t		rq_id;	/* Set from atomic by common_read     */
	sid_t			sid;	/* (in & out) fast association index  */
	tid_t			tid;	/* (out) fast association index       */
	fid_t			fid;	/* (inter) file index for stats       */
	uint8_t			retry;	/* 0 or retries left (1 = last retry) */
};

#define NO_START ((off_t)-1)	/* Changed to positive of zero when a start
				   stream pushed back to the incoming thread. */

struct close_action {
	uint64_t fh;
};

struct free_action {
	uint64_t fh;
	uint64_t recv; /* Number of received requests from orchestrator */
	tid_t tid;
};

struct start_action {
	struct timespec t_start;
	struct timespec t_end;
	uint64_t 	fh;
	void		*sh;
	int		fd;
	int		err;
	sid_t		sid;
};


#ifdef _STATS
struct stats_set_action {
	struct timespec start0;
	struct timespec start;
	struct timespec end;
	unsigned long long t_now;
	uint64_t fh;
	ssize_t	 written;
	enum action_type r_type;
	sid_t		sid;
	tid_t		tid;
};

struct stats_get_action {
	struct private_data *pdata;
	struct aw_stats *t_stats;	/* Thread stats (+ total of all) */
	struct aw_stats *f_stats;	/* File stats */
	struct spd *t_spd;		/* Thread speed counters (+ all) */
	struct spd *f_spd;		/* File speed counters */
};
#endif

struct action {
	struct action *next;
	enum action_type type;
	sem_t *sem_done;
	union {
		struct read_action  r;
		struct gap	    g;
		struct close_action c;
		struct free_action  f;
		struct start_action t;
#ifdef _STATS
		struct stats_set_action  ss;
		struct stats_get_action  sg;
#endif
	} a;
};

		/* This structure exactly fits in 64 bytes (typical cache
		 * line size) in 64bits. Alignment is still forced for 32 bits.
		 * Each stream is allocated to a single thread and accessed
		 * only by this thread. The attribution of streams to threads
		 * is done with an "atomic map" (see get_new_stream). */
struct stream {
_Alignas(LEVEL1_DCACHE_LINESIZE)
	sid_t			next;	/* Next in list: free or fh          */
					/* NOTE: single list (end element when
					 * 	 N_STREAMS) for free list.
					 * 	 circular list for files.    */
	fid_t			fid;	/* Index on thread 0's map           */
	unsigned int		n_rq;	/* Pending read requests on stream   */
	uint64_t		fh;	/* file to which this stream belongs
					   fh is as in struct fuse_file_info */
	void			*sh;	/* Opaque handle on the stream	     */
	off_t			pos;	/* Current read position	     */
	off_t			end;	/* End of last buffer on the stream  */
	uint64_t		last_rq;/* Last request served by this stream*/
	time_t			last_trf; /* Seconds are ok for the algo.   */
	struct action		*queue;	/* R/W actions + gaps on the stream  */
};

static struct stream strms[N_STREAMS];	/* The main stream array */

/** Inter-thread shared variables (hence atomic access) */

static _Atomic uint64_t rq_id = 0; /* Request id of the last queued read. */

static _Atomic map_t free_streams_map = UINTPTR_MAX;

/** "Orchestration" variables, used only by the O_THREAD */
	/* This serves for O_THREAD to manage which worker has which file */
static uint64_t o_fh  [N_FILES] = {0};
static uint64_t o_sent[N_FILES] = {0}; /* Number of sent req for free synchro */
static fid_t    o_list[N_FILES]; /* Linked busy/free list */

	/* first_idx inside o_top is busy list, one per thread, hence has
	 * to be allocated because the number of threads is variable */
struct o_main {
	unsigned int n_files;	/* Number of files on that thread */
	fid_t	     first_fid; /* Index of first file on that thread */
};
static struct o_main *o_top;	/* Busy list of file slots for all workers */
static fid_t o_free_head = 0;   /* Free list of file slots */


/** Static variables - END */

#include "astreamfs_worker_stats.h"


/*
 * @brief default logging does nothing
 *
 * @param priority
 * @param format string
 * @param variable parameters according to format string
 * @return 0
 */
static void def_log (int priority_unused, const char *format_unused, ...)
{
	(void)priority_unused;
	(void)format_unused;
}

/*
 * @brief default critical error handler function
 *
 * This handler simply exits with res, since nothing can be seriously done to
 * recover from critical errors!
 *
 * @param error code (or 0 if ok)
 * @param line number for debug purpose
 * @return O (no error) or does not return, exits instead with error code
 */
static int def_error(int res, int line)
{
	if (0 == res)
		return 0;
	else
		exit(res);
}

/*
 * @brief returns a suitable anticipation buffer given buffer size & offset
 *
 * This default method is exposed to that it can be called by a user
 * provided anticipation function
 *
 * @param file reference: 'fh' as in the struct fuse_file_info pointer
 * @param a clue given when close reads exist (or NO_ANTICIPATION_CLUE)
 * @param size of the requested read
 * @param offset of the requested read
 * @return size of suitable pre-buffer
 */
size_t aw_default_anticipation(uint64_t fh_unused, size_t anticipation_clue,
			       size_t sz, off_t offset)
{
	(void)fh_unused;

	if (NO_ANTICIPATION_CLUE != anticipation_clue)
		return anticipation_clue;

	if (max_gap >= offset)	/* Read from 0 if low offset */
		return offset;

	if (pagesize >= sz )	/* No anticipation is less/eq than a "page" */
		return 0;

	if (max_gap <= sz)		/* Max anticipation supported */
		return max_gap;

	if ((~(sz - 1) & sz) == sz)	/* Only true for powers of 2*/
		return sz / 2;	/* When buffers are inverted the second that
				 * was received first is at least double sz */

	/* Return the power of 2 above sz (sz is not 0 and below max_gap) */
	return (size_t)( 1UL << (sizeof(long) * 8 - __builtin_clzl(sz) - 1) );
}

/*
 * @brief default is no to retry
 *
 * @param file reference: 'fh' as in the struct fuse_file_info pointer
 * @param offset of the requested retry
 * @return 0 = no retry
 */
static unsigned int def_retry(uint64_t fh_unused, off_t offset_unused)
{
	(void)fh_unused;
	(void)offset_unused;

	return 0;
}

/*
 * @brief default is_idle function
 *
 * By default readers will never be considered idle.
 * This could quickly exhaust streams and create a lot of "solo" read.
 *
 * @param file reference: 'fh' as in the struct fuse_file_info pointer
 * @param stream handle
 * @param time when the last read request was responded
 * @param time when the current read request arrived
 * @return whether the network handle is considered idle (true) or busy (false)
 */
static bool def_close_if_idle(uint64_t fh_unused, void *sh_unused,
			      time_t last_read_unused, time_t now_unused,
			      bool force_unused)
{
	(void)sh_unused;
	(void)last_read_unused;
	(void)now_unused;
	(void)force_unused;

	return false;
}

/*
 * @brief default store_last_opt
 *
 * Optimisation function called to store an opaque optimisation data
 * Works with the next one. It is called only when needed.
 *
 * @param struct fi from fuse (returned at open)
 * @param the last opaque optimisation data to store
 * @return none
 */
static void def_store_last_opt(	struct fuse_file_info *fi_unused,
				uint32_t opt_unused)
{
	(void)fi_unused;
	(void)opt_unused;
}

/*
 * @brief default load_last_opt
 *
 * Optimisation function called to get the opaque optimisation data
 *  When streaming, the optimisation avoid a potentially costly
 *  search on all the streams to find the right one.
 *
 * @param struct fi from fuse (returned at open)
 * @return the last stream index
 */
static uint32_t def_load_last_opt(struct fuse_file_info *fi_unused)
{
	(void)fi_unused;
	return UNKNOWN_OPT;
}

/*
 * @brief default fh_from_fi
 *
 * Optimisation function called to get the unique remote file reference
 *  from the struct fuse_file_info pointer 'fi'.
 *
 *  See detailed comments about the unique remote file reference in hrader file
 *  astreamfs_worker.h
 *
 * @param struct fi from fuse (returned at open)
 * @return the 'fh' private user data
 */
static uint64_t def_fh_from_fi(struct fuse_file_info *fi)
{
	return fi->fh;
}

#include "astreamfs_worker_dbg.c" /* Include the debug-trace wrapper */

/*
 * @brief helpers for allocation
 */

static void *op_aligned_alloc(size_t alignment, size_t size)
{
	void *ptr;
	ptr = aw_ops.aligned_alloc(alignment, size);
	if (NULL == ptr)
		AW_ERROR(errno);
	return ptr;
}
#undef op_aligned_alloc

/* In all case, redefine those as macro since indirect call is used */
#define aligned_alloc(a,s) op_aligned_alloc(a,s)
#define malloc(sz) op_aligned_alloc(alignof(max_align_t), sz)
#define free(p) aw_ops.free(p)

/*
 * @brief post an action to a thread
 *
 * @param the read action to assign
 * @param the thread where to post the action
 * @return none
 */
static void post_action(struct action *a, tid_t tid)
{
	unsigned int spin = 0;

	AW_ASSERT(NULL != a);
	AW_ASSERT(tid < max_threads);

	a->next = atomic_load_explicit(&ts[tid].actions_queue,
				       memory_order_acquire);
	while(!atomic_compare_exchange_strong_explicit( &ts[tid].actions_queue,
							&a->next, a,
							memory_order_acq_rel,
							memory_order_acquire))
		spin++;
	AW_ERROR(eventfd_write(tc[tid].event_fd, 1));
	aw_count_contentions(spin);
}
#undef post_action

/*
 * @brief finish an action
 *
 * @param the action pointer
 * @return none
 */
static void finish_action(const struct action *a)
{
	/* Post semaphore if needed to "respond" */
	if (NULL != a->sem_done)
		AW_ERROR(sem_post(a->sem_done));
}
#undef finish_action



#include "astreamfs_worker_stats.c"


/*
 * @brief wait for action for the asynchronous reader
 *
 * "Action" is either:
 * 	- A message from a fuse stream calling for action
 *	- A file becoming ready for read
 *
 * There are 3 states for active streams:
 * - No request pending: neither polling nor trying to read is necessary
 *	fd: negative (events does not matter)
 * - Read can be possible: inital state after start or read received
 *	fd: positive, events: 0 (this will still catch errors when poll)
 * - Poll necessary: after read returned EGAIN
 * 	fd: positive, events: set
 *
 * @param private thread data
 * @return none
 */
static void wait_for_actions_or_reads(struct private_data *pdata)
{
	int res;
	int timeout;
	struct timespec now;
	now.tv_sec = 0;

	if (0 == pdata->nst)	/* when no active stream */
		if (pdata->owned_streams == N_STREAMS) {
			timeout = -1;	/* all closed: wait forever new action*/
		}
		else {	/* some streams are not closed: janitor */
			time_t wait_until;
			clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
			if (pdata->last_activity + CLEAN_WAIT <=
			    pdata->last_clean + CLEAN_MAX_WAIT)
				wait_until = pdata->last_activity + CLEAN_WAIT;
			else
				wait_until = pdata->last_clean + CLEAN_MAX_WAIT;
			if (now.tv_sec > wait_until)
				timeout = 0;	/* janitor time elapsed */
			else			/* wait janitor time    */
				timeout = 1000 * (wait_until - now.tv_sec);
		}
	else
		if (0 == pdata->pst)
			timeout = 0;
		else
			timeout = POLL_WAIT;

	if (0 != timeout) {
		res = poll(pdata->fds, pdata->mfds + 1, timeout);
		if (-1 == res) {
			int err = errno;
			aw_ops.log(LOG_ERR,
				   "main async poll error: %d.\n", err);
		}
		/* res == 0 is timeout, read timeout is handled in do_reads */
	}


	if (0 != pdata->fds[0].revents || 0 == timeout) {
		if (0 != (pdata->fds[0].revents & (POLLERR | POLLHUP))) {
			aw_ops.log(LOG_CRIT,
				   "eventfd for actions expectedly HUP or Error.\n");
			AW_ERROR(EINVAL);
		}
		else {
			/* There is at least one action to do, pop it */
			pdata->actions =
			      atomic_fetch_and_explicit(&ts[tidx].actions_queue,
							0,
							memory_order_acq_rel);
		}
	}
}
#undef wait_for_actions_or_reads

/*
 * @brief returns the current 'state' of a stream
 *
 * The state returned is always one of INACTIVE, ACTIVE_TRF, ACTIVE_POLL
 *
 * @param private thread data
 * @param the index of the stream to get the state
 * @return current state
 */
static enum stream_state get_state(const struct private_data *pdata,
				   sid_t sid)
{
	if (0 > pdata->fds[sid + 1].fd)
		return INACTIVE;
	if (0 == pdata->fds[sid + 1].events)
		return ACTIVE_TRF;
	else
		return ACTIVE_POLL;
}
#undef get_state

/*
 * @brief set fd for polling and re-compute the nfds (for poll)
 *
 * @param private thread data
 * @param the index of the stream on which the state applies to set fd
 * @param the old state
 * @param the new state
 * @param fd, used only when state is STARTING and FREED
 * @return none
 */
static void set_fd(struct private_data *pdata , sid_t sid,
		   enum stream_state old_state, enum stream_state state,
		   int fd)
{
	switch (state) {
 		case INACTIVE   : /* fd is negated, if not already */
				  if (0 > pdata->fds[sid + 1].fd)
					return;
				  pdata->fds[sid + 1].fd =
							-pdata->fds[sid + 1].fd;
				  if (ACTIVE_POLL == old_state)
					pdata->pst -= 1;
				  if (sid + 1 != pdata->mfds)
					break;
				  pdata->mfds--;
				  while (0 != pdata->mfds &&
				         0 > pdata->fds[pdata->mfds].fd)
					pdata->mfds--;
				  break;
		case ACTIVE_TRF: /* fd is made positive, if not already */
				  if (ACTIVE_POLL == old_state)
					pdata->pst -= 1;
				  pdata->pst -= 1;
				  /* fallthrough */
		case ACTIVE_POLL: pdata->pst += 1;
				  if (0 < pdata->fds[sid + 1].fd)
					return;
				  pdata->fds[sid + 1].fd =
							-pdata->fds[sid + 1].fd;
				  if (sid >= pdata->mfds)
					pdata->mfds = sid + 1;
				  break;
		case STARTING:    /* Always called from a free state */
				  pdata->fds[sid + 1].fd = -fd;
				  return;
		case FREED:	  /* Reset fd to the value for free */
				  pdata->fds[sid + 1].fd = fd;
				  return;
	}
}
#undef set_fd

/*
 * @brief set st state, which is done through revents and fd, and
 *	side effect on external variables.
 *
 * @param private thread data
 * @param the index of the stream on which the state applies
 * @param the new state
 * @return none
 */
static void set_st_state(struct private_data *pdata,
			 sid_t sid, enum stream_state state)
{
	enum stream_state old_state;

	old_state = get_state(pdata, sid);
	if (old_state == state)
		return;

	set_fd(pdata, sid, old_state, state, 0);
	switch (state) {
		case ACTIVE_TRF:  pdata->fds[sid + 1].events = 0;
				  break;
		case ACTIVE_POLL: pdata->fds[sid + 1].events = POLLIN;
				  break;
		case INACTIVE   : pdata->nst -= 1;
				  return;
		case STARTING:	  /* Never called with those values*/
		case FREED:	  return;
	}

	/* Only ACTIVE_TRF and ACTIVE_POLL cases run this code, the other
	 * cases returned directly from the switch */
	if (old_state == INACTIVE)
		pdata->nst += 1;
}
#undef set_st_state

/*
 * @brief free a gap
 *
 * @param the gap to be freed
 * @param true = free both buf and action struct, false free only action struct
 * @return none
 */
static void free_gap(struct action *g, bool full)
{
	if (full)
		free(g->a.g.buf);
	free(g);
}
#undef free_gap

/*
 * @brief free all gaps of a stream
 *
 * @param index of the stream
 * @return none
 */
static void free_all_gaps(sid_t sid)
{
	struct action *g;

	AW_ASSERT(sid < N_STREAMS);

	for(g = strms[sid].queue;
	    NULL != g && ACTION_GAP == g->type;
	    g = strms[sid].queue) {
		strms[sid].queue = g->next;
		free_gap(g, true);
	}
}
#undef free_all_gaps

/*
 * @brief wrapper on supplied close_if_idle callback
 *
 * When the stream is closed also free potential "gap" memory
 *
 * This does not "release" the stream because it can be needed immediately
 * by the same file (fi).
 *
 * @param private thread data
 * @param the index of the stream to reset
 * @param force flag to close whatever the time
 * @return whether it was idle then closed.
 */
static bool is_idle(struct private_data *pdata, sid_t sid, bool force)
{
	AW_ASSERT(sid < N_STREAMS);

	/* Don't close streams that have pending read requests! */
	if (0 != strms[sid].n_rq)
		return false;

	if (NULL == strms[sid].sh ||
	    aw_ops.close_if_idle(strms[sid].fh,
				 strms[sid].sh,
				 strms[sid].last_trf,
				 pdata->now,
				 force)) {
		free_all_gaps(sid);
		strms[sid].sh    = NULL;
		set_fd(pdata, sid, FREED, FREED, -1);
		stats_count_streams(false, sid);
		return true;
	}
	return false;
}
#undef is_idle

/*
 * @brief perform the 'free' action
 *
 * This is taking note that a 'fh' is not owned anymore.
 *
 * @param the fh to be freed/removed from the lists
 * @param number of received requests for this file
 * @param the thread that was managing it
 * @return none;
 */
static void do_free(uint64_t fh, uint64_t recv, tid_t tid)
{
	fid_t *p, fid;

	AW_ASSERT(tid < max_threads);

	if (O_THREAD != tidx) {
		struct action *a;

		a = malloc(sizeof(struct action));

		a->type	    = ACTION_FREE;
		a->a.f.fh   = fh;
		a->a.f.recv = recv;
		a->a.f.tid  = tid;
		post_action(a, O_THREAD);
		return;
	}

	/* With the above, this only runs when this is thread O_THREAD. */

	/* Find the fh in the list for that thread, it should always be
	 * found: *p != N_FILES in the for() test is a bug protection. */
	for ( p  = &o_top[tid].first_fid; *p != N_FILES && o_fh[*p] != fh
					;  p  = &o_list[*p]);
	AW_ASSERT(*p != N_FILES);
	fid = *p;

	AW_ASSERT(o_sent[fid] >= recv);
	if (o_sent[fid] > recv) {
		/* Rare case of "partial" free */
		aw_ops.log(LOG_NOTICE,
			   "Partial free from tid=%u fh=%"PRIu64" recv=%"PRIu64" < sent=%"PRIu64"\n",
			   tid, fh, recv, o_sent[fid]);
		o_sent[fid] -= recv;
		return;
	}
	o_sent[fid] = 0;

	/* Remove it from the thread list */
	*p = o_list[*p];
	o_top[tid].n_files -= 1;
	/* add it back to the global free list */
	p = stats_release_slot();
	o_list[fid] = *p;
	*p = fid;
	o_fh[fid] = 0;
}
#undef do_free


/*
 * @brief detach a stream that is attached to a file (fi)
 *
 * @param private thread data
 * @param the index of the stream to detach
 * @return none
 */
static void detach_stream(struct private_data *pdata, sid_t sid)
{
	sid_t i;

	AW_ASSERT(sid < N_STREAMS);

	if (sid == strms[sid].next) {
		/* last stream of the file was detached */
		do_free(strms[sid].fh, pdata->recv[strms[sid].fid], tidx);
		pdata->recv[strms[sid].fid] = 0;
		pdata->n_files -= 1;
		stats_count_files(false);
	}
	else {
		/* remove idx stream from the circular list */
		i = sid;
		while (strms[i].next != sid)
			i = strms[i].next;
		strms[i].next = strms[sid].next;
	}
}
#undef detach_stream

/*
 * @brief release a stream to the free list
 *
 * @param private thread data
 * @param the index of the stream to make free
 * @return none
 */
static void release_stream(struct private_data *pdata, sid_t sid)
{
	map_t map, new_map;
	unsigned char *p;
	unsigned int spin = 0;

	detach_stream(pdata, sid);

	/* First add back the stream to the free map */
	map = atomic_load_explicit(&free_streams_map, memory_order_acquire);

	do {
		spin++;
		new_map = map | ((map_t)1 << sid);
	} while(!atomic_compare_exchange_strong_explicit(&free_streams_map,
							 &map, new_map,
							 memory_order_acq_rel,
							 memory_order_acquire));

	aw_count_contentions(spin -1);

	/* Then remove it from the list of "owned streams" of the thread */
	for (p = &pdata->owned_streams; *p != sid; p = &pdata->list[*p]);
	*p = pdata->list[*p];
	pdata->list[sid] = NOT_OWNED;
}
#undef release_stream


/*
 * @brief fills a read with a gap
 *
 * @param adress in the list of gaps
 * @param the read action to assign
 * @return NULL when snatched, otherwise gap pointer.
 */
static struct action * gap_to_read(struct action **where, struct action *a)
{
	struct action *g;
	size_t off_g, off_a, to_copy;
	bool snatch;
	int i;

	g = *where;
	if (a->a.r.offset >= g->a.g.offset) {
		/* Copying first part */
		off_g = a->a.r.offset - g->a.g.offset;
		off_a = 0;
		i = 0;
	}
	else {
		/* Copying second part */
		off_g = 0;
		off_a = a->a.r.written;
		i = 1;
	}
	snatch = (0 == off_g && a->a.r.size - off_a >= g->a.g.written);
	if (off_g >= g->a.g.written) {
		to_copy = 0;
	}
	else {
		to_copy = g->a.g.written - off_g;
		if (to_copy > a->a.r.size - off_a)
			to_copy = a->a.r.size - off_a;
	}
	a->a.r.written += to_copy;
	stats_count_over(-to_copy, a->a.r.sid);
	if (0 == a->a.r.bufp->count) {
		/* read() bytes are copied */
		if (0 != to_copy)
			memcpy(((char *)a->a.r.bufp->buf[0].mem) + off_a,
			       g->a.g.buf + off_g,
			       to_copy);
		if (snatch) {
			*where = g->next;
			free_gap(g , true);
			return NULL;
		}
	}
	else {
		/* readbuf() buffer is passed without copying */
		if (snatch) {
			a->a.r.bufp->buf[i].mem = g->a.g.buf;
			*where = g->next;
			free_gap(g , false);
			return NULL;
		}
		a->a.r.bufp->buf[i].mem = aligned_alloc(pagesize,
						        a->a.r.bufp->buf[i].size
						       );
		if (0 != to_copy)
			memcpy(a->a.r.bufp->buf[i].mem,
			       g->a.g.buf + off_g,
			       to_copy);
	}
	/* Shrink case to avoid copy back */
	if (0 != off_g && off_g + a->a.r.size >= g->a.g.written)
		g->a.g.size = (off_g > g->a.g.written) ? off_g : g->a.g.written;
	return g;
}
#undef gap_to_read

/*
 * @brief tests if a read request fits the indexed stream
 *
 * If so, either queue it or respond if it fits a buffer.
 * 
 * Gaps and Read requests are stored in offset ascending order, gaps always
 * first.
 * Overlap of read requests is NOT managed on purpose. It happens only when
 * the kernel asks for the same part of the file more than once, which means
 * cache have been dropped, either intentionnaly or because of memory pressure.
 * When caches are dropped, it is expected to have slower responses, and the
 * complexity to handle overlapped buffers is not worth the little performance
 * gain in (hopefully rare) case of dropped caches.
 * Nevertheless, overlap situation exist and in this case the algorithm should
 * make the overlapped request select or start another stream.
 *
 * @param private thread data
 * @param the read action to assign
 * @param the index of the stream to test
 * @return true if read fits (and action is done) or false otherwise
 */
static bool read_fits_stream(struct private_data *pdata,
			     struct action *a, sid_t sid)
{
	struct action *g, **where, *g_stayed = NULL;
	bool gap_found;

	/* Does it belong to the same file? */
	if (a->a.r.fh != strms[sid].fh)
		return false;
	/* Is it 'before'? */
	if (NULL == strms[sid].queue || ACTION_READ == strms[sid].queue->type) {
		/* Only read are queued here, since overlaps are directed to
		 * another stream, the only test is to see if the stream's
		 * offset (pos) allows filling the request buffer from start */
		if (a->a.r.offset < strms[sid].pos)
			return false;
	}
	else {
		if (a->a.r.offset < strms[sid].queue->a.g.offset)
			return false;
	}
	/* Is it 'after? */
	if (a->a.r.offset + a->a.r.size > strms[sid].end + max_gap)
		return false;

	/* Always test idle before use! */
	if (is_idle(pdata, sid, false)) {
		release_stream(pdata, sid);
		return false;
	}

	/* It is in the range of this stream, store it sorted if possible */
	where = &strms[sid].queue;
	gap_found = false;
	/* Looking at gaps, if any */
	for (g = *where;
	     NULL != g && ACTION_GAP == g->type;
	     where = &g->next, g = *where) {
		/* Skip gap as long as this is 'after' */
		if (a->a.r.offset >= g->a.g.offset + g->a.g.size)
			continue;
		/* There must not be a hole */
		if (a->a.r.offset < g->a.g.offset)
			return false;
		/* Here the request start is within the current gap */
		if (a->a.r.offset + a->a.r.size <=
		    g->a.g.offset + g->a.g.size || 
		    strms[sid].pos <= g->a.g.offset + g->a.g.size) {
			/* Request fully inside that gap, or partially but
			 * the stream offset allows continued copy
			 * When the request goes beyond the gap being written
			 * it still must not overlap an existing pending read.
			 * If the gap is being written, it means there is a
			 * pending read (at least one) after that gap */
			if (a->a.r.offset + a->a.r.size >
			    g->a.g.offset + g->a.g.size  &&
			    a->a.r.offset + a->a.r.size > g->next->a.r.offset)
				return false;
			a->a.r.sid = sid; /* Needed: overread stats */
			g_stayed = gap_to_read(where, a);
			gap_found = true;
			break;
		}
		/* Rare case where this spans 2 blocks
		 * The next must: be a gap, be continuous and big
		 * enough to fill the request with the first block */
		if (NULL == g->next || ACTION_READ == g->next->type ||
		    a->a.r.offset + a->a.r.size >
			g->next->a.g.offset + g->next->a.g.size ||
		    g->a.g.offset + g->a.g.size != g->next->a.g.offset)
			return false;
		/* Unless algorithm bug, the current gap must be
		 * totally full and strictly before the read, test
		 * it to be safe, since the case is rare */
		if (a->a.r.offset == g->a.g.offset ||
		    g->a.g.size   != g->a.g.written)
			return false;
		if (0 != a->a.r.bufp->count) {
			a->a.r.bufp->count = 2;
			a->a.r.bufp->buf[1].fd    = -1;
			a->a.r.bufp->buf[1].flags = 0;
			a->a.r.bufp->buf[1].size  = a->a.r.offset +
						    a->a.r.size -
						    g->next->a.g.offset;
			a->a.r.bufp->buf[0].size -= a->a.r.bufp->buf[1].size;
		}
		a->a.r.sid = sid; /* Needed: overread stats */
		gap_to_read(where, a);
		where = &g->next;
		g = *where;
		g_stayed = gap_to_read(where, a);
		gap_found = true;
		break;
	}
	if (!gap_found) {
		if (&strms[sid].queue != where) {
			/* There were gaps and none matched or made the read
			 * to be rejected (return false). Before looking where
			 * to store it in the read queue, repeat the initial
			 * test to see if the request could be filled. */
			if (a->a.r.offset < strms[sid].pos)
				return false;
			/* Here it can be filled, it can still be rejected below
			 * if there is an overlap with other pending requests */
		}
		/* Now, look at stored reads (if any) */
		for (; NULL != g; where = &g->next, g = *where) {
			/* Skip read as long as this is 'after' */
			if (a->a.r.offset >= g->a.r.offset + g->a.r.size)
				continue;
			/* Must be in a  'hole' between reads */
			if (a->a.r.offset + a->a.r.size <= g->a.r.offset)
				break;
			/* Here it overlap reads: does not fit */
			return false;
		}
	}

	/* If it exited the loop, a place was found for this read.
	 * It might already be full, due to gap copy/snatch, in which case
	 * it is finished, but otherwise the read is queued at its place. */
	a->a.r.sid = sid;/* Save sid if it was N_STREAMS, before finish/return*/
	strms[sid].last_rq = a->a.r.rq_id;
	if (a->a.r.written == a->a.r.size) {
		/* When the request is finished because of a gap, there is
		 * no need freeing old gaps. */
		finish_action(a);
	}
	else {
		strms[sid].n_rq += 1;
		if (strms[sid].end < a->a.r.offset + a->a.r.size)
			strms[sid].end = a->a.r.offset + a->a.r.size;
		if (NULL != g_stayed)		/* A gap was used but stayed: */
			where = &g->next;	/*   this read goes after gap */
		a->next = *where;
		*where = a;
		if (NULL != strms[sid].sh)
			set_st_state(pdata, sid, ACTIVE_TRF);
		if (NULL == a->a.r.bufp->buf[0].mem)
			a->a.r.bufp->buf[0].mem = aligned_alloc(pagesize,
								a->a.r.size);
	}
	return true;
}
#undef read_fits_stream

/*
 * @brief close a stream if possible
 *
 * If not possible, computes biggest is necessary.
 *
 * @param private thread data
 * @param index of the stream
 * @param (pointer to) index of oldest stream so far (N_STREAMS if none).
 * @return index of the closed stream or N_STREAMS if not possible
 */
static bool can_close(struct private_data *pdata, sid_t sid, sid_t *oldest)
{
	/* Never close streams that have pending requests */
	if (0 != strms[sid].n_rq)
		return false;
	/* If idle, that's done!*/
	if (is_idle(pdata, sid, false))
		return true;

	/* Otherwise change "oldest" if needed */
	if (N_STREAMS == *oldest ||
	    strms[sid].last_rq < strms[*oldest].last_rq)
		*oldest = sid;
	return false;
}
#undef can_close

/*
 * @brief try to close the oldest owned stream
 *
 * This is always called when all streams are not-free, so no need testing it.
 *
 * @param private thread data
 * @return index of the closed stream or N_STREAMS if not possible
 */
static sid_t close_an_owned_stream(struct private_data *pdata)
{
	sid_t i, oldest;

	oldest = N_STREAMS;
	for (i = pdata->owned_streams; N_STREAMS != i; i = pdata->list[i])
		if (can_close(pdata, i, &oldest))
			return i;

	/* Exiting the loop means none of the streams where idle. Now the
	 * "oldest" last_rq is choosen, is_idle is called with NULL, NULL to
	 * force close it, which should work, but return N_STREAMS if failed. */
	if (N_STREAMS != oldest)
		if (is_idle(pdata, oldest, true))
			return oldest;
	return N_STREAMS;
}
#undef close_an_owned_stream


/*
 * @brief try to close a stream from the fi streams
 *
 * Same as the close_a_stream except that the loop condition is only on the
 * current fi instead of all the streams.
 *
 * @param private thread data
 * @param the index of one of the streams of fi.
 * @return index of the closed stream or N_STREAMS if not possible
 */
static sid_t close_fh_stream(struct private_data *pdata, sid_t sid)
{
	sid_t i, oldest;

	i	  = sid;
	oldest	  = N_STREAMS;
	do {
		if (can_close(pdata, i, &oldest))
			return i;
		i = strms[i].next;
	} while(i != sid);

	/* Same comment as above */
	if (N_STREAMS != oldest)
		if (is_idle(pdata, oldest, true))
			return oldest;
	return N_STREAMS;
}
#undef close_fh_stream


/*
 * @brief returns the index of a new stream
 *
 * Each bit on the "map" represents a thread. When the bit is 1, the
 * thread is available, when the bit is 0, the stream is already owned
 * by one of the reader threads.
 *
 * @param private thread data
 * @return index of the new stream or N_STREAMS if not possible
 */
static sid_t get_new_stream(struct private_data *pdata, uint64_t fh)
{
	sid_t sid;
	map_t map, new_map;
	unsigned int spin = 0;

	/* First try to get a new stream doing atomics on the map */
	map = atomic_load_explicit(&free_streams_map, memory_order_acquire);

	do {
		if (0 == map) {
			/* map == 0 means all bits of the map are zero, which
			 * means there are no available streams anymore
			 * In this case, try to reclaim one of this thread's. */
			sid = close_an_owned_stream(pdata);
			if (N_STREAMS == sid)
				return N_STREAMS; /* No available stream! */

			/* Don't 'detatch' if it is for the same fh! */
			if (fh != strms[sid].fh)
				detach_stream(pdata, sid);

			return sid;	/* Stream already owned by this thread,
					 * no need changing "map". */
		}
		else {
			/* Note: ffsl function returns the position of the
			 * least significant bit set on the map. Starts at 1. */
			sid = ffsl(map) - 1;
			new_map = map & (UINTPTR_MAX - ((map_t)1 << sid));
		}
		spin ++;
	} while(!atomic_compare_exchange_strong_explicit(&free_streams_map,
							 &map, new_map,
							 memory_order_acq_rel,
							 memory_order_acquire));

	aw_count_contentions(spin -1);

	/* A stream was grabbed, now insert it into the "owned streams" list */
	strms[sid].fh = 0;
	pdata->list[sid] = pdata->owned_streams;
	pdata->owned_streams = sid;

	return sid;
}
#undef get_new_stream

/*
 * @brief find thread managing that fh
 *
 * Must be run from thread O_THREAD that own o_top, o_fh, o_list.
 *
 * @param the 'fh' inside the fuse file info
 * @param set to the file index of the fh (if found)
 * @return the index of the thread owning this file or max_threads if not found
 */
static tid_t find_fh(uint64_t fh, fid_t *fid)
{
	tid_t t;
	fid_t i;

	/* No need searching in O_THREAD, it would have been found in the
	 * "local" search */

	for (t = 0; t < max_threads; t++)
		if (t != O_THREAD)
			for (i = o_top[t].first_fid;
			     N_STREAMS != i;
			     i = o_list[i]
			    )
				if (o_fh[i] == fh) {
					*fid = i;
					return t;
				}
	return max_threads;
}
#undef find_fh


/*
 * @brief find a stream in the owned streams
 *
 * Used when no index is already provided, or the index is outdated.
 *
 * @param private thread data
 * @param fuse file info pointer to the file
 * @return the index of a stream of this file or N_STREAMS when not found
 */
static sid_t find_owned_fh(const struct private_data *pdata, uint64_t fh)
{
	sid_t i;

	for (i = pdata->owned_streams; N_STREAMS != i; i = pdata->list[i])
		if (strms[i].fh == fh)
			return i;
	return N_STREAMS;
}
#undef find_owned_fh

/*
 * @brief when all streams or files are busy, do a 'solo' range read
 *
 *
 * @param the read action concerned
 * @returns none
 */
static void busy_do_solo(struct action *a)
{
	a->a.r.tid = max_threads; /* Do not alter encode mark */
	a->type = ACTION_SOLO;
	finish_action(a);
}
#undef busy_do_solo

/*
 * @brief compute the maximum streams available for that file
 *
 * To avoid starvation, the streams should be evenly distributed to threads.
 * The share of streams for each thread is computed at init.
 * This just returns the share considering the current number of files.
 *
 * @param private data
 * @returns max streams allowed at the moment
 */
static sid_t max_streams_allowed(const struct private_data *pdata)
{
	sid_t mx = (share_of_streams + pdata->n_files - 1) / pdata->n_files;
        return (MAX_STREAMS_BY_FILE < mx) ? MAX_STREAMS_BY_FILE
					  : ((MIN_STREAMS_BY_FILE > mx) ?
					      MIN_STREAMS_BY_FILE : mx    );
}
#undef max_streams_allowed

/*
 * @brief reclaim streams from greedy files
 *
 * This is part of the starvation handling mechanism with max_streams_allowed.
 * Each time a new file is allocated to a thread, reclaim streams from files
 * on the same thread that were too greedy.
 *
 * @param private thread data
 * @return none
 */
static void reclaim_streams(struct private_data *pdata)
{
	sid_t    n_streams[pdata->n_files], sid, max;
	uint64_t fh[pdata->n_files];
	unsigned int n_known_files = 0, i;

	if (1 == pdata->n_files)
		return; 	/* Nothing to reclaim: the file is alone! */

	/* Construct the number of streams by fh */
	for (sid = pdata->owned_streams; N_STREAMS != sid
				       ; sid = pdata->list[sid]) {
		for (i = 0; i < n_known_files; i++)
			if (strms[sid].fh == fh[i]) /* File already known */
				break;
		if (i == n_known_files) {
			/* New file when above loop runs until the end */
			n_streams[n_known_files] = 1;
			fh[n_known_files] = strms[sid].fh;
			n_known_files += 1;
			AW_ASSERT(n_known_files <= pdata->n_files);
		}
		else {
			/* Count the stream on existing file */
			n_streams[i] += 1;
		}
	}

	max = max_streams_allowed(pdata);

	/* Now reclaim from each "greedy" fh */
	for (i = 0; i < n_known_files; i++) {
		while (n_streams[i] > max) {
			sid = close_fh_stream(pdata,
					      find_owned_fh(pdata, fh[i]) );
			if (N_STREAMS == sid)
				break;	/* Cannot reclaim more here */
			release_stream(pdata, sid);
			n_streams[i] -= 1;
		}
	}
}
#undef reclaim_streams

/*
 * @brief search potential siblings to optimise the anticipation
 *
 * "sibling" is a read action that is contiguous the action passed.
 *
 * It happens that application might send 2 sequential read requests "at the
 * same time" after opening a file. When those requests also arrive "at the
 * same time" on the action queue, anticipation can be optimised, assuming
 * there will not be another sibling before the lowest offset of all
 * sibling currently queued.
 * The sequential initial requests can obviously be in any order (joys of
 * parallelism!)
 *
 * When no sibling is found, the standard anticipation mechanism will be called.
 * In this case NO_ANTICIPATION_CLUE is returned.
 * When siblings are found, the suggested anticipation is returned.
 *
 * @param the read action to search for siblings
 * @return the anticipation clue
 */
static size_t sibling_anticipation(struct action *a)
{
	struct action *b;
	off_t low = a->a.r.offset;
	bool found = false;

	for (b = a->next; NULL != b; b = b->next)
	{
		if (ACTION_READ != b->type || a->a.r.fh != b->a.r.fh)
			continue;
		/* Now this is a READ action on the same fh, look for
		 * boundaries. */
		if (a->a.r.offset + a->a.r.size == b->a.r.offset) {
			/* Just above! */
			found = true;
		}
		else if (b->a.r.offset + b->a.r.size <= low) {
			/* Below */
			if (low > max_gap &&
			    b->a.r.offset + b->a.r.size < low - max_gap)
				continue; /* too far below! */
			/* Below in the range 0 - max-gap */
			low = b->a.r.offset;
			found = true;
			b = a;	/* Reset the loop for not-in-order reads */
		}
	}

	return (found) ? (size_t)(a->a.r.offset - low) : NO_ANTICIPATION_CLUE;
}
#undef sibling_anticipation

/*
 * @brief clean old streams
 *
 * This is called only when the asyncrhonous reader thread has received no
 * read action within the defined delay (CLEAN_WAIT) or on some situations
 * in associate_read_to_stream.
 *
 * There can be calls like stats that are not counted as "activity", so
 * the function only performs cleaning if time has elapsed.
 *
 * @param private thread data
 * @return none
 */
static void clean_old_streams(struct private_data *pdata)
{
	sid_t i, next;

	/* This flags serves to clean only once (max) per do_action() */
	if (pdata->cleaned)
		return;
	pdata->cleaned = true;

	pdata->last_activity = pdata->now;
	pdata->last_clean    = pdata->now;
				/* Mark it as "activity" to avoid triggering
				it immediately again, and so it will wait
				again for CLEAN_WAIT before repeating if
				all streams were not closed */

	for (i = pdata->owned_streams; N_STREAMS != i; i = next) {
		next = pdata->list[i];  /* Because it might be released below!*/
		if (0 != strms[i].fh) {
			if (is_idle(pdata, i, false))
				release_stream(pdata, i);
		}
	}
}
#undef clean_old_streams


/*
 * @brief selects a thread for a 'fh' with load balancing
 *
 * Get the streams with less files. Thread O_THREAD is counted as if it
 * had one file more because of the orchestration workload.
 *
 * @param fh to distribute
 * @return the selected thread;
 */
static tid_t load_balance_threads(uint64_t fh)
{
	fid_t i, *p;
	tid_t t, m;

	unsigned int min_files;

	min_files = o_top[O_THREAD].n_files + 1;
	m = 0;
	for (t = 1; t < max_threads; t++)
		if (o_top[t].n_files < min_files) {
			min_files = o_top[t].n_files;
			m = t;
		}

	/* Update the o_ list/maps to note that this fh is now associated */
	p = stats_get_slot(fh);		/* Grab a slot being 'stats friendly' */
	i = *p;				/* Save the value 		*/
	*p = o_list[*p];		/* Remove from the chained list */
	o_top[m].n_files += 1;
	o_list[i] = o_top[m].first_fid;	/* Chain on the m thread list */
	o_top[m].first_fid = i;		/* i is new head of the thread m list */
	o_fh[i] = fh;

	return m;
}
#undef load_balance_threads

/*
 * @brief when a read action is orchestrated (O_THREAD) this is where it is sent
 *
 * This function is always called from O_THREAD when the read is orchestrated.
 *
 * @param the read action to send
 * @param thread index (tid) where to send it
 * @param file index (fid) on the orchestrator
 * @return false to keep running in the same stream, true otherwise;
 */
static bool send_read(struct action *a, tid_t tid, fid_t fid)
{
	AW_ASSERT(fid != N_FILES);
	a->a.r.sid = N_STREAMS;
	a->a.r.tid = tid;
	a->a.r.fid = fid;

	if (tid == O_THREAD)
		return false;

	o_sent[fid] += 1; /* Count rq sent from orchestrator when not itself */

	post_action(a, tid);
	return true;
}
#undef send_read

/*
 * @brief assigns a read action to a stream
 *
 * This is a central part of the algorithm!
 *
 * This function is called when the fast-association, and even searching for
 * a file in the current thread (when relevant) has failed.
 * When the request was marked as "new file" (idx = N_STREAMS and
 * tid = max_threads), no 'fast association' was done... same when the
 * caller did not provide load/store_opt, hence the importance of it.
 *
 * This function is always called from O_THREAD when the read is orchestrated.
 *
 * @param the read action to assign
 * @return false to keep running in the same stream, true otherwise;
 */
static bool associate_read_to_thread(struct action *a)
{
	tid_t t;
	fid_t fid;

	/* Do a global search for the fh. Only O_THREAD can do that. */
	t = find_fh(a->a.r.fh, &fid);
	if (max_threads != t)
		/* File was found on another thread (or no load/store_opt) */
		return send_read(a, t, fid);

	/* From here it is a completely new file (fh) and it needs to be
	 * associated to a thread. */

	if (N_FILES == o_free_head) {
		/* But if not free slots on the file list, already max files! */
		busy_do_solo(a);
		return true;
	}

	t = load_balance_threads(a->a.r.fh);

	return send_read(a, t, o_top[t].first_fid);
}
#undef associate_read_to_thread

/*
 * @brief assigns a read action to a stream
 *
 * This is a central part of the algorithm!
 *
 * It is meant to be fast, when the caller correctly uses last/store_opt, it
 * gives a clue to which thread/stream the incoming read request belongs. When
 * that works, the association happens with minimum effort and efficiently.
 *
 * Second, when the file is the same, has not been "released" by the thread
 * due to inactivity or resource pressure on streams, it can be a different
 * range of the same file (fh). In this case, the thread is correct (no
 * thread-switch needed) and the range might either already exist (just a
 * few more tests and atomics) or need a new "start".
 *
 * On the other hand, when the file (fh) does not belong to that thread, the
 * request must be passed back to the orchestrator (thread 0) to select the
 * appropriate thread.
 * When this was a new file the read/read_buf functions have no prior knowledge
 * of the "opt" opaque data, and the request is already on thread 0.
 *
 * @param private thread data
 * @param the read action to assign
 * @return none
 */
static void associate_read_to_stream(struct private_data *pdata,
				     struct action *a)
{
	sid_t sid, skip;
	size_t anticipation;
	bool   orchestrated = (N_FILES != a->a.r.fid); /* See below */

	/* Read actions arrive here
	 * - For worker threads (all but orchestrator) :
	 *   -1) From a fuse thread (via common_read()) -optimisation-
	 *   -2) From the orchestrator (via post_action())
	 *   ==> To differentiate, 1) fid is N_FILES, 2) fid is set
	 * - For the orchestrator
	 *   -1) From a fuse thread in 3 situations:
	 *	-a) New file
	 *	-b) optimisation is not used (no encode/decode)
	 *	-c) when the orchestrator is working as worker thread
	 *   -2) Back from another worker thread
	 *   ==> 2 is equivalent to 1-a since the file was closed worker side
	 *   ==> There is no difference with 1-a and 1-b, sid is N_STREAMS
	 *	 the difference will be noticed if find_fh() finds something!
	 *   ==> Same as workers, when sid is set, it's 1-c
	 * */

	sid = N_STREAMS;	/* N_STREAMS = not found, assuming worse! */

	/* Fast association first to be fast and efficient for 'stream' cases.
	 * Anyway in other cases, there will be more tests and a potential
	 * start_stream which take considerably more time.
	 * Fast association needs:
	 * 	- to have a valid 'sid' (means encode/decode was used)
	 *	- that 'sid' needs to still belong to that thread (otherwise
	 *	  it would be 'illegal' to peek into the streams table without
	 *	  any _Atomics!)
	 */
	if (N_STREAMS != a->a.r.sid) {
		if (OWNED(pdata->list[a->a.r.sid])) {
			if (read_fits_stream(pdata, a, a->a.r.sid)) {
				pdata->last_activity = pdata->now;
				return;
			}
			/* Fast association failed: is it at least same file? */
			/* Note: read_fits_stream() might disown,
			 *	 so retest ownership */
			if (OWNED(pdata->list[a->a.r.sid]) &&
			    strms[a->a.r.sid].fh == a->a.r.fh)
				sid = a->a.r.sid;
		}
	}
	skip = sid;

	/* Try to find a match in "owned streams" */
	if (N_STREAMS == sid) {
		/* Clean before trying to grab the fh, since it can free it */
		clean_old_streams(pdata);
		sid = find_owned_fh(pdata, a->a.r.fh);
	}

	/* Is still not found at all on this thread */
	if (N_STREAMS == sid) {
		if (O_THREAD == tidx) {
			/* Contrary to other threads, when the orchestrator
			 * receives a->a.r.tid == max_threads it comes from
			 * common_read that had no prior encode. Hence either
			 * a new file, or the encode/decode optimisation was
			 * not provided. associate_read_to_thread will then
			 * do as the name means in both cases. */
			 if (associate_read_to_thread(a))
				return;
			/* When the association was with another thread, the
			 * return was true and this thread's (orchestrator)
			 * work is over. Otherwise return is false so that it
			 * continues here.
			 * Note that in the case encode/decode was not provided
			 * but the stream is handled here, the previous test
			 * would have catched it with find_owned_fh. So at this
			 * point in the orchestrator, it is only "new file". */
		}
		else {
			if (!orchestrated) {
				/* And so here is can only be an old existing
				 * stream that reused encode/decode after this
				 * thread has released the file. The action is
				 * then sent back to the orchestrator to decide
				 * what to do with it, resetting sid/tid as for
				 * a new file. */
				a->a.r.sid = N_STREAMS;
				a->a.r.tid = max_threads;
				post_action(a, O_THREAD);
				return;
			}
			/* And so here it is a new file since the branch
			 * above would have returned.
			 * It is also possible that the orchestrator thinks it
			 * is not a new file, but the last stream of the file
			 * was closed with close_old_streams in do_action, so
			 * the local file was not found above. If so an obsolete
			 * free would hve been sent, and here it is same as new
			 * file since local file structs are freed. */
		}
		/* Then here, also "new file" for both branches */
	}

	pdata->last_activity = pdata->now; /* Is an "activity" for cleaning */
	a->a.r.tid = tidx;

	if (N_STREAMS == sid) {
		/* Still not found = new file + first stream */
		if (orchestrated)
			pdata->recv[a->a.r.fid] += 1;
		sid = get_new_stream(pdata, a->a.r.fh);
		/* No free slot, return action is SOLO */
		if (N_STREAMS == sid) {
			/* This should be a very rare situation where there
			 * was file slots but no streams. In this case, do a
			 * 'solo' and also set 'fh' free again */
			busy_do_solo(a);
			do_free(a->a.r.fh, ((orchestrated) ? 1 : 0), tidx);
			return;
		}
		/* Found a free slot and this is a new stream */
		strms[sid].next = sid;
		strms[sid].fid  = a->a.r.fid;	/* set in send_read
						 * in case of new file */
		pdata->n_files += 1;
		stats_count_files(true);
		reclaim_streams(pdata);
	}
	else {
		/* fh was found but need to associate to a stream or
		 * grab a new one (or solo when that fails). */
		sid_t n, i, next;
		bool last;
		/* Look if one of the streams of fi fits the read request
		 * Iterate on fi linked streams starting at sid.
		 * The iteration runs as long a it does not loop back to sid.
		 * It is a bit tricky when sid item itself is closed and
		 * removed by read_fits_stream! */
		n = 0;
		last = false;
		/* Get the fid to propagate it to the potential new stream, and
		 * in the read action in case of start stream statistics.
		 * Get it here because it is possible that all streams of the
		 * file be closed and released ! */
		a->a.r.fid = strms[sid].fid;
		for (i = sid; !last; i = next) {
			/* Because read_fits_stream() might close and remove
			 * the stream : save 'next' index before. */
			next = strms[i].next;
			last = (next == sid);
			n++;
			if (i == skip)
				continue;
			if (read_fits_stream(pdata, a, i)) {
				/* Note: the received counter must not be
				 * incremented before read_fits_stream in case
				 * it would close all streams of the file, the
				 * received counter would be one to many.
				 * So it is counted here and after the loop
				 * for the case there is not match. */
				if (orchestrated)
					pdata->recv[a->a.r.fid] += 1;
				return;
			}
			if (!OWNED(pdata->list[i])) {
				/* This item was removed by read_fits_stream! */
				n--;		  /* remove it from the count */
				if (i == sid)	  /* Shift end test if first  */
					sid = next;
			}
		}
		if (orchestrated)
			pdata->recv[a->a.r.fid] += 1;

		/* No open stream on fi matched. Now this will be either solo
		 * or starting a stream. When n=0 it means read_fit_stream
		 * closed the last stream of this file. It is then possible that
		 * n is 0 which would divide by zero in max_streams_allowed */
		if (0 != n  && n >= max_streams_allowed(pdata)) {
			/* Already max streams on this fi, try to close one */
			i = close_fh_stream(pdata, sid);
			/* All fi streams are "busy" */
			if (N_STREAMS == i)  {
				busy_do_solo(a);
				return;
			}
		}
		else {	/* Not max streams, grap a new one */
			i = get_new_stream(pdata, a->a.r.fh);
			if (N_STREAMS == i)  {
				/* No free slot, return action is SOLO */
				busy_do_solo(a);
				return;
			}
			if (0 == n) {
				/* Unique stream: all streams were closed, count
				 * of files was decremented too:  */
				strms[i].next = i;
				pdata->n_files += 1;
				stats_count_files(true);
			}
			else {
				/* get_new_stream() might return a stream that
				 * this 'fh' already owns, if so don't insert */
				if (strms[i].fh != a->a.r.fh) {
					strms[i].next = strms[sid].next;
					strms[sid].next = i;
				}
			}
		}
		strms[i].fid = a->a.r.fid;
		sid = i;
	}
	/* Start stream on sid */
	anticipation = aw_ops.anticipation(a->a.r.fh,
					   sibling_anticipation(a),
					   a->a.r.size,
					   a->a.r.offset);
	strms[sid].pos =
	a->a.r.start   = a->a.r.offset - anticipation;
	a->a.r.sid     = sid;
	finish_action(a); /* Push the action back then finish initialisations */

	strms[sid].fh       = a->a.r.fh;
	strms[sid].n_rq	    = 1;
	strms[sid].last_rq  = a->a.r.rq_id;
	strms[sid].last_trf = pdata->now;
	strms[sid].end      = a->a.r.offset + a->a.r.size;
	strms[sid].queue    = a;
	a->next		    = NULL; /* next also used: chain actions on thread*/
	if (NULL == a->a.r.bufp->buf[0].mem)
		a->a.r.bufp->buf[0].mem = aligned_alloc(pagesize, a->a.r.size);
}
#undef associate_read_to_stream

/*
 * @brief flush a stream
 *
 * When error or end of file, the stream should be "flushed".
 * Note that since gaps (and thus the "anticipation" gap) are freed, the
 * caller should filter out read that are beyond EOF to avoid inefficiency.
 *
 * @param private thread data
 * @param the index of the stream to flush
 * @param error code to return (must be negative or 0) to pending read requests
 * @return none
 */
static void flush_stream(struct private_data *pdata, sid_t sid, int err)
{
	struct action *a;

	set_st_state(pdata, sid, INACTIVE);
	strms[sid].n_rq = 0;	/* So that is_idle force-close the stream will*/
	is_idle(pdata, sid, true);   /* close, free gaps, leave pending reads */

	/* Respond to all read requests */
	for (a = strms[sid].queue; NULL != a; a = strms[sid].queue) {
		strms[sid].queue = a->next;
		if (0 != err)	/* Still return what is written on EOF */
			a->a.r.written = err;
		a->a.r.sid = N_STREAMS;
		finish_action(a);
	}

	release_stream(pdata, sid);
}
#undef flush_stream

/*
 * @brief retry or flush a stream
 *
 * When an error can be recovered, retry is attempted is the caller returns
 * non-zero on the retry callback, otherwise, or when retries are exhausted,
 * it is simply flushed returning the error.
 *
 * @param private thread data
 * @param the action on which the read was occurring
 * @param the index of the stream to flush
 * @param error code to return (must be negative or 0) to pending read requests
 * @return none
 */
static void retry_or_flush(struct private_data *pdata, struct action *a,
			   sid_t sid, int err)
{
	uint8_t *n_retry;

	/* Skip gaps since it concerns the first read action */
	while (ACTION_GAP == a->type)
		a = a->next;
	n_retry = &a->a.r.retry;
	/* 0 as retry indicates that the retry callback was never used
	 * for that read block, so do it now.
	 * The number of retries is then for each read block, hence the callback
	 * is also used when the error is 0, in the unlikely case the last
	 * read block of a file already had retries, to check whether that
	 * indicates the end of file when retries are not needed. */
	if (0 == *n_retry || 0 == err) {
		unsigned int i;
		i = aw_ops.retry(strms[sid].fh, strms[sid].pos);
		if (0 == i) {
			*n_retry = 0;
			/* Will then be flushed bellow either because retries
			 * are not needed or it is known to be the end of file
			 * hence the stream is not needed anymore.
			 * Set to zero in case it was not already zero. */
		}
		else {
			/* Don't reset retry counts if already set! */
			if (0 == *n_retry) {
				i += 1;
				if (i > UINT8_MAX)
					*n_retry = UINT8_MAX;
				else
					*n_retry = i;
			}
		}
	}

	if (1 >= *n_retry) {
		/* Either no retries needed or exhausted failed retries. */
		if (1 == *n_retry)
			aw_ops.log(LOG_ERR,
				    "Too many failed retries on %"PRIu64" at %"PRId64"\n",
				    strms[sid].fh, (int64_t)strms[sid].pos);
		flush_stream(pdata, sid, err);
		return;
	}

	/* Now do the retry! */
	*n_retry -= 1;
	stats_count_retries(sid);
	/* This test is because when retry is invoked on an error at start
	 * stream, the stream won't be running and does not need closing! */
	if (NULL != strms[sid].sh) {
		/* Hence close that stream since it has been opened */
		if (!aw_ops.close_if_idle(strms[sid].fh, strms[sid].sh,
					  strms[sid].last_trf, pdata->now,
					  true)) {
			/* Unexpected error while closing! */
			flush_stream(pdata, sid, err);
			return;
		}
		strms[sid].sh = NULL;
		set_st_state(pdata, sid, INACTIVE);
		set_fd(pdata, sid, FREED, FREED, -1);
	}
	stats_count_streams(false, sid);
	a->a.r.start = strms[sid].pos;
	finish_action(a);
}
#undef retry_or_flush

/*
 * @brief assigns a read action to a stream
 *
 * @param private thread data
 * @param the start action to manage
 * @return none
 */
static void start_reading_stream(struct private_data *pdata,
				 const struct action *a)
{
	sid_t sid = a->a.t.sid; /* For code readability  */

	/* Count the stream once it is opened otherwise the average might
	 * be off. */
	stats_count_streams(true, sid);

	if (0 != a->a.t.err) {
		/* start_streams themselves can be retried if the callback
		 * returned -EREMOTEIO, otherwise the error is consider
		 * irrecoverable: pending reads on the stream are flushed. */
		if (-EREMOTEIO == a->a.t.err)
			retry_or_flush(pdata, strms[sid].queue, sid,-EREMOTEIO);
		else
			flush_stream(pdata, sid,
				     (-ENODATA == a->a.t.err) ? 0 : a->a.t.err);
		return;
	}
	strms[sid].sh = a->a.t.sh;

	set_fd(pdata, sid, STARTING, STARTING, a->a.t.fd);
	set_st_state(pdata, sid, ACTIVE_TRF);

	stats_update_start_time(a);
}
#undef start_reading_stream

/*
 * @brief perform the 'close' action
 *
 * @param private thread data
 * @param the close action to manage
 * @return whether the action can be freed (see bug report #5);
 */
static bool do_close(struct private_data *pdata, struct action *a)
{
	tid_t t;
	sid_t i, sid, next;
	fid_t fid;

	if (O_THREAD == tidx) {
		stats_clean_old(a->a.c.fh, N_STREAMS);
		t = find_fh(a->a.c.fh, &fid);
		if (max_threads == t) {	/* Not found = Ok nothing to close! */
			return true;
		}
		if (t != O_THREAD) { /* Found elsewhere than O_THREAD */
			post_action(a , t);
			/* bug #5 - don't free the action since it was
			 *          passed to another thread */
			return false;
		}
	}
	/* flush all streams of this 'fh' with EINTR */

	sid = find_owned_fh(pdata, a->a.c.fh);
	stats_clean_old(a->a.c.fh, strms[sid].fid);
	next = sid;
	do {
		i = next;
		next = strms[i].next;;
		flush_stream(pdata, i, -EINTR);
	} while (next != sid);
	return true;
}
#undef do_close

/*
 * @brief perform the 'destroy' action
 *
 * @param private thread data
 * @return none;
 */
static void do_destroy(struct private_data *pdata)
{
	sid_t i, next;

	if (0 != pdata->nst) {
		aw_ops.log(LOG_WARNING,
			   "%u streams are reported active, bug?\n",
			   pdata->nst);
	}
	for (i = pdata->owned_streams; N_STREAMS != i; i = next) {
		next = pdata->list[i]; /* in case removed by flush */
		if (0 != strms[i].fh)
			flush_stream(pdata, i, -EINTR);
	}
}
#undef do_destroy

/*
 * @brief do the actions for the asynchronous reader
 *
 * "Action" is: see above.
 * Actions are handled and removed from the queue.
 * Some will be managed by the function called for each action and
 * immediately responded.
 * Read actions essentially are queued on the stream they belong to.
 *
 * @param private thread data
 * @return none
 */
static bool do_actions(struct private_data *pdata)
{
	struct action *a, *next, **pa;
	bool continue_working = true;

	for (a = pdata->actions; NULL != a; a = a->next) {
		eventfd_t value;
		/* Each action 'wrote' 1 in the eventd, must 'read' it now */
		AW_ERROR(eventfd_read(pdata->fds[0].fd, &value));
	}

	/* -2 to avoid tight poll loop due to times truncated to seconds,
	 * also clean on stream pressure (when no more streams) */
	pdata->cleaned = false;
	if ((0 == pdata->nst &&
	     pdata->now > pdata->last_activity + CLEAN_WAIT - 2) ||
	     pdata->now > pdata->last_clean + CLEAN_MAX_WAIT - 2)
		clean_old_streams(pdata);

	/* stat_set are done before close so that the file they relate to
	 * has a chance to still be opened */
	stats_set_time(pdata);

	/* Do all the "close"/"free" actions first, if any */
	for(pa = &pdata->actions, a = *pa; NULL != a; a = *pa)
	{
		if (0 != ((ACTION_CLOSE | ACTION_FREE) & a->type)) {
			/* For close and free, remove action from queue,
			 * and in relevant cases (#bug 5) also free action. */
			*pa = a->next;
			if (ACTION_FREE == a->type)
				do_free(a->a.f.fh, a->a.f.recv, a->a.f.tid);
			else if (!do_close(pdata, a))
				continue;
			free(a);
			continue;
		}
		pa = &a->next;
	}

	for (a = pdata->actions; NULL != a; a = next) {
		/* "next" is saved now because 'a' might be removed/freed */
		next = a->next;
		switch (a->type) {
			case ACTION_READ :
				associate_read_to_stream(pdata, a);
				break;
			case ACTION_START :
				start_reading_stream(pdata, a);
				break;
				/* The actual close or stat_set was done above,
				 * remains to free buf. allocated "fuse-side" */
				break;
			case ACTION_DESTROY :
				/* Will be done last, just flag it */
				continue_working = false;
				break;
			case ACTION_STATS_SET :
				/* Nothing to do, stats_set_time() removed
				 * stats_set action when any: this case
				 * should not happen! */
				break;
			case ACTION_STATS_GET :
				stats_raw_copy(pdata, a);
				break;
			case ACTION_CLOSE: /* Should not happen: these types  */
			case ACTION_FREE:  /* action removed from queue above */
			default :	   /* This is a bug if it happens! */
				AW_ASSERT(false);
				break;
		}
	}
	/* All actions are done, reset private action to NULL */
	pdata->actions = NULL;

	/* Do destroy as the last action */
	if (!continue_working)
		do_destroy(pdata);

	return continue_working;
}
#undef do_actions


/*
 * @brief do the read for the stream passed as index
 *
 * @param private thread data
 * @param the index of the stream to read
 * @return none
 */
static void read_stream(struct private_data *pdata, sid_t sid)
{
	struct action *a, *g, **where;
	int res;

	/* Skip full gaps */
	where = &strms[sid].queue;
	a = *where; 			/* For readability */
	while (ACTION_GAP == a->type && a->a.g.size == a->a.g.written) {
		/* Remove this gap if too old (it is necessarily full!) */
		if (a->a.g.offset + a->a.g.size + rev_gap <= strms[sid].pos) {
			*where = a->next;
			free_gap(a, true);
		}
		else {
			where = &a->next;
		}
		a = *where;
	}

	/* Is a gap needed now? */
	if (ACTION_READ == a->type && a->a.r.offset > strms[sid].pos) {
		g = malloc(sizeof(struct action));
		g->type = ACTION_GAP;
		g->next = a;
		*where = g;
		g->a.g.offset  = strms[sid].pos;
		g->a.g.written = 0;
		g->a.g.retry   = 0;
		if (a->a.r.offset - strms[sid].pos > max_gap_block_size)
			g->a.g.size = max_gap_block_size;
		else
			g->a.g.size = a->a.r.offset - strms[sid].pos;
		g->a.g.buf     = aligned_alloc(pagesize, g->a.g.size);
		a = g;
	}
	if (ACTION_GAP == a->type) {
		res = aw_ops.read(strms[sid].sh,
				  a->a.g.buf  + a->a.g.written,
				  a->a.g.size - a->a.g.written);
		if (0 < res) {
			strms[sid].last_trf = pdata->now;
			pdata->last_activity = pdata->now;
			strms[sid].pos += res;
			a->a.g.written += res;
			a->next->a.r.retry = 0;
			stats_count_over(res, sid);
			/* Nothing to do when the gap becomes full */
			return;
		}
	}
	else {
		int j;
		size_t s;
		/* When there are 2 blocks the 1st one is always full
		 * because it comes from a gap copy. The 2nd can be partial */
		if (2 == a->a.r.bufp->count) {
			j = 1;
			s = a->a.r.bufp->buf[0].size;
		}
		else {
			j = 0;
			s = 0;
		}
		res = aw_ops.read(strms[sid].sh,
				  ((char *)a->a.r.bufp->buf[j].mem)
							 + a->a.r.written - s,
				  a->a.r.size - a->a.r.written);
		if (0 < res) {
			strms[sid].last_trf = pdata->now;
			pdata->last_activity = pdata->now;
			strms[sid].pos += res;
			a->a.r.written += res;
			a->a.r.retry    = 0;
			if (a->a.r.written != a->a.r.size) {
				set_st_state(pdata, sid, ACTIVE_TRF);
				return;
			}
			/* When the read becomes full,
			 * remove it from queue and respond */
			*where = a->next;
			finish_action(a);	/* Respond as soon a possible */
			if (NULL == *where)	/* No more to read: inactive  */
				set_st_state(pdata, sid, INACTIVE);
			else
				set_st_state(pdata, sid, ACTIVE_TRF);
			strms[sid].n_rq -= 1;
			return;
		}
	}
	if (-EAGAIN == res) {
		strms[sid].last_trf = pdata->now;	/* For poll timeout */
		set_st_state(pdata, sid, ACTIVE_POLL);
		return;
	}
	/* Don't log on error: the user provided callback might have done it.
	 * That will also flush on EOF */
	if (0 >= res)
		retry_or_flush(pdata, a, sid, res);
}
#undef read_stream

/*
 * @brief do the read/writes for the asynchronous worker
 *
 * @param private thread data
 * @return none
 */
static void do_transfers(struct private_data *pdata)
{
	sid_t i, *p;
	int err;

	/* Note the current slot pointed by p could be flushed (error, EOF).
	 * For the loop instruction, if the value is the same, it can proceed,
	 * otherwise it means it has been flushed and replaced by another value,
	 * hence p is not changed to continue on that other value. */
	for (p = &pdata->owned_streams; N_STREAMS != *p;
	     p = (*p == i) ? &pdata->list[i] : p) {
		i = *p;
		if (0 > pdata->fds[i + 1].fd)
			continue;
		if (0 ==  pdata->fds[i + 1].events ||
		    0 != (pdata->fds[i + 1].revents & (POLLIN | POLLOUT))) {
			read_stream(pdata, i);
			continue;
		}
		if (0 != (pdata->fds[i + 1].revents & POLLERR)) {
			aw_ops.log(LOG_WARNING,
				   "socket error on stream %u\n", i);
			err = -EIO;
		}
		else if (0 == pdata->fds[i + 1].revents) {
			/* Test timeout situation */
			if (strms[i].last_trf + READ_TIMEOUT >= pdata->now)
				continue;
			aw_ops.log(LOG_WARNING,
				   "read timeout on socket for stream %u\n", i);
			err = -ETIME;
		}
		else {
			/* Could be POLLHUP, normal at the end let read do it */
			continue;
		}
		retry_or_flush(pdata, strms[i].queue, i, err);
	}
}
#undef do_transfers

/*
 * @brief the asynchronous reader itself!
 *
 * This is started by aw_init and does all the work in a separated thread.
 *
 * @param unused single pointer argument for the thread routine
 * @return NULL
 */
static void * async_worker(void *arg)
{
	sid_t i;
	struct timespec now;
	struct private_data pdata;

	tidx = ((struct start_data *)arg)->tid;

	((struct start_data *)arg)->res = 0;
	AW_ERROR(sem_post(((struct start_data *)arg)->sem_done));

	/* Initialise private data */
	for (i = 1; i < 1 + N_STREAMS; i++) {
		pdata.fds[i].events = 0;
		pdata.fds[i].fd     = -1;
	}
	pdata.fds[0].fd     = eventfd(0, EFD_SEMAPHORE);
	tc[tidx].event_fd   = pdata.fds[0].fd;
	pdata.fds[0].events = POLLIN;
	pdata.n_files	    = 0;
	pdata.now	    = 0;
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	pdata.last_activity = now.tv_sec;
	pdata.last_clean    = now.tv_sec;
	pdata.mfds	    = 0;
	pdata.nst	    = 0;
	pdata.pst	    = 0;
	pdata.actions	    = NULL;
	pdata.owned_streams = N_STREAMS;
	for (i = 0; i < N_STREAMS; i++)
		pdata.list[i] = NOT_OWNED;
	for (i = 0; i < N_FILES; i++)
		pdata.recv[i] = 0;

	/** Main loop */
	/* Note that first do_transfers will do nothing due to thread private
	 * variable initialised above, no neeed adding an extra test! */
	do {
		do_transfers(&pdata);
		wait_for_actions_or_reads(&pdata);
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		pdata.now = now.tv_sec;
	} while (do_actions(&pdata));

	close(pdata.fds[0].fd);

	return NULL;
}
#undef async_worker


/*
 * @brief does a read of the exact specification passed.
 *
 * This is when there is absolutely no way finding a free stream, since it
 * is utterly unoptimised doing a stream start/read/stop for a single read.
 *
 * NOTE/TODO: so far there is no retry on solo_reads. It could be added.
 *
 * @param path of the file to read.
 * @param buf vector to be allocated and populated.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
static int solo_read( const char *path, struct fuse_bufvec *bufp,
		      size_t size, off_t offset, struct fuse_file_info *fi)
{
	int err;
	void *sh;
	int written;
	ssize_t sz;
	struct pollfd sfd;

	err = aw_ops.start_stream(path,	fi, size, offset, &sh, &sfd.fd);
	if (-ENODATA == err)
		return 0;
	if (0 != err)
		return err;

	if (NULL == bufp->buf[0].mem)
		bufp->buf[0].mem = aligned_alloc(pagesize, size);

	sfd.events = POLLIN | POLLPRI;
	written = 0;
	while( written != size) {
		sz = aw_ops.read(sh, ((char *)bufp->buf[0].mem) + written
				   , size - written);
		if (0 < sz) {
			written += sz;
		}
		else if (0 == sz) {
			size = written;
		}
		else if (-EAGAIN == sz) {
			int res;
			res = poll(&sfd, 1, READ_TIMEOUT * 1000);
			if (-1 == res) {
				written = -errno;
				aw_ops.log(LOG_ERR,
					   "poll error %d for solo read.\n",
					   -written);
				break;
			}
			if (0 == res) {
				/* Timeout waiting for socket to be ready */
				written = -ETIME;
				aw_ops.log(LOG_WARNING,
					   "timeout on socket for solo read.\n");
				break;
			}
		}
		else {
			written = sz;
			break;
		}
	}
	aw_ops.close_if_idle(aw_ops.fh_from_fi(fi), sh, 0, 0, true);
	return written;
}
#undef solo_read

/*
 * @brief decode the opaque 'opt' into tid and idx.
 *
 * @param the opaque 'opt' to decode.
 * @param where to store tid (thread index) decoded.
 * @param where to store sid (stream index) decoded.
 * @return none
 */
static void decode_opt(uint32_t opt, tid_t *tid, sid_t *sid)
{
	if (UNKNOWN_OPT == opt) {
		*sid = N_STREAMS;
		*tid = max_threads;
	}
	else {
		*sid = opt & 0x0000FFFF;
		*tid = opt >> 16;
	}
}
#undef decode_opt

/*
 * @brief encode tid and idx into an opaque 32 bits unsigned int 'opt'.
 *
 * @param tid (thread index) to encode.
 * @param sid (stream index) to encode.
 * @return opaque result encoded as unsigned 32bits int
 */
static uint32_t encode_opt(tid_t tid, sid_t sid)
{
	return ( ((uint32_t)tid) << 16 ) + (uint32_t)sid;
}
#undef encode_opt

/*
 * @brief common read code to both sorts of read.
 *
 * 'Standard' read has a buf->count to zero and a buffer already set.
 * 'read_buf' has a buf->count of 1 (and space for 2) and no allocated buf
 *            space at this point (in case a 'gap' is grabbed).
 *
 * @param path of the file to read.
 * @param buf vector to be allocated and populated.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
static int common_read( const char *path, struct fuse_bufvec *bufp,
			size_t size, off_t offset, struct fuse_file_info *fi)
{
	struct action a, t;
	sid_t sid;
	tid_t tid;
	sem_t sem_done;

	STAT_DECL(struct timespec, t_start0); /* Real time start */
	STAT_DECL(struct timespec, t_start);  /* After start_stream */

	stats_get_time(&t_start0);
	stats_move_tm(&t_start, &t_start0);

	bufp->buf[0].fd   = -1;
	bufp->buf[0].size = size;

	AW_ERROR(sem_init(&sem_done, 0, 0));

	decode_opt(aw_ops.load_last_opt(fi), &a.a.r.tid, &sid);
	tid = (max_threads == a.a.r.tid) ? O_THREAD : a.a.r.tid;

	a.next     = NULL;
	a.type     = ACTION_READ;
	a.sem_done = &sem_done;
	a.a.r.bufp	= bufp;
	a.a.r.size	= size;
	a.a.r.offset	= offset;
	a.a.r.written	= 0;
	a.a.r.start	= NO_START;
	a.a.r.fh	= aw_ops.fh_from_fi(fi);
	a.a.r.sid	= sid;
	a.a.r.fid	= N_FILES;
	a.a.r.retry	= 0;
	a.a.r.rq_id	= atomic_fetch_add_explicit(&rq_id, 1,
						    memory_order_relaxed);

	post_action(&a, tid);

	while(1) {
		AW_ERROR(sem_wait(&sem_done));
		/* When solo, tid and sid can be out of range */
		if ((sid != a.a.r.sid || tid != a.a.r.tid) &&
		     a.a.r.sid < N_STREAMS && a.a.r.tid < max_threads) {
			sid = a.a.r.sid;
			tid = a.a.r.tid;
			aw_ops.store_last_opt(fi, encode_opt(tid, sid));
		}

		if (ACTION_READ == a.type) {
			if (NO_START == a.a.r.start)
				break;	/* Regular read is done */
			/* A start stream action is necessary */
			stats_move_tm(&t.a.t.t_start, &t_start);
			t.a.t.err = aw_ops.start_stream(path, fi, 0,
							a.a.r.start, &t.a.t.sh,
							&t.a.t.fd);
			if (0 != t.a.t.err) {
				/* Signal the error to the reader */
				t.a.t.sh = NULL;
				t.a.t.fd = -1;
			}
			a.a.r.start = NO_START; /* Reset read action start */
			t.type      = ACTION_START;
			t.sem_done  = NULL;
			t.a.t.fh    = a.a.r.fh;
			t.a.t.sid   = a.a.r.sid;
			stats_get_time(&t.a.t.t_end);
			stats_move_tm(&t_start, &t.a.t.t_end);
			post_action(&t, tid);
		}
		else {
			/* This is solo */
			a.a.r.written = solo_read(path, bufp,
						  size, offset, fi);
			break;
		}
	}

	AW_ERROR(sem_destroy(&sem_done));

	stats_update_read_time(&t_start0, &t_start, &a);

	return a.a.r.written;
}
#undef common_read


/**
 * @brief initialise operations for the asynchonous reader
 *
 * The aw_operations structure passed must have non NULL functions for
 * start_stream and read, otherwise EINVAL is returned.
 * All the other members can be NULL, and in this case the statically
 * initialised defaulf will be used.
 *
 * On top of initialising the aw_operations structures, this functions also
 * initialises various semi-constants, and starts the asynchronous reader
 * thread. Should that fail, an error is returned.
 *
 * The default number of threads which is MIN( 1, available_procs / 2 ).
 * The maximum number of threads really configured is always limited to the
 * number of available procs, and to N_STREAMS / MIN_STREAMS_BY_FILE,
 * whichever the lowest.
 *
 * If the user specifies min/max_thrds, the function will try to find a
 * suitable number of threads inside the boundaries explained above.
 * Specifying 0 for either min or max_thrds means that the caller does not care
 * respectively about min or max.
 *
 * The last parameter specifies the number of gap buffers (outstanding requests)
 * to be used. The default is to use 4 buffers. This is needed by standard Linux
 * utilities and kernel read-ahead to avoid unwanted stream starts. More gaps
 * can help on semi-random read patterns at the expense of potential "over-read"
 * and memory consumption. With the default setting a sequential access, such
 * as with cp, dd, pv, rsync, md5sum, etc... should use only a single stream,
 * unless high memory pressure or when dropping caches on purpose.
 * The 'auto' feature (TODO) will select a relevant number of gaps considering
 * the average time it takes to open a stream, the average time for reads of
 * standard buffers, and the available memory at startup. "Auto" feature needs
 * statistics. When statistics are not activated, Auto will become default.
 * Use GAP_BUFFERS_DEFAULT or GAP_BUFFERS_AUTO for those features.
 * The selected value cannot be lower than 4 and will be adjusted accordingly.
 *
 * The unit is the maximum read block used by kernel, currently 32 pages which
 * on most systems is 128KB.
 *
 * IMPORTANT: this function is not thread-safe and this should be called only
 *	      once, usually at the start of the program.
 *
 * @param pointer to aw_operations to use.
 * @param minimum number of threads (0 = unspecified, use default).
 * @param maximum number of threads (0 = unspecified, use default).
 * @param gap buffers to use (-1 = auto [todo] 0 = unspecified, use default).
 * @return 0 or error
 */
int aw_init(struct aw_operations const *a,
	    unsigned int min_thrds, unsigned int max_thrds /* ,
	    unsigned int gap_buffers */ )
{
	tid_t t;
	sid_t i;
	unsigned int n_threads;
	int res, active_procs;
	sem_t sem_done;

	/* Set the "operations" functions */
	if (NULL == a || NULL == a->start_stream || NULL == a->read ||
	    (NULL != a->store_last_opt && NULL == a->load_last_opt) ||
	    (NULL == a->store_last_opt && NULL != a->load_last_opt)   )
		return EINVAL;

	/* "Memory" functions must be either all NULL or all not NULL */
	if ( ! ( (NULL == a->aligned_alloc &&
		  NULL == a->free 	   &&
		  NULL == a->release)		||
		 (NULL != a->aligned_alloc &&
		  NULL != a->free 	   &&
		  NULL != a->release)
		)
	   )
		return EINVAL;

	/* min and max_thrds must be coherent! */
	if (0 != max_thrds && max_thrds < min_thrds)
		return EINVAL;

	/* Except for .log where aw_ops is always used, AW_OPS is a macro
	 * replaced by aw_ops or aw_dbg in astreamfs_worker_dbg.c, depending
	 * on the DEBUG flag */
				       AW_OPS.start_stream  = a->start_stream  ;
				       AW_OPS.read          = a->read          ;
	if (NULL != a->aligned_alloc ) AW_OPS.aligned_alloc = a->aligned_alloc ;
	if (NULL != a->free          ) AW_OPS.free          = a->free          ;
	if (NULL != a->release       ) AW_OPS.release       = a->release       ;
	if (NULL != a->log           ) aw_ops.log           = a->log           ;
	if (NULL != a->error         ) AW_OPS.error         = a->error         ;
	if (NULL != a->anticipation  ) AW_OPS.anticipation  = a->anticipation  ;
	if (NULL != a->retry	     ) AW_OPS.retry	    = a->retry	       ;
	if (NULL != a->close_if_idle ) AW_OPS.close_if_idle = a->close_if_idle ;
	if (NULL != a->store_last_opt) AW_OPS.store_last_opt= a->store_last_opt;
	if (NULL != a->load_last_opt ) AW_OPS.load_last_opt = a->load_last_opt ;
	if (NULL != a->fh_from_fi    ) AW_OPS.fh_from_fi    = a->fh_from_fi    ;

	/* Initialise pagesize and related semi-constants */
	pagesize = sysconf(_SC_PAGESIZE);
	if (pagesize <= 0 ||
	    pagesize > LONG_MAX /
			(MAX_PAGES_READ_AHEAD * MAX_OUTSTANDING_REQUESTS))
		pagesize = DEFAULT_PAGESIZE;
	max_gap_block_size = pagesize * MAX_PAGES_READ_AHEAD;
	max_gap = max_gap_block_size * MAX_OUTSTANDING_REQUESTS;

	/* On 32 bit systems due to memory fragmentation, the "reverse" gap
	 * must be bigger to avoid dropping gaps prematurely. */
#if UINTPTR_MAX == 0xffffffff
/* 32-bit */
	rev_gap = max_gap_block_size * (MAX_OUTSTANDING_REQUESTS + 1);
#else
	rev_gap = max_gap;
#endif

	/* Compute the number of threads */
		/* Compute default number of threads */
	active_procs = sysconf(_SC_NPROCESSORS_ONLN);
	n_threads = active_procs / 2;
		/* Account for min and max */
	if (min_thrds > n_threads)
		n_threads = min_thrds;
	if (0 != max_thrds && n_threads > max_thrds)
		n_threads = max_thrds;
		/* Test boundaries */
	if (0 == n_threads)
		n_threads = 1;
	if (N_STREAMS / MIN_STREAMS_BY_FILE < n_threads)
		n_threads = N_STREAMS / MIN_STREAMS_BY_FILE;
	if (n_threads > active_procs)
		n_threads = active_procs;
	max_threads = n_threads;

	share_of_streams = (N_STREAMS + max_threads - 1) / max_threads;

	/* Allocation of threads related variables */
	tc = malloc(max_threads * sizeof(struct thread_consts));
	ts = aligned_alloc(LEVEL1_DCACHE_LINESIZE,
			   max_threads * sizeof(struct thread_shared));
	memset(ts, '\0', max_threads * sizeof(struct thread_shared));

	/* Orchestration initialisation */
	o_top = malloc(max_threads * sizeof(struct o_main));
	for (t = 0; t < max_threads; t++) {
		o_top[t].n_files   = 0;
		o_top[t].first_fid = N_FILES;
	}
	for (i = 0; i < N_STREAMS; i++)
		o_list[i] = i + 1;

	/* And starts the readers' async threads */
	struct start_data sdata[max_threads];

	AW_ERROR(sem_init(&sem_done, 0, 0));
	for (t = 0; t < max_threads; t++) {
		sdata[t].sem_done = &sem_done;
		sdata[t].tid = t;
		AW_ERROR(pthread_create(&tc[t].aw_thread, NULL,
					async_worker, &sdata[t]));
	}

	for (t = 0; t < max_threads; t++)
		AW_ERROR(sem_wait(&sem_done));

	AW_ERROR(res = sem_destroy(&sem_done));

	for (t = 0; t < max_threads; t++)
		AW_ERROR(sdata[t].res);

	return 0;
}
#undef aw_init

/**
 * @brief terminates the asynchonous reader
 *
 * This will typically be called with the 'destroy' handler of the fuse
 * application.
 *
 * There must be no pending request at that moment, but normally the fuse
 * library checks that and won't allow 'destroy' if resources are still used,
 * instead fusermount -u will return a busy error message.
 *
 * @param none
 * @return none
 */
void aw_destroy(void)
{
	unsigned int i;
	struct action a;
	void *retval;

	a.type = ACTION_DESTROY;

	for (i = 0; i < max_threads; i++)
		if (O_THREAD != i)
			post_action(&a, i);

	/* There is no need for a semaphore here, synchro is the join below */
	for (i = 1; i < max_threads; i++)
		AW_ERROR(pthread_join(tc[i].aw_thread, &retval));

	/* Kill orchestration thread last, so other send their "free" action */
	post_action(&a, O_THREAD);
	AW_ERROR(pthread_join(tc[0].aw_thread, &retval));

	free(tc);
	free(ts);
	free(o_top);

	stats_destroy();
}
#undef aw_destroy

/**
 * @brief get information of main internal constants of the async reader
 *
 * If this is called before aw_init, n_threads will be set to 1.
 *
 * @param pointer to aw_info structure to fill out.
 * @return none
 */
void aw_get_info(struct aw_info *info)
{
	if (NULL != info) {
		info->n_streams  = N_STREAMS;
		info->n_threads  = max_threads;
		info->block_size = max_gap_block_size;
		info->max_gap    = max_gap;
	}
}
#undef aw_get_info

/**
 * @brief optional, when the caller wants to force close all streams of a 'fi'
 *
 * It is not necessary for the application to call this function at fuse
 * 'release' since there is a 'background cleaner' triggered when the
 * asynchronous reader has been idle for long enough (CLEAN_WAIT seconds) that
 * will close individual streams when they become idle. Streams will also be
 * reclaimed when needed by other files.
 *
 * Nevertheless, the streams association to a file ('fi') is based on the value
 * of 'fh' in the struct fuse_file_info. Should the application want to reuse
 * those 'fh', it should first call aw_close with that 'fh' to be sure there are
 * no bits and pieces remaining of the 'old' file using the same 'fh'.
 *
 * @param file reference: 'fh' as in the struct fuse_file_info pointer
 * @return EBUSY if there are pending requests, 0 otherwise.
 */
void aw_close(uint64_t fh)
{
	struct action *a;

	a = malloc(sizeof(struct action));

	a->type     = ACTION_CLOSE;
	a->a.c.fh   = fh;

	post_action(a, O_THREAD);
}
#undef aw_close

/**
 * @brief this is the read callback as fuse wants it!
 *
 *
 * @param path of the file to read.
 * @param buffer where data is to be copied.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
int aw_read(const char *path, char *buf, size_t size, off_t offset,
	    struct fuse_file_info *fi)
{
	struct fuse_bufvec bv;

	if (0 == size)		/* Shortcut for zero sized read */
		return 0;

	memset(&bv, '\0', sizeof(struct fuse_bufvec));
	bv.count = 0;	/* This flags read to differentiate with read_buf */
	bv.buf[0].mem = buf;

	return common_read(path, &bv, size, offset, fi);
}
#undef aw_read

/**
 * @brief this is the read_buf callback as fuse wants it!
 *
 *
 * @param path of the file to read.
 * @param buf vector to be allocated and populated.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
int aw_read_buf(const char *path, struct fuse_bufvec **bufp,
		size_t size, off_t offset, struct fuse_file_info *fi)
{
	int res;

	/* Allocate and initialise bufp: needed even for "fast returns"
	 * See note on "gaps", the maximum bufvec 'count' is 2, so bv is
	 * allocated with 2 buffer (even if in most cases 1 only is needed).
	 * This will avoid new allocation/free in rare case 2 bufs are needed
	 * and the excess of memory consumed is reasonable. */
	*bufp = malloc(sizeof(struct fuse_bufvec) + sizeof(struct fuse_buf));
	memset(*bufp, '\0', sizeof(struct fuse_bufvec));

	if (0 == size) {		/* Shortcut for zero sized read */
		(*bufp)->count = 0;
		aw_ops.release(*bufp);
		return 0;
	}
	else {
		(*bufp)->count = 1;
	}
	/* buf[0].mem is not allocated here in case gaps are snatched. */

	res = common_read(path, *bufp, size, offset, fi);
		/* call op_release on bufp elements that will be free() later */

	if (0 <= res && size != res) {
		/* Adjust bufvec sizes if partial read (normally EOF) */
		if (1 == (*bufp)->count) {
			(*bufp)->buf[0].size = res;
		}
		else {
			if ((*bufp)->buf[0].size >= res) {
				(*bufp)->buf[0].size = res;
				free((*bufp)->buf[1].mem);
				(*bufp)->count = 1;
			}
			else {
				(*bufp)->buf[1].size = res -
						       (*bufp)->buf[0].size;
			}
		}
	}

	/* call op_release on bufp elements that will be free() later */
	aw_ops.release((*bufp)->buf[0].mem);
	if (2 == (*bufp)->count)
		aw_ops.release((*bufp)->buf[1].mem);
	aw_ops.release(*bufp);

	return res;
}
#undef aw_read_buf
