/*
 * astreamfs and derivative work (1fichierfs) common code header
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/stat.h>
#include <syslog.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdatomic.h>
#include <inttypes.h>
#include <curl/curl.h>
#include <poll.h>

/*
 * @brief options handling related macros
 *
 */
#define SZ_SHORT_ARG 2    /* Short arguments like -u have a size of 2        */
#define O_OFFSET 2        /* Offset from -- arg to -o arg is 2: double '-'   */
#define SZ_STD_ARG_TYPE 2 /* size of the standard argument type, ex. %i is 2 */

			  /* This macro generates both long arg and o-arg    */
#define FUSE_L_O_ARG(long_name, key) \
  FUSE_OPT_KEY(long_name, key), FUSE_OPT_KEY(long_name + O_OFFSET, key)

			  /* This macro generates all: short + the above     */
#define FUSE_ALL_ARG(short_name, long_name, key) \
  FUSE_OPT_KEY(short_name, key), FUSE_L_O_ARG(long_name, key)


/*
 * @brief HTTP Status codes we use
 *
 */
#define HTTP_OK				(200)
#define HTTP_PARTIAL_CONTENT		(206)
#define HTTP_BAD_REQUEST		(400)
#define HTTP_FORBIDDEN			(403)
#define HTTP_NOT_FOUND			(404)
#define HTTP_GONE			(410)
#define HTTP_TOO_MANY_REQUESTS		(429)
#define HTTP_MOVED_PERMANENTLY		(301)
#define HTTP_FOUND			(302)
#define HTTP_SEE_OTHER			(303)
#define HTTP_TEMPORARY_REDIRECT		(307)
#define HTTP_PERMANENT_REDIRECT		(308)
#define HTTP_INTERNAL_SERVER_ERROR	(500)
#define HTTP_NOT_IMPLEMENTED		(501)
#define HTTP_BAD_GATEWAY		(502)
#define HTTP_SERVICE_UNAVAILABLE	(503)
#define HTTP_GATEWAY_TIME_OUT		(504)

/*
 * @brief max size of the request and of a single response header
 *
 */
#define MAX_RQ_SZ 2048
#define STRLEN_MAX_INT64 20 /* Max length of int64 as string:
				  length=12345678901234567890
			       unsigned: 18446744073709551616
			       signed  : -9223372036854775807 */

/*
 * @brief Global parameters of the daemon.
 *
 */
struct params {
	struct stat mount_st;
	bool	foreground;
	bool	debug;
	bool	noatime;
	bool	noexec;
	bool	nosuid;
	bool	no_splice_read;
	unsigned int  log_level;
	unsigned int  threads;
	unsigned long log_size;
	char *log_file;
	char *user_agent;
	char *filesystem_name;
	char *filesystem_subtype;
	char *ca_file;
#ifdef _STATS
	char *stat_file;
#endif
};
extern struct params params;

/* This is for curl_raw temporary code! */
struct curl_raw_handle {
	CURL *curl;
	curl_socket_t sockfd;
	size_t remains;	/* what remains of the initial overflow */
	size_t used;	/* how much was used of the overflow */
	bool chunked;
	bool transfer_complete;
	int  transfer_rc; /* assigned after completed transfer */
	char overflow[];
};

/* This is for use with open/astreamfs_worker */
#define TYPE_READ	1	/* The file is read only */
#define TYPE_WRITE	2	/* The file was opened first for write */
#define TYPE_SPECIAL	4	/* Special: refresh, stats */
struct fi_to_fh {
		uint32_t type;
	_Atomic uint32_t last_opt;
		uint64_t fh;	
};

#define KBUF (131072)		/* Readahead kernel buffer max size (128KB) */

#if defined(_MEMCOUNT) || defined(_STATS)
#define SZ_BUF_HR (6) /* Size of buffer needed for stat conversion Num to str */
#define STAT_MAX_SIZE	KBUF
struct stats {
	pthread_mutex_t	initmutex;
	size_t		size;
	char		buf[STAT_MAX_SIZE];
};
#endif

/*
 * @brief log utility.
 *
 * The log utility printf either on terminal, on a file or to syslog depending
 * on the situation.
 *
 * We have here global external variables, macro and function definition.
 *
 * The use is with the macro lprintf(LOG_xxx, ...)
 * LOG_xxx is a log level as defined in syslog.h
 * The rest are the same paramaters as printf.
 */

extern const char log_prefix[]; /* This needs to be defined along the include */
extern bool init_done;
extern FILE *log_fh;
extern struct timespec main_start;

extern struct aw_info engine_info; /* For stats and DL token type */


#ifdef _STATS
typedef unsigned long long tm_index_t;

	/* Look at the algorithm to change it. SPEED_RES must be even	      */
#define SPEED_RES (8UL)	/* 8 ticks per second to compute transfer speed       */
#define SPEED_MEM (2UL)	/* 2 seconds of speed memory to avoid spike artefacts */

#define CLOCK_PREC (10000UL)  /* Display clock precision, here 100 milli sec  */

#define CLOCK_RES (1000000000UL) /* Clock resolution is nano second           */
	/* Look at the algorithm to change it. SPEED_RES must be even	      */

#define SPEED_SZ (SPEED_RES * SPEED_MEM) /* Convenience macro */

#define SZ_BUF_TIME (STRLEN_MAX_INT64 + 6) /* Size of buffer needed for       */
#define SZ_BUF_REF (7)			   /* various conversions             */

/*
 * @brief prints statistics in the buffer, checking for overflow
 *
 *  Macro to simplify the code. Used only in out_stats()
 *    - prints in the buffer
 *    - update the count of bytes
 *    - if overflow, returns.
 */
#define SNPRINTF(format, ...) do{\
	size_t cp;\
	cp = snprintf(&stts->buf[stts->size],\
		      STAT_MAX_SIZE - stts->size,\
		      format,\
		      __VA_ARGS__ );\
	if (cp >= STAT_MAX_SIZE - stts->size) {\
		stts->size = STAT_MAX_SIZE;\
		return stts->size - 1;}\
	stts->size += cp; } while(0)

#endif

#define MAX_LEVEL 7

#ifdef DEBUG
	#undef DEBUG
	#define DEBUG(...) lprintf(LOG_DEBUG, __VA_ARGS__)
	#define _DEBUG
#else
	#define DEBUG(...)
	#undef _DEBUG
#endif

/* Macro apply only to constant strings defined as: const char var[]="foo"; 
 * _Generic with no default will give an error if the type is wrong
 * WARNING: don't apply to const char *var = "foo"; The result would be the
 *	    size of the pointer! */
#define lengthof(var) (_Generic(var, const char * : sizeof(var) - 1))

extern void lprintf(int level, const char *pFormat, ... );

/*
 * @brief Utility macro to simplify atomics that have really long names!
 *
 * Note: this uses a "trick" that works for GCC and CLANG and now standard C++2a
 *	 to have relevant default memory models for those macro, but still be
 *	 able to overload the default if needed.
 */
/* 16.04 does not yet support the standard __VA_OPT__, continue using gcc spec.
#define DEF_OR_ARG(model,...) memory_order_ ## model

#define LOAD(a,...)    atomic_load_explicit(&(a), \
				DEF_OR_ARG(__VA_ARGS__ __VA_OPT__(,) acquire))
#define STORE(a,v,...) atomic_store_explicit(&(a), (v), \
				DEF_OR_ARG(__VA_ARGS__ __VA_OPT__(,) release))
#define ADD(a,v,...)   atomic_fetch_add_explicit(&(a), (v), \
				DEF_OR_ARG(__VA_ARGS__ __VA_OPT__(,) acq_rel))
#define SUB(a,v,...)   atomic_fetch_sub_explicit(&(a), (v), \
				DEF_OR_ARG(__VA_ARGS__ __VA_OPT__(,) acq_rel))
#define AND(a,v,...)   atomic_fetch_and_explicit(&(a), (v), \
				DEF_OR_ARG(__VA_ARGS__ __VA_OPT__(,) acq_rel))
#define OR(a,v,...)    atomic_fetch_or_explicit( &(a), (v), \
				DEF_OR_ARG(__VA_A1fichierfs DEBUG=1RGS__ __VA_OPT__(,) acq_rel))
*/
#define DEF_OR_ARG(z,def,arg,...) memory_order_ ## arg

#define LOAD(a,...)    atomic_load_explicit(&(a), \
				DEF_OR_ARG(,##__VA_ARGS__,__VA_ARGS__, acquire))
#define STORE(a,v,...) atomic_store_explicit(&(a), (v), \
				DEF_OR_ARG(,##__VA_ARGS__,__VA_ARGS__, release))
#define ADD(a,v,...)   atomic_fetch_add_explicit(&(a), (v), \
				DEF_OR_ARG(,##__VA_ARGS__,__VA_ARGS__, acq_rel))
#define SUB(a,v,...)   atomic_fetch_sub_explicit(&(a), (v), \
				DEF_OR_ARG(,##__VA_ARGS__,__VA_ARGS__, acq_rel))
#define AND(a,v,...)   atomic_fetch_and_explicit(&(a), (v), \
				DEF_OR_ARG(,##__VA_ARGS__,__VA_ARGS__, acq_rel))
#define OR(a,v,...)    atomic_fetch_or_explicit( &(a), (v), \
				DEF_OR_ARG(,##__VA_ARGS__,__VA_ARGS__, acq_rel))

#define XCHG(a,d,v) atomic_compare_exchange_strong_explicit(&(a), &(d), (v),   \
							 memory_order_acq_rel, \
							 memory_order_acquire)

/*
 * @brief Utility macro to simplify error handling on curl_easy_setopt and
 *        curl_easy_getinfo
 */
#define CURL_EASY_SETOPT(c,o,a,fmt) \
	do { CURLcode res = curl_easy_setopt(c,o,a); \
	     if (CURLE_OK != res) \
	       lprintf( LOG_CRIT, \
			"%s at %d, err %d: curl_easy_setopt(c, " #o ", " fmt ")\n", \
			__FILE__, __LINE__, res, a);\
	   } while(0)

#define CURL_EASY_GETINFO(c,o,p) \
	do { CURLcode res = curl_easy_getinfo(c,o,p); \
	     if (CURLE_OK != res) \
		lprintf( LOG_CRIT, \
			"%s at %d, err %d: curl_easy_getinfo(c, " #o ",p)\n", \
			__FILE__, __LINE__, res );\
	   } while(0)


#ifdef _MEMCOUNT /* Using accounting memory allocation functions from
		    astreamfs_worker_mem */
extern void *fs_alloc(size_t size);
extern void *fs_calloc(size_t nmemb, size_t size);
extern char *fs_strdup (const char *s);

	#define calloc(n,s)	   fs_calloc(n,s)
	#define realloc(p,s)	   aw_realloc(p,s)
	#ifdef strdup /* Defined on some /usr/include/string.h */
		#undef strdup
	#endif
	#define strdup(s)	   fs_strdup(s)
	#define malloc(sz)	   aw_aligned_alloc(alignof(max_align_t), sz)
	#define aligned_alloc(a,s) aw_aligned_alloc(a,s)
	#define free(p)		   aw_free(p)
#endif

/*
 * @brief Mutex lock-unlock utility.
 *
 * Simplifies handling and logging of mutex lock/unlock.
 */
extern void   lock(pthread_mutex_t *mutex, bool *locked);
extern void unlock(pthread_mutex_t *mutex, bool *locked);

/*
 * @brief semaphore wait, post, etc... utlity.
 *
 * Simplifies handling and logging of sem_wait, sem_post, sem_trywait
 */
extern int wait(sem_t *sem);
extern int post(sem_t *sem);
extern int trywait(sem_t *sem);
extern int timedwait(sem_t *sem, struct timespec *until);

/*
 * @brief allocates and initialises a fuse_bufvec suitable for read_buf
 *
 * @param size for data buffer.
 * @return the allocated and initialised fuse_bufvec pointer.
 */
extern struct fuse_bufvec * init_bufp(size_t size);

/*
 * @brief Global contention logging and counting.
 *
 * @param: spin count
 * @param: function name
 * @return: none
 */
extern void spin_add(unsigned long spin, const char* fct_name);

/*
 * @brief initialises the aw_worker + stats if necessary
 *
 * @param operations structure for the worker
 * @param information about engine init (in: n_threads to set)
 * @return private data fuse context
 */
extern void *worker_init(const	struct aw_operations *aw_ops, 
				struct aw_info	     *engine_info);

/*
 * @brief auxilliary function to wait on a socket
 *
 * @param the socket to wait on
 * @param events to wait for
 * @param timeout in milliseconds
 * @return -1 if error, otherwise the length of the rq string.
 */
extern int wait_on_socket(curl_socket_t sockfd, int events, long timeout_ms);

/*
 * @brief utility that grabs the status code from the first response line
 *
 * @param header buffer
 * @param buffer size
 * @param where to store the result
 * @return none. Stores 0 in result on error.
 */
void http_response_status_code(char *buf, size_t sz, int *http_code);

/*
 * @brief utility that grabs the file size from the range response header
 *
 * @param header buffer
 * @param buffer size
 * @param where to store the size
 * @return true if ok, false if error
 */
bool http_file_size(char *buf, size_t sz, off_t *file_size);

/*
 * @brief builds and sends the request
 *
 * @param curl handle
 * @param the url
 * @param additionnal headers
 * @param the user's callback (same as curl's header callback)
 * @param the userdata to be passed to the user's callback
 * @param the offset for range size
 * @param size of the range (0 means up to end of the file)
 * @param timeout indication (if needed pass not NULL)
 * @return -1 if error, otherwise the length of the rq string.
 */
extern struct curl_raw_handle 
	*send_request(	CURL *curl, char *url, const char *hdr,
			size_t (*callback)(char *p, size_t, size_t, void *),
			void *userdata, off_t off, size_t size, bool * timeout);

/* Function called on system error in astream_worker (apart from malloc).
 *
 *	System errors are typically semaphore error codes or the like.
 *	The result of the system call is passed to the function.
 *	The function should either exit the program, considering
 *	such errors are critical thus never return.
 *	Returning to the caller after such errors will probably
 *	trigger undefined behaviour.
 *
 * @param the error (0 = no error)
 * @param line where the error happened
 * @return the error (but see comment, it should not return!)
 */
extern int astreamfs_error(int err, int line);

/* Optimisation callback to store an opaque optimisation data
 *
 * @param struct fi from fuse (returned at open)
 * @param the last opaque optimisation data to store
 * @return none
 */
extern void astreamfs_store_last_opt(struct fuse_file_info *fi, uint32_t opt);

/* Optimisation callback to get the opaque optimisation data
 *  When streaming, the optimisation avoid a potentially costly
 *  search on all the streams to find the right one.
 * 
 * @param struct fi from fuse (returned at open)
 * @return the last opaque optimisation data
 */
extern uint32_t astreamfs_load_last_opt(struct fuse_file_info *fi);

/* Optimisation callback to get the unique remote file reference
 *  from the struct fuse_file_info pointer 'fi'.
 *
 *  See detailed comments in astreamfs_worker.h about the unique
 *  remote file reference.
 *
 * @param struct fi from fuse (returned at open)
 * @return the 'fh' private user data
 */
extern uint64_t astreamfs_fh_from_fi(struct fuse_file_info *fi);

/* callback to read a chunk of a stream
 *
 *	This has the same semantics as the system read function,
 *	except for return value on error. Instead of returning -1
 *	and setting errno, the error must be returned as a negative
 *	value, same as expected by fuse (eg return -errno)
 *
 *	Since polling is used by the main loop of the asynchronous
 *	reader, it is perfectly fine if this read callback does not
 *	retrieve as much data as expected. It can also happen that
 *	read returns no data although the socket was ready (eg openssl
 *	buffering), in such case it should return -EAGAIN. This
 *	discriminates from returning no data with no error, which
 *	means End of File: only case this function should return 0.
 *
 * @param opaque stream handle (returned by start_stream) to read from
 * @param buffer where to store read data
 * @param count max bytes expected
 * @return Ok: number of bytes read, KO: negative error
 */
extern ssize_t astreamfs_read_callback(void *sh, void *buf, size_t count);

#if defined(_MEMCOUNT) || defined(_STATS)
#define KILO (1024)

/*
 * @brief format an unsigned long long as "human readable" à la 'curl'
 *
 *   The result is always 5 characters long (+ '\0')
 *   If possible display 5 digits (only if num is under 99999)
 *   otherwise display max 4 digits and a possible exponent (K, M, G, etc...)
 *   According to the number of digit in the integer part, displays fraction
 *   digits.
 *   Examples: 24370  1792K  92.1M  1.04G
 *
 * @param number to format
 * @param buffer to store the result (must be at least SZ_BUF_HR size)
 * @return the buffer formatted.
 */
extern char *format_hr(unsigned long long num, char *buf);

#endif

#ifdef _STATS
/*
 * @brief format an signed long long as "human readable" à la 'curl'
 *
 *   Same as format_hr, but for signed numbers
 *
 * @param number to format
 * @param buffer to store the result (must be at least SZ_BUF_HR size)
 * @return the buffer formatted.
 */
extern char *format_sr(long long num, char *buf);

/*
 * @brief formats very small quantities (one digit)
 *
 * @param quatity
 * @return digit (or * if more than 9).
 */
extern char small_value(unsigned int v);

/*
 * @brief returns the current time index
 *
 *  Gets the current time (MONOTONIC_COARSE).
 *  Compute the index from the delay (now - main_start) shifting sec and nsec
 *  according to the speed resolution (SPEED_RES)
 *
 * @param timespec pointer where the current timespec will be stored
 * @return time index.
 */
extern tm_index_t tm_to_tm_index(struct timespec *now);

/*
 * @brief returns a time average
 *
 *  Gets the average of the time stored in the timespec for n items
 *
 * @param timespec
 * @param number of items for the average
 * @param buffer to store the result
 * @return the result.
 */
extern const char *ts_avg_to_str(struct timespec *t,
				 unsigned long long n, char *buf);

/*
 * @brief displays the stats header: uptime
 *
 * @param pointer to the stats structure
 * @param where to store the timespec for now
 * @param where to store the tm_index for now
 * @return size written so far.
 */
extern size_t out_stat_header(struct stats *stts, struct timespec *now
						, tm_index_t *tm_now);

/*
 * @brief displays the readers statistic part
 *
 * @param pointer to the stats structure
 * @param where file stats (needed for the list of active files)
 * @param function to get the reference of active files
 * @param user data to pass to that function
 * @return size written so far.
 */
extern size_t out_stat_read(struct stats *stts,
			    struct aw_stats *f_stats,
			    const char *(ref)(uint64_t, char *, void *),
			    void *userdata);

#endif

#ifdef _MEMCOUNT
/*
 * @brief prepare the content of the memory stats.
 * Note: _MEMCOUNT can be defined and no stats
 * 
 * @param buffer where to write the statistics
 * @param offset in the buffer (written so far)
 * @param max size of the buffer
 * @return 
 */
extern size_t out_stats_mem(char *buf, size_t offset, size_t max);

#else
#define out_stats_mem(b,o,m)
#endif

/*
 * @brief Expands arguments from the command line
 */
extern void expand_args(struct fuse_args *args,
			const char **long_names, size_t n_long_names,
			const char *end_expand);

/*
 * @brief checks the mount directory path
 *
 * Note: since it is using strerror (NOT thread safe) this MUST be used
 *       only in the initialisation phase before going multithreaded.
 *
 * @param path of the mountpoint to check
 * @return none
 */
extern void check_mount_path(const char *path);


/*
 * @brief logs the start date-time and start message
 *
 * Note: main should start with clock_gettime to pass the start time.
 *
 * @param pointer to time_t representing the start date
 * @return none
 */
extern void start_message(time_t *t);

/*
 * @brief extracts an integer from an argument string
 */
extern uint64_t read_int(const char *arg);

/*
 * @brief extracts and stores a string from a long argument, or -o fuse form.
 */
extern void read_str(const char *arg, char **where);

/*
 * @brief string utility: identical length at begining of 2 strings 
 */
extern size_t eqlen(const char *s1, const char *s2);
