/*
 * This is part of astreamfs and derivative work
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**************************************************************************
 * This is the trace debug for astreamfs_reader
 */

#ifdef _DEBUG

#define AW_DEBUG(...) (aw_ops.log(LOG_DEBUG, __VA_ARGS__))

	/* Note: AW_ASSERT is an assert-like mechanism. assert.h was not
	 * suitable since spitting out errors on stderr does not work once
	 * the driver is daemonised. Assertions are active with DEBUG mode
	 * and yield no code without DEBUG */
#define AW_ASSERT(expr) {if (!(expr)) aw_assert_fail(#expr,__FILE__,__LINE__);}

#define AW_ERROR(err) (aw_dbg.error((err), (int)__LINE__))
/* AW_OPS is for aw_init*/
#define AW_OPS aw_dbg

				 /*0                 1                   2*/
				 /*1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0*/
static const char *indent_async = ".....................................**";
static _Thread_local const char *indent =
				  "+++++++++++++++++++++++++++++++++++++**";
static _Thread_local unsigned int call_level = 0;


#define LVL (call_level * 2 + 1)  /* The indent value: starts at 1 and step 2 */


#define TIDX  (indent == indent_async ? '0' + (tidx / 10) : '*'), \
	      (indent == indent_async ? '0' + (tidx % 10) : '*')

/** callback functions trace **/
static struct aw_operations aw_dbg =
{
	.aligned_alloc	= aw_aligned_alloc,
	.free		= aw_free,
	.release	= aw_release,
	.log		= NULL,		/* Not used */
	.error		= def_error,
	.anticipation	= aw_default_anticipation,
	.retry		= def_retry,
	.close_if_idle	= def_close_if_idle,
	.store_last_opt = def_store_last_opt,
	.load_last_opt	= def_load_last_opt,
	.fh_from_fi	= def_fh_from_fi,
	.start_stream	= NULL,
	.read		= NULL,
};
static struct aw_operations aw_ops;

/*
 * @brief assertion-like function
 *
 * Code should use the macro AW_ASSERT(expression) which would call this
 * function when DEBUG is on.
 * 
 * @param the expression that failed as a string
 * @param the source file
 * @param the line it failed
 * @return none - exit of the program
 */
static void aw_assert_fail(const char *expr, const char *src, size_t line)
{
	/* Explicitly log with error level in case debug code is on but
	 * debug level is not 7 (DEBUG) */
	aw_ops.log(LOG_ERR,
		   "[%c%c]%.*s !! Assertion failed, file %s, line %lu, expression: %s\n",
		   TIDX, LVL, indent, src, line, expr);
	AW_ERROR(1);
}

static void *dbg_aligned_alloc(size_t alignment, size_t size)
{
	void *res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.aligned_alloc(%lu,%lu)\n",
		TIDX, LVL, indent,
		(unsigned long)alignment, (unsigned long)size);
	res = aw_dbg.aligned_alloc(alignment, size);
	AW_DEBUG("[%c%c]%.*s << aw_ops.aligned_alloc(%lu,%lu) = %p\n",
		TIDX, LVL, indent,
		(unsigned long)alignment, (unsigned long)size, res);
	return res;
}

static void dbg_free(void *ptr)
{
	AW_DEBUG("[%c%c]%.*s >> aw_ops.free(%p)\n", TIDX, LVL, indent, ptr);
	aw_dbg.free(ptr);
	AW_DEBUG("[%c%c]%.*s << aw_ops.free(%p)\n", TIDX, LVL, indent, ptr);
}

static void dbg_release(void *ptr)
{
	AW_DEBUG("[%c%c]%.*s >> aw_ops.release(%p)\n", TIDX, LVL, indent, ptr);
	aw_dbg.release(ptr);
	AW_DEBUG("[%c%c]%.*s << aw_ops.release(%p)\n", TIDX, LVL, indent, ptr);
}

static int dbg_error(int err, int line)
{
	int res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.error(%d,%d)\n",
		 TIDX, LVL, indent, err, line);
	res = aw_dbg.error(err, line);
	AW_DEBUG("[%c%c]%.*s << aw_ops.aligned_alloc(%lu,%lu) = %p\n",
		TIDX, LVL, indent, err, line, res);
	return res;
}

static size_t dbg_anticipation(uint64_t fh, size_t anticipation_clue,
			       size_t size, off_t offset)
{
	size_t res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.anticipation(%"PRIu64",%lu,%lu,%"PRId64")\n",
		TIDX, LVL, indent, fh, (unsigned long)anticipation_clue,
		(unsigned long)size, (int64_t)offset);
	res = aw_dbg.anticipation(fh, anticipation_clue, size, offset);
	AW_DEBUG("[%c%c]%.*s << aw_ops.anticipation(%"PRIu64",%lu,%lu,%"PRId64") = %lu\n",
		TIDX, LVL, indent, fh, (unsigned long)anticipation_clue,
		(unsigned long)size, (int64_t)offset, (unsigned long)res);
	return res;
}

static unsigned int dbg_retry(uint64_t fh, off_t offset)
{
	unsigned int res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.retry(%"PRIu64",%"PRId64")\n",
		TIDX, LVL, indent, fh, (int64_t)offset);
	res = aw_dbg.retry(fh, offset);
	AW_DEBUG("[%c%c]%.*s << aw_ops.retry(%"PRIu64",%"PRId64") = %u\n",
		TIDX, LVL, indent, fh, (int64_t)offset, res);
	return res;
}

static bool dbg_close_if_idle(uint64_t fh,void *sh, time_t last_read, time_t now
				       , bool force)
{
	bool res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.close_if_idle(%"PRIu64",%p,%ld,%ld,%d)\n",
		TIDX, LVL, indent,
		fh, sh, (long)last_read, (long)now, (int)force);
	res = aw_dbg.close_if_idle(fh, sh, last_read, now, force);
	AW_DEBUG("[%c%c]%.*s << aw_ops.close_if_idle(%"PRIu64",%p,%ld,%ld,%d) = %d\n",
		TIDX, LVL, indent,
		fh, sh, (long)last_read, (long)now, (int)force, (int)res);
	return res;
}

static void dbg_store_last_opt(struct fuse_file_info *fi, uint32_t opt)
{
	AW_DEBUG("[%c%c]%.*s >> aw_ops.store_last_opt(%p,0x%.8"PRIx32")\n",
		 TIDX, LVL, indent, fi, opt);
	aw_dbg.store_last_opt(fi, opt);
	AW_DEBUG("[%c%c]%.*s << aw_ops.store_last_opt(%p,0x%.8"PRIx32")\n",
		 TIDX, LVL, indent, fi, opt);
}

static uint32_t dbg_load_last_opt(struct fuse_file_info *fi)
{
	uint32_t res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.load_last_opt(%p)\n",
		 TIDX, LVL, indent, fi);
	res = aw_dbg.load_last_opt(fi);
	AW_DEBUG("[%c%c]%.*s << aw_ops.load_last_opt(%p) = %"PRIx32"\n",
		 TIDX, LVL, indent, fi, res);
	return res;
}

static uint64_t dbg_fh_from_fi(struct fuse_file_info *fi)
{
	uint64_t res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.fh_from_fi(%p)\n",
		 TIDX, LVL, indent, fi);
	res = aw_dbg.fh_from_fi(fi);
	AW_DEBUG("[%c%c]%.*s << aw_ops.fh_from_fi(%p) = %"PRIu64"\n",
		 TIDX, LVL, indent, fi, res);
	return res;
}

static int dbg_start_stream(const char *path, struct fuse_file_info *fi,
			    size_t size, off_t start_offset, void **sh,
			    int *fd)
{
	int res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.start_stream(%s,%p,%lu,%"PRId64",%p,%p)\n",
		TIDX, LVL, indent,
		path, fi, (unsigned long)size, (int64_t)start_offset, sh, fd);
	res = aw_dbg.start_stream(path, fi, size, start_offset, sh, fd);
	AW_DEBUG("[%c%c]%.*s << aw_ops.start_stream(%s,%p,%lu,%"PRId64",%p,%d) = %d\n",
		TIDX, LVL, indent,
		path, fi, (unsigned long)size, (int64_t)start_offset,
		((0 == res) ? *sh : NULL), ((0 == res) ? *fd : 0), res);
	return res;
}

static ssize_t dbg_read(void *sh, void *buf, size_t count)
{
	ssize_t res;
	AW_DEBUG("[%c%c]%.*s >> aw_ops.read(%p,%p,%lu)\n",
		TIDX, LVL, indent, sh, buf, (unsigned long)count);
	res = aw_dbg.read(sh, buf, count);
	AW_DEBUG("[%c%c]%.*s << aw_ops.read(%p,%p,%lu) = %ld\n",
		TIDX, LVL, indent, sh, buf, (unsigned long)count,
		(long)res);
	return res;
}

static struct aw_operations aw_ops =
{
	.aligned_alloc	= dbg_aligned_alloc,
	.free		= dbg_free,
	.release	= dbg_release,
	.log		= def_log,
	.error		= dbg_error,
	.anticipation	= dbg_anticipation,
	.retry		= dbg_retry,
	.close_if_idle	= dbg_close_if_idle,
	.store_last_opt	= dbg_store_last_opt,
	.load_last_opt	= dbg_load_last_opt,
	.fh_from_fi	= dbg_fh_from_fi,
	.start_stream	= dbg_start_stream,
	.read		= dbg_read,
};


/** async worker trace **/
#define STATIC static

#define TRACE0(fct, typer, fmtr, castr) \
	static typer orig_##fct(void); \
	STATIC typer        fct(void)  \
	{ \
		typer res; \
		AW_DEBUG("[%c%c]%.*s >> "#fct"()\n", TIDX, LVL, indent);\
		call_level++; \
		res = orig_##fct(); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"() = "fmtr"\n", \
			TIDX, LVL, indent, (castr)res);\
		return res; \
	}


#define TRACEV0(fct) \
	static void orig_##fct(void); \
	STATIC void        fct(void)  \
	{ \
		AW_DEBUG("[%c%c]%.*s >> "#fct"()\n", TIDX, LVL, indent);\
		call_level++; \
		orig_##fct(); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"()\n", TIDX, LVL, indent);\
	}

#define TRACE1(fct, type1, fmt1, cast1, typer, fmtr, castr) \
	static typer orig_##fct(type1 arg1); \
	STATIC typer        fct(type1 arg1)  \
	{ \
		typer res; \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1")\n", \
			TIDX, LVL, indent, (cast1)arg1);\
		call_level++; \
		res = orig_##fct(arg1); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1") = "fmtr"\n", \
			TIDX, LVL, indent, (cast1)arg1, \
			(castr)res);\
		return res; \
	}

#define TRACEV1(fct, type1, fmt1, cast1) \
	static void orig_##fct(type1 arg1); \
	STATIC void        fct(type1 arg1)  \
	{ \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1")\n", \
			TIDX, LVL, indent, (cast1)arg1);\
		call_level++; \
		orig_##fct(arg1); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1")\n", \
			TIDX, LVL, indent, (cast1)arg1);\
	}

#define TRACE2(fct, type1, fmt1, cast1 \
		  , type2, fmt2, cast2, typer, fmtr, castr) \
	static typer orig_##fct(type1 arg1, \
				type2 arg2);\
	static typer        fct(type1 arg1, \
				type2 arg2) \
	{ \
		typer res; \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
					  , (cast2)arg2);\
		call_level++; \
		res = orig_##fct(arg1, arg2); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2") = "fmtr"\n", \
			TIDX, LVL, indent, (cast1)arg1 \
					  , (cast2)arg2,\
			(castr)res);\
		return res; \
	}

#define TRACEV2(fct, type1, fmt1, cast1  \
		   , type2, fmt2, cast2) \
	static void orig_##fct(type1 arg1, \
			       type2 arg2);\
	STATIC void        fct(type1 arg1, \
			       type2 arg2) \
	{ \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
					  , (cast2)arg2);\
		call_level++; \
		orig_##fct(arg1, arg2); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
					  , (cast2)arg2);\
	}

#define TRACE3(fct, type1, fmt1, cast1 \
		  , type2, fmt2, cast2 \
		  , type3, fmt3, cast3, typer, fmtr, castr) \
	static typer orig_##fct(type1 arg1, \
				type2 arg2, \
				type3 arg3);\
	STATIC typer        fct(type1 arg1, \
				type2 arg2, \
				type3 arg3) \
	{ \
		typer res; \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2","fmt3")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
					  , (cast2)arg2  \
					  , (cast3)arg3);\
		call_level++; \
		res = orig_##fct(arg1, arg2, arg3); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2","fmt3") = "fmtr"\n", \
			TIDX, LVL, indent, (cast1)arg1  \
					  , (cast2)arg2  \
					  , (cast3)arg3, \
			(castr)res);\
		return res; \
	}

#define TRACEV3(fct, type1, fmt1, cast1  \
		   , type2, fmt2, cast2  \
		   , type3, fmt3, cast3) \
	static void orig_##fct(type1 arg1, \
			       type2 arg2, \
			       type3 arg3);\
	STATIC void        fct(type1 arg1, \
			       type2 arg2, \
			       type3 arg3) \
	{ \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2","fmt3")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3);\
		call_level++; \
		orig_##fct(arg1, arg2, arg3); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2","fmt3")\n",\
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3);\
	}

#define TRACE4(fct, type1, fmt1, cast1 \
		  , type2, fmt2, cast2 \
		  , type3, fmt3, cast3 \
		  , type4, fmt4, cast4, typer, fmtr, castr) \
	static typer orig_##fct(type1 arg1, \
				type2 arg2, \
				type3 arg3, \
				type4 arg4);\
	STATIC typer        fct(type1 arg1, \
				type2 arg2, \
				type3 arg3, \
				type4 arg4) \
	{ \
		typer res; \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2","fmt3","fmt4")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4);\
		call_level++; \
		res = orig_##fct(arg1, arg2, arg3, arg4); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2","fmt3","fmt4") = "fmtr"\n",\
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4  \
				   , (castr)res); \
		return res; \
	}

#define TRACEV4(fct, type1, fmt1, cast1  \
		   , type2, fmt2, cast2  \
		   , type3, fmt3, cast3  \
		   , type4, fmt4, cast4) \
	static void orig_##fct(type1 arg1, \
			       type2 arg2, \
			       type3 arg3, \
			       type4 arg4);\
	static void        fct(type1 arg1, \
			       type2 arg2, \
			       type3 arg3, \
			       type4 arg4) \
	{ \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2","fmt3","fmt4")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4);\
		call_level++; \
		orig_##fct(arg1, arg2, arg3, arg4); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2","fmt3","fmt4")\n",\
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4);\
	}

#define TRACE5(fct, type1, fmt1, cast1 \
		  , type2, fmt2, cast2 \
		  , type3, fmt3, cast3 \
		  , type4, fmt4, cast4 \
		  , type5, fmt5, cast5, typer, fmtr, castr) \
	static typer orig_##fct(type1 arg1, \
				type2 arg2, \
				type3 arg3, \
				type4 arg4, \
				type5 arg5);\
	STATIC typer        fct(type1 arg1, \
				type2 arg2, \
				type3 arg3, \
				type4 arg4, \
				type5 arg5) \
	{ \
		typer res; \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2","fmt3","fmt4","fmt5")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4  \
				   , (cast5)arg5);\
		call_level++; \
		res = orig_##fct(arg1, arg2, arg3, arg4, arg5); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2","fmt3","fmt4","fmt5") = " \
			fmtr"\n",\
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4  \
				   , (cast5)arg5  \
				   , (castr)res);\
		return res; \
	}


#define TRACEV5(fct, type1, fmt1, cast1  \
		   , type2, fmt2, cast2  \
		   , type3, fmt3, cast3  \
		   , type4, fmt4, cast4  \
		   , type5, fmt5, cast5) \
	static void orig_##fct(type1 arg1, \
			       type2 arg2, \
			       type3 arg3, \
			       type4 arg4, \
			       type5 arg5);\
	static void        fct(type1 arg1, \
			       type2 arg2, \
			       type3 arg3, \
			       type4 arg4, \
			       type5 arg5) \
	{ \
		AW_DEBUG("[%c%c]%.*s >> "#fct"("fmt1","fmt2","fmt3","fmt4","fmt5")\n", \
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4  \
				   , (cast5)arg5);\
		call_level++; \
		orig_##fct(arg1, arg2, arg3, arg4, arg5); \
		call_level--; \
		AW_DEBUG("[%c%c]%.*s << "#fct"("fmt1","fmt2","fmt3","fmt4","fmt5")\n",\
			TIDX, LVL, indent, (cast1)arg1  \
				   , (cast2)arg2  \
				   , (cast3)arg3  \
				   , (cast4)arg4  \
				   , (cast5)arg5);\
	}

/*  */
TRACEV2(post_action, struct action *, "%p", struct action *
		   , tid_t	    , "%u", unsigned int)
#define post_action orig_post_action

/*  */
TRACEV1(finish_action, const struct action *, "%p" , struct action *)
#define finish_action orig_finish_action


/*  */
TRACEV1(wait_for_actions_or_reads,
			struct private_data *, "%p", struct private_data *)
#define wait_for_actions_or_reads orig_wait_for_actions_or_reads

/*  */
TRACE2(get_state, const struct private_data * , "%p" , struct private_data *
		, sid_t			, "%u" , unsigned int
		, enum stream_state	, "%d" , int)
#define get_state orig_get_state

/*  */
TRACEV5(set_fd, struct private_data *, "%p" , struct private_data *
	      , sid_t		     , "%u" , unsigned int
	      , enum stream_state    , "%d" , int
	      , enum stream_state    , "%d" , int
	      , int		     , "%d" , int)
#define set_fd orig_set_fd

/*  */
TRACEV3(set_st_state, struct private_data *, "%p" , struct private_data *
		    , sid_t		   , "%u" , unsigned int
		    , enum stream_state    , "%d" , int)
#define set_st_state orig_set_st_state

/*  */
TRACEV2(free_gap, struct action *, "%p" , struct action *,
		  bool           , "%d" , int)
#define free_gap orig_free_gap

/*  */
TRACEV1(free_all_gaps, sid_t , "%u" , unsigned int)
#define free_all_gaps orig_free_all_gaps

/*  */
TRACE3(is_idle, struct private_data *, "%p" , struct private_data *
	      , sid_t		     , "%u" , unsigned int
	      , bool    	     , "%d" , int
	      , bool		     , "%d" , int)
#define is_idle orig_is_idle

/*  */
TRACEV3(do_free, uint64_t     , "%"PRIu64, uint64_t
	       , uint64_t     , "%"PRIu64, uint64_t
	       , tid_t	      , "%u"     , unsigned int)
#define do_free orig_do_free


/*  */
TRACEV2(detach_stream, struct private_data *, "%p"     , struct private_data *
		     , sid_t, "%u" , unsigned int)
#define detach_stream orig_detach_stream

/*  */
TRACEV2(release_stream, struct private_data *, "%p", struct private_data *
		      , sid_t		     , "%u", unsigned int)
#define release_stream orig_release_stream

/*  */
TRACE2(op_aligned_alloc, size_t, "%ld" , long
		       , size_t, "%ld" , long
		       , void *, "%p"  , void *)
#define op_aligned_alloc orig_op_aligned_alloc

/*  */
TRACE2(gap_to_read, struct action **, "%p" , struct action **
		  , struct action * , "%p" , struct action *
		  , struct action * , "%p" , struct action *)
#define gap_to_read orig_gap_to_read

/*  */
TRACE3(read_fits_stream, struct private_data *, "%p" , struct private_data *
		       , struct action *      , "%p" , struct action *
		       , sid_t		      , "%u" , unsigned int
		       , bool		      , "%d" , int)
#define read_fits_stream orig_read_fits_stream

/*  */
TRACE3(can_close, struct private_data * , "%p" , struct private_data *
		, sid_t			, "%u" , unsigned int
		, sid_t *		, "%p" , unsigned int *
		, bool			, "%d" , int)
#define can_close orig_can_close

/*  */
TRACE1(close_an_owned_stream, struct private_data *, "%p", struct private_data *
			    , sid_t		   , "%u", unsigned int)
#define close_an_owned_stream orig_close_an_owned_stream

/*  */
TRACE2(close_fh_stream, struct private_data *, "%p" , struct private_data *
		       , sid_t		      , "%u" , unsigned int
		       , sid_t		      , "%u", unsigned int)
#define close_fh_stream orig_close_fh_stream

/*  */
TRACE2(get_new_stream, struct private_data *, "%p"     , struct private_data *
		     , uint64_t		    , "%"PRIu64, uint64_t
		     , sid_t		    , "%u"     , unsigned int)
#define get_new_stream orig_get_new_stream

/*  */
TRACE2(find_fh, uint64_t, "%"PRIu64, uint64_t
	      , sid_t * , "%p"     , sid_t *
	      , tid_t   , "%u"     , unsigned int)
#define find_fh orig_find_fh

/*  */
TRACE2(find_owned_fh, const struct private_data *, "%p", struct private_data *
		    , uint64_t			 , "%"PRIu64, uint64_t
		    , sid_t			 , "%u"     , unsigned int)
#define find_owned_fh orig_find_owned_fh

/*  */
TRACEV1(busy_do_solo, struct action *, "%p" , struct action *)
#define busy_do_solo orig_busy_do_solo

/*  */
TRACE1(max_streams_allowed, const struct private_data *, "%p"
			  , struct private_data *
			  , sid_t, "%u", unsigned int)
#define max_streams_allowed orig_max_streams_allowed

/*  */
TRACEV1(reclaim_streams, struct private_data *, "%p" , struct private_data *)
#define reclaim_streams orig_reclaim_streams

/*  */
TRACE1(sibling_anticipation,struct action *      , "%p" , struct action *
			   ,size_t		 , "%lu", unsigned long)
#define sibling_anticipation orig_sibling_anticipation

/*  */
TRACE1(load_balance_threads , uint64_t, "%"PRIu64 , uint64_t
			    , tid_t   , "%u" 	  , unsigned int)
#define load_balance_threads orig_load_balance_threads

/*  */
TRACE3(send_read, struct action *, "%p" , struct action *
		, tid_t		 , "%u" , unsigned int
		, fid_t		 , "%u" , unsigned int
		, bool		 , "%d" , int)
#define send_read orig_send_read

/*  */
TRACE1(associate_read_to_thread , struct action *, "%p" , struct action *
				,bool		 , "%d" , int)
#define associate_read_to_thread orig_associate_read_to_thread

/*  */
TRACEV2(associate_read_to_stream
		, struct private_data *, "%p" , struct private_data *
		, struct action *, "%p" , struct action *)
#define associate_read_to_stream orig_associate_read_to_stream

/*  */
TRACEV3(flush_stream, struct private_data *, "%p" , struct private_data *
		    , sid_t		   , "%u" , unsigned int
		    ,          int	   , "%d" , int)
#define flush_stream orig_flush_stream

/*  */
TRACEV4(retry_or_flush, struct private_data *, "%p" , struct private_data *
		      , struct action *      , "%p" , struct action *
		      , sid_t		     , "%u" , unsigned int
		      ,          int	     , "%d" , int)
#define retry_or_flush orig_retry_or_flush

/*  */
TRACEV2(start_reading_stream, struct private_data *, "%p", struct private_data *
			    , const struct action *, "%p", struct action *)
#define start_reading_stream orig_start_reading_stream

/*  */
TRACEV1(clean_old_streams, struct private_data *, "%p" , struct private_data *)
#define clean_old_streams orig_clean_old_streams

/*  */
TRACE2(do_close, struct private_data *	, "%p", struct private_data *
		, struct action *	, "%p", struct action *
		, bool			, "%u", unsigned int)
#define do_close orig_do_close

/*  */
TRACEV1(do_destroy, struct private_data *	, "%p", struct private_data *)
#define do_destroy orig_do_destroy

/*  */
TRACE1(do_actions, struct private_data *, "%p" , struct private_data *
		 , bool			, "%d" , int)
#define do_actions orig_do_actions

/*  */
TRACEV2(read_stream, struct private_data *, "%p" , struct private_data *
		   , sid_t		  , "%u" , unsigned int)
#define read_stream orig_read_stream

/*  */
TRACEV1(do_transfers, struct private_data *, "%p" , struct private_data *)
#define do_transfers orig_do_transfers

/* This one is special because when debugging it initialises once the tid */
static void *orig_async_worker(void * arg);
static void *     async_worker(void * arg)
{
	void *res;
	indent = indent_async;
	AW_DEBUG("[%.2u]%.*s >> async_worker(%p=%d)\n",
		 (unsigned int)((struct start_data *)arg)->tid, LVL, indent,arg,
		 (unsigned int)((struct start_data *)arg)->tid);
	call_level++;
	res = orig_async_worker(arg);
	call_level--;
	AW_DEBUG("[%c%c]%.*s << async_worker(%p) = %p\n",
		 TIDX, LVL, indent, arg, res);
	return res;
}
#define async_worker orig_async_worker

/*  */
TRACE5(solo_read, const char *           , "%s"      , char *
		, struct fuse_bufvec *   , "%p"      , struct fuse_bufvec *
		, size_t                 , "%lu"     , unsigned long
		, off_t                  , "%"PRId64 , int64_t
		, struct fuse_file_info *, "%p"      , struct fuse_file_info *
		, int                    , "%d"      , int)
#define solo_read orig_solo_read

/*  */
TRACEV3(decode_opt, uint32_t, "%lx" , unsigned long
	          , tid_t * , "%p"  , tid_t *
	          , sid_t * , "%p"  , sid_t *)
#define decode_opt orig_decode_opt

/*  */
TRACE2(encode_opt, tid_t   , "%u"  , unsigned int
	         , sid_t   , "%u"  , unsigned int
	         , uint32_t, "%lu" , unsigned long)
#define encode_opt orig_encode_opt

/*  */
TRACE5(common_read, const char *           , "%s"      , char *
		  , struct fuse_bufvec *   , "%p"      , struct fuse_bufvec *
		  , size_t                 , "%lu"     , unsigned long
		  , off_t                  , "%"PRId64 , int64_t
		  , struct fuse_file_info *, "%p"      , struct fuse_file_info *
		  , int                    , "%d"      , int)
#define common_read orig_common_read



/** Library functions trace **/
#undef  STATIC
#define STATIC

/* This one is special because it cannot log at start:
 * 	aw_ops.log is set to default that does not log! */
static int orig_aw_init(struct aw_operations const *a,
			unsigned int min_thrds, unsigned int max_thrds);
int aw_init(struct aw_operations const *a,
	    unsigned int min_thrds, unsigned int max_thrds)
{
	int res;
	if (NULL != a->log)
		a->log(LOG_DEBUG, "[%c%c]%.*s >> aw_init(%p,%u,%u)\n"
				, TIDX, LVL, indent, a, min_thrds, max_thrds);
	res = orig_aw_init(a, min_thrds, max_thrds);
	AW_DEBUG("[%c%c]%.*s << aw_init(%p,%u,%u) = %d\n",
		 TIDX, LVL, indent, a, min_thrds, max_thrds, res);
	return res;
}
#define aw_init orig_aw_init

/*  */
TRACEV0(aw_destroy)
#define aw_destroy orig_aw_destroy

/*  */
TRACEV1(aw_get_info, struct aw_info *, "%p", struct aw_info *)
#define aw_get_info orig_aw_get_info

/*  */
TRACEV1(aw_close, uint64_t, "%"PRIu64, uint64_t)
#define aw_close orig_aw_close

/*  */
TRACE5(aw_read, const char *           , "%s"      , char *
	      , char *                 , "%p"      , char *
	      , size_t                 , "%lu"     , unsigned long
	      , off_t                  , "%"PRId64 , int64_t
	      , struct fuse_file_info *, "%p"      , struct fuse_file_info *
	      , int                    , "%d"      , int)
#define aw_read orig_aw_read

/*  */
TRACE5(aw_read_buf, const char *           , "%s"      , char *
		  , struct fuse_bufvec **  , "%p"      , struct fuse_bufvec **
		  , size_t                 , "%lu"     , unsigned long
		  , off_t                  , "%"PRId64 , int64_t
		  , struct fuse_file_info *, "%p"      , struct fuse_file_info *
		  , int                    , "%d"      , int)
#define aw_read_buf orig_aw_read_buf


#ifdef _STATS
struct aw_stat_times;

/*  */
TRACEV2(add_stats_tm, struct aw_stat_times * , "%p", struct aw_stat_times *
		    , const struct timespec *, "%p", struct timespec *)
#define add_stats_tm orig_add_stats_tm

/*  */
TRACEV3(diff_tm, struct timespec *, "%p" , struct timespec *
	       , const struct timespec *, "%p" , struct timespec *
	       , const struct timespec *, "%p" , struct timespec *)
#define diff_tm orig_diff_tm

/*  */
TRACEV5(speed_add, struct spd *	     , "%p"  , struct spd *
		 , unsigned long long, "%llu", unsigned long long
		 , unsigned long long, "%llu", unsigned long long
		 , unsigned long long, "%llu", unsigned long long
		 , ssize_t           , "%ld" , long)
#define speed_add orig_speed_add

/*  */
TRACEV2(sum_stats, struct aw_stats * , "%p"  , struct aw_stats *
		 , tid_t	     , "%u"  , unsigned int)
#define sum_stats orig_sum_stats

/*  */
TRACEV3(compute_speeds, struct aw_stats *, "%p"  , struct aw_stats *
		      , struct spd *	 , "%p"  , struct spd *
		      , tick_t		 , "%llu", unsigned long long)
#define compute_speeds orig_compute_speeds

/*  */
TRACEV2(real_move_tm, struct timespec *, "%p" , struct timespec *
		    , const struct timespec *, "%p" , struct timespec *)
#define real_move_tm orig_real_move_tm

/*  */
TRACEV2(real_raw_copy, const struct private_data *, "%p", struct private_data *
		     , struct action *, "%p", struct action *)
#define real_raw_copy orig_real_raw_copy

/*  */
TRACEV1(real_count_retries, sid_t, "%u", unsigned int)
#define real_count_retries orig_real_count_retries

/*  */
TRACEV2(real_count_streams, bool , "%d", int
			  , sid_t, "%u", unsigned int)
#define real_count_streams orig_real_count_streams

/*  */
TRACEV2(real_count_over , ssize_t, "%ld", long
			, sid_t  , "%u" , unsigned int)
#define real_count_over orig_real_count_over

/*  */
TRACEV1(real_count_files, bool, "%d", int)
#define real_count_files orig_real_count_files

/*  */
TRACEV1(real_update_start_time, const struct action *, "%p", struct action *)
#define real_update_start_time orig_real_update_start_time

/*  */
TRACEV1(real_get_time, struct timespec *, "%p", struct timespec *)
#define real_get_time orig_real_get_time

/*  */
TRACEV1(real_set_time, struct private_data *, "%p", struct private_data *)
#define real_set_time orig_real_set_time


/*  */
TRACEV3(real_update_read_time, struct timespec *, "%p"      , struct timespec *
			     , struct timespec *, "%p"      , struct timespec *
			     , const struct action *  , "%p", struct action *)
#define real_update_read_time orig_real_update_read_time

/*  */
TRACE1(real_get_slot, uint64_t, "%"PRIu64 , uint64_t
		    , fid_t * ,	"%p"	  , fid_t *)
#define real_get_slot orig_real_get_slot

/*  */
TRACE0(real_release_slot, fid_t *, "%p", fid_t *)
#define real_release_slot orig_real_release_slot

/*  */
TRACEV2(real_clean_old, uint64_t , "%"PRIu64, uint64_t
		      , fid_t    , "%u"     , unsigned int)
#define real_clean_old orig_real_clean_old

/*  */
TRACEV0(real_destroy)
#define real_destroy orig_real_destroy

/*  */
TRACE0(aw_init_stats, int, "%d", int)
#define aw_init_stats orig_aw_init_stats

/*  */
TRACEV3(aw_get_stats, struct aw_stats *     , "%p", struct aw_stats *
		    , struct aw_stats *     , "%p", struct aw_stats *
		    , struct aw_solo_stats *, "%p", struct aw_solo_stats *)
#define aw_get_stats orig_aw_get_stats

#endif /* _STATS */

#else /* _DEBUG */
	#define AW_DEBUG(...)
	#define AW_ASSERT(expr)
	#define AW_ERROR(err) (aw_ops.error((err), (int)__LINE__))
	/* AW_OPS is for aw_init*/
	#define AW_OPS aw_ops

	#define do_free_exception(map_last)

	static struct aw_operations aw_ops =
	{
#ifdef _MEMCOUNT
	.aligned_alloc	= aw_aligned_alloc,
	.free		= aw_free,
#else
	.aligned_alloc	= aligned_alloc,
	.free		= free,
#endif
	.release	= aw_release,
	.log		= def_log,
	.error		= def_error,
	.anticipation	= aw_default_anticipation,
	.retry		= def_retry,
	.close_if_idle	= def_close_if_idle,
	.store_last_opt = def_store_last_opt,
	.load_last_opt	= def_load_last_opt,
	.fh_from_fi	= def_fh_from_fi,
	.start_stream	= NULL,
	.read		= NULL
	};
#endif /* _DEBUG */
