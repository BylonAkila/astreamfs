.\" This is 1fichierfs manpage
.TH 1FICHIERFS: "1" "January 2025" "1fichierfs: version 1.9.6" "User Commands"
.SH NAME
1fichierfs: \- mount a 1fichier account
.SH SYNOPSIS
.B 1fichierfs
[\fI\,--api-key='APIKey'\/\fR] [\fI\,options\/\fR] \fI\,mountpoint\/\fR
.SH DESCRIPTION
Mount a 1fichier cloud storage account to a local directory.
.PP
The tree under the \fI\,mountpoint\/\fR will be the same as it is on the
page
.I `My files`
of the remote cloud storage at 1fichier.com.
In order to be able to use the API a subscription to the service
is necessary (as of February 2023).
.PP
.B 1fichierfs
inherits from 
.B astreamfs
it's stream optimization. Because a lot of data
access involve sequential read,
.B 1fichierfs
will try its best to limit the number of requests to the server by
asynchonously streaming on an already opened connection.
This is very important when using https (default at 1fichier.com)
to avoid latency and TLS handshakes delays. This essential feature makes
.B 1fichierfs
especially well suited for streaming media, or for sequential access mode
such as restoring a backup copy from the cloud storage.
.PP
Conversely, a random read access will yield poor performance. Since
random access would mean doing a request per access, adding latency
and TLS handshakes would in any case give mediocre thoughput. Thus,
it is recommended to download the file locally if the type of access
is known to be random. Downloading the file implies sequential read:
streaming will help here.
.PP
\fB1fichierfs\fR handles shares: sent and received (including read-only and
hidden links).

.SS Character translation and non-atomic operations
\fB1fichier.com\fR forbids some characters in file and directory names.
Those characters are the usual \fB/\fR and \fBbinary zero\fR (always forbidden)
and also \fB$ < > \\ `\fR. The two characters \fB' "\fR are also forbidden for
directory names. By default, \fB1fichierfs\fR will translate those characters to
alternate UTF-8 "fullwidth" forms of the same characters, when necessary.
To avoid confusion, these alternate characters will be forbidden in names
(error: illegal sequence). \fB1fichier.com\fR also removes leading or trailing
spaces (or equivalent) in names. In that case \fB1fichierfs\fR will
prefix or suffix the filename with an "invisible" UTF-8 sequence to make
those names possible. The inconvenient of these substitutions is that it
reduces the available filename length, which otherwise is 250. The translation
is done transparently both way, so that the client software can make use of the
seven "forbidden" characters or space wherever in names, which is expected
of a regular filesystems (e.g. ext4).

The substitution characters have been chosen to be the same as rclone, for the
sake of software compatibility, and also because when looking at the webpage
"My files" on \fB1fichier.com\fR, they are visually close to the character
they replace.

The option \fB\-\-raw\-names\fR is available to avoid the translation. With
this option the presence of one of the seven forbidden characters, or of
leading or trailing spaces in file or directory names will return an error
(illegal sequence). The advantage is that it does not limit name length nor
forbid the alternate sequences.
Note that if this option is used and files or directories are created, renamed,
linked with one of the alternate characters, then \fB1fichierfs\fR is
started again without the option, the translation will be shown instead of
the alternate characters. If that could confuse client software, avoid mixed
usage: with an without the option.

\fBNon-atomicity\fR : renaming a file towards an existing file is not an
atomic operation because there is no corresponding atomic API.
The source file is renamed first, the target directory then has two files
with the same name (which is possible on 1fichier.com), then the target is
removed. In the event a power outage or crash occurs between those two
operations, \fB1fichierfs\fR will de-duplicate and will show only one of the
files having the same name: the most recent one. This de-duplication choice
allows to support utilities like rsync which modify with a rename (by default),
even in the case of power outage at the critical moment. Indeed, the new
modified file will have the date where it was created on the 1fichier.com
account, and will therefore be more recent than the one it was updating.

A log entry (Notice level) is created for each file shadowed by a directory
or file having the same name.

Renaming or deleting a file or directory that shadowed a file will make this
latter appear, this is why reparing duplicated files is more reliable from
the web interface of the 1fichier.com account.

.SS Writing
\fB1fichierfs\fR allows writing files on the remote storage (aka uploading).
It is limited to sequential write of new files (with some leeway to accomodate
write patterns like the one of encfs).
.PP
Overwriting, appending or random write (beyond the leeway) is not supported.
.PP
Because of how the "upload process" is implemented on 1fichier.com, there
are some limitations:
.PD 0.1v
.RS
.PP
- As long as the "upload process" is not over, files are visible through
\fB1fichierfs\fR (not on the web page) but are not accessible for reading.
.PP
- Once the transfer is completed the file stays on the FTP server for 5 minutes
(this server side limitation might be lifted in the future).
.PP
- At that point, the transfer from the FTP server to the storage starts. It
takes some time depending mostly on the size of the file.
.PP
- Once this stage is completed, the file becomes visible in the upload
directory.
.PP
- There \fB1fichierfs\fR will move and rename the file where it is
expected. From that point the file becomes visible (web), but cannot be read
immediately (it can be deleted or renamed/moved though).
.PP
- In the last step, the server does some work on the file (possibly
verify the checksum). In this phase an attempt to read will return \'File
unavailable or corrupted\' and the read request will be responded
with EBUSY: \'Device or resource busy\'.
.RE
.PD
.PP
\Exception:\fR "small" files (up to 4096 bytes) remain readable during the whole
process. For example, this facilitates moving files to Trash with Nautilus
or Nemo.
.PP
\fBParallel operations:\fR same as kernel filesystems, it is possible to
deleted, move or rename a file in the process of writing \fBat any stage\fR.
If the file is deleted in the writing phase, writing will continue (although
much faster because write back to FTP is not needed anymore), unless
the \fB-o hard_remove\fR is used, in which case fuse will respond with ENOENT
(file not found) at the next write, without calling \fB1fichierfs\fR.
.PP
\fBNote:\fR apart from the first 5 minutes wait, the other steps are exactly
the same with other upload methods: http or remote upload.
.PP
\fBWriting environment:\fR to write on the remote storage, \fB1fichierfs\fR
needs two elements: a directory for uploads (fixed name:
\fB/.upload.1fichierfs\fR) and a FTP sub-account, either computed by default or
specified by the option \fI\,\-\-ftp\-user\/\fR. \fB1fichierfs\fR does a
"lazy initialisation", which means it will initialise all it can in the
background when part of the elements are already present, but otherwise will
only create those two elements when writing is actually requested.
If there is no intent to write on the remote storage through \fB1fichierfs\fR
anymore, and if no write operation was performed since mount, removing
\fB/.upload.1fichierfs\fR will trigger \fB1fichierfs\fR to automatically
remove the (potential) FTP sub-account, and so "clean" the writing
environment.
.PP
Although empty files are not possible on \fB1fichier.com\fR, \fB1fichierfs\fR
supports their creation, for instance with the \fBtouch\fR command. Creating
such an empty file is also enough to trigger the writing environment
initialisation. This can be useful when running several instances of
\fB1fichierfs\fR to avoid messing up with the creation of the writing
environment by forcing it on one of the instances before mounting the others.
.PP

.SS Resuming
It is recommended to wait that all the steps above are over before unmounting
\fB1fichierfs\fR. Looking at the statistics can help determine when it is safe
to exit the program, see description below. Note: the last stage (checksum
control) can still fail, and the file remain "corrupted". This is why
1fichier.com advise in its online help to always check the stored file
(with a \fBdiff\fR for example) before deleting the local copy.
.PP
Nevertheless, if \fB1fichierfs\fR was unmounted prematurely, it will try
to resume all pending upload processes at the next start, thanks to
resume information written as soon as a transfer is over. If the file can
safely be renamed or deleted, the resume information is removed. When starting
\fB1fichierfs\fR looks in the background if some of the resume information
stayed and if so will perform the missing steps.
.PP
\fBImportant recommendation:\fR to avoid losing files and to make resuming
more efficient, it is advised to use FTP \fBautomatic\fR mode. Indeed, files
will stay on the FTP server only 24 hours. Hence, if the mode is \fBmanual\fR
and more than 24 hours elapsed without a FTP process trigger, all
pending files will be lost. Also, the resuming process does not trigger the FTP
process to avoid interfering with other possible parallel instances of
\fB1fichierfs\fR or web operation.
.PP
In some cases there can be a mismatch with files and resume information.
When \fB1fichierfs\fR is killed, crashed or in case of electrical powered down,
any ongoing upload will yield a file without resume information. Anyway those
files are incomplete and should be discarded. Resume information being written
when the transfer is over, the file is not yet visible on the upload directory,
which can temporarily lead to resume information with no file. Same can happen
when several instances of \fB1fichierfs\fR are running. A permanent resume
information with no file can happen in manual FTP mode (see recommendation
above). If the user deleted the target directory of an upload before resuming,
resuming will fail, and the files and resume information will stay in the
upload directory, triggering an error each time the program starts, until
the files and directories are manually deleted.
.PP
The \fB\-\-dry\-run\fR option will simulate a resume and log useful information
to take care of this kind of situation.
Use it with \fB\-\-log\-level 5\fR (or higher), all relevant messages being
warnings or notices. The user can then take relevant steps to recover some
data or clean the upload directory.
.PP
\fBRecommendation:\fR do NOT put files or folders of your own in the
upload directory, because it will slow down the upload and resume processes.
\fB1fichierfs\fR protects from that, but it is always possible from the
web pages.
.PP

.SS IPV6 and dual IP stack warning
The API itself can be freely used with any IP stack beacuse it
works with the API key and the IP restrictions can be set for both
IPV4 an IPV6 with relevant masks.
.PP
Nevertheless, reading files depends on the download engine of
1fichier.com. It is shared with the web version, and checks that
there is only one IP at the same time downloading for an account. As a
consequence, when using IPV6 it is not possible to read files from
several devices connected to a single home router, because unlike NATed IPV4,
the server will see two different IPV6 addresses and fail the second download.
.PP
When using dual stack without forcing the traffic either IPV4 or IPV6 with
the relevant option, it is not common but possible that some traffic occurs
on both stacks. If this happens when reading files, a failure will happen
on that file.
.PD 0.1v
.PP
To avoid those untimely errors when running a dual IP stack, it is
recommended to force  \fB1fichierfs\fR with either IPV6 or IPV4.
When using several devices connected to a single home router, IPV4 is the
right choice.
.PD
.PP

.SS Permissions
Unless overridden by options like \fBumask\fR, \fBnoexec\fR, etc...
files and directories permissions are inherited from the permissions,
UID and GID of the \fI\,mountpoint\/\fR.
In readonly mode, write flags are set to zero.
.PP
Some files and directories are protected. Those are: the statistic file
specified by the option \fB\-\-stat\-file\fR, the refresh file specified
by \fB\-\-refesh\-file\fR and also the upload directory
\fB.upload.1fichierfs\fR which is also write protected. Nevertheless, it is
possible to delete or move out of this directory files whose age is more
than 15 minutes (or the duration specified with \fB\-\-resume\-delay\fR)
so to be able to "clean" the directory in case of crash or sudden power off
during an upload.
.PP
\fBNote\fR that since this is a fuse mount, by default, it will be accessible
only to the user who mounted it, regardless of the permissions for group
and others.
.PD 0.1v
.PP
To make use of the mount for programs running with another user
(like Plex for example), use the standard fuse \fB\-o allow_other\fR option.
.PD
.PP
\fBReminder:\fR un-mounting is not done with `umount`,
but like any other fuse driver with
.RS
.PP
.B "$ fusermount \-u mountpoint"
.RE

.SS Logging and exit policy
For the log levels, see below (\fB\-\-log\-level\fR).
.PP
In the initialization phase, and when \fB1fichierfs\fR is running
foreground, it will exit on any condition that has a severity
level of \fBerror\fR or \fBcritical\fR.
.PP
Once \fB1fichierfs\fR goes to daemon, only \fBcritical\fR conditions
will trigger the exit.
.PP
This is why the root directory content of the remote storage is
fetched prior to daemonizing.
It catches potential errors like a wrong API Key or a temporary server outage.
Such errors will make the mount useless anyway.
.PP
.PD 0.1v
\fBEmerg\fR and \fBAlert\fR messages: not used by \fB1fichierfs\fR.
.PP
\fBCritical\fR: error on memory allocation, mutexes, semaphores,
curl intializing...
.PP
\fBError\fR: parameters inconsistency, server not in sync with the cache state,
error response from an API...
.PP
\fBWarning\fR: recoverable parameter inconsistency, file unexpectedly removed
from the server...
.PP
\fBNotice\fR: successful initialization, all write operations: transfer
completed, delete, rename, hard-link...
.PP
\fBInfo\fR: all API calls, important function calls.
.PP
\fBDebug\fR: the asynchronous engine traces all calls on entry and exit. Other
significant places of the algorithm also log a trace.
Debug messages are only present when the program is built with DEBUG=1.
To reduce memory footprint, packaged version might not contain those
messages and will warn if this level is used.
.PD

.SS Statistics
.PP
Here is how it looks like, with the detail of all fields.
.PP
.RS
Uptime: 09:33:31
.PP
.PD 0.1v
Readers:
     Files    Streams  Start streams             Fuse requests
.PP
    Cur/Tot.  Cur/Tot. AvgTim MaxTim Down. N.Req AvgTim MaxTim Speed Av.Sp Over.
.PP
02: 1     2   1     2 1.8107 3.1085  195M  1569 .02357 .10383 9110K 8858K  132K
.PP
└─(   1) 2/1  1     1 .51306 .51306  195M  1568 .02357 .10383 9110K 8908K  8192
.PP
03: 0     1   0     1 .47210 .47210 2345M 18763 .02368 .14188     0 8943K     0
.PP
-------------------------------------------------------------------------------
.PP
Tt  1     3   1     3 1.3645 3.1085 2540M 20332 .02367 .14188 9110K 8936K  132K
.PP
Solo                                 791K     3 .39741 2.4567       1.94M 
.PD
.PP
.PD 0.1v
Writers:
.PP
    Upload N.Req  Avg Time  Max Time   Ref     N.Err Speed Av.Sp
.PP
[01] 17.2G 19.1M    0.0000    2.9793 |   3|        0 30.7M 25.1M
.PP
[02] 5110M 5666K    0.0000    2.9723  idle         0     0 30.0M
.PP
[03] 1869M 2072K    0.0000    2.9357  idle         0     0 28.7M
.PP
----------------------------------------------------------------
.PP
Tot. 24.0G 26.6M    0.0000    2.9793               0 30.7M 29.9M
.PD
.PP
.PD 0.1v
API:
.PP
                        N.Req  Avg Time  Max Time Retry N.Err
.PP
folder/ls.cgi         :   417    0.1787    0.5126     0     0
.PP
download/get_token.cgi:    56    0.1551    0.1820     0     0
.PP
user/info.cgi         :    37    0.1644    0.4714     0     0
.PP
file/ls.cgi           :    79    0.1616    0.2076     0     0
.PP
file/mv.cgi           :    40    0.2043    0.3454     0     0
.PP
file/rm.cgi           :     4    0.2067    0.3141     0     0
.PP
folder/mv.cgi         :     2    0.1602    0.1858     0     0
.PP
folder/mkdir.cgi      :     3    0.1735    0.1888     0     0
.PP
ftp/users/rm.cgi      :     1    0.1657    0.1657     0     0
.PP
ftp/users/add.cgi     :     1    0.3343    0.3343     0     0
.PP
ftp/process.cgi       :    40    0.1567    0.1819     0    40
.PP
--------------------------------------------------------------
.PP
Total                 :   680    0.1745    0.5126     0    40
.PD
.PP
         Trig. Timer Poll. Write   API Error  Auto   Total
.PD 0.1v
.PP
Refresh:     0    27    23     2     7     0     0      59
.PD
.PP
             Number Memory
.PD 0.1v
.PP
Allocations:  9493K  1172M
.PP
Frees      :  9493K  1171M
.PP
Difference :    435  1081K
.PP
Contentions:     16      3
.PD
.PP
Live streams: |Write| 3    (Read) 2
.PD 0.1v
.PP
  Ref   Size  Left Lnk Er N.Loc N.Stm Path
.PP
|   1|    15 04:54   0       Wait_FTP /foo/bar/test.txt
.PP
|   2| 27.1K 00:30   0       Wait_mv  /foo/bar/debug.txt
.PP
|   3| 10.4G --:--   0       Writing  /bar/foo.iso
.PP
------
.PP
(   1)  866M 28:49   2        1     6 /Videos/big_buck_bunny_1080p_stereo.ogg
.PP
(   2)     1 29:41   1        1     1 /Test/B/C.TXT
.PD
.RE
.PP
Details:
.PD 0.1v
.PP
.RS
\fBUptime\fR: how long \fB1fichierfs\fR has been running.
.PP
\fBReaders\fR: statistics are shown for each individual thread, each file,
total and single requests (Solo). Apart from the total, lines are displayed
only when there contain data. If \fB1fichierfs\fR needs to start doing single
ranges for some read requests, the performance could drop dramatically, and it
is advisable to lower the number of parallel and random reads.
.RS
\fBFiles.:\fR for threads, number of files currently opened, and total number of
files opened so far. For files, it is replaced by the référence in the list
of open files, followed by a short counter of pending requests / buffers.
.PP
\fBStreams.:\fR number of streams currently used and total used so far.
.PP
\fBStart stream:\fR average and maximum time to start a stream.
.PP
\fBDown.:\fR number of bytes read.
.PP
\fBN.Req:\fR number of read requests completed.
.PP
\fBFuse requests:\fR average and maximum time of fuse requests.
.PP
\fBSpeed:\fR current read spead.
.PP
\fBAv.Sp:\fR average read spead. Note that this is not the average for the
uptime (which can easily be computed from the number of bytes read)
but it takes only into accounts instants where some data was read.
.PP
\fBOver:\fR amount of excess read. This is due to the anticipation algorithm,
and to buffers that are filled but not consumed. With sequential access,
this number should be zero of very low.
.RE
.PP
\fBWriters\fR: statistics are shown for each individual writer and for the
total. Writers are started when necessary, so only the started writers are
shown. This section is absent when no file has been written.
.RS
\fBUpload:\fR number of bytes written.
.PP
\fBN.Req:\fR number of write requests completed.
.PP
\fBAvg Time:\fR average time of a write request.
.PP
\fBMax Time:\fR maximum time of a write request.
.PP
\fBRef:\fR reference to the file being currently written, or 'idle' if none.
.PP
\fBN.Err:\fR number of errors.
.PP
\fBSpeed:\fR current write speed.
.PP
\fBAv.Sp:\fR average write speed. See readers note. Also note that all read
and write speeds are measured from received fuse requests. That might differ
from the network speed if there is some overwrite (eg with encfs).
.RE
\fBAPI\fR: routes are shown only when they have been used for at least one call.
.RS
\fBN.Req:\fR number of API calls.
.PP
\fBAvg Time:\fR average time of an API call.
.PP
\fBMax Time:\fR maximum time of an API call.
.PP
\fBRetry:\fR number of retries. API calls will be retried when the server
returns 'HTTP 429: too many requests'. Each retry is done with a higher delay.
If that counter is not zero, it is advised to try reducing parallel operations
on the mount.
.PP
\fBN.Err:\fR number of errors.
.RE
\fBRefresh\fR: shows the number of refresh of each category, respectively:
Triggered, Timer, Polling, Write, API, Error, Auto and a Total.
API and Write refresh only the subtree that needs refreshing,
whereas other categories refresh the whole tree. Auto is at startup (when
using --root option) and at exit (always).
.PP
\fBMemory\fR: shows the number of malloc/free with the amount of memory
involved, and the difference (what is currently allocated). Contentions
number is shown with the longest spin count.
.PP
\fBLive streams\fR: shows the files being written or in the process of being
stored, and read, or recently closed with a download ticket still valid.
Write or Read files categories are shown only if any. The count is displayed
on the title line.
.RS
.PD 2v
.PP
\fBIMPORTANT:\fR when the count shows 0 "write" file and the corresponding
list is empty, or show only zero size files, it means all written files are
stored on the 1fichier account, and it is safe to exit the program. On the
contrary, when files are present on the "write" list, exiting the program might
trigger data loss if FTP is in "manual" mode as explained in the write section
on top of this manpage.
.PP
.PD 0.1v
\fBRef:\fR reference that links to the writers or readers.
.PP
\fBSize:\fR size of the file (maximum write offset, when writing).
.PP
\fBLeft:\fR time left until the downlad ticket expires. Once expired, the
file is removed from the list when another file is opened. For writing, the
time makes sense only in the storage process, an estimation of the remaining
time to the next state is shown.
.PP
\fBLnk:\fR number of links on the file. One per 'open' and one per reader.
.PP
\fBEr:\fR '\fBE\fR' if there was an error on the file. From there, no more
operation can be done on this file. Trying to open it will return
\'access denied\' until a refresh is done that will remove it from the list.
For written files, the file must be deleted to clear the error.
.PP
\fBN.Loc:\fR (read) number of downlad tickets ('locations') taken for this file.
.PP
\fBN.Stm:\fR (read) number of streams opened for this file. Each 'stream' is a
distinct range request.
.PP
This two items are replaced with the step in the writing process. Steps are
Writing, Wait_FTP (5 minutes wait to trigger the FTP process), Wait_mv or
Wait_rm (depending on if the file needs to be moved or deleted once it is
uploaded on the storage), and the last step is Done. Steps can be preprended
with \fBrm/\fR if the file was deleted during the write phase of the
storage process.
.PP
\fBPath:\fR initial path of the file. Note that the key to access files
on the \fB1fichier\fR account is the URL, not the file path. Should the
file be renamed of moved, opening that newly moved file will use the same
entry, because the URL is not changed when renaming or moving.
For write, this it the target path of the file. Note: if a directory is
renamed inside the path, it won't show in that name, but the file will
still be put in the right place when the upload process is over.
.RE
.RE
.PD
.PP

.SH OPTIONS
All options can be used the standard way, or the fuse way with \fB\-o\fR
.P
Example, to set verbosity to DEBUG level, all three methods
below are equivalent:
.EX
.RS
-l 7
--log-level=7
-o log-level=7
.RE
.EE
.PP
The \fB\-o\fR form is mostly useful when the mount is specified in the
\fI\,/etc/fstab\/\fR.
.SS "1fichierfs options:"
.TP
\fB\-\-api\-key\fR=\fI\,MyKey\/\fR
The API key of the 1fichier.com account.
.PD 0.1v
.PP
.RS
This simplest method to specify the API key has an inconvenient:
the API Key will be visible in clear text with the
.B ps
command.
.PP
It this is a security issue in the configuration it is used,
see other methods below.
.PD
.PP
If the API key starts with \fB@\fR, it is interpreted as a filename (after
removal of the leading \fB@\fR) which contains the API Key.
.PD 0.1v
.PP
With this method, the API Key is not visible with the
.B ps
command, but is still in clear text in the specified file.
Security can be further refined with the next method.
.PD
.PP
\fB@\fR alone means standard input : \fBstdin\fR.
.PD 0.1v
.PP
This allows, for example, piping with a program that decrypts the API Key.
.PD
.PP
Ultimately, if this option is not present, the user will
be prompted for the API Key.
.RE
.TP
\fB\-l\fR \fB\-\-log\-level\fR=\fI\,N\/\fR
Verbosity level. Default: 4 (warning).
.PP
.RS
The levels are those of \fBsyslog\fR. See \fBman 3 syslog\fR.
.PP
The higher, the most verbose. Since
.B 1fichierfs
does not emit ALERT or EMERG messages, specifying log-level 0 or 1 will
make the program completely quiet.
.RE
.TP
\fB\-\-log\-file\fR=\fI\,filepath\/\fR
Log will be saved to that file instead of stderr
(when foreground) or syslog.
.PP
.RS
This is especially recommended at high level of logging such as 7 (DEBUG),
because the program emits a lot of messages that would otherwise flood
the syslog.
.RE
.TP
\fB\-\-log\-size\fR=\fI\,size\/\fR The maximum log size (in MB).
.PP
.RS
After this size, the log will wrap back to the start of the file.
The option is ignored when \fI\,log-level\/\fR is not 7 (DEBUG) or if no
\fI\,log-file\/\fR is specified. This option exists only if the executable was
compiled with the DEBUG flag, else it will return "unknown option".
.PP
.RE
.TP
\fB\-\-raw\-names\fR
Names read from the storage or passed to the API are not translated. This
limits the usable character set as some characters are forbidden by the storage
(see above).
.PP
.TP
\fB\-\-network\-wait\fR=\fI\,time\/\fR
Default: 60 (seconds)
.PD 0.1v
.PP
.RS
This is how long to wait for network to become ready when \fB1fichierfs\fR
starts. If the network does not become ready within the specified delay,
the program will exit without mounting. A delay of zero will fail the
initialisation immediately if the network was not ready.
.PP
This is useful for recent Linux distributions that start the session without
waiting for network readiness. If \fB1fichierfs\fR is launched at session start,
it won't fail in such a case, but simply wait (by default one minute) for
the network before starting the mount.
.PP
\fBNote:\fR the default behaviour resembles _netdev in \fB/etc/fstab\fR,
because obviously \fB1fichierfs\fR is a network mount! But _netdev is not
suitable because fuse mounts should not be started by root.
Nevertheless, it is a good idea to declare the \fB1fichierfs\fR mount in
\fB/etc/fstab\fR, but with \fBnoauto\fR and \fBuser\fR options. Then mounting
can be done at session startup or later by the user without root privilege.
In this case the _netdev  option is not passed from \fB/etc/fstab\fR to the
explicit mount, thus cannot be used reliably as a hint to wait or not.
.RE
.PD
.PP
.HP
\fB\-\-refresh\-file\fR=\fI\,filename\/\fR
.HP
\fB\-\-refresh\-hidden\fR
.TP
\fB\-\-refresh\-time\fR=\fI\,M\/\fR
These options control how the full directory tree
is refreshed from the server.
.PP
.RS
Regardless of these three options, refresh is always done on directories
concerned by a write operation (targets of rename and hard-link, ...).
A global refresh is systematically done if a file that should be there has
been removed from the server (HTTP 404). This can happen if the file was
removed from the web interface while \fB1fichierfs\fR was running.
.PP
If a file returns a read error, subsequent opening of the same file will fail
with 'access denied' until a refresh ( DEBUG=1on any directory) is done.
After that refresh, the user can try reading the file again.
.PP
\fB\-\-refresh\-file\fR shows an empty file on
the root of the \fI\,mountpoint\/\fR.
.PD 0.1v
.PP
Opening this file triggers a global refresh.
.PP
If \fI\,filename\/\fR is shadowed by an already existing file or
directory on the root of the mount, a warning is sent,
and the \fB\-\-refresh\-file\fR option is ignored.
.PD
.PP
If \fB\-\-refresh\-hidden\fR is specified, the file will not be shown.
.PD 0.1v
.PP
Trying to open it will return 'no such file' but will still
trigger a global refresh.
.PP
This is best suited for refresh by CLI.
.PD
.PP
Unless shadowed, the \fB\-\-refresh\-file\fR, regardless of whether it
is hidden (\fB\-\-refresh\-hidden\fR) or not, is protected from removal,
linking, renaming and overwriting by a rename or link as source or target.
Such operations will return an access denied error.
.PP
.B \-\-refresh\-time
specifies a time \fI\,M\/\fR, in \fBminutes\fR, after which the
directories entries cache will be dropped. Specifying 0 means
no refresh: directories and files will be cached forever unless one of the
refresh cases explained above occurs.
.RE
.TP
\fB\-\-no\-ssl\fR=\fI\,path\/\fR
By default on \fB1fichier.com\fR transfers occur with SSL/TLS, unless the
parameter "Force the downloads without SSL" (sic) has been ticked or the
no_ssl flag is set for an individual file via an API call. Regardless of those
settings, specifying this option forces SSL/TLS deactivativation for the whole
tree under \fI\,path\/\fR.
.PP
.RS
This option can occur multiple times to specify different \fI\,paths\/\fR.
.PP
Writing in a \fI\,path\/\fR specified as such will occur without data
encryption.
.PP
Typical usages: when the files and filenames stored are already encrypted,
when using a low-specced machine where SSL/TLS is slow (at the expense
of security/privacy), when files are "public" (Linux ISO images, ...), etc...
.PD
.RE
.TP
\fB\-\-encfs\fR=\fI\,path\/\fR
For whole tree under \fI\,path\/\fR the option makes encfs renaming of files
possible. This is typically needed when doing a backup to an encrypted path
with rsync that writes to temporary files before renaming. 
.PP
.RS
This option can occur multiple times to specify different \fI\,paths\/\fR.
.PP
\fBRECOVERY:\fR when encfs renames a file, it has to change the header. The
header is the 8 first bytes of the file. Since this operation is not possible
with the cloud storage without rewriting the whole file, instead, the name
of the file is appended with 12 base64 characters corresponding with the new
8 bytes header. So, in this case, reading the file from the
standard web interface will result in a file which content and name are
different and encfs will not decrypt it. It is possible to recover both the
content ant the name of the file as expected by encfs with the following
commands where ${src} is the file name :
.PD 0
.PP
\fB{ printf '%s' "${src}" | tail -c12 | sed 's/,/+/g;s/-/\//g' | base64 -d; tail -c+9 "${src}"; } >content\fR
.PP
For the name, simply:
.PP
\fBprintf '%s' "${src}" | head -c-12\fR
.PP
With this option
.B 1fichierfs
hides the 12 characters appended to the name. Consequently, to use the above
commands with
.B 1fichierfs
instead of with files downloaded via the standard web interface, the mount must
be made excluding the concerned \fI\,paths\/\fR from the \fB\-\-encfs\fR option. 
.PD
.PP
\fBNOTE:\fR \fI\,path\/\fR as specified in both options can be absolute or
relative. A relative \fI\,path\/\fR is relative to the \fB\-\-root\fR passed.
When no \fB\-\-root\fR is specified or \fB\-\-root\fR is '/', absolute and
relative \fI\,paths\/\fR are the same. 
.PP
\fBNOTE:\fR \fI\,path\/\fR as specified in both options do not need to exist
when the program is started. The option will apply once and if the
\fI\,path\/\fR is created, or cease to apply when the \fI\,path\/\fR does not
exist anymore.
.PD
.RE
.TP
\fB\-\-stat\-file\fR=\fI\,filename\/\fR
Virtual file that shows live statistics of \fB1fichierfs\fR. Same rules
as for \fB\-\-refresh\-file\fR apply. See header section for statistics
detail. If this option is not used, no statistics are gathered. When the
executable was built with the NOSTATS=1 option to make it lighter, this
option is not available and will return an error.
.TP
\fB\-\-root\fR=\fI\,path\/\fR
Instead of mounting the whole \fB1fichierfs\fR account, mount only the remote
\fI\,path\/\fR specified. The path must exist. Mounting a share is
also possible.
.TP
\fB\-\-ftp\-user\fR=\fI\,name\/\fR
Specify the FTP user \fI\,name\/\fR to use. If this option is not present
\fB1fichierfs\fR will use an existing FTP account pointing to the right
directory if it exists, or an account matching the random pattern. If none
of the existing accounts meets those conditions, \fB1fichierfs\fR creates a
reasonably unique name from the random pattern including the nanosecond's
startup time and a default prefix.
.TP
\fB\-\-no\-upload\fR
Do not allow uploads (writing). Unlike -o ro, rename, del, etc... are still
allowed.
.TP
\fB\-\-dry\-run\fR
Simulates a resume operation without actually renaming or deleting files, only
logging happens. Useful messages are notices and warnings, so it is recommended
to use that option with a log level of 5. The filesystem is not mounted when
using that option.
.TP
\fB\-\-resume\-delay\fR=\fI\,seconds\/\fR
By default, \fB1fichierfs\fR will clean broken resume information if it is
older than 27 hours. This duration takes into account that files are kept
24 hours on the ftp server, then a 300GB file (max allowed) might take some time
to show. This timer also affects file protection within the upload directory.
.TP
\fB\-\-threads\fR=\fI\,number\/\fR
By default, \fB1fichierfs\fR runs with a number of threads equal to half the
number of available processors, with a minimum of 1 and a maximum of 8.
The option allows to change this default value. However, the value will be
kept in the range between 1 and the number of processors, always capped to 8.

.SS "curl options used by 1fichierfs (`man curl` for detail):"
.HP
\fB\-4\fR \fB\-\-ipv4\fR
.HP
\fB\-6\fR \fB\-\-ipv6\fR
.HP
\fB\-k\fR \fB\-\-insecure\fR
.TP
\fB   \-\-cacert\fR
.TP
\fB\-A\fR \fB\-\-user\-agent\fR
The default user agent is: \fB1fichierfs/1.9.6\fR
.PP
.RS
With \fB\-A\fR '' no user\-agent header will be sent.
.RE
.SS "General options:"
.TP
\fB\-o\fR opt,[opt...]
mount options
.TP
\fB\-h\fR   \fB\-\-help\fR
print help
.TP
\fB\-V\fR   \fB\-\-version\fR
print version
.SS "Common FUSE options:"
For a complete list of fuse options, see fuse documentation or
.B 1ficherfs \-\-help
.TP
\fB\-d\fR   \fB\-o\fR debug
enable debug output (implies \fB\-f\fR)
.TP
\fB\-f\fR
foreground operation
.TP
\fB\-s\fR
disable multi\-threaded operation
.TP
\fB\-o\fR allow_other
allow access to other users
.TP
\fB\-o\fR allow_root
allow access to root
.TP
\fB\-o\fR auto_unmount
auto unmount on process termination
.TP
\fB\-o\fR nonempty
allow mounts over non\-empty file/dir
.TP
\fB\-o\fR fsname=NAME
set filesystem name  (default: \fB\1fichier.com\fR)
.TP
\fB\-o\fR subtype=NAME
set filesystem type (default: \fB\1fichierfs\fR)
.TP
\fB\-o\fR kernel_cache
cache files in kernel
.TP
\fB\-o\fR [no]auto_cache
enable caching based on modification times (off)
.TP
\fB\-o\fR umask=M
set file permissions (octal)
.TP
\fB\-o\fR uid=N
set file owner
.TP
\fB\-o\fR gid=N
set file group
.TP
\fB\-o\fR big_writes
enable larger than 4kB writes
.RS
.PD 0.1v
\fBNote:\fR specifying this option is highly recommended when \fB1fichierfs\fR
is used for writing: it could save about half of the CPU (during write
operations).
.PD
.RE
.SH "EXAMPLES"
.TP
\fB$ 1fichierfs ~/1fichier\fR
That enough! Mount the remote storage on the \fB1fichier\fR directory
within the home of the user. The user is prompted for the API key.
.TP
\fB$ 1fichierfs --api-key=@~/.1fichier.key --refresh-time=10 ~/1fichier\fR
Mount using the API key in the file ~/.1fichier.key
.PD 0.1v
The local directory tree will be synced with the server if it has not
been refreshed in the last 10 minutes (600 seconds).
.PD
.TP
\fB1fichierfs /home/user/1fichier fuse api-key=@/home/user/.1fichier.key,refresh-time=10,user,noauto 0 0\fR
This is same as above, but suited for \fI\,/etc/fstab\/\fR
.PD 0.1v
.PP
.RS
The user can then mount his home \fB1fichier\fR directory
\fI\,on demand\/\fR, by issuing
an unprivileged mount command, or through the use of a file explorer
such as Nemo or Nautilus, with a simple click on the directory.
.RE
.PD
.SH "AUTHORS"
Written and maintained by Alain Bénédetti.
.SH "DISCLAIMER"
The author is not affiliated in any way with the company running the
\fB\1fichier.com\fR cloud storage service.
.PP
This work is not an "official" driver written by \fB\1fichier.com\fR,
it is only provided in the hope it can be useful to somebody.
.SH "REPORTING BUGS"
Post bug reports at https://gitlab.com/BylonAkila/astreamfs
.PP
In French, also here: https://forum.ubuntu-fr.org/viewtopic.php?pid=22032052
.SH "COPYRIGHT"
Copyright © 2018-2025 Alain Bénédetti.
.PP
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
.PD 0.1v
.PP
This is free software: you are free to change and redistribute it.
.PP
There is NO WARRANTY, to the extent permitted by law.
.PD
.SH "SEE ALSO"
\fBsyslog\fR(3) \fBastreamfs\fR(1) \fBfusermount\fR(1)
\fBfuse\fR(8) \fBmount\fR(8)
