#
# astreamfs and derivative work (1fichierfs) Makefile
#
#
#  Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
#
#  License:
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

ATOMIC := $(shell  ./test_atomic.sh)


CFLAGS = -Wall -D_FILE_OFFSET_BITS=64 -I/usr/include/fuse \
          $(shell curl-config --cflags) -std=c11
ifeq ($(origin DEBUG), undefined)
    ifeq ($(origin NOSTATS), undefined)
	CFLAGS += -O2 -D_MEMCOUNT
    else
	CFLAGS += -O2 -DNOSTATS
    endif
else
    ifeq ($(origin NOSTATS), undefined)
	CFLAGS += -g -DDEBUG -D_MEMCOUNT
    else
	CFLAGS += -g -DDEBUG -DNOSTATS -D_MEMCOUNT
    endif
endif
ifdef MEMCOUNT
	CFLAGS += -D_MEMCOUNT
endif

# $(info $$CFLAGS is [${CFLAGS}])

TARGET = astreamfs
LIBS := -lfuse $(ATOMIC) -pthread $(shell curl-config --libs)
CC = gcc

.PHONY: default all clean

default: $(TARGET)
all: default

astreamfs_worker.o: astreamfs_worker.c astreamfs_worker.h \
		    astreamfs_worker_stats.h astreamfs_worker_stats.c \
		    astreamfs_worker_dbg.c
	$(CC) $< $(CFLAGS) -c -o $@

astreamfs_worker_mem.o: astreamfs_worker_mem.c astreamfs_worker_mem.h
	$(CC) $< $(CFLAGS) -c -o $@

astreamfs_util.o: astreamfs_util.c astreamfs_util.h
	$(CC) $< $(CFLAGS) -c -o $@

$(TARGET): astreamfs.c astreamfs_util.o astreamfs_util.h astreamfs_worker.h \
           astreamfs_worker_mem.h astreamfs_worker.o astreamfs_worker_mem.o
	$(CC) $< $(CFLAGS) astreamfs_util.o astreamfs_worker.o \
	      astreamfs_worker_mem.o $(LIBS) -o $@

1fichierfs_options.o: 1fichierfs_options.c astreamfs_util.h 1fichierfs.h \
		      astreamfs_worker.h astreamfs_worker_mem.h
	$(CC) $< $(CFLAGS) -c -o $@

ifndef NOSTATS
1fichierfs_stats.o: 1fichierfs_stats.c astreamfs_util.h 1fichierfs.h \
	    astreamfs_worker.h astreamfs_worker_mem.h
	$(CC) $< $(CFLAGS) -c -o $@
endif

1fichierfs_rcu.o: 1fichierfs_rcu.c astreamfs_util.h 1fichierfs.h \
		  astreamfs_worker.h astreamfs_worker_mem.h
	$(CC) $< $(CFLAGS) -c -o $@

1fichierfs_write.o: 1fichierfs_write.c astreamfs_util.h 1fichierfs.h \
		    astreamfs_worker.h astreamfs_worker_mem.h
	$(CC) $< $(CFLAGS) -c -o $@

1fichierfs_read.o: 1fichierfs_read.c 1fichierfs_read.h astreamfs_util.h \
		   1fichierfs.h astreamfs_worker.h astreamfs_worker_mem.h
	$(CC) $< $(CFLAGS) -c -o $@

ifdef NOSTATS
1fichierfs: 1fichierfs.c 1fichierfs.h astreamfs_util.h 1fichierfs_read.h \
	   astreamfs_worker.h astreamfs_worker_mem.h astreamfs_worker_dbg.c \
	   astreamfs_worker_stats.c astreamfs_util.o 1fichierfs_options.o \
	   1fichierfs_read.o 1fichierfs_write.o 1fichierfs_rcu.o \
	   astreamfs_worker.o astreamfs_worker_mem.o
	$(CC) $< $(CFLAGS) 1fichierfs_options.o \
	      astreamfs_worker.o astreamfs_util.o 1fichierfs_write.o \
	      1fichierfs_rcu.o 1fichierfs_read.o astreamfs_worker_mem.o \
	      $(LIBS) -o $@
else
1fichierfs: 1fichierfs.c 1fichierfs.h astreamfs_util.h 1fichierfs_read.h \
	   astreamfs_worker.h astreamfs_worker_mem.h astreamfs_worker_dbg.c \
	   astreamfs_worker_stats.c astreamfs_util.o 1fichierfs_options.o \
	   1fichierfs_read.o 1fichierfs_write.o 1fichierfs_rcu.o \
	   astreamfs_worker.o astreamfs_worker_mem.o 1fichierfs_stats.o
	$(CC) $< $(CFLAGS) 1fichierfs_options.o \
	      astreamfs_worker.o astreamfs_util.o 1fichierfs_write.o \
	      1fichierfs_stats.o 1fichierfs_rcu.o 1fichierfs_read.o \
	      astreamfs_worker_mem.o \
	      $(LIBS) -o $@
endif

clean:
	-rm -f *.o
	-rm -f $(TARGET)

install:
ifdef DESTDIR
	mkdir -p $(DESTDIR)/usr/bin/
	cp $(TARGET) $(DESTDIR)/usr/bin/
	mkdir -p $(DESTDIR)/usr/share/man/man1/
	cp $(TARGET).1.gz $(DESTDIR)/usr/share/man/man1/
	mkdir -p $(DESTDIR)/usr/share/man/fr/man1/
	cp $(TARGET).fr.1.gz $(DESTDIR)/usr/share/man/fr/man1/$(TARGET).1.gz
else
	cp $(TARGET) /usr/bin/
	cp man/$(TARGET).1.gz /usr/share/man/man1/
	cp man/$(TARGET).fr.1.gz /usr/share/man/fr/man1/$(TARGET).1.gz
endif

uninstall:
ifdef DESTDIR
	rm $(DESTDIR)/usr/bin/$(TARGET)
	rm $(DESTDIR)/usr/share/man/man1/$(TARGET).1.gz
	rm $(DESTDIR)/usr/share/man/fr/man1/$(TARGET).1.gz
else
	rm /usr/bin/$(TARGET)
	rm /usr/share/man/man1/$(TARGET).1.gz
	rm /usr/share/man/fr/man1/$(TARGET).1.gz
endif

