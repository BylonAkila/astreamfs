/**
 * 1fichierfs:	mount your 1fichier account (fuse).
 *		This work is derived from astreamfs that is a generic
 *		http(s) fuse reader. It uses the 1fichier's API to manage
 *		the directory tree and get download tickets.
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Feature:
 *	Same as astreamfs but "specialised" to 1fichier
 *	On top of reading files from the server in a stream-optimised way,
 *	the APIs allow to:
 *	- Automatically read remote directory content and synchronize to
 *	  the server tree.
 *	- Report the usage of "Cold Storage" and compute available storage.
 *	- Delete files and directories (`rm` and `rmdir` commands)
 *	- Rename -and move- (`mv` command) files and directories
 *	- Hard link files only (`ln` command, no soft link, hard link only)
 *	- Create directories (`mkdir` command)
 *  - write files (since 1.6.0) -limited to 'almost' sequential/ new files-
 *
 * Usage:
 *	Either
 *		read the help provided by: `1fichier -h` or
 *		look at the help section below, under astreamfs_opt_proc()
 *
 * Compile with:
 *	cc -Wall 1fichierfs.c	$(pkg-config fuse --cflags --libs)\
				$(curl-config --cflags --libs)\
				-o 1fichierfs -lpthread
 *
 * Compile dependancy:
 *	curl and fuse library
 *
 * Version: 1.9.6
 *
 *
 * History:
 * 	2024/11/05: 1.9.6 encfs rename friendly + "flags" attached to dir
 * 	2024/01/06: 1.9.5 log_size, Fix: infinite loop bug/warnings/deprecation
 * 	2023/02/04: 1.9.4 Fix: crash del before write/ low speed stats, single
 * 	2022/10/03: 1.9.3 bug#5: use after free, sync bug and fix reclaim_stream
 * 	2022/05/22: 1.9.2 optim. read+start; Fixed a crash; Unused engine retry
 * 	2022/05/08: 1.9.1 Fix: crash + stats memory corrupt. on some situtations
 * 	2022/04/10: 1.9.0 Improved engine done - Common code backported
 * 	2022/01/16: 1.9.0 Improved engine
 * 	2021/03/07: 1.8.4 When writers busy: wait instead of returning EAGAIN
 * 	2021/03/02: 1.8.3 Deduplicate dir & files + rename on exist (for rsync)
 * 	2021/01/23: 1.8.2 Bug fix: date compute in resume.
 * 	2020/12/20: 1.8.1 Better "resume": mitigation of 403 and tweaks.
 * 	2020/12/05: 1.8.0 Write environment lazy initialisation + bug fixes.
 * 	2020/09/26: 1.7.2 small files write stay in memory during upload cycle.
 * 	2020/05/21: 1.7.1.1 statfs returns max name lg: needed by 20.04 Nautilus
 * 	2020/05/17: 1.7.1 Now wait network readiness on init (--network-wait=60)
 * 	2020/04/27: 1.7.0 Translation of forbidden characters (+ option not to).
 * 	2020/03/27: 1.6.5.3 Fix: read bug; Write errors code; Improvs; Cleaning.
 * 	2020/03/17: 1.6.5.2 Improved and simpler rename/del upload process.
 *	2020/03/14: 1.6.5.1 Crash: small buf (ftp_del) Fix: buf queue (prv_ins)
 *	2020/03/07: 1.6.5 Full feature version. + doc and upload dir protection.
 *	2020/02/29: 1.6.4 New: resuming first implementation.
 *	2020/02/01: 1.6.3 Bugfix and handling of special+forbid char in names.
 *	2020/01/20: 1.6.2 Write statistics, improvements and bugfixes.
 *	2020/01/12: 1.6.1 Del/Ren 100% Ok during write, better algo, bugfixes...
 *	2019/12/17: 1.6.0 MAJOR: writing is now enabled (create + sequential).
 *	2019/11/24: 1.5.5 Stats +contention FIX: engine (4 bugs) stats times (1)
 *	2019/11/22: 1.5.4 better and more resilient algo for download tokens.
 *	2019/11/10: 1.5.3 bug & crash fix on shares (+ optimised with --root)
 *	2019/10/30: 1.5.2 New: mount a path of the account + bug & crash fix.
 *	2019/10/17: 1.5.1 Crash: if ls.cgi API error / better options handling
 *	2019/08/06: 1.5.0 Shared folders including read-only + "hidden links".
 *	2019/06/10: 1.4.1 Crash: in size computation for stats read.
 *	2019/06/08: 1.4.0 New: added curl options --insecure and --cacert
 *	2019/06/08: 1.3.2 Crash:(Jaxx21@ubuntu-fr) typo with stats (parse_check)
 *	2019/06/03: 1.3.1 Fix: avg speed computation. Add: Memory stats.
 *	2019/05/28: 1.3.0 New: Statistics. Bug: engine broken on stream change.
 *	2019/05/22: 1.2.2 Fix bug: correctly display content of shared folders
 *	2019/05/19: 1.2.1 Fix crash: don't free streams when they are in use!
 *	2019/05/12: 1.2.0 Bugfixes (memory-management,...) / New no_ssl feature
 *	2019/05/11: 1.1.1 Fixed bugs on the first refactored version.
 *	2019/05/09: 1.1.0 Refactored live streams (avoid getting new DL-tickets)
 *	2019/04/28: 1.0.11 fix: arg subtype, JSON-esc names mv/cp,stat st_blocks
 *	2019/04/02: 1.0.10 ls: folders+files/New storage algo/Engine: header chk
 *	2019/03/24: 1.0.9 API chg: opti ls folder+files/ date ok dirs/ del optim
 *	2019/03/05: 1.0.8 API change: atomic and much simpler mv (folders)
 *	2019/03/04: 1.0.7 API change: atomic and much simpler ln (files)
 *	2019/02/28: 1.0.6 API change: atomic mv (files) in all cases
 *	2019/02/23: 1.0.5 API change: rename/move directory is now possible!
 *	2019/02/09: 1.0.4 info API changed; refr-time = min; protect refrsh-file
 *	2019/02/03: 1.0.3 Clean + make it compile and work on 32bits!
 *	2019/02/03: 1.0.2 API change: limit 1/5min user/info + get max storage.
 *	2019/01/21: 1.0.1 mkdir added.
 *	2019/01/19: 1.0.0 Last possible realistic implem: hard linking.
 *	2019/01/18: 0.9.7 Rename files in all cases (rename + move).
 *	2019/01/16: 0.9.6 Remove file (rm) and directory (rmdir) implemented.
 *	2019/01/13: 0.9.5 Fix bugs on statfs / (RW) Rename files same directory
 *	2019/01/05: 0.9.1 Resist to server error, retry (429) and free storage!
 *	2019/01/01: 0.9.0 Cope with server out of sync + Valgrinded bugs.
 *	2018/12/29: 0.8.7 Complete refresh: trigger, hidden and timer.
 *	2018/12/24: 0.8.6 First version with refresh and RCU freeing.
 *	2018/12/23: 0.8.5 Separate file and dir requests in preparation of RCU.
 *	2018/12/22: 0.8.4 Several bugfix + API Key can be specified in a file.
 *	2018/12/16: 0.8.3 Cleaning: moved common code to astreamfs_util.c/h
 *	2018/12/09: 0.8.2 "Good enough" initial specialized version, based on
 *			astreamfs as of version 0.8.2
 *
 *
 * Notes:
 *  Clocks: we use mainly CLOCK_MONOTONIC_COARSE, and CLOCK_REALTIME only when
 *   necessary, that is to display start time, and for sem_timedwait.
 *   To correctly time the 'download ticket' validity period, CLOCK_BOOTTIME
 *   should be used instead, because it takes care of suspend time. However,
 *   CLOCK_BOOTTIME is more than 50 times slower that CLOCK_MONOTONIC_COARSE,
 *   because it does a system call instead of using vdso (simulated system call)
 *   So it might happen, when resuming after suspend, that a 'download ticket'
 *   the program sees as valid is in fact invalid. Anyway, the server will
 *   respond with 410 (gone) and a new ticket will be requested. This
 *   possible short delays after resume are the price to pay for the
 *   simplification and optimisation. Anyway, there are many things that have
 *   to be "restarted" after resume, first one being the network!..
 *
 * May 2019 Refactoring:
 *  Information needed to display the directory tree and needed to stream are
 *  now separate. struct file gets the former, and struct lfile the latter.
 *  Common parts: size and URL are copied on open().
 *  This allows:
 *    - To keep download tickets for their full duration (30 min) regardless
 *	of any refresh() that happened during the "ticket" validity period.
 *    - A more straightforward RCU free code since it does not need anymore to
 *	check if files are opened or not.
 *    - Lower memory footprint (on normal conditions assuming a reasonable
 *      amount of files are opened at the same time). This is also true because
 *      the 'st' structure (144 bytes in 64bits) is now replaced in 'file' by
 *      the only information we need for files: size and create date (16 bytes).
 *      That change alone saves 128 bytes for each file currently cached in the
 *      directory tree.
 *  Other changes:
 *    - The list of opened files is managed mainly at open().
 *    - refresh() takes care of potential files that were marked as ERROR.
 *	This is a feature.
 *	When there is a read error on a file, it is marked as error and EIO or
 *	EREMOTEIO are returned. Subsequent open() of the same file return
 *	EACCESS. Any refresh will purge the files that were marked as error
 *	from the list, allowing the user to retry.
 *    - release() only decrements the opened counter.
 *    - Also release() does NOT send the close signal to the async_reader unless
 *	the file is marked as error. This means that this stream will stay live
 *	for 45 seconds and could serve requests for the same file. That works
 *	well for files under 16K (curl default buffer), they will be served
 *	from memory within the 45 seconds regardless of kernel cache parameter.
 *    - This change also makes open() and release() completely lockless.
 *	Note that open() is not waitless since the new 'live' (opened and
 *	alive) 'streams' struct is managed with atomics & RCU (no mutex locks).
 *
 * Note: using only the CA that signs *.1fichier.com certificates greatly
 *       reduces the memory footprint on GnuTLS (around 32MB instead of 96MB!)
 *       To make this CA file, look at the curl documentation or do:
$ openssl s_client -showcerts -servername 1fichier.com -connect 1fichier.com:443 >1fichier.pem
 *       Then extract from that file the first certificate (--begin/--end cert)
 */

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <sys/statvfs.h>
#include <signal.h>

#include "1fichierfs.h"

/*******************************************************
 *  These are the things you can change at compile time.
 */

/*  See 1fichier.h to change that:
#define PROG_NAME	"1fichierfs"
#define PROG_VERSION	"1.2.0"
*/
       const char log_prefix[] = PROG_NAME;	/* Prefix for log messages    */

extern const char def_user_agent[];		/* See 1fichierfs_options.c   */
extern const char def_type[];			/* See 1fichierfs_options.c   */
extern const char def_fsname[];			/* See 1fichierfs_options.c   */

/* See definition and comments in 1fichier.f
#define DEF_JSON_BUF_SIZE (16 * 1024 + 1) */
						/* Upon HTTP 429 (too many
						 * requests), this sets delays
						 * and number of retries.     */
static const useconds_t retry_delay_ms[]= {  350000,  700000, 1000000 };

#define NETWORK_WAIT_INTVL (2) /* Wait interval (sec) for network to be ready */

static const char hidden_url[] = "{hidden}";

/********************************************************
 *  Do NOT change the following definitions or variables.
 *  Things would break!
 *  These definitions and variables are either system dependent,
 *  or depend on how 1fichier works.
 */
#define ST_BLK_SZ (512)		/* This is the block size stat's st_blocks */
#define DEFAULT_BLK_SIZE (4096)	/* This is the block size for statvfs */

#define URL_API_EP "https://api.1fichier.com/v1/"
static const char url_api_ep[]		= URL_API_EP;
static const char *api_urls[] = {
/*	FOLDER_LS		= 0, */ URL_API_EP"folder/ls.cgi",
/*	DOWNLOAD_GET_TOKEN	= 1, */ URL_API_EP"download/get_token.cgi",
/*	USER_INFO		= 2, */ URL_API_EP"user/info.cgi",
/*	FILE_MV			= 3, */ URL_API_EP"file/mv.cgi",
/*	FILE_CHATTR		= 4, */ URL_API_EP"file/chattr.cgi",
/*	FILE_RM			= 5, */ URL_API_EP"file/rm.cgi",
/*	FILE_CP			= 6, */ URL_API_EP"file/cp.cgi",
/*	FOLDER_MV		= 7, */ URL_API_EP"folder/mv.cgi",
/*	FOLDER_RM		= 8, */ URL_API_EP"folder/rm.cgi",
/*	FOLDER_MKDIR		= 9, */ URL_API_EP"folder/mkdir.cgi",
/*	FTP_USER_LS		=10, */ URL_API_EP"ftp/users/ls.cgi",
/*	FTP_USER_RM		=11, */ URL_API_EP"ftp/users/rm.cgi",
/*	FTP_USER_ADD		=11, */ URL_API_EP"ftp/users/add.cgi",
/*	FTP_PROCESS		=13, */ URL_API_EP"ftp/process.cgi",
};

static const char *refresh_msgs[] = {
/*	REFRESH_TRIGGER		= 0, */ "trigger",
/*	REFRESH_HIDDEN		= 1, */ "hidden",
/*	REFRESH_TIMER		= 2, */ "timer",
/*	REFRESH_POLLING		= 3, */ "polling",
/*	REFRESH_WRITE		= 4, */ "write",
/*	REFRESH_MV		= 5, */ "move",
/*	REFRESH_LINK		= 6, */ "link",
/*	REFRESH_UNLINK		= 7, */ "unlink",
/*	REFRESH_MKDIR		= 8, */ "mkdir",
/*	REFRESH_RMDIR		= 9, */ "rmdir",
/*	REFRESH_404		=10, */ "HTTP 404",
/*	REFRESH_EXIT		=11, */ "exit",
/*	REFRESH_INIT_ROOT	=12, */ "init_root",
};


/* These are constants set to 1fichier timeouts */
#define CURL_KEEPALIVE_TIME	( 20L)
#define CURL_CNX_TIMEOUT	( 30L)


/*****************************************/
/** Fuse behaviour: "hidden" files.
 * - For deletion, fuse does not trust the UserSpace drive to correctly do RCU
 *   and uses a trick. Unless the -o hard_remove option is passed, when a file
 *   is deleted while it is opened, fuse will rename it instead, so that
 *   subsequent read/write operation succeed.
 *   Once all handles on that file are closed (or upon fusermount -u) the
 *   renamed files are deleted.
 * - When hard_remove option is passed, that does not happen and files are
 *   immediately deleted even when they are open. In this case, fuse responds
 *   -ENOENT to the subsequent read/write on the handles without calling the
 *   driver.
 *
 * 1fichierfs
 * - hides those files in the directory list
 * - does proper RCU on write: no need uploading to save bandwidth!
 * - //TODO test if these names can be received as source/target of rename/link
 *   and if so filter them out.
 */

static const char fuse_hidden[] = ".fuse_hidden";

/****/

struct params1f params1f= {
/* curl_IP_resolve */	CURL_IPRESOLVE_WHATEVER,
/* api_key	   */	NULL,
/* no_ssl.n	   */	{ 0UL,
/* no_ssl.a	   */	  NULL, },
/* encfs.n	   */	{ 0UL,
/* encfs.a	   */	  NULL, },
/* refresh_file	   */	NULL,
/* ftp_user	   */	NULL,
/* remote_root	   */	NULL,
/* refresh_time	   */	0UL,
/* resume_delay    */   97200UL, /* 27h = 24h (manual FTP) + 300G max size */
/* refresh_hidden  */	false,
/* readonly	   */	false,
/* insecure	   */	false,
/* no_upload	   */	false,
/* dry_run	   */	false,
/* raw_names	   */	false,
/* uid		   */	-1,
/* gid		   */	-1,
/* network_wait    */	60, /* Default wait 60 sec at init */
};

mode_t st_mode;
				      /* auth_header is auth_bearer + API key */
static const char auth_bearer[]= "Authorization: Bearer ";
static char *auth_header = NULL;

_Atomic bool exiting = false;
_Atomic unsigned long refresh_start;


#define F_DEL(f)   (0 != (LOAD(((f)->flags  )) & FL_DEL  ))


_Atomic (struct lfiles *)live = NULL;

struct file {
	const char		*URL;
	const char		*filename;
	time_t			cdate;
	unsigned long long	size;
	_Atomic unsigned int	flags;
};



#define IS_SHARED_ROOT(d) (0 == d->id && d->shared)
#define SHARER_EMAIL(d) (d->name + d->email_off)



struct dentries {
	unsigned long	n_subdirs;
	unsigned long	n_files;
	struct dir	*subdirs;
	struct file	files[];
};

static char def_root[] =  "/";

struct dir root = {
/*	*d		*/	NULL,
/*	lookupmutex	*/	PTHREAD_MUTEX_INITIALIZER,
/*	parent		*/	NULL,
/*	name		*/	def_root,
/*	id		*/	0,
/*	cdate		*/	0,
/*	test_encfs	*/	false,
/*	encfs		*/	false,
/*	test_no_ssl	*/	false,
/*	no_ssl		*/	false,
/*	shared		*/	false,
/*	readonly	*/	false,
/*	hidden		*/	false,
/*	email_off; 	*/	0UL,
};

static struct dir upload_d = {
/*	*d		*/	NULL,
/*	lookupmutex	*/	PTHREAD_MUTEX_INITIALIZER,
/*	parent		*/	&root,
/*	name		*/	NULL,
/*	id		*/	UPLOAD_DIR_ID_NOT_INITIALISED,
/*	cdate		*/	0,
/*	test_encfs	*/	false,
/*	encfs		*/	false,
/*	test_no_ssl	*/	false,
/*	no_ssl		*/	false,
/*	shared		*/	false,
/*	readonly	*/	false,
/*	hidden		*/	false,
/*	email_off; 	*/	0UL,
			  };

/* setter/getter for upload_dir_id.
 * Setter must be called in a single thread before any getter to avoid locks */
long long get_upload_id(void)		{ return upload_d.id;	   }
void	  set_upload_id(long long id)	{	 upload_d.id = id; }

/* Note: 1fichier has put a hard limit of minimum time 5 minutes between calls
 *	 to user_info. Calling more often triggers a 403, and if too many
 *	 calls are made despite the 403, the user is temporarily banned!
 *	 We could RCU the storage values, but for the moment it is not worth
 *	 it, a simple mutex (needed anyway) seems enough.
 *	 Hence all storage related variable don't need to be atomic, they
 *	 are read/written under lock.
 */
#define USER_INFO_DELAY (305) /* 5min + 5sec to be sure: time of req + round */
#define FL_STOR_ERROR	1
#define FL_STOR_REFRESH 2

static pthread_mutex_t	storage_mutex = PTHREAD_MUTEX_INITIALIZER;
static off_t		avail_storage;
static off_t		max_storage;
static time_t		storage_next_update = 0;
static unsigned int	storage_flags = FL_STOR_REFRESH;

/* Variables and define for the API call throttling and retries.
 */
static _Atomic unsigned long n_API_calls = 0;
static _Atomic bool throttled = false;
static pthread_mutex_t	apimutex = PTHREAD_MUTEX_INITIALIZER;
		/* How many times the retry can happen is derived from
		 * the number of elements of retry_delay_ms (see above) */
#define MAX_RETRY (sizeof(retry_delay_ms)/sizeof(retry_delay_ms[0]))


/*
 * Specific rcu cleaner functions for dentries
 */
static void rcu_free_dent(struct dentries *dent)
{
	unsigned long i;
	int res;

	for (i = 0; i < dent->n_subdirs; i++) {
		if (NULL != dent->subdirs[i].dent) {
			DEBUG(	"Freeing dentries for subdir: %s/%p\n",
				dent->subdirs[i].name,
				dent->subdirs[i].dent);
			res = pthread_mutex_destroy(&dent->subdirs[i].lookupmutex);
			if (0 != res)
				lprintf(LOG_CRIT, "error: %d on pthread_mutex_destroy.\n", res);
			rcu_free_dent(dent->subdirs[i].dent);
		}
	}

	DEBUG("Freeing dentries: %p\n", dent);
	free(dent);
}

static void rcu_free_dentries(struct rcu_item *head)
{
	rcu_free_dent(rcu_get_p_struct_from_item(head));
}


bool is_fuse_hidden(const char *filename) {
	return (0 == strncmp(filename, fuse_hidden, lengthof(fuse_hidden)));
}

static size_t get_json_response( const char *ptr, const size_t size,
				 const size_t nmemb, void *userdata)
{
	size_t chunk = size * nmemb;
	struct json *j = (struct json *)userdata;

	if ( j->cb + chunk + 1 > DEF_JSON_BUF_SIZE ) {
		if (j->cb < DEF_JSON_BUF_SIZE) {
			j->pb = malloc(j->cb + chunk + 1);
			memcpy(j->pb, j->buf,j->cb);
		}
		else {
			j->pb = realloc(j->pb, j->cb + chunk + 1);
			if ( NULL == j->pb )
			lprintf(LOG_CRIT,
				"out of memory. Cannot allocate %lu bytes.\n",
				j->cb + chunk + 1);
		}
	}
	memcpy(j->pb + j->cb, ptr, chunk);
	j->cb += chunk;
	return chunk;
}

void json_init(struct json *j)
{
	j->cb = 0;
	j->pb = j->buf;
}

void json_free(struct json *j)
{
	if (j->pb != j->buf)
		free(j->pb);
}

/* TODO use a real JSON parser instead of strstr
 *   Instead, to search the member "foo", we search directly "foo", since we
 *   know keys are NOT escaped by 1fichier. We also check that after "foo"
 *   we find some whitespace then a ':'. Otherwise we loop since "foo" could
 *   also have been a value.
 *   That works because :
 *	- keys are NOT escaped by 1fichier
 *	- valid json cannot contain "foo" inside a string (it would have to
 *	  be escaped like "bar\"foo\"bar", hence strstr won't find "foo")
 *
 */

#define JSON_STR_ERR ((size_t)-1)
       const char json_item_start[]= JSON_ITEM_START;
static const char json_escapes[] = "\"\\/bfnrt";
static const char json_sub_folders[]= "\"sub_folders\"";
       const char json_items[]= "\"items\"";
static const char json_shares[]= "\"shares\"";
       const char json_filename[]= "\"filename\"";
       const char json_url[]= "\"url\"";
static const char json_email[]= "\"email\"";
static const char json_hide_links[]= "\"hide_links\"";
static const char json_rw[]= "\"rw\"";
static const char json_size[]= "\"size\"";
static const char json_date[]= "\"date\"";
static const char json_name[]= "\"name\"";
static const char json_id[]= "\"id\"";
static const char json_cdate[]= "\"create_date\"";
static const char json_cold_storage[]= "\"cold_storage\"";
static const char json_hot_storage[]= "\"hot_storage\"";
static const char json_allowed_cold_storage[]= "\"allowed_cold_storage\"";
       const char json_folder_id[]= "\"folder_id\"";
static const char json_status[]= "\"status\"";
static const char json_OK[]= "\"OK\"";
static const char json_message[]= "\"message\"";

/* Translations to avoid forbidden characters.
 */
static struct {
	char from;
	char to[3];
	int  files;
} translation[] = {
	{ '"'   , { '\xef', '\xbc', '\x82' }, 0 },
	{ '$'   , { '\xef', '\xbc', '\x84' }, 1 },
	{ '\x27', { '\xef', '\xbc', '\x87' }, 0 },
	{ '<'   , { '\xef', '\xbc', '\x9c' }, 1 },
	{ '>'   , { '\xef', '\xbc', '\x9e' }, 1 },
	{ '\x5c', { '\xef', '\xbc', '\xbc' }, 1 },
	{ '`'   , { '\xef', '\xbd', '\x80' }, 1 },
};

/* Head and tail spaces protection
 */
static char head_repl[3] = { '\xe2', '\x81', '\xa0' };


char *json_find_key(const char *json, const char *str)
{
	char *p;
	do
	{
		p = strstr(json, str);
		if (NULL == p)
			return NULL;
		p += strlen(str);
		while ( *p == '\x09' || *p == '\x0A' ||
			*p == '\x0D' || *p == ' ')
			p++;
	} while (':' != *p);
	p++;
	while ( *p == '\x09' || *p == '\x0A' ||
		*p == '\x0D' || *p == ' ')
		p++;
	return p;
}

static int json_u_decode(const char *esp, char *dest)
{
	unsigned i;
	char cpc[5];
	uint32_t cp;

	for (i = 0; i < 4; i++) {
		cpc[i] = esp[i];
		if (!isxdigit(esp[i]))
			return 0;
	}
	cpc[i] = '\0';
	cp = strtol(cpc, NULL, 16);
	/* \u0000 in the json input for dir and file names is forbidden. */
	if (0 == cp)
		return 0;
	if (cp < 0x80) {
		*dest = (char)cp;
		return 1;
	}
	if (cp < 0x800) {
		*dest++ = (cp >> 6)   | 0xC0;
		*dest   = (cp & 0x3F) | 0x80;
		return 2;
        }
	if (cp < 0x10000) {
		*dest++ = (cp >> 12)	     | 0xE0;
		*dest++ = ((cp >> 6) & 0x3F) | 0x80;
		*dest++ = (cp	     & 0x3F) | 0x80;
		return 3;
	}
	if (cp < 0x110000) {
		*dest++ = (cp >> 18)	     | 0xF0;
		*dest++ = ((cp >>12) & 0x3F) | 0x80;
		*dest++ = ((cp >> 6) & 0x3F) | 0x80;
		*dest++ = (cp        & 0x3F) | 0x80;
		return 4;
        }
	return 0;
}

/* When translating, skip a lead or trail mark */
#define trans_skip_lead(p, tr) 			\
		((TR_NONE == tr || params1f.raw_names) ? p : 	\
				((*(p)     == head_repl[0] &&	\
				  *(p + 1) == head_repl[1] &&	\
				  *(p + 2) == head_repl[2])   ? p + 3 : p))

#define trans_skip_trail(p, tr) 		\
		((TR_NONE == tr || params1f.raw_names) ? false :\
				((*(p)     == head_repl[0] &&	\
				  *(p + 1) == head_repl[1] &&	\
				  *(p + 2) == head_repl[2] &&	\
				  *(p + 3) == '"' )   ? true : false))

/*
 * @brief performs a reverse translation from the server's name
 *
 * If there is a match in the translation table (to) returns the reverse
 * translation character (from) and skips 2 characters.
 * Otherwise, or in raw_names mode, returns the character at the position
 * of the pointer and skip is zero.
 *
 * @param start point of the translation
 * @param (output) number of chars to skip
 * @param type of translation to perform
 * @return translation (a single character)
 */

static char rev_trans(const char *p, size_t *skip, enum translation_type tr)
{
	unsigned int i;

	*skip = 0;
	if (TR_NONE == tr || params1f.raw_names)
		return *p;
	for (i = 0; i < sizeof(translation) / sizeof(translation[0]); i++) {
		if (*p < translation[i].to[0])
			break;
		if (*p == translation[i].to[0]) {
			if (*(p + 1)  < translation[i].to[1])
				break;
			if (*(p + 1) == translation[i].to[1]) {
				if (*(p + 2)  < translation[i].to[2])
					break;
				if (*(p + 2) == translation[i].to[2]) {
					if (TR_DIR  == tr ||
					    0 != translation[i].files) {
						*skip = 2;
						return translation[i].from;
					}
				}
			}
		}
	}
	return *p;
}


/*
 * @brief performs a translation to a compatible name for the server
 *
 * If there is a match in the translation table (to):
 * - in raw mode, this is an error (impossible character) then 0 is returned
 * - otherwise stores the translation and returns the translation size (3)
 *
 * If no match, stores the current character and returns 1 (size of char!)
 *
 * @param character to translate
 * @param (output) where to store the translation
 * @param type of translation to perform (TR_FILE or TR_DIR)
 * @return number of characters outputed
 */
size_t translate(char c, char *trans, enum translation_type tr)
{
	unsigned int i;

	for (i = 0; i < sizeof(translation) / sizeof(translation[0]) - 1; i++)
		if (c <= translation[i].from)
			break;
	if (c != translation[i].from ||
	    (TR_FILE  == tr && 0 == translation[i].files)) {
		*trans = c;
		return sizeof(char);
	}
	if (params1f.raw_names)
		return 0;
	*(trans    ) = translation[i].to[0];
	*(trans + 1) = translation[i].to[1];
	*(trans + 2) = translation[i].to[2];
	return sizeof(translation[0].to);
}

/*
 * @brief length of the json string once translated as specified
 *
 * The length takes into account escaped json characters that will be
 * 'unescaped' and special translations needed for files and directories
 * as specified for that call.
 *
 * @param json string (points on the leading ")
 * @param type of translation needed
 * @return length of the translated string
 */

size_t json_strlen(const char *json, enum translation_type tr)
{
	const char *p = json;
	size_t	s = 0, sz;

	if (NULL == p || '"' != *p)
		return JSON_STR_ERR;
	for (p = trans_skip_lead(p + 1, tr); '\0' != *p && '"' != *p; p++, s++)
	{
		if ('\\' == *p) {
			p++;
			if ('u' == *p) {
				char decode[4];
				sz = json_u_decode(p + 1, decode);
				if (0 == sz)
					return JSON_STR_ERR;
				p += 4;
				s += sz - 1;
				// TODO surrogate pairs
			}
			else
				if (NULL == strchr(json_escapes, *p))
					return JSON_STR_ERR;

		}
		else {
			if (trans_skip_trail(p, tr))
				break;
			rev_trans(p, &sz, tr);
			p += sz;
		}
	}
	return s;
}

/*
 * @brief copies the json string to destination with translation as specified
 *
 * Destination must be big enough for the copy. json_strlen with the same
 * translation will ensure that.
 *
 * @param json string (points on the leading ")
 * @param destination of the translation
 * @param type of translation needed
 * @return length of the translated string
 */

char *json_strcpy(const char *json, char *dest, enum translation_type tr)
{
	size_t sz;
	const char *p = json;

	if (NULL == p || '"' != *p)
		return NULL;
	p++;
	for (p = trans_skip_lead(p, tr); '\0' != *p && '"' != *p; p++, dest++)
	{
		if ('\\' == *p) {
			p++;
			switch (*p) {
				case '"' : *dest = '"';
					   break;
				case '\\': *dest = '\\';
					   break;
				case '/' : *dest = '/';
					   break;
				case 'b' : *dest = '\x08';
					   break;
				case 'f' : *dest = '\x0C';
					   break;
				case 'n' : *dest = '\x0A';
					   break;
				case 'r' : *dest = '\x0D';
					   break;
				case 't' : *dest = '\x09';
					   break;
				case 'u' : sz = json_u_decode(p + 1, dest);
					   if (0 == sz)
						return NULL;
					   p    += 4;
					   dest += sz - 1;
					   break;
				default	 : return NULL;
			}
		}
		else {
			if (trans_skip_trail(p, tr))
				break;
			*dest = rev_trans(p, &sz, tr);
			p += sz;
		}
	}
	*dest = '\0';
	return dest + 1;
}

/*
 * @brief check filename illegal spaces, chars, and length
 *
 * Both the API and the Web interface SILENTLY remove some whitespace (table
 * below) before/after the name. This behaviour being unsuitable for
 * a filesystem, when using a filename for creation, this function must be
 * called to check for leading/trailing "spaces".
 *
 * At the same time, it also checks the filename length and other forbidden
 * characters to avoid having to call the API if it is know it will fail.
 *
 * @param string to check
 * @param type of translation needed (TR_FILE or TR_DIR)
 * @return O or code ENAMETOOLONG (speaks by itself) or illegal spaces: EILSEQ
 */

/* Trimmed are characters from 0x09 (TAB) to 0x0D (LF) and 0x20 (Space: ' ') */

#define isforbidden(c) ((0x09 <= c && 0x0D >= c) || 0x20 == c)


static int forbidden_space(const char *str) {
	DEBUG(	"forbidden leading or trailing spaces in `%s`\n", str);
	return -EILSEQ;
}

int forbidden_chars(const char *str, enum translation_type tr)
{
	const char *p;
	size_t len, skip, sz;
	char buf[3];

	/* Check only the last part of the path, the other parts are tested
	 * by fuse before the current call, checking each part of path */
	p = strrchr(str, '/');
	if (NULL == p)
		p = str;
	else
		p++;

// TODO in NOT raw-names, should not start with head_repl

	if (isforbidden(*(p))) {
		if (params1f.raw_names)
			return forbidden_space(str);
		else
			len = 3;
	}
	else {
		len = 0;
	}

	for (; '\0' != *p; p++) {
		sz = translate(*p, buf, tr);
		if (0 == sz) {
			DEBUG(	"forbidden character in `%s` near %c (0x%02X)\n",
				str, *p, (int)*p);
			return -EILSEQ;
		}
		len += sz;
		if (!params1f.raw_names) {
			rev_trans(p, &skip, tr);
			if (0 != skip) {
				DEBUG(	"forbidden sequence in `%s`: %c%c%c\n",
					str, *p, *(p+1), *(p+2));
				return -EILSEQ;
			}
		}
	}

	/* Then check 'trailing space' */
	if (isforbidden(*(p - 1))) {
		if (params1f.raw_names)
			return forbidden_space(str);
		else
			len += 3;
	}
	if (MAX_FILENAME_LENGTH >= len)
		return 0;
	else
		return -ENAMETOOLONG;
}


/*
 * @brief translates and JSON-escapes a filename or directory name
 *
 * Copy the string into the buffer, translating and JSON escaping when
 * necessary, according to the translation pattern required.
 * The trailer string is then appended at the end.
 *
 * The buffer length must be MAX_ESCAPED_LENGTH + sizeof(trailer) to fit
 * the worst case scenario (all characters escaped to \unnnn).
 *
 * @param string to be tranlasted and escaped
 * @param buffer where the translation is copied
 * @param type of translation needed (TR_FILE or TR_DIR)
 * @param trailer to append.
 * @return none
 */

static void name_translate(const char *str, char *buf,
			   enum translation_type tr, const char *trailer)
{
	const char *p = str;
	/* Leading space, have been filtered out for raw name as EILSEQ */
	if (!params1f.raw_names)
		if (isforbidden(*p) ||
		    (p[0] == head_repl[0] &&
		     p[1] == head_repl[1] &&
		     p[2] == head_repl[2])) {
			buf[0] = head_repl[0];
			buf[1] = head_repl[1];
			buf[2] = head_repl[2];
			buf += 3;
		}


	/* Copy with escape and translation if needed */
	for (; '\0' != *p; p++)
		if (0 == (*p & 0x80) && 0x20 > *p) {
			sprintf(buf, "\\u%04x", (int)*p);
			buf += 6;
		}
		else if ( *p == '"' ) { /* \ is translated */
			buf[0] = '\\';
			buf[1] = '"';
			buf += 2;
		}
		else {
			if (params1f.raw_names)
				*buf++ = *p;
			else
				buf += translate(*p, buf, tr);
		}

	/* Trailing space */
	if (!params1f.raw_names)
		if (isforbidden(*(p - 1)) ||
		    (p - str >= 3 &&
		     p[-3] == head_repl[0] &&
		     p[-2] == head_repl[1] &&
		     p[-1] == head_repl[2])) {
			buf[0] = head_repl[0];
			buf[1] = head_repl[1];
			buf[2] = head_repl[2];
			buf += 3;
		}

	strcpy(buf, trailer);
}

static int cmp_files(const void *f1,const void *f2)
{
	return strcmp(((const struct file *)f1)->filename,
		      ((const struct file *)f2)->filename);
}

static void init_fs(struct file *fs)
{
	STORE(fs->flags, 0);
}

static int cmp_dirs(const void *d1,const void *d2)
{
	return strcmp(((const struct dir *)d1)->name,
		      ((const struct dir *)d2)->name);
}

static char *parse_check(char  *ptr, struct dir *d, const char * msg) {
	if (NULL == ptr)
		return NULL;
	if ( '[' == *ptr ) {
		*ptr = '\0';
		return ptr + 1;
	}
	else {
		lprintf(LOG_ERR,
			"Ignoring: %s of `%s` id=%"PRId64", is not an array!\n",
			msg, d->name, d->id);
		return NULL;
	}
}

static void parse_err(const char * pattern, struct dir *d)
{
	lprintf(LOG_ERR,
		"Ignoring: error parsing %s of `%s` id=%"PRId64"\n",
		pattern, d->name, d->id);
}

/* Character table of base64 as used in encfs */
static _Alignas(LEVEL1_DCACHE_LINESIZE) uint8_t base64_encode[64] =
{
/* 0x00 */	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
/* 0x08 */	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
/* 0x10 */	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
/* 0x18 */	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
/* 0x20 */	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
/* 0x28 */	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
/* 0x30 */	'w', 'x', 'y', 'z', '0', '1', '2', '3',
/* 0x38 */	'4', '5', '6', '7', '8', '9', ',', '-',
};

/* Character table to decode  */
static _Alignas(LEVEL1_DCACHE_LINESIZE) uint8_t base64_decode[128] =
{
/* 0x00 */	64,64,64,64,64,64,64,64,	64,64,64,64,64,64,64,64,
/* 0x10 */	64,64,64,64,64,64,64,64,	64,64,64,64,64,64,64,64,
/* 0x20 */	64,64,64,64,64,64,64,64,	64,64,64,64,62,63,64,64,
/* 0x30 */	52,53,54,55,56,57,58,59,	60,61,64,64,64,64,64,64,
/* 0x40 */	64, 0, 1, 2, 3, 4, 5, 6,	 7, 8, 9,10,11,12,13,14,
/* 0x50 */	15,16,17,18,19,20,21,22,	23,24,25,64,64,64,64,64,
/* 0x60 */	64,26,27,28,29,30,31,32,	33,34,35,36,37,38,39,40,
/* 0x70 */	41,42,43,44,45,46,47,48,	49,50,51,64,64,64,64,64,
};

#define B64_ILLEGAL (64)	    /* Illegal base64 char are coded 64 above */
#define IS_ENCFS_SPECIAL(name) ('\1' == *(name - 1))
#define P_ENCFS_HEADER(name)   (name - 1 - ENCFS_HEADER_SZ)

/*
 * @brief check if this is an encfs name, a special case, and get size
 *
 * When this is not a valid encfs (or encfs modified/special) name, false is
 * returned and neither size not special are assigned a value.
 *
 * @param filename, either zero terminated or a json string
 * @param where to return the size (if not NULL)
 * @param whether this is a special case (rewritten) (if not NULL)
 * @return true if it matches encfs or encfs+special pattern, false otherwise
 */
bool is_encfs_name(const char *name, size_t *size, bool *special)
{
	size_t sz;
	for(sz = 0 ; '\0' != *name && '"' != *name; sz++, name++) {
		/* Should not happen: 1fichier.com limits names to 250 bytes */
		if (MAX_FILENAME_LENGTH < sz)
			return false;
		/* must be a valid character in the table above */
		if (127 < (uint8_t)*name)
			return false;
		if (B64_ILLEGAL == base64_decode[(uint8_t)*name]) {
		/* '=' is illegal, unless it is the last char in which case
		 * it marks a "special" case where header was rewritten,
		 * provided the filename lenght is sufficient! */
			if ( '=' == *name && sz > ENCFS_HEADER_B64_SZ - 1 &&
			    ('"' == *(name + 1) || '\0' == *(name + 1)) ) {
				sz -= ENCFS_HEADER_B64_SZ - 1;
				break;
			}
			return false;
		}
	}
	if (0 == sz)
		return false;
	if (NULL != size)
		*size = sz;
	if (NULL != special)
		*special = ('=' == *name);
	return true;
}


static size_t json_value_length(const char *json, const char *key, char **val,
				enum translation_type tr)
{
	char *p;
	size_t sz;

	if (NULL != val)
		*val = NULL;
	p = json_find_key(json, key);
	if (NULL == p)
		return 0;
	sz = json_strlen(p, tr);
	if (NULL != val)
		*val = p + 1;
	return (JSON_STR_ERR == sz) ? 0 : sz;
}


static const char name_email_link[]= " on ";

static unsigned long parse_count(const char *s, const char *pattern1,
				 const char *pattern2, size_t add,
				 size_t *s_str, bool *f_refresh, bool *f_stat,
				 struct dir *d)
{
	unsigned long n;
	char *p, *q, *val;
	size_t t, sz = 0, r1 = 0;
	int s2 = 0;
	bool f_r = *f_refresh;

	/* For "special encfs rewrite names", filename are preceded by the
	 * 8 bytes of header plus '\1', other files have '\0' (which always
	 * requires 1 more char for 1st occurrence).
	 * The 12 chars ending chars are removed thus the filename is 3 bytes
	 * shorter : 12 - 8 + 1 (for '\1'). */

#define PARSING_FILES (0 == add) /* Code maintanability, when parsing files this
				    function is called with 0 for add */

	if (NULL == s)
		return 0;
	p = strstr(s, json_item_start);
	if (NULL == p)
		return 0;
	if (f_r)
		r1 = strlen(params1f.refresh_file);
#ifdef _STATS
	size_t r2 = 0;
	bool f_s = *f_stat;
	if (f_s)
		r2 = strlen(params.stat_file);
#endif
	n = 0;
	while(true) {
		q = strstr(p + sizeof(json_item_start), json_item_start);
		if (NULL != q)
			*q = '\0';
		t = json_value_length(p, pattern1, &val,
				      (PARSING_FILES) ? TR_FILE : TR_DIR);
		if (0 == t) {
			parse_err(pattern1, d);
			return 0;
		}

		if (PARSING_FILES && d->encfs) {
			bool special;
			if ('=' == val[t - 1] &&
			    is_encfs_name(val, NULL, &special) && special)
				s2 = ENCFS_HEADER_B64_SZ + 1 - ENCFS_HEADER_SZ;
			else
				s2 = (0 == sz) ? 1 : 0;
		}
		sz += t + 1 + s2;
		if (f_r && r1 == t
			&& 0 == strncmp(val, params1f.refresh_file, r1)) {
			lprintf(LOG_WARNING,
				"refresh triggger off: a file or directory `/%s` already exists in `%s`.\n",
				params1f.refresh_file, d->name);
			*f_refresh = f_r = false;
		}
#ifdef _STATS
		if (f_s && r2 == t
			&& 0 == strncmp(val, params.stat_file, r2)) {
			lprintf(LOG_WARNING,
				"statistics off: a file or directory `/%s` already exists in `%s`.\n",
				params.stat_file, d->name);
			*f_stat = f_s = false;
		}
#endif

		if (NULL != pattern2) {
			t = json_value_length(p, pattern2, NULL, TR_NONE);
			if (PARSING_FILES) {
				if (0 == t) {
					parse_err(pattern2, d);
					return 0;
				}
				sz += t + 1;
			}
			else {
				if (0 != t)
					sz += t + add;
			}
		}
		n++;
		if (NULL == q)
			break;
		*q = json_item_start[0];
		p = q;
	}
	*s_str = sz;
	return n;
}

enum path_match {
	NO_MATCH,
	KEEP_TESTING,
	MATCH,
};
/*
 * @brief matches a chunk of path with the list
 *
 * @param the directory to test
 * @param the list of paths
 * @param index where to start searching
 * @param length of path chnunks so far
 * @return the result
 */
static void match_chunk(struct dir *d, struct paths *ppaths,
			unsigned long *i, size_t *len)
{
	size_t l;

	if (NULL == d->parent) {
		/* End recursion at root */
		*i = 0;
		*len = 0;
		return;
	}
	else {
		match_chunk(d->parent, ppaths, i, len);
	}

	while (*i < ppaths->n) {
		l = eqlen(d->name, &ppaths->a[*i][*len]);
		if ( '\0' == d->name[l]) {
			/* name chunk fully matched */
			if ('\0' == ppaths->a[*i][*len + l]) {
				/* Full match, only happens on 1st recurs */
				*len += l;
				return;
			}
			if ('/' == ppaths->a[*i][*len + l]) {
				/* More chunks to test */
				*len += l + 1;
				return;
			}
		}
		if (d->name[l] > ppaths->a[*i][*len + l])
			*i += 1;
		else
			*i = ppaths->n;
	}
}

/*
 * @brief find a match for a directory in a list of paths
 *
 * The result is one of the enum.
 *
 * @param the directory to test
 * @param the list of paths
 * @return the result
 */

static enum path_match match_list(struct dir *d, struct paths *ppaths)
{
	unsigned long i;
	size_t len;

	match_chunk(d, ppaths, &i, &len);
	if (i < ppaths->n) {
		if ('\0' == ppaths->a[i][len])
			return MATCH;
		else
			return KEEP_TESTING;
	}

	return NO_MATCH;
}

/*
 * @brief initializes directory encfs and no_ssl flags
 *
 * This is an "expensive" test hence the test_encfs and test_no_ssl flags.
 * When those are false, it means no test is required: parent's inheritance OK.
 *
 * @param the directory
 * @return none
 */
static void set_folder_flags(struct dir *d)
{
	if (d->test_encfs) {
		switch(match_list(d, &params1f.encfs)) {
			case MATCH:	   DEBUG("encfs for: %s\n", d->name);
					   d->encfs = true; /* Fallthrough */
			case NO_MATCH:	   DEBUG("encfs stop: %s\n", d->name);
					   d->test_encfs = false;
			case KEEP_TESTING: break;	    /* Do nothing  */
		}
	}
	if (d->test_no_ssl) {
		switch(match_list(d, &params1f.no_ssl)) {
			case MATCH:	   DEBUG("no_ssl for: %s\n", d->name);
					   d->no_ssl = true; /* Fallthrough */
			case NO_MATCH:	   DEBUG("no_ssl stop: %s\n", d->name);
					   d->test_no_ssl = false;
			case KEEP_TESTING: break;	    /* Do nothing  */
		}
	}
}

/*
 * @brief initializes sub folders of a directory
 *
 * @param the start of json string where folders are
 * @param pointers to dentries of the directory that is being initialized
 * @param where to store folders names
 * @param the parent (directory being initialized)
 * @return true if all goes well, false on error condition-(
 */
static bool fill_folders(char *start, struct dentries *dent, char *s,
			 struct dir *parent)
{
	char *p, *q;
	struct dir *x;
	struct tm tm;
	bool is_root = (parent == &root);

	x = dent->subdirs;
	p = strstr(start, json_item_start);
	if (NULL == p) {
		lprintf(LOG_ERR, "unexpected condition in fill_folders.\n");
		return false;
	}
	while(true) {
		start = strstr(p + sizeof(json_item_start), json_item_start);
		if (NULL != start)
			*start = '\0';
		x->dent = NULL;
		pthread_mutex_init(&x->lookupmutex, NULL);
		x->id = 0;
		q = json_find_key(p, json_id);
		if (NULL == q) {
			lprintf(LOG_ERR, "could not find id for dir: %s\n",
				x->name);
			return false;
		}
		else {
			errno = 0;
			x->id = strtoll(q, NULL, 10);
			if (0 != errno) {
				lprintf(LOG_ERR,
					"error reading id for dir: %s\n",
					x->name);
				return false;
			}
		}
		x->name   = s;
		x->parent = parent;
		/* Subdir inherits flags of the parent by default */
		x->test_encfs	= parent->test_encfs;
		x->encfs	= parent->encfs;
		x->test_no_ssl	= parent->test_no_ssl;
		x->no_ssl	= parent->no_ssl;
		x->shared	= parent->shared;
		x->readonly	= parent->readonly;
		x->hidden	= parent->hidden;

		s = json_strcpy(json_find_key(p, json_name), s, TR_DIR);
		q = json_find_key(p, json_email);
		if (NULL == q) {
			if (is_root && 0 == strcmp(x->name, upload_d.name)) {
				x->readonly = true;
				x->name = upload_d.name;
			}
		}
		else {
			memcpy(s - 1,
			       name_email_link, lengthof(name_email_link));
			x->email_off = s - x->name + lengthof(name_email_link)
					 - 1;
			s = json_strcpy(q, x->name + x->email_off, TR_NONE);
			q = json_find_key(p, json_hide_links);
			x->shared = true;
			if (NULL != q && '1' == *q) {
				x->hidden   = true;
				x->readonly = true;
			}
			else {
				q = json_find_key(p, json_rw);
				if (NULL == q || '1' != *q)
					x->readonly = true;
			}
		}
		x->cdate = params.mount_st.st_ctim.tv_sec;

		/* Don't read create_date on root shares: they don't have one!*/
		if (!IS_SHARED_ROOT(x)) {
			q = json_find_key(p, json_cdate);
			if (NULL == q || '"' != *q) {
				lprintf(LOG_WARNING,
					"could not find create_date for dir: %s\n",
					x->name);
			}
			else {
				q= strptime(q + 1, "%Y-%m-%d %H:%M:%S", &tm);
				if ( NULL == q || '"' != *q) {
					lprintf(LOG_WARNING,
						"error reading create_date for dir: %s\n",
						x->name);
				}
				else {
					tm.tm_isdst = -1; /* mktime guess TZ */
					x->cdate = mktime(&tm);
				}
			}
		}
		/* Set no_ssl and encfs flag for this subdir */
		set_folder_flags(x);

		x++;
		if (NULL == start)
			break;
		*start = json_item_start[0];
		p = start;
	}

	qsort(dent->subdirs, dent->n_subdirs, sizeof(struct dir), cmp_dirs);

	return true;
}

/**
 * This is especially for encfs.
 * When renaming a file, encfs rewrites the first 8 bytes of the file.
 * Those 8 bytes will be converted to base 64, same as encfs does with ',' and
 * '-' as extra char, plus '=' at the end to mark it (encfs never puts '=')
 * This will be added to the file name (if there is enought space to do so,
 * otherwise rename will be denied)
 * So what is needed is a translation from 8 bytes to 12 char base64
 * These 2 functions provide that.
 *
 * The result is compatible with standard base64 encoding except that encfs uses
 * special characters , and - instead of + and / for RFC4648 since "/" will not
 * be suitable for filenames!
 * To recover the file, let ${src} be the name of it, and ${dest} where to copy

{ printf '%s' "${src}" | tail -c12 | sed 's/,/+/g;s/-/\//g' | base64 -d; tail -c+9 "${src}"; } >"${dest}"

 * For the filename, simply remove the last 12 characters of names ending with =
 * Note that to see names with ending '=' in a path via 1fichierfs, the mount
 * must not specify the encfs option for that path, otherwise the names will be
 * made correct for encfs removing the appended 12 characters in that path.
 *
 * This will generate a binary header for filename.
 * Files which name was altered could be filtered since they end with '=' which
 * is not a character encfs uses in filenames.
 */

/*
 * @brief codes the key into a characters base64 string
 *
 * @param pointer to the key to code
 * @param destination buffer, must be at least ENCFS_HEADER_B64_SZ chars big.
 * @return none
 */

void code64(const char *key, char *dest)
{
        /* Make all unsigned to avoid signed arithmetics pitfalls! */
        const uint8_t *k = (const uint8_t *)key;
        uint8_t *d = (uint8_t *)dest;

	/* Encoding 8 bytes, no loop */
	d[ 0] = base64_encode[ (k[0] & 0xFC) >> 2 ];
	d[ 1] = base64_encode[((k[0] & 0x03) << 4) + ((k[1] & 0xF0) >> 4)];
	d[ 2] = base64_encode[((k[1] & 0x0F) << 2) + ((k[2] & 0xC0) >> 6)];
	d[ 3] = base64_encode[  k[2] & 0x3F ];

	d[ 4] = base64_encode[ (k[3] & 0xFC) >> 2 ];
	d[ 5] = base64_encode[((k[3] & 0x03) << 4) + ((k[4] & 0xF0) >> 4)];
	d[ 6] = base64_encode[((k[4] & 0x0F) << 2) + ((k[5] & 0xC0) >> 6)];
	d[ 7] = base64_encode[  k[5] & 0x3F ];

	d[ 8] = base64_encode[ (k[6] & 0xFC) >> 2 ];
	d[ 9] = base64_encode[((k[6] & 0x03) << 4) + ((k[7] & 0xF0) >> 4)];
	d[10] = base64_encode[((k[7] & 0x0F) << 2) ];
        d[11] = '=';
}


/*
 * @brief decodes a 12 bytes base 64 string into an uint64_t (8 bytes)
 *
 * @param the source string to decode
 * @param pointer to the key (or 8 char bytes).
 * @return false if src was not a valid base64 string, true if all OK
 */

bool decode64(const char *src, char *key)
{
        int i;
        /* Make all unsigned to avoid sign arithmetics pitfalls! */
        const uint8_t *s = (const uint8_t *)src;
        uint8_t *k = (uint8_t *)key;

	/* Check junk */
	for (i = 0; i < ENCFS_HEADER_B64_SZ - 1; i++)
		if (127 < s[i] || B64_ILLEGAL == base64_decode[s[i]])
			return false;
        if ('=' != src[ENCFS_HEADER_B64_SZ - 1])
                return false;

	/* Decode, no loop same as above */
	k[0] =  (base64_decode[s[ 0]] << 2) +
	        (base64_decode[s[ 1]] >> 4) ;
	k[1] = ((base64_decode[s[ 1]] & 0x0F) << 4) +
	       ((base64_decode[s[ 2]] & 0x3C) >> 2) ;
	k[2] = ((base64_decode[s[ 2]] & 0x03) << 6) +
		 base64_decode[s[ 3]];

	k[3] =  (base64_decode[s[ 4]] << 2) +
	        (base64_decode[s[ 5]] >> 4) ;
	k[4] = ((base64_decode[s[ 5]] & 0x0F) << 4) +
	       ((base64_decode[s[ 6]] & 0x3C) >> 2) ;
	k[5] = ((base64_decode[s[ 6]] & 0x03) << 6) +
		 base64_decode[s[ 7]];

	k[6] =  (base64_decode[s[ 8]] << 2) +
	        (base64_decode[s[ 9]] >> 4) ;
	k[7] = ((base64_decode[s[ 9]] & 0x0F) << 4) +
	       ((base64_decode[s[10]] & 0x3C) >> 2) ;
        return true;
}


/*
 * @brief copy an encfs key
 *
 * @param destination of the key
 * @param source of the key
 * @return destination
 */
char *encfs_copy_key(char *key_dest, const char *key_src)
{
	if (NULL != key_dest && NULL != key_src) {
		*((uint64_t *)key_dest) = *((uint64_t *)key_src);
		return key_dest;
	}
	return NULL;
}

/*
 * @brief copy an encfs key
 *
 * ATTENTION, destination string size must be at least:
 *   	      strlen(filename) + ENCFS_HEADERB64_SZ + 1
 *
 * @param destination string,
 * @param raw file name
 * @param raw key to decode and append
 * @return destination
 */
char *encfs_appendb64_key(char *dest, const char *filename, const char *key)
{
	if (NULL != dest && NULL != filename && NULL != key) {
		char *p;
		p = stpcpy(dest, filename);
		code64(key, p);
		p[ENCFS_HEADER_B64_SZ] = '\0';
		return dest;
	}
	return NULL;
}


static bool fill_files( char *start, struct dentries *dent, char *s,
			struct dir *d)
{
	char *p, *q;
	struct file *x;
	struct tm tm;
	bool first = true, spec;
	size_t sz;

	if (NULL == start)
		return true;

	for (x = dent->files;
	     NULL != (p = strstr(start, json_item_start));
	     x++, start = p + sizeof(json_item_start)) {
		init_fs(x);
		if (d->hidden) {
			x->URL = hidden_url;
		}
		else {
			x->URL = s;
			s = json_strcpy(json_find_key(p, json_url), s, TR_NONE);
		}
		q = json_find_key(p, json_filename);
		if (d->encfs && is_encfs_name(q + 1, &sz, &spec) && spec) {
			q += 1;
			decode64(q + sz, s);
			s += ENCFS_HEADER_SZ;
			*s++ = '\1';
			x->filename = s;
			memmove(s, q, sz);
			s += sz;
			*s++ = '\0';
			first = false;
		}
		else {
			if (d->encfs && first) {
				first = false;
				*s++ = '\0';
			}
			x->filename = s;
			s = json_strcpy(q, s, TR_FILE);
		}
		q = json_find_key(p, json_size);
		if (NULL == q) {
			lprintf(LOG_ERR,
				"could not find size for file: %s\n",
				x->filename);
			return false;
		}
		else {
			errno = 0;
			x->size = strtoll(q, NULL, 10);
			if (0 != errno) {
				lprintf(LOG_ERR,
					"error reading size for file: %s\n",
					x->filename);
				return false;
			}
		}
		q = json_find_key(p, json_date);
		if (NULL == q || '"' != *q) {
			lprintf(LOG_ERR,
				"could not find date for file: %s\n",
				x->filename);
			return false;
		}
		else {
			q= strptime(q + 1, "%Y-%m-%d %H:%M:%S", &tm);
			if ( NULL == q || '"' != *q) {
				lprintf(LOG_ERR,
					"error reading date for file: %s\n",
					x->filename);
				return false;
			}
			else {
				tm.tm_isdst = -1; /* let mktime guess TmZone */
				x->cdate = mktime(&tm);

			}
		}
	}

	return true;
}

static void parse_all(struct dir *d, struct json *j, struct dentries **dent)
{
	char    *subdirs = NULL,  *files = NULL;
	size_t s_subdirs = 0   , s_files = 0;
	char *s;
	const char *url_pattern;
	unsigned long n_subdirs, n_files, i, k;
	bool f_refresh, f_stat, f_shadowed;

	f_refresh = (&root == d	 && NULL != params1f.refresh_file);
#ifdef _STATS
	f_stat    = (&root == d	 && NULL != params.stat_file);
#else
	f_stat    = false;
#endif

	if (NULL != j) {
	/* Removing text inside "shares" to avoid fooling the algorithm.
	 * Assuming there is no ']' in shares. The only place it could
	 * be is in the e-mail string. ']' is allowed in an e-mail with
	 * restrictions so at the moment this will fail and is a //TODO
	 * to be merged with "use a real JSON parser!". */
		s = j->pb;
		while (NULL != (s = json_find_key(s, json_shares))) {
			if ( '[' != *s++ )
				continue;
			while (']' != *s && '\0' != *s)
				*s++ = ' ';
		}

		subdirs = json_find_key(j->pb, json_sub_folders);
		files	= json_find_key(j->pb, json_items);
		subdirs = parse_check(subdirs, d, json_sub_folders);
		files	= parse_check(files  , d, json_items);
	}

	n_subdirs = parse_count(subdirs, json_name, json_email,
				lengthof(name_email_link),
				&s_subdirs, &f_refresh, &f_stat, d);
	if (d->hidden)
		url_pattern = NULL;
	else
		url_pattern = json_url;
	n_files	  = parse_count(files  , json_filename, url_pattern, 0,
				&s_files  , &f_refresh, &f_stat, d);

	if (f_refresh) {
		if (params1f.refresh_hidden)
			f_refresh = false;
		else
			n_files++;
	}
	if (f_stat)
		n_files++;

	*dent = malloc(sizeof(struct dentries) + s_subdirs + s_files +
		       sizeof(struct dir) * n_subdirs +
		       sizeof(struct file) * n_files);

	(*dent)->n_subdirs = n_subdirs;
	(*dent)->n_files   = n_files;
	(*dent)->subdirs   = (struct dir *)&(*dent)->files[n_files];

	s = (char *)(&(*dent)->subdirs[n_subdirs]);
	if (0 != (*dent)->n_subdirs &&
	    ! fill_folders(subdirs, *dent, s, d))
		(*dent)->n_subdirs = 0;

	if (0 != (*dent)->n_files) {
		if ( fill_files(files, *dent, s + s_subdirs, d) ) {
			i = n_files - ((f_refresh) ? 1:0) - ((f_stat) ? 1:0);
			if (f_refresh) {
				init_fs(&(*dent)->files[i]);
				(*dent)->files[i].size = 0;
				(*dent)->files[i].cdate =
						params.mount_st.st_ctim.tv_sec;
				(*dent)->files[i].URL =
				(*dent)->files[i].filename =
						(char *)params1f.refresh_file;
				i++;
			}
#ifdef _STATS
			if (f_stat) {
				init_fs(&(*dent)->files[i]);
				(*dent)->files[i].size = STAT_MAX_SIZE;
				(*dent)->files[i].cdate =
						params.mount_st.st_ctim.tv_sec;
				(*dent)->files[i].URL = NULL;
				(*dent)->files[i].filename =
						(char *)params.stat_file;
			}
#endif
			qsort((*dent)->files, (*dent)->n_files,
			      sizeof(struct file), cmp_files);

		}
		else {
			(*dent)->n_files = 0;
		}
	}

	/* Deduplication of entries
	 * flags is used non-atomically because 'flagged' elements will be
	 * removed in the same function since find_path does not expect
	 * duplicated names */
	f_shadowed = false;  /* Indicates whether some files are "shadowed" */

	/* Mark as DEL files having the name of a directory */
	i = k = 0;
	while (i < (*dent)->n_subdirs && k < (*dent)->n_files) {
		int cmp;
		cmp =  strcmp((*dent)->subdirs[i].name,
			      (*dent)->files[k].filename );
		if (0 == cmp) {
			f_shadowed = true;
			(*dent)->files[k].flags = FL_DEL;
			lprintf(LOG_NOTICE,
				"in parent directory %s (%llu), file %s shadowed by a directory.\n",
				d->name, d->id, (*dent)->files[k].filename);
			k++;
		}
		else {
			if (cmp > 0)
				k++;
			else
				i++;
		}
	}

	/* Mark as DEL duplicated files in the directory */
	i = 0;
	k = 1;
	while (k < (*dent)->n_files) {
		if (0 != LOAD((*dent)->files[k].flags, relaxed)) {
			k++;
			continue;
		}
		if ((*dent)->files[i].flags != FL_DEL &&
		    0 ==  strcmp((*dent)->files[i].filename,
				 (*dent)->files[k].filename )) {
			f_shadowed = true;
			lprintf(LOG_NOTICE,
				"in parent directory %s (%llu), duplicated filename %s, keeping most recent.\n",
				d->name, d->id, (*dent)->files[i].filename);
			/* Identical names, keep more recent */
			if ((*dent)->files[i].cdate > (*dent)->files[k].cdate) {
				(*dent)->files[k].flags = FL_DEL;
				k++;
				continue;
			}
			(*dent)->files[i].flags = FL_DEL;
		}
		i = k;
		k = i + 1;
	}

	/* Removing duplicates from the file list */
	if (f_shadowed) {
		i = 0;
		k = 0;
		while (i < (*dent)->n_files) {
			if ((*dent)->files[i].flags == FL_DEL) {
				k++;
			}
			else {
				if (0 != k)
					memmove(&(*dent)->files[i - k],
						&(*dent)->files[i]    ,
						sizeof(struct file));
			}
			i++;
		}
		(*dent)->n_files -= k;
	}

	STORE(d->dent, *dent);
}

/*
 * @brief utility to initialise a curl handle
 *
 * The handle is initialised and populated according to the global parameters
 *
 * //TODO better signal handler? see: https://curl.se/mail/lib-2018-12/0076.html
 *
 * @param none
 * @return the initialised curl handle
 */
CURL *curl_init(bool is_http)
{
	CURL *curl;

	curl = curl_easy_init();
	if (NULL == curl)
		lprintf( LOG_CRIT, "initializing curl easy handle.\n" );
	CURL_EASY_SETOPT(curl, CURLOPT_IPRESOLVE, params1f.curl_IP_resolve
			     , "%ld");
	if (is_http && NULL != params.user_agent)
		CURL_EASY_SETOPT(curl, CURLOPT_USERAGENT, params.user_agent,
				 "%s");
	if (params1f.insecure)
		CURL_EASY_SETOPT(curl, CURLOPT_SSL_VERIFYPEER, 0, "%ld");
	if (NULL != params.ca_file)
		CURL_EASY_SETOPT(curl, CURLOPT_CAINFO, params.ca_file, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPALIVE, 1L,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPIDLE, CURL_KEEPALIVE_TIME,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPINTVL,CURL_KEEPALIVE_TIME,"%ld");
//	CURL_EASY_SETOPT(curl, CURLOPT_NOSIGNAL, 1L,"%ld"); /* SIGPIPE ignored*/
	CURL_EASY_SETOPT(curl, CURLOPT_CONNECTTIMEOUT, CURL_CNX_TIMEOUT,"%ld");

	return curl;
}

static enum hCode curl_json_perform_wrapped(CURL *curl, struct json *j,
					    enum api_routes rt,
					    const char *name_msg)
{
	static const char try_again_message[] = "File unavailable";
	static const char ftp_trigger_info[]  = "Not in manual FTP mode";
	CURLcode res;
	char *p;
	long http_code;

	res = curl_easy_perform(curl);

	j->pb[j->cb]='\0';
	if (CURLE_OK != res) {
		if (!init_done &&
		    ( CURLE_COULDNT_RESOLVE_HOST  == res ||
		      CURLE_COULDNT_RESOLVE_PROXY == res ||
		      CURLE_COULDNT_CONNECT       == res)
		    )
			return hCnxErr;
		lprintf(LOG_ERR,
			"Ignoring: error %ld on curl_easy_perform url=`%s` name=`%s`.\n",
			res, api_urls[rt], name_msg);
		return hEIO;
	}

	CURL_EASY_GETINFO(curl, CURLINFO_RESPONSE_CODE, &http_code);
	if (HTTP_OK != http_code) {
		lprintf(LOG_ERR,
			"Ignoring: (http_code: %ld) url=`%s` name=`%s`.\n",
			http_code, api_urls[rt], name_msg);
		switch (http_code) {
			case HTTP_NOT_FOUND:		return hNotFound;
			case HTTP_TOO_MANY_REQUESTS:	return hTooManyRq;
			case HTTP_FORBIDDEN:		return hForbidden;
			default:			return hEIO;
		}
	}

	p = json_find_key(j->pb, json_status);
	if (NULL == p ||
	    0 != strncmp(p, json_OK, lengthof(json_OK))) {
		enum hCode ret = hOk;
		/* The FTP trigger message is not an error, it was confusing
		 * to log it as such: @Jarodd on forum.ubuntu-fr.org */
		p = json_find_key(j->pb, json_message);
		if (NULL == p)
			ret = hEIO;
		else if (FTP_PROCESS == rt &&
			 0 == strncmp(p + 1, ftp_trigger_info
					   , lengthof(ftp_trigger_info)))
			ret = hOk;
		else if (DOWNLOAD_GET_TOKEN == rt &&
			 0 == strncmp(p + 1, try_again_message
					   , lengthof(try_again_message)))
			ret = hBusy;
		else
			ret = hEIO;
		lprintf(((ret == hEIO) ? LOG_ERR : LOG_INFO),
			"Ignoring: status is NOT OK, url=`%s` name=`%s` response=%s.\n",
			api_urls[rt], name_msg, j->pb);
		return ret;
	}
	return hOk;
}

enum hCode curl_json_perform(char *postdata, struct json *j,
			     enum api_routes rt, const char *name_msg)
{
	unsigned long i_API_call;
	enum hCode res;
	unsigned int retry, network_wait;
	bool too_many_rq = false;
	bool locked = false;
	CURL *curl;
	struct curl_slist *list;

	curl = curl_init(true);
	list = curl_slist_append(NULL, auth_header);
	if (NULL != list)
		list = curl_slist_append(list,
					  "Content-Type: application/json");
	if (NULL == list)
		lprintf(LOG_CRIT, "building headers (curl_slist_append).\n");

	CURL_EASY_SETOPT(curl, CURLOPT_HTTPHEADER, list, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, get_json_response, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_WRITEDATA, (void *)j, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_POSTFIELDS, postdata, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_URL, api_urls[rt], "%s");

#ifdef _STATS
	struct timespec start;
	if (NULL != params.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);
#endif

	i_API_call = ADD(n_API_calls, 1L, relaxed) + 1L;
	lprintf(LOG_INFO, "<<< API(in) (iReq:%lu) %s POST=%s name=%s\n"
			  , i_API_call
			  , api_urls[rt] + lengthof(url_api_ep)
			  , postdata
			  , name_msg);

	too_many_rq = LOAD(throttled);
	if (too_many_rq)
		lock(&apimutex, &locked);

	retry = network_wait = 0;
	while(1) {
		res = curl_json_perform_wrapped(curl, j, rt, name_msg);
		if (hTooManyRq == res) {
			if (!too_many_rq) {
				too_many_rq = true;
				STORE(throttled, true);
				lock(&apimutex, &locked);
			}
			json_free(j);
			json_init(j);
			retry++;
			if (retry >= MAX_RETRY)
				break;
			usleep(retry_delay_ms[retry]);
		}
		else {
			if (hCnxErr == res) {
				if (network_wait < params1f.network_wait) {
					usleep(NETWORK_WAIT_INTVL * 1000000);
					network_wait += NETWORK_WAIT_INTVL;
					continue;
				}
				else {
					lprintf(LOG_ERR,
						"Could not connect to network at startup.\n");
					break;
				}
			}
			if (too_many_rq && 0 == retry)
				STORE(throttled, false);
			break;
		}
	}
	unlock(&apimutex, &locked);
	if (!init_done && 0 != network_wait)
		lprintf(LOG_NOTICE,
			"Waited %u seconds for network at startup.\n",
			network_wait);
#ifdef _STATS
	if (NULL != params.stat_file)
		update_api_stats(rt, &start, res, retry);
#endif
	lprintf(LOG_INFO, ">>> API(out) (iReq:%lu) retry=%u json size=%lu hCode=%d\n"
			  , i_API_call
			  , retry
			  , j->cb
			  , (int)res);

	curl_slist_free_all(list);
	curl_easy_cleanup(curl);
	return res;
}

static bool init_dir_done(struct dir *d, struct dentries **dent,
			  memory_order order)
{
	*dent = atomic_load_explicit(&d->dent, order);
	if (NULL == *dent)
		return false;
	return true;
}

/*
 * @brief initializes a directory with files and folders
 *
 * The **dent parameter is an optimisation. Because it must be accessed
 * atomically, and those atomic operations are done here, it avoid the
 * caller to do atomic loads when the dentries pointers is needed after init.
 *
 * @param the directory to be initialized
 * @param pointers to dentries of that directory
 * @return none
 */
static void init_dir(struct dir *d, struct dentries **dent)
{
	static const char ls_post[] = "{\"folder_id\":%lld,\"files\":1}";
	static const char ls_shared[] =
		"{\"folder_id\":0,\"sharing_user\":\"%s\",\"files\":1}";
	enum hCode res;
	struct json j;

	if (init_dir_done(d, dent, memory_order_acquire))
		return;

	DEBUG("init_dir: `%s` (id=%"PRId64")\n", d->name, d->id);

	lock(&d->lookupmutex, NULL);
	/* In case another process simultaneously initialises the same dir
	 * we must now look again at pointers. Since we are just behind
	 * a lock relaxed is enough here.
	 */
	if (init_dir_done(d, dent, memory_order_relaxed)) {
		DEBUG("init_dir already done: `%s`\n", d->name);
		unlock(&d->lookupmutex, NULL);
		return;
	}
	json_init(&j);
	if (IS_SHARED_ROOT(d)) {
		char buf[sizeof(ls_shared) + strlen(d->name) ];
		snprintf(buf, sizeof(buf), ls_shared, SHARER_EMAIL(d));

		res = curl_json_perform(buf, &j, FOLDER_LS, d->name);
	}
	else {
		char buf[sizeof(ls_post) + STRLEN_MAX_INT64];
		snprintf(buf, sizeof(buf), ls_post, d->id);

		res = curl_json_perform(buf, &j, FOLDER_LS, d->name);
	}
	parse_all(d, ((hOk == res) ? &j : NULL), dent);
	json_free(&j);
	if (0 != params1f.refresh_time && &root == d) {
		struct timespec	  now;
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		STORE(refresh_start, now.tv_sec, relaxed);
	}
	unlock(&d->lookupmutex, NULL);

#ifdef _DEBUG
	if (LOG_DEBUG <= params.log_level) {
		char buf[32];
		char hint;
		int i;
		DEBUG(	"`%s` has %ld subdirs and %ld files\n",
			d->name,
			(*dent)->n_subdirs,
			(*dent)->n_files );
		for (i = 0; i< (*dent)->n_subdirs; i++) {
			ctime_r(&(*dent)->subdirs[i].cdate, buf);
			buf[24]='\0';
			if ((*dent)->subdirs[i].shared)
				if ((*dent)->subdirs[i].hidden)
					if ((*dent)->subdirs[i].readonly)
						hint = 'E';
					else
						hint = 'H';
				else
					if ((*dent)->subdirs[i].readonly)
						hint = 'R';
					else
						hint = 'W';
			else
				hint = ' ';

			DEBUG( "%03d: [%c% 11lld|%s] %s\n",
				i + 1,
				hint,
				(*dent)->subdirs[i].id,
				buf,
				(*dent)->subdirs[i].name);
		}
		for (i = 0; i< (*dent)->n_files; i++) {
			ctime_r(&(*dent)->files[i].cdate, buf);
			buf[24]='\0';
			DEBUG(	"%03d: [% 12"PRId64"|%s] %s => %s\n",
				i + 1,
				(*dent)->files[i].size,
				buf,
				(*dent)->files[i].filename,
				(*dent)->files[i].URL);
		}
	}
#endif
}

static void free_cur_live_dump_new(struct lfiles *old, struct lfiles *new,
				   const char *where)
{
	if (NULL != old) {
		DEBUG(">> %s: rcu file array pointer %p\n", where, old);
		rcu_free_ptr(old);
	}
	if (NULL == new) {
		DEBUG(">> %s: new live list is now empty\n", where);
		return;
	}
#ifdef _DEBUG
	else {
		unsigned long i;
		for (i=0; i < new->n_lfiles; i++) {
			unsigned long cnt;
			cnt = LOAD(new->a_lfiles[i]->counter);
			DEBUG(	">> %s: new live list[%lu] %s (%lX) >%lu\n",
				where,
				i,
				((new->a_lfiles[i]->hidden) ?
					new->a_lfiles[i]->path		 :
					new->a_lfiles[i]->URL
				),
				cnt,
				new->a_lfiles[i]->loc_until);
		}
	}
#endif
}

/* For statistics, perserve number of locations when freing a streamer */
static _Atomic unsigned long n_locations = 0;

/*
 * Getter of the global location counter of freed streamer (historical stat)
 * "relaxed" is ok for statistics!
 */
unsigned long get_stats_glob_locations()
{
	return LOAD(n_locations, relaxed);
}

/*
 * Specific rcu cleaner functions for lfile
 */
static void rcu_free_lfile(struct rcu_item *head)
{
	int res;
	struct lfile *s = rcu_get_p_struct_from_item(head);

	DEBUG(	">> RCU and free lfile %p(->%p): %s (%lX).\n",
		s, s->location, s->URL, LOAD(s->counter));

	free(s->location);
	res = pthread_mutex_destroy(&s->loc_mutex);
	if (0 != res)
		lprintf(LOG_CRIT, "error: %d on pthread_mutex_destroy.\n", res);
	ADD(n_locations, s->nb_loc, relaxed);
	free(s);
}

static void free_lfile(struct lfile *s)
{
	rcu_free_struct(s, rcu_free_lfile);
}

/* Available storage need not be refreshed for operations that
 * do not change it. According to 1fichier's answer, these are:
 * rename/move and create/delete directory. Hence in these operation
 * this function is called with the refresh_storage flag to false.
 *
 * Refresh also cleans potential lfiles with the ERROR flag on.
 */

static void refresh(struct dir *d, enum refresh_cause cause, bool ref_stor)
{
	unsigned long	spin = 0;
	struct dentries *dent;
	struct lfiles *cur, *new;
	unsigned long	i, j, counter;

	update_refresh_stats(cause);

	dent = AND(d->dent, 0);
	if (NULL != dent) {
		lprintf(LOG_INFO, ">> Refreshing on %s %s=%p.\n"
				  , refresh_msgs[cause], d->name, dent);
		rcu_free_struct(dent, rcu_free_dentries);
	}
	if (ref_stor) {
		lock(&storage_mutex, NULL);
		storage_flags |= FL_STOR_REFRESH;
		unlock(&storage_mutex, NULL);
	}

	cur = LOAD(live);
	do {
		if (0 != spin)
			free(new);
		spin++;
		if (NULL == cur)
			return;
		for (i = j = 0; i < cur->n_lfiles; i++) {
			counter = LOAD(cur->a_lfiles[i]->counter);
			if (0 == (counter & OPEN_MASK) &&
			    0 != (counter & FL_ERROR))
				j++;
		}
		if (0 == j)
			return;
		new = malloc(sizeof(struct lfiles) +
			     sizeof(void *) * (cur->n_lfiles - j));
		for (i = j = 0; i < cur->n_lfiles; i++) {
			counter = LOAD(cur->a_lfiles[i]->counter);
			if (0 == (counter & OPEN_MASK) &&
			    0 != (counter & FL_ERROR))	   {
				/* When a lfile is marked as error, the first
				 * refresh will delete it. Unlikely we have
				 * several refreshes in parallel in which case
				 * if FL_DEL is already marked, we just continue
				 * because it means another parallel refresh
				 * just did DEL/call_rcu
				 */
				unsigned long new_counter;

				new_counter = counter & FL_DEL;
				if (unlikely(new_counter == counter))
					continue;
				if (XCHG(cur->a_lfiles[i]->counter,
					 counter,
					 new_counter))
					free_lfile(cur->a_lfiles[i]);
			}
			else {
				if (0 == (counter & FL_DEL))
					new->a_lfiles[j++] = cur->a_lfiles[i];
			}

		}
		/* Note that there could be more nodes in error that what we
		 * counting initially if error happen in parallel of refresh
		 * Hence we always alloc, and free if necessary.
		 */
		if (0 == j) {
			free(new);
			new = NULL;
		}
		else {
			new->n_lfiles = j;
		}
	} while(!XCHG(live, cur, new));
	spin_add(spin -1, "refresh");
	free_cur_live_dump_new(cur, new, "Refresh");
}

/* Avoids have to expose bot refresh and root for global refresh.
 */
void refresh_root(enum refresh_cause cause)
{
	refresh(&root, cause, true);
}

static int find_dir(const void *key,const void *dir)
{
	int res;
	const struct pbcb *k = (const struct pbcb *)key;
	const struct dir  *d = (const struct dir *)dir;

	res =  strncmp( k->pb, d->name, k->cb );
	if (0 != res)
		return res;
	if ('\0' == d->name[k->cb])
		return 0;
	else
		return -1;
}

static int find_file(const void *key,const void *file)
{
	int res;
	const struct pbcb *k = (const struct pbcb *)key;
	const struct file *f = (const struct file *)file;

	res =  strncmp( k->pb, f->filename, k->cb );
	if (0 != res)
		return res;
	if ('\0' == f->filename[k->cb])
		return 0;
	else
		return -1;
}

static void timed_refresh()
{
	struct timespec	  now;
	unsigned long	start;

	start = LOAD(refresh_start);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	if (now.tv_sec - start >= params1f.refresh_time &&
	    XCHG(refresh_start, start, now.tv_sec))
	/* If atomic exchange fails, it means another thread saw the
	 * the timeout and already refreshed, so we don't do it twice!
	 */
		refresh(&root, REFRESH_TIMER, true);
}

static bool refresh_for_write(struct dir *d, long long id)
{
	unsigned long i;
	struct dentries *dent;

	if (NULL == d || (&upload_d == d && def_root == root.name))
		d = &root;
	if (id == d->id) {
		refresh(d, REFRESH_WRITE, true);
		return true;
	}
	dent =  LOAD(d->dent);
	if (NULL == dent)
		return false;
	for (i = 0; i < dent->n_subdirs; i++)
		if (refresh_for_write(&dent->subdirs[i], id))
			return true;
	return false;
}


void find_path(const char *path, struct walk *w)
{
	const char *p, *q;
	struct dir *d = &root;
	struct dentries *dent;
	struct pbcb key;

	DEBUG("find_path: `%s`\n", path);

	if ((NULL == params1f.refresh_file ||
	     0 != strcmp(path + 1, params1f.refresh_file)) &&
	     0 != params1f.refresh_time)
		timed_refresh();

	w->is_dir  = true;
	w->is_wf   = false;
	w->res.dir = NULL;
	w->parent  = NULL;
	if ('\0' == path[1]) { /* This is only true when path is '/' */
		w->res.dir = &root;
		w->parent  = &root;
		return;
	}
	for (p = path + 1; NULL != (q = strchr(p, '/')); p = q + 1) {
		key.pb = (char *)p;
		key.cb = q - p;
		init_dir(d, &dent);
		d = bsearch(&key, dent->subdirs, dent->n_subdirs
				, sizeof(struct dir), find_dir);
		if (NULL == d)
			return;
	}
	key.pb = (char *)p;
	key.cb = strlen(p);
	w->parent = d;
	init_dir(d, &dent);
	w->res.dir = bsearch(&key, dent->subdirs, dent->n_subdirs
				 , sizeof(struct dir), find_dir);
	if (NULL == w->res.dir) {
		w->is_dir = false;
		w->res.fs = bsearch(&key, dent->files, dent->n_files
					, sizeof(struct file), find_file);
		if (NULL != w->res.fs && F_DEL(w->res.fs))
			w->res.fs = NULL;
		/* Now search in the temporary files being written, not an
		 * 'else' from previous F_DEL case because a file might
		 * have been deleted and written as a new file. */
		if (NULL == w->res.fs)
			write_find(p, w);
	}
}

struct dir *refresh_upload(const char *path, struct dentries **dent)
{
	struct walk w;

	if (def_root == root.name) {
		find_path(path, &w);
		if (!w.is_dir || NULL == w.res.dir)
			return NULL;
		refresh(w.res.dir, REFRESH_POLLING, false);
		find_path(path, &w);
		if (!w.is_dir || NULL == w.res.dir)
			return NULL;
		init_dir(w.res.dir, dent);
		return w.res.dir;
	}
	else {
		*dent = AND(upload_d.dent, 0);
		lprintf(LOG_INFO, ">> Refreshing on %s %s=%p.\n"
				, refresh_msgs[REFRESH_POLLING]
				, upload_d.name
				, *dent);
		rcu_free_struct(*dent, rcu_free_dentries);
		update_refresh_stats(REFRESH_POLLING);
		init_dir(&upload_d, dent);
		return &upload_d;
	}
}

static int find_filename(const void *filename,const void *file)
{
	const struct file *f = (const struct file *)file;

	return strcmp(filename, f->filename);
}

const char *find_file_upload(struct dir *d, const char *filename)
{
	struct file *f;
	struct dentries *dent;

	init_dir(d, &dent);

	f = bsearch(filename, dent->files, dent->n_files
			    , sizeof(struct file), find_filename);
	if (NULL == f)
		return NULL;
	return f->URL;
}

const char * list_subdir_upload(struct dentries *dent, unsigned long *i,
				long long *subdir_id, time_t *cdate)
{
	unsigned long idx = *i;
	if (idx >=  dent->n_subdirs)
		return NULL;
	*i += 1;
	*cdate	   = dent->subdirs[idx].cdate;
	*subdir_id = dent->subdirs[idx].id;
	return dent->subdirs[idx].name;
}

const char * list_file_upload(struct dentries *dent, unsigned long *i,
			      const char **URL, time_t *cdate)
{
	unsigned long idx = *i;
	if (idx >=  dent->n_files)
		return NULL;
	*i += 1;
	*cdate = dent->files[idx].cdate;
	*URL   = dent->files[idx].URL;
	return dent->files[idx].filename;
}

/*
 * @brief check if write is forbidden in the directory.
 *
 * @param dir struct pointer of the directory to check
 * @param whether to log a message
 * @param file found to check age and parent.
 * @return true is write is forbidden (false otherwise)
 */

bool forbidden_write(struct dir * d, bool msg, struct walk *w)
{
	if (d->readonly) {
	/* It is forbidden to write when the parent directory is readonly:
	 * share or the upload directory */
		if (NULL != w &&
		    get_upload_id() == w->parent->id &&
		    !w->is_dir && !w->is_wf) {
			/* For the upload directory, moving or deleting
			 * "orphaned" files is allowed as long as they
			 * are old enough */
			struct timespec now;

			clock_gettime(CLOCK_REALTIME_COARSE, &now);
			if (now.tv_sec >= w->res.fs->cdate +
					  params1f.resume_delay)
				return false;
		}
		if (msg)
			lprintf(LOG_WARNING,
				"no write access on protected directory: `%s`.\n",
				d->name);
		return true;
	}
	return false;
}



static int unfichier_getattr_wrapped(const char *path, struct stat *stbuf)
{
	struct walk w;

	find_path(path, &w);

	if (NULL == w.res.fs) {
		if (params1f.refresh_hidden &&
		    NULL != params1f.refresh_file && &root == w.parent &&
		    0 == strcmp(path + 1, params1f.refresh_file))
			refresh(&root, REFRESH_HIDDEN, true);
		memset(stbuf, 0, sizeof(struct stat));
		return -ENOENT;
	}

	memcpy(stbuf, &params.mount_st, sizeof(struct stat));
	if (w.is_dir) {
		stbuf->st_atim.tv_sec =
		stbuf->st_ctim.tv_sec =
		stbuf->st_mtim.tv_sec = w.res.dir->cdate;
		if (forbidden_write(w.res.dir, false, NULL)) {
			stbuf->st_mode &= ~(S_IWUSR | S_IWGRP | S_IWOTH);
		}
	}
	else {
		stbuf->st_mode		= st_mode &
					  ~(S_IWUSR | S_IWGRP | S_IWOTH);
		stbuf->st_nlink		= 1;
		if (w.is_wf) {
			write_getattr(stbuf, (struct wfile *)w.res.fs);
		}
		else {
			stbuf->st_size	 = w.res.fs->size;
			stbuf->st_atim.tv_sec =
			stbuf->st_ctim.tv_sec =
			stbuf->st_mtim.tv_sec = w.res.fs->cdate;
		}
		stbuf->st_blocks = stbuf->st_size / ST_BLK_SZ;
	}
	return 0;
}

static int unfichier_getattr(const char *path, struct stat *stbuf)
{
	int res;

	DEBUG("getattr: `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_getattr_wrapped(path, stbuf);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}


static int unfichier_readdir_wrapped(const char *path, void *buf,
				     fuse_fill_dir_t filler, off_t offset,
				     struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;
	struct walk w;
	unsigned long i;
	struct dentries *dent;

	find_path(path, &w);
	if (NULL == w.res.dir || !w.is_dir)
		return -ENOENT;

	init_dir(w.res.dir, &dent);

	if (0 != filler(buf, ".", NULL, 0))
		return -EBADF;
	if (0 != filler(buf, "..", NULL, 0))
		return -EBADF;

	for (i = 0; i < dent->n_subdirs; i++)
		if ( 0 != filler(buf, dent->subdirs[i].name, NULL, 0))
			return -EBADF;
	for (i = 0; i < dent->n_files; i++)
		if (!F_DEL(&dent->files[i]) &&
		    !is_fuse_hidden(dent->files[i].filename))
			if (0 != filler(buf, dent->files[i].filename, NULL, 0))
				return -EBADF;

	/* Now add the temporary files being written */
	return write_readdir(w.res.dir->id, path, buf, filler);
}

static int unfichier_readdir(const char *path, void *buf,
			     fuse_fill_dir_t filler, off_t offset,
			     struct fuse_file_info *fi)
{
	int res;

	DEBUG("readdir: `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_readdir_wrapped(path, buf, filler, offset, fi);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}


static int cmp_live(const void *URL,const void *p2)
{
	const struct lfile *s = *(const struct lfile **)p2;

	if (s->hidden)
		return 1;
	else
		return strcmp(URL, s->URL);
}
static int cmp_hidden(const void *path,const void *p2)
{
	const struct lfile *s = *(const struct lfile **)p2;

	if (s->hidden)
		return strcmp(path, s->path);
	else
		return -1;
}

/*
 * @brief manage list of opened files
 *
 * When a file not present in the list is opened, these functions create the new
 * array list, sorting out potential no longer needed files.
 *
 * Principle:
 *   The counter has 3 parts,
 *   - number of times the file is opened
 *   - a DEL flag
 *   - an ERROR flag
 * When the file is not needed anymore: not opened, not error, and not yet DEL,
 * (it means the whole counter is zero), then the algorithm tries to set the DEL
 * flag. If it succeeds (atomic_compare_exchange), then the memory is RCUed and
 * the now unneeded file is removed from the array.
 * Doing so ensures that only one thread will succeed in setting the DEL flag,
 * avoiding a double RCU that will trigger a double-free.
 * Also, if a thread wants to reuse a file while another one wants to delete it
 * the first doing the exchange will win, and the other thread will loop.
 *
 * The ERROR flag is not managed here, but during refresh. Files having had an
 * error are kept until refresh has been triggered, to avoid uselessly trying
 * to open them.
 */

static bool keep_lfile(struct lfile *s, struct timespec *t)
{
	unsigned long counter, spin = 0;
	time_t until;

	counter = LOAD(s->counter);
	do {
		spin++;

		if (0 != (counter & FL_ERROR) || 0 != (counter & OPEN_MASK))
			return true;
		if (0 != (counter & FL_DEL))
			return false;
		/* From here: no error, not marked as del, and file not opened
		 *	      thus the only possible value of counter is: 0 */
		until = LOAD(s->loc_until, relaxed);
		if (0 == t->tv_nsec)
			clock_gettime(CLOCK_MONOTONIC_COARSE, t);
		if (until > t->tv_sec)
			return true;
	 } while(!XCHG(s->counter, counter, counter | FL_DEL));
	spin_add(spin - 1, "keep_lfile");
	free_lfile(s);
	return false;
}

static int cmp_lfiles(const struct lfile *s1, const struct lfile *s2)
{
	if (s1->hidden) {
		if (s2->hidden)
			return strcmp(s1->path, s2->path);
		else
			return -1;
	}
	else {
		if (s2->hidden)
			return 1;
		else
			return strcmp(s1->URL, s2->URL);
	}
}

static struct lfiles *new_lfile_array(struct lfiles *cur,
				      struct lfile *added_lfile,
				      struct timespec *t)
{
	struct lfiles *new;
	unsigned int i;
	bool ins = false;

	if (NULL == cur) {
		new = malloc(sizeof(struct lfiles) + sizeof(void *));
		new->n_lfiles	  = 1;
		new->a_lfiles[0] = added_lfile;
		return new;
	}

	/* We might allocated more memory than needed, but it is
	 * only pointers here, so no big deal
	 */
	new = malloc(sizeof(struct lfiles) +
		     sizeof(void *) * (cur->n_lfiles + 1));
	new->n_lfiles = 0;
	for (i = 0; i < cur->n_lfiles; i++) {
		if (!keep_lfile(cur->a_lfiles[i], t))
			continue;
		if (!ins && cmp_lfiles(added_lfile, cur->a_lfiles[i]) <= 0) {
			ins = true;
			new->a_lfiles[new->n_lfiles++] = added_lfile;
		}
		new->a_lfiles[new->n_lfiles++] = cur->a_lfiles[i];
	}

	if (!ins)
		new->a_lfiles[new->n_lfiles++] = added_lfile;

	return new;
}

static struct lfile *find_lfile(const char *path,
				struct walk *w,
				struct lfiles *live,
				unsigned long *counter)
{
	struct lfile **found;

	if (NULL == live) {
		/* This situation happens only the first time a file is
		 * opened. Due to parallelislm, the message can possibly
		 * be displayed several times. */
		DEBUG("Open: first opened file(s).\n");
		return NULL;
	}

	if (w->parent->hidden)
		found = bsearch(path, live->a_lfiles, live->n_lfiles,
				sizeof(void *), cmp_hidden);
	else
		found = bsearch(w->res.fs->URL, live->a_lfiles,
				live->n_lfiles, sizeof(void *), cmp_live);
	if (NULL == found)
		return NULL;

	*counter = LOAD((*found)->counter);
	if (0 != ((*counter) & FL_DEL))
		return NULL;
	return *found;
}


/*
 * @brief allocates and initialises new opened file.
 *
 * Note: all must be initialised here because all fields might be used as
 *	 soon as it is linked to "live" in case two open on the same file
 *	 occur at the same time.
 *
 * @param pointer to the file structure
 * @param time of opening
 * @param full path
 * @param pointer to parent directory
 * @return initialised lfile pointer
 */

static struct lfile *new_lfile(	struct file *f, struct timespec *t,
				const char * path, struct dir *parent)
{
	struct lfile *new_lfile = NULL;
	size_t sz1;

	if (0 == t->tv_nsec)
		clock_gettime(CLOCK_MONOTONIC_COARSE, t);
	if (parent->hidden)
		sz1 = strlen(f->filename)  +
		      strlen(parent->name) - parent->email_off + 2;
	else
		sz1 = strlen(f->URL) + 1;
	new_lfile = malloc(sizeof(struct lfile) + sz1 + strlen(path) + 1);
	memset(new_lfile, 0, sizeof(struct lfile));
	strcpy(new_lfile->path, path);
	new_lfile->URL = (char *)(new_lfile->path + strlen(path) + 1);

	if (parent->hidden) {
		strcpy(new_lfile->URL, f->filename);
		new_lfile->email = new_lfile->URL + strlen(f->filename) + 1;
		strcpy(new_lfile->email, parent->name + parent->email_off);
		new_lfile->id = parent->id;
	}
	else {
		strcpy(new_lfile->URL, f->URL);
	}
	STORE(new_lfile->counter, OPEN_INC);
	new_lfile->no_ssl	 = parent->no_ssl;
	new_lfile->encfs	 = parent->encfs;
	new_lfile->hidden	 = parent->hidden;
	new_lfile->size		 = f->size;
	pthread_mutex_init(&new_lfile->loc_mutex, NULL);

	if ('\0' == *(f->filename -1)) {
		new_lfile->has_encfs_key = false;
	}
	else {
		new_lfile->has_encfs_key = true;
		encfs_copy_key(new_lfile->encfs_key,
			       P_ENCFS_HEADER(f->filename));
	}

	if (new_lfile->no_ssl) {
		char *p;
		/* Benchmark, name starts with _ use new */
		p = strrchr(path, '/');
		if (NULL == p || '#' == *(p + 1)) {
			lprintf(LOG_INFO, "http using curl sockets.\n");
		}
		else {
			lprintf(LOG_INFO, "http using raw socket.\n");
		}
	}
	else {
		lprintf(LOG_INFO, "https using curl sockets.\n");
	}

	return new_lfile;
}


/*
 * @brief does the actual work for opening files
 *
 * Opened files and files that have a still valid location are accessible
 *   through the global atomic pointer 'live'.
 * This points to a structure that has an array of pointers to those opened
 *   lfiles. This is desirable, because even when a file have been closed, the
 *   'download ticket' (location) can still be valid. Thus it saves the time
 *   to get a new download ticket if the file is opened again in the validity
 *   period. There is also a limit on total "alive" download locations, so
 *   that helps with this limit. This can't be handled like astreamfs because
 *   the directory tree can be flushed, and storing the location in the tree
 *   means it can be lost when refreshed, resulting in sub-optimal behavior.
 *   This also saves memory when there are a lot of files and few at a time are
 *   opened (normal situation!).
 *
 * This global list is managed without mutexes but with an atomic exchanges.
 * It could suffer from live lock in situation where the driver would be bombed
 * with parallel opens, but is a conscious choice because such a situation
 * is probably not a standard use!
 * See discussions about pro and cons of live-locks/spin-locks vs mutexes.
 *
 * All the management of the list is done at open. Release (close) only
 * decrements the counter (number of times the files is opened).
 * Refresh takes care of removing files with errors.
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_open_wrapped(const char *path, struct fuse_file_info *fi)
{
	struct walk w;
	unsigned long spin1 = 0, spin2;
	unsigned long counter;
	struct timespec t;
	struct lfiles *live_cur, *live_new = NULL;
	struct lfile  *lfile_found, *lfile_new = NULL;
	struct fi_to_fh *fifh;
	int res = 0;

	find_path(path, &w);
	DEBUG(	"found: is_dir=%s is_wf=%s %p\n",
		((w.is_dir) ? "true" : "false"),
		((w.is_wf) ? "true" : "false"),
		w.res.fs);
	/* Fuse documentation of open() says that O_CREAT flag is filtered
	 * out, and presumably this triggers a call to create() instead of
	 * open(). Hence here the file is supposed to exist because O_CREAT
	 * was not specified: thus the test returns ENOENT if the path did
	 * not exist. This is just a precaution since fuse seems to be doing
	 * some calls to readdir/getattr before opening, and does not call
	 * open() when the file does not exist with no O_CREAT flag. */
	if (NULL == w.res.fs || w.is_dir)
		return -ENOENT;

	/* Delegate to write_open when file is in the write cycle */
	if (w.is_wf)
		return write_open(path, &w, fi);

	/* Cannot "overwrite" a file that is in the storage
	 * Note that for renaming encfs opens RW because it needs to read
	 * the first 8 bytes before rewriting them, so nothing to do here! */
	if (O_WRONLY == (fi->flags & O_ACCMODE))
		return -EACCES;

	/* From there no error can be returned, hence it is ok to alloc fifh */
	fifh = malloc(sizeof(struct fi_to_fh));
	fi->fh = (uintptr_t)fifh;

	if (params1f.refresh_file == w.res.fs->filename) {
		refresh(&root, REFRESH_TRIGGER, true);
		fifh->fh   = (uintptr_t)params1f.refresh_file;
		fifh->type = TYPE_SPECIAL;
		return 0;
	}
#ifdef _STATS
	if (params.stat_file == w.res.fs->filename) {
		struct stats *stts;

		stts = malloc(sizeof(struct stats));
		pthread_mutex_init(&stts->initmutex, NULL);
		stts->size = 0;
		fifh->fh   = (uintptr_t)stts;
		fifh->type = TYPE_SPECIAL;
		/* Stats are computed at first read to avoid computing them
		 * if the stat file is only opened/closed. */
		return 0;
	}
#endif

	fifh->type = TYPE_READ;
	STORE(fifh->last_opt, UNKNOWN_OPT);

	live_cur = LOAD(live);
	do {
		spin1++;
		t.tv_nsec = 0;
		if (NULL != live_new) {		/* If loop here, live_new is  */
			free(live_new);		/* known invalid: xchg failed */
			live_new = NULL;	/* so it must be freed        */
		}
		spin2 = 0;			/* Try to find lfile in array */
		lfile_found = find_lfile(path, &w, live_cur, &counter);
		if (NULL != lfile_found) {
			/* Found it, now try inc Open Counter */
			do {
				spin2++;
				if (0 != (counter & FL_ERROR)) {
					res = -EACCES;
					break;
				}
				DEBUG(">> Open: file found: %s.\n", path);
			} while(!XCHG(lfile_found->counter,
				      counter,
				      counter + OPEN_INC));
			spin_add(spin2 - 1, "unfichier_open_wrapped(1)");
			if (0 != res)	/* File was found but marked: error */
				break;

			/* Success, free a lfile if alloc in previous loop  */
			free(lfile_new);	/* since not needed anymore */
			lfile_new = lfile_found;
			break;
		}
		/* Not found, get a new lfile (if not one already */
		if (NULL ==  lfile_new)
			lfile_new = new_lfile(w.res.fs, &t, path, w.parent);

		/* Add it to the array and try XCHG the array */
		live_new = new_lfile_array(live_cur, lfile_new, &t);
	} while(!XCHG(live, live_cur, live_new));
	spin_add(spin1 - 1, "unfichier_open_wrapped(2)");

	if (0 != res) {	/* File was found but already marked as error */
		free(fifh);
		fi->fh = 0;
		return res;
	}

	fifh->fh = (uintptr_t)lfile_new;

	if (NULL != live_new) {
		free_cur_live_dump_new(live_cur, live_new, "Open");
		aw_close(fifh->fh); /* Only for new files, in case this address
				       was used, released and used again! */
	}

	DEBUG(	">> Open: lfile handle: %p (0x%lX).\n",
		lfile_new,
		LOAD(lfile_new->counter));

	fi->keep_cache = 1;

	return 0;
}

/*
 * @brief fuse callback for open
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_open(const char *path, struct fuse_file_info *fi)
{
	int   res;

	DEBUG(">> open(%s,\"%s\"/0x%X)\n", path,
		(  ((fi->flags & O_ACCMODE) == O_RDONLY) ? "R" :
		  (((fi->flags & O_ACCMODE) == O_WRONLY) ? "W" : "RW")
		),
		(int)fi->flags
	     );

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_open_wrapped(path, fi);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}


/*
 * @brief fuse callback for release
 *
 * Close instruction to async reader are only sent if the file is not opened
 * anymore and there was an error. Otherwise the lfile is left open in case
 * it is shortly needed again. This also avoid having to lock here and in open
 * because a file marked as error cannot be re-opened with the same handle.
 * Note that lfiles can't be freed until the counter is not zero (still opened)
 * hence the rcu_lock not needed here.
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_release(const char *path, struct fuse_file_info *fi)
{
	struct fi_to_fh *fifh;
	struct lfile *s;
	int res = 0;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));

	if (TYPE_SPECIAL == fifh->type) {
		/* This is the constant address we have for refresh */
		if (fifh->fh == (uintptr_t)params1f.refresh_file) {
			DEBUG(">> Releasing trigger file.\n");
			free(fifh);
			return res;
		}

#ifdef _STATS
		/* This is the stat file */
		struct stats *stts = (struct stats *)((uintptr_t)(fifh->fh));

		DEBUG(">> Releasing stat file.\n");
		res = pthread_mutex_destroy(&stts->initmutex);
		if (0 != res)
			lprintf(LOG_CRIT,
				"error: %d on pthread_mutex_destroy.\n", res);
		free(stts);
		free(fifh);
		return res;
#endif
	}
	else if (TYPE_WRITE == fifh->type) {
		res = write_release((struct wfile *)((uintptr_t)(fifh->fh)));
	}
	else {
		s = (struct lfile *)((uintptr_t)(fifh->fh));
#ifdef _DEBUG
		DEBUG(">> Releasing %s (0x%lX).\n", path
						  , SUB(s->counter, OPEN_INC));
#else
		SUB(s->counter, OPEN_INC);
#endif
	}

	free(fifh);
	return res;
}

bool ok_json_get(const char *json_str, const char *pattern, off_t *value)
{
	char *p;
	p = json_find_key(json_str, pattern);
	if (NULL == p) {
		lprintf(LOG_ERR,
			"Unexpected/Ignoring: did not find %s key in json response (URL=%s)\n",
			pattern, api_urls[USER_INFO]);
		return false;
	}
	if ( 1 != sscanf(p, "%"SCNd64, value) ) {
		lprintf(LOG_ERR,
			"Unexpected/Ignoring: could not read the value of %s (URL=%s)\n",
			pattern, api_urls[USER_INFO]);
		return false;
	}
	return true;
}

#define TERA_BYTE (1024ULL * 1024 * 1024 * 1024)

static int extract_avail(enum hCode hc, const char *json_str)
{
	off_t hot, cold, allowed, x;
	if (hOk != hc)
		return -EIO;
	if (!ok_json_get(json_str, json_cold_storage, &cold))
		return -EIO;
	if (!ok_json_get(json_str, json_hot_storage, &hot))
		return -EIO;
	if (!ok_json_get(json_str, json_allowed_cold_storage, &allowed))
		return -EIO;
	DEBUG(	"<<< unfichier_statfs cold=%llu, hot=%llu, allowed=%llu\n",
		cold, hot, allowed);
	allowed *= TERA_BYTE;
	x = (allowed + hot + DEFAULT_BLK_SIZE - 1) / DEFAULT_BLK_SIZE;
	max_storage = x;
	if (cold >= allowed)
		x = 0;
	else
		x = (allowed - cold) / DEFAULT_BLK_SIZE;
	avail_storage = x;
	return 0;
}

static int unfichier_statfs(const char *path, struct statvfs *buf)
{
	struct timespec	now;
	int res;
	bool refresh = false;

	/* Note: this function does NOT use possibly dangling pointer.
	 *	 It only uses static root (if refresh is needed) and
	 *	 static available_storage and the like.
	 *	 Hence it does not need rcu_lock, etc...
	 */

	lock(&storage_mutex, NULL);

	if (0 != storage_flags) {
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		if (0 == storage_next_update ||
		    now.tv_sec > storage_next_update) {
			storage_next_update = now.tv_sec + USER_INFO_DELAY;
			refresh = true;
		}
	}
	if (refresh) {
		static const char empty_json_object[]="{}";
		enum hCode hc;
		struct json j;

		DEBUG("<<<(in) unfichier_statfs\n");

		json_init(&j);
		hc = curl_json_perform((char *)empty_json_object,
				       &j,
				       USER_INFO,
				       (char *)path);
		res = extract_avail(hc, j.pb);
		storage_flags = (0 == res) ? 0 : FL_STOR_ERROR;
		json_free(&j);
	}
	else {
		DEBUG("<<<(in/out)>>> unfichier_statfs\n");
		res = (0 == (FL_STOR_ERROR & storage_flags)) ? 0 : -EIO;
	}
	buf->f_blocks = max_storage;
	buf->f_bfree  =
	buf->f_bavail = avail_storage;

	unlock(&storage_mutex, NULL);

	buf->f_bsize  = DEFAULT_BLK_SIZE;
	buf->f_frsize = DEFAULT_BLK_SIZE;

	buf-> f_namemax = MAX_FILENAME_LENGTH;

	return res;
}

static bool forbidden_change(struct walk *w)
{
	if (w->is_dir && w->res.dir->shared) {
		lprintf(LOG_WARNING,
			"cannot rename or remove the shared directory: `%s`.\n",
			w->res.dir->name);
		return true;
	}
	return false;
}

static bool forbidden_operation(struct dir * d)
{
	if (d->hidden) {
		lprintf(LOG_WARNING,
			"impossible operation with 'hidden links' shared directory: `%s`.\n",
			d->name);
		return true;
	}
	return false;
}

/*
 * @brief check if the path is locked
 *
 * Concerns:
 * - the stat file
 * - the refresh file
 *
 * The check is always done towards existing files
 *
 * @param path to be checked
 * @return true if this is a reserved name.
 */
bool locked_files(struct walk *w, const char *msg)
{
	if (w->res.fs->filename == params1f.refresh_file) {
		lprintf(LOG_WARNING,
			"impossible to %s the refresh_file.\n",
			msg);
	    return true;
	}
#ifdef _STATS
	if (w->res.fs->filename == params.stat_file) {
		lprintf(LOG_WARNING,
			"impossible to %s the stat_file.\n",
			msg);
	    return true;
	}
#endif
	return false;
}

/*
 * @brief check if the name (file or dir) is reserved
 *
 * Concerns:
 * - the upload directory: when the directory is not already present
 * - the refresh file: with the refresh-hidden option when the file is not shown
 *
 * Since the check is always done towards NON-existing file/dir, the only
 * way is to do a costly strcmp.
 *
 * @param path to be checked
 * @param id of parent directory
 * @return true if this is a reserved name.
 */

bool reserved_names(const char* path, long long parent_id)
{
	return (root.id == parent_id &&
		(  0 == strcmp(path + 1, upload_d.name) ||
		  (params1f.refresh_hidden &&
		   0 == strcmp(path + 1, params1f.refresh_file)) ));
}

/* Note: see man 2 rename
 *  rename is always called with either 2 files (since we don't have
 *  symlinks in the mount), or with a dir as oldpath (from), in which case
 *  newpath (to) is either non existing or an empty dir.
 *
 *  In this version of fuse (26) we do not have the 'flags' of renameat2
 *  Since anyway it is impossible to do "atomic exchange" talking to a server
 *  that does not, the code has the behaviour of RENAME_NOREPLACE flag.
 *  This means that when newpath (to) exists, the return code is EEXIST.
 *  Should the user want to rename replacing an existing file, he shall
 *  first delete the target, then do the rename.
 *  rename also documents that in case of 2 hardlinks referring to the same
 *  content, it does nothing. To avoid having to call the server to check that,
 *  the code does not do that, instead it will also return EEXIST in this case.
 */

static const char json_trailer[] = "\"}";

static enum hCode rename_file(const char * url, long long dest_id,
			      const char *rename_to,
			      struct json *j, const char *from)
{
	static const char mv_file_post[] = "{\"urls\":[\"%s\"],\"destination_folder_id\":%lld,\"rename\":\"";
	int sz;

	char post[sizeof(mv_file_post)	+ strlen(url)
					+ STRLEN_MAX_INT64
					+ MAX_ESCAPED_LENGTH
					+ sizeof(json_trailer) ];
	sz = sprintf(post, mv_file_post, url, dest_id);
	name_translate(rename_to, post + sz, TR_FILE, json_trailer);
	return curl_json_perform(post, j, FILE_MV, from);
}

static enum hCode chattr_file(const char *url,
			      const char *rename_to,
			      struct json *j, const char *from)
{
	static const char chattr_post[]	 = "{\"urls\":[\"%s\"],\"filename\":\"";
	int sz;

	char post[sizeof(chattr_post) + strlen(url)
				      + MAX_ESCAPED_LENGTH
				      + sizeof(json_trailer)];

	sz = sprintf(post, chattr_post, url);
	name_translate(rename_to, post + sz, TR_FILE, json_trailer);
	return curl_json_perform(post, j, FILE_CHATTR, from);
}

static enum hCode rename_dir(long long dir_id, long long dest_id,
			     const char *rename_to,
			     struct json *j, const char *from)
{
	static const char mv_dir_post[]	 = "{\"folder_id\":%lld,\"destination_folder_id\":%lld,\"rename\":\"";
	int sz;

	char post[sizeof(mv_dir_post) + 2 * STRLEN_MAX_INT64
				      + MAX_ESCAPED_LENGTH
				      + sizeof(json_trailer) ];
	sz = sprintf(post, mv_dir_post, dir_id, dest_id);
	name_translate(rename_to, post + sz, TR_DIR, json_trailer);
	return curl_json_perform(post, j, FOLDER_MV, from);
}

static int unfichier_rename_wrapped(const char *from, const char *to,
				    enum refresh_cause ref_cause,
				    struct walk *wt, bool *del,
				    bool internal)
{
	static const char mv_fusr_post[] = "{\"urls\":[\"%s\"],\"destination_folder_id\":0,\"destination_user\":\"%s\",\"rename\":\"";
	static const char mv_dusr_post[] = "{\"folder_id\":%lld,\"destination_folder_id\":0,\"destination_user\":\"%s\",\"rename\":\"";
	static const char msg_rename[]   = "rename";
	enum hCode hc;
	struct walk wf;
	char *q, *r = NULL;
	char to_encfs[MAX_FILENAME_LENGTH + 1];
	int res;
	struct json j;

	/* Note that Linux kernel (normally!) does NOT call the fuse driver in
	 * error situations like renaming of different types: file to dir, dir
	 * to file or dir to subdir, component of paths does not exist.
	 * The test is done anyway... better safe than sorry... but this error
	 * test code should never be reached.
	 * So, on Linux: EINVAL, EISDIR, ENOTDIR, should never be returned.
	 * Since this code mimics in fact renameat2(RENAME_NOREPLACE), kernel
	 * DOES call this function when target exists. Thus the only test that
	 * is hit before calling the move APIs is EEXIST
	 * When calling the move APIs, ENOENT can be returned (file/dir removed
	 * from server) or EREMOTEIO (other error on server).
	 * EACCESS is also returned when trying to play with the refresh_file,
	 * or when doing an impossible operation with shares or upload dir.
	 */

	find_path(from, &wf);
	if (NULL == wf.res.fs)
		return -ENOENT;

	/* Avoid useless tests when renaming 'resume directories' */
	if (REFRESH_WRITE != ref_cause) {
		if (!wf.is_dir && root.id == wf.parent->id
			       && locked_files(&wf, msg_rename))
			return -EACCES;

		if (forbidden_change(&wf) ||
		    forbidden_write(wf.parent, true, &wf))
			return -EACCES;

		res = forbidden_chars(to, (wf.is_dir) ? TR_DIR : TR_FILE);
		if (0 != res)
			return res;
	}

	find_path(to, wt);
	/* Note: the test to check whether a directory is renamed to its
	 *       descendant is complex and is left to the API that returns
	 *       an error code when such situation happens. */
	if (NULL == wt->parent)
		return -ENOENT;		/* Dir component of path not found */
	if (REFRESH_WRITE != ref_cause) {
		if (NULL != wt->res.fs) {
			if (wt->is_dir && !wf.is_dir)
				return -EISDIR;	/* Ren a file to existing dir */
			if (!wt->is_dir && wf.is_dir)
				return -ENOTDIR;/* Ren a dir to existing file */
			if (wt->is_dir && wf.is_dir)
				return -EEXIST;	/* Ren exist dir unsupported  */
			if (!wt->is_dir && !wf.is_dir)
				*del = true; 	/* Ren existing file: 2 steps */
		}
		if (forbidden_write(wt->parent, true, NULL))
			return -EACCES;
		if (reserved_names( to, wt->parent->id))
			return -EACCES;
		/* Temp file being written to (or waiting)		      */
		/* //TODO: but this is not atomic until file lists are merged!*/
		if (wf.is_wf) {
			res = write_rename(&wf, wt, from, to);
			if (-EAGAIN == res)
				res = unfichier_rename_wrapped(from,
							       to,
							       ref_cause,
							       wt,
							       del,
							       internal);
			return res;
		}
	}


	q = strrchr(to, '/') + 1;

	/* Directories */
	json_init(&j);
	if (wf.is_dir) {
		if (IS_SHARED_ROOT(wt->parent)) {
			char post[sizeof(mv_dusr_post)+ STRLEN_MAX_INT64
						      + strlen(wt->parent->name)
						      + MAX_ESCAPED_LENGTH
						      + sizeof(json_trailer) ];
			res = sprintf(post, mv_dusr_post
					  , wf.res.dir->id
					  , SHARER_EMAIL(wt->parent));
			name_translate(q, post + res, TR_DIR, json_trailer);
			hc = curl_json_perform(post, &j, FOLDER_MV, from);
		}
		else {
			hc = rename_dir(wf.res.dir->id,
					wt->parent->id,
					q,
					&j, from);
		}
		json_free(&j);
		refresh(wf.parent, REFRESH_MV, false);
		refresh(wt->parent, REFRESH_MV, false);

		switch (hc) {
			case hOk	: return 0;
					/* Probably delete in parallel? */
			case hNotFound	: return -ENOENT;
			default		: return -EREMOTEIO;
		}
	}

	/* Files */
	/** encfs notes:
	 * encfs (or any filesystem) cannot move files out of its root tree.
	 * When that is done, it is via manipulating the "encrypted" counterpart
	 * of the encryption filesystem on which encfs sits (1fichierfs here).
	 * 1fichierfs will not forbid that unless the encfs file is already
	 * "header-rewritten", because the rewriting code will not be triggered
	 * once the file has been moved out of the encfs declared tree,
	 * same if the new name does not match the encfs pattern.
	 */
	if (wf.parent->encfs && IS_ENCFS_SPECIAL(wf.res.fs->filename)) {
		size_t sz;
		bool special;

		if (!wt->parent->encfs) {
			lprintf(LOG_WARNING ,
				"cannot move encfs-header-rewritten file %s out of encfs tree to %s\n",
				from, to);
			return -EACCES;
		}

		if (!is_encfs_name(q, &sz, &special)) {
			lprintf(LOG_WARNING ,
				"cannot rename encfs-header-rewritten file %s to a not-encfs filename: %s\n",
				from, to);
			return -EACCES;
		}

		if (!special) {
		/* When the file "from" already is special (header was
		 * rewritten) and the file "to" is not but still a valid
		 * encfs filename, the header must be propagated to "to" */
			if (sz + ENCFS_HEADER_B64_SZ > MAX_FILENAME_LENGTH)
				return -ENAMETOOLONG;
			encfs_appendb64_key(to_encfs, q,
					    P_ENCFS_HEADER(wf.res.fs->filename));
			q = to_encfs;
		}
		else {
		/* When the "to" file corresponds to the pattern of
		 * 'encfs_special' if this is an "internal" call from
		 * rewrite-header, no change is needed. If the call is not
		 * "internal" it is blocked to avoid tampering */
			if (!internal)
				return -EACCES;
		}
	}

	if (wf.parent == wt->parent)
	{	/* Same directory, using chattr */
		hc = chattr_file(wf.res.fs->URL, q, &j, from);
	}
	else {
		if (IS_SHARED_ROOT(wt->parent)) {
			char post[sizeof(mv_fusr_post) + strlen(wf.res.fs->URL)
						       + strlen(wt->parent->name)
						       + MAX_ESCAPED_LENGTH
						       + sizeof(json_trailer)];
			res = sprintf(post, mv_fusr_post
					  , wf.res.fs->URL
					  , SHARER_EMAIL(wt->parent));
			name_translate(q,
				       post + res,
				       TR_FILE,
				       json_trailer);
			hc = curl_json_perform(post, &j, FILE_MV, from);
		}
		else {
			hc = rename_file(wf.res.fs->URL,
					 wt->parent->id,
					 q,
					 &j, from);
		}
	}
	if (hOk == hc)
		r = strstr(j.pb, wf.res.fs->URL);
	json_free(&j);

	/* Refresh target on error and if not deleted "to" in phase 2 */
	if (NULL == r || !*del || wt->parent->encfs)
		refresh(wt->parent, ref_cause, false);

	if (NULL == r) {
		refresh(wf.parent, ref_cause, false);
		if (hNotFound == hc)
			return -ENOENT;	 /* Probably delete in parallel? */
		else
			return -EREMOTEIO;
	}
	else {
		if (wf.parent != wt->parent) {
			if (wf.parent->encfs)
				refresh(wf.parent, ref_cause, false);
			else
				OR(wf.res.fs->flags, FL_DEL);
		}
	}
	return 0;
}

/*
 * @brief rename resume directories.
 *
 * @param current name of the directory
 * @param target name to be renamed to
 * @param id of the directory to be renamed
 * @param id of the parent to be renamed to (/.upload.1fichierfs)
 * @param dirname part of the target name.
 * @return O if success, othewise negative error code.
 */

int rename_dir_upload(const char *from,  const char *to, long long dir_id,
		      long long dest_id, const char *dirname)
{
	struct json j;
	int res;
	struct walk wt;
	enum hCode hc;
	bool del;

	if (0 == strcmp(from, to))
		return 0;

	json_init(&j);

	if (def_root == root.name) {
		res = unfichier_rename_wrapped(from, to, REFRESH_WRITE,
					       &wt, &del, true);
	}
	else {
		hc = rename_dir(dir_id, dest_id, dirname, &j, from);

		switch (hc) {
			case hOk	: res = 0;
					  break;
					/* Probably delete in parallel? */
			case hNotFound	: res = -ENOENT;
					  break;
			default		: res = -EREMOTEIO;
		}
	}

	json_free(&j);
	return res;
}

/*
 * @brief rename file from upload directory to the target.
 *
 * Renaming with the dir ID is more reliable because it is resistant to
 * a rename or move of any directory inside the target path.
 *
 * However, if the target directory is deleted (which cannot happen in the
 * same 1fichierfs that created the file since this directory is non-empty, but
 * can happen from another instance of 1fichierfs or the Web page) then
 * created anew, and assuming the rest of the path was untouched, the file
 * can be renamed using the full path (not during resume since the full path
 * is not kept).
 *
 * @param full path of target (if not NULL, used if rename with ID/file failed)
 * @param directory ID of the target where to rename
 * @param URL of the file in the upload directory
 * @param filename part of the full path to rename to.
 * @return O if success, othewise negative error code.
 */
int rename_upload(const char *to, const long long dest_id,
	          const char *URL , const char *filename)
{
	enum hCode hc;
	struct json j;
	struct walk wt;
	char *p;
	int res;

	DEBUG("rename_upload(%s,%lld,%s,%s)\n", to, dest_id, URL, filename);

	json_init(&j);

	hc = (get_upload_id() == dest_id) ?
			chattr_file(URL, filename, &j, NULL) :
			rename_file(URL, dest_id, filename, &j, NULL);

	if (hOk == hc)
		p = strstr(j.pb, URL);
	else
		p = NULL;

	if (NULL == p) {
		switch (hc) {
			case hNotFound:
				res = -ENOENT;	/* Parallel delete via web? */
				break;
			case hForbidden:
				res = -EACCES;	/* Parent dir removed? */
				break;
			default:
				res = -EREMOTEIO; /* Misc error */
		}
	}
	else {
		res = 0;
	}
	refresh_for_write(NULL, dest_id);
	refresh_for_write(&upload_d, get_upload_id());

	if (0 != res && NULL != to) {
		lprintf(LOG_WARNING,
			"failed to rename from upload dir using target dir ID %lld, trying full path: %s\n",
			dest_id, to);
		/* Refresh all since a dir was deleted out of this instance */
		refresh_for_write(NULL, 0);
		find_path(to, &wt);

		res = 0;
		if (NULL != wt.res.fs)
			res = -EEXIST;
		else if (NULL == wt.parent)
			res = -ENOENT;
		else if (forbidden_write(wt.parent, true, NULL))
			res = -EACCES;
		if (0 == res)
			res = rename_upload(NULL, wt.parent->id, URL, filename);
		else
			lprintf(LOG_ERR,
				"%d, failed to rename with full path: %s\n",
				res, to);
	}

	json_free(&j);
	return res;
}

static char *unlink_file(const char *URL, enum hCode *hc, const char *filename);

/*
 * @brief rename fuse callback and internal counterpart
 *
 * @param source path
 * @param destination path
 * @param internal flag: false for fuse callback, true for "internal" calls
 * @return return code of the rename
 */
static int unfichier_rename_internal(const char *from, const char *to,
				     bool internal)
{
	int res;
	struct walk wt;
	bool del = false;

	DEBUG(	"<<<(in) unfichier_rename `%s` to `%s`\n",
		from, to);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_rename_wrapped(from, to, REFRESH_MV, &wt, &del,
				       internal);

	/* Step 2 is to remove after rename if necessary */
	if (0 == res && del) {
		if (wt.is_wf) {
			res = write_unlink(&wt);
		}
		else {
			enum hCode hc;
			if (NULL == unlink_file(wt.res.fs->URL, &hc, to)) {
				if (hNotFound == hc)
					res = -ENOENT;	 /* Parallel delete */
				else
					res = -EREMOTEIO;
			}
		}
		refresh(wt.parent, REFRESH_MV, false);
	}

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_rename `%s` to `%s` res=%d\n"
			  , from, to, res);

	return res;
}

static int unfichier_rename(const char *from, const char *to)
{
	return unfichier_rename_internal(from, to, false);
}

/*
 * @brief do the actual rename for encfs special rewrite header
 *
 * @param path of the file
 * @param buffer (as uint64_t * since it expects 8 characters)
 * @return return code of the rename
 */
static int encfs_rename(const char *path, const char *buf)
{
	size_t sz;

	sz = strlen(path);
	{
		char to[sz + ENCFS_HEADER_B64_SZ + 1];

		if (NULL == encfs_appendb64_key(to, path, buf))
			return -EACCES;
		return unfichier_rename_internal(path, to, true);
	}
}

/*
 * @brief called by unfichier_write to check if it is an encfs rewrite key
 *
 * @param path of the file
 * @param buffer to write
 * @param size to write
 * @param offset in the file
 * @param fuse file info handle
 * @return error (-EACCESS) or OK
 */
int encfs_rewrite_key(const char *path, const char *buf,
		      size_t size, off_t offset, struct fuse_file_info *fi)
{
	struct lfile *s;
	char *filename;
	struct fi_to_fh *fifh;
	int res;

	/* The rewrite key pattern of encfs is offset 0, size 8 */
	if (0 != offset || ENCFS_HEADER_SZ != size)
		return -EACCES;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));
	s = (struct lfile *)((uintptr_t)(fifh->fh));

	/* Now look if this is encfs declared and if it is OK to increase
	 * filename length */
	if (!s->encfs)
		return -EACCES;

	filename = strrchr(s->path, '/') + 1;
	if (!is_encfs_name(filename, NULL, NULL))
		return -EACCES;

	if (!s->has_encfs_key &&
	    strlen(filename) + ENCFS_HEADER_B64_SZ > MAX_FILENAME_LENGTH)
		return -EACCES;

	/* From there, all is OK to rename */
	DEBUG("encfs_rewrite_key of %#018"PRIX64"\n", *((uint64_t *)buf));
	/* Note, using the path passed by write callback because path stored
	 * in lfile is that when the file was first opened, not taking into
	 * account renames */
	res = encfs_rename(path, buf);
	if (0 == res) {
		/* Must update the live file key, since it is not refreshed
		 * from the actual name as long as a download token is valid */
		s->has_encfs_key = true;
		encfs_copy_key(s->encfs_key, buf);
		return ENCFS_HEADER_SZ;
	}
	else {
		return -EACCES;
	}
}


static int unfichier_link_wrapped(const char *source, const char *link)
{
	static const char cp_post[] = "{\"urls\":[\"%s\"],\"folder_id\":%lld,\"rename\":\"";
	static const char cu_post[] = "{\"urls\":[\"%s\"],\"folder_id\":0,\"sharing_user\":\"%s\",\"rename\":\"";
	static const char msg_link_from[] = "link from";
	enum hCode hc;
	struct walk ws, wl;
	char *p = NULL, *lk;
	int sz, res;

	find_path(source, &ws);
	if (NULL == ws.res.fs)
		return -ENOENT;
	/* The API works only on files: -ENOSYS on directories
	 */
	if (ws.is_dir)
		return -ENOSYS;
	if (ws.is_wf)
		return -EBUSY;
	if (forbidden_operation(ws.parent))
		return -EACCES;
	if (get_upload_id() == ws.parent->id)
		return -EACCES;
	if (!ws.is_dir && root.id == ws.parent->id
		       && locked_files(&ws, msg_link_from))
		return -EACCES;

	res = forbidden_chars(link, TR_FILE);
	if (0 != res)
		return res;
	find_path(link, &wl);
	if (NULL == wl.parent)
		return -ENOENT;
	if (reserved_names(link, wl.parent->id))
		return -EACCES;
	if (NULL != wl.res.fs)
		return -EEXIST;
	if (forbidden_write(wl.parent, true, NULL))
		return -EACCES;

	lk = strrchr(link  , '/') + 1;

	{
		struct json j;
		json_init(&j);
		if (IS_SHARED_ROOT(wl.parent)){
			char post[sizeof(cu_post) + strlen(ws.res.fs->URL)
						  + strlen(wl.parent->name)
						  + MAX_ESCAPED_LENGTH
						  + sizeof(json_trailer) ];
			sz = sprintf(post, cu_post
					 , ws.res.fs->URL
					 , SHARER_EMAIL(wl.parent));
			name_translate(lk, post + sz, TR_FILE, json_trailer);
			hc = curl_json_perform(post, &j, FILE_CP, source);
		}
		else {
			char post[sizeof(cp_post) + strlen(ws.res.fs->URL)
						  + STRLEN_MAX_INT64
						  + MAX_ESCAPED_LENGTH
						  + sizeof(json_trailer) ];
			sz = sprintf(post, cp_post
					 , ws.res.fs->URL
					 , wl.parent->id);
			name_translate(lk, post + sz, TR_FILE, json_trailer);
			hc = curl_json_perform(post, &j, FILE_CP, source);
		}
		if (hOk == hc)
			p = strstr(j.pb, ws.res.fs->URL);
		json_free(&j);
	}


	refresh(wl.parent, REFRESH_LINK, true);
	if (hOk != hc || NULL == p) {
		/* Linking does not change source dir: refresh it on error only.
		 * If source = link's target, this refresh does nothing since
		 * we just refreshed the link's target directory above.
		 */
		refresh(ws.parent, REFRESH_LINK, false);
		if (hNotFound == hc)
			return -ENOENT;	 /* Probably parallel delete? */
		else
			return -EREMOTEIO;
	}
	return 0;
}

static int unfichier_link(const char *source, const char *link)
{
	int res;

	DEBUG(	"<<<(in) unfichier_link `%s` -> `%s`\n", source, link);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_link_wrapped(source, link);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_link `%s` -> `%s` res=%d\n"
			  , source, link, res);
	return res;
}

/*
 * @brief fuse callback for unlink
 *
 * ... and related functions.
 * As usual the callback is the wrapper for RCU and debug messages.
 *
 * For deleting a file in the upload directory, unlink_upload is called and
 * if root is / (default) will use the wrapped function, so that the user
 * will have the same view of the directory. If root is not '/', the upload
 * directory is not accessible within the mount, hence unlink_file is called
 * directly.
 *
 * @param path file to unlink
 * @return 0 if Ok or error code (negative)
 */


static char *unlink_file(const char *URL, enum hCode *hc, const char *filename)
{
	static const char rm_post[] = "{\"files\":[{\"url\":\"%s\"}]}";
	char post[sizeof(rm_post) + strlen(URL)];
	char *p = NULL;
	struct json j;

	json_init(&j);

	sprintf(post, rm_post, URL);
	*hc = curl_json_perform(post, &j, FILE_RM, filename);
	if (hOk == *hc)
		p = strstr(j.pb, URL);
	json_free(&j);

	return p;
}

static int unfichier_unlink_wrapped(const char *filename)
{
	static const char msg_unlink[] = "unlink";
	enum hCode hc;
	char *p = NULL;
	struct walk w;


	find_path(filename, &w);
	if (NULL == w.res.fs || w.is_dir)
		return -ENOENT;
	if (forbidden_write(w.parent, true, &w) || locked_files(&w, msg_unlink))
		return -EACCES;

	if (w.is_wf)
		return write_unlink(&w);

	p = unlink_file(w.res.fs->URL, &hc, filename);

	if (NULL == p) {
		refresh(w.parent, REFRESH_UNLINK, true);
		if (hNotFound == hc)
			return -ENOENT;	 /* Probably deleted in parallel! */
		else
			return -EREMOTEIO;
	}
	else {
		OR(w.res.fs->flags, FL_DEL);
	}

	return 0;
}

/*
 * @brief delete file from upload directory to the target.
 *
 * @param URL of the file to delete
 * @param name of the file to be deleted in the upload directory
 * @return O if success, othewise negative error code.
 */
int unlink_upload(const char * URL, const char *ftpname)
{
	enum hCode hc;

	if (NULL == unlink_file(URL, &hc, ftpname)) {
		if (hNotFound == hc)
			return -ENOENT;	 /* Parallel delete via web? */
		else
			return -EREMOTEIO;
	}
	refresh_for_write(&upload_d, get_upload_id());
	return 0;
}

static int unfichier_unlink(const char *path)
{
	int res;

	DEBUG("<<<(in) unfichier_unlink `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_unlink_wrapped (path);
	if (-EAGAIN == res)
		res = unfichier_unlink_wrapped (path);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_unlink `%s` res=%d\n"
			  , path, res);

	return res;
}


/*
 * @brief fuse callback for rmdir
 *
 * ... and related functions.
 * As usual the callback is the wrapper for RCU and debug messages.
 *
 * @param path directory to remove
 * @return 0 if Ok or error code (negative)
 */

static int do_rmdir(const char *dirname, long long id)
{
	static const char rmdir_post[] = "{\"folder_id\":%lld}";
	enum hCode hc;
	char post[sizeof(rmdir_post) + STRLEN_MAX_INT64];
	int res;
	struct json j;
	json_init(&j);

	sprintf(post, rmdir_post, id);
	hc = curl_json_perform(post, &j, FOLDER_RM, dirname);
	json_free(&j);

	if (hNotFound == hc) {
		res = -ENOENT;	/* Parallel delete? */
	}
	else {
		if (hOk == hc)
			res = 0;
		else
			res = -EREMOTEIO;
	}
	return res;
}

static int unfichier_rmdir_wrapped(const char *dirname, bool protect_upload)
{
	struct walk w;
	struct dentries *dent;
	unsigned long i;
	bool undo;
	int res;

	find_path(dirname, &w);
	if (NULL == w.res.dir || !w.is_dir)
		return -ENOENT;

	if (protect_upload && forbidden_write(w.parent, true, NULL))
			return -EACCES;

	init_dir(w.res.dir, &dent);
	if (0 != dent->n_subdirs)
		return -ENOTEMPTY;
	for (i = 0; i < dent->n_files; i++)
		if (!F_DEL(&dent->files[i]))
			return -ENOTEMPTY;

	/* rm upload directory only when not already used for write */
	if (get_upload_id() == w.res.dir->id) {
		if (!write_init_undo_1())
			return -EACCES;
		undo = true;
	}
	else {
		if (forbidden_change(&w))
			return -EACCES;
		undo = false;
	}

	res = do_rmdir(dirname, w.res.dir->id);
	refresh(w.parent, REFRESH_RMDIR, false);

	if (undo)
		write_init_undo_2(res);

	return res;
}

static int unfichier_rmdir(const char *dirname)
{
	int res;

	DEBUG("<<<(in) unfichier_rmdir `%s`\n", dirname);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_rmdir_wrapped(dirname, true);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_rmdir `%s` res=%d\n"
			  , dirname, res);
	return res;
}

int rmdir_upload(const char *dirname, long long id)
{
	if (def_root == root.name)
		return unfichier_rmdir_wrapped(dirname, false);
	else
		return do_rmdir(dirname, id);
}


/*
 * @brief fuse callback for mkdir
 *
 * ... and related functions.
 * As usual the callback is the wrapper for RCU and debug messages.
 *
 * @param path directory to remove
 * @return 0 if Ok or error code (negative)
 */


static int do_mkdir(const char *dirname, long long parent_id,
		    long long *created_id)
{
	int	    res;
	enum hCode hc;
	struct json j;
	static const char mkdir_post[]= "{\"folder_id\":%lld,\"name\":\"";
	char post[sizeof(mkdir_post) + STRLEN_MAX_INT64
				     + MAX_ESCAPED_LENGTH
				     + sizeof(json_trailer) ];

	json_init(&j);

	res = sprintf(post, mkdir_post, parent_id);
	name_translate(dirname, post + res, TR_DIR, json_trailer);

	hc = curl_json_perform(post, &j, FOLDER_MKDIR, dirname);


	if (hOk == hc) {
		if (ok_json_get(j.pb, json_folder_id
				    , (off_t *)created_id)) {
			res = 0;
		}
		else {
			lprintf(LOG_WARNING,
				"could not get created directory id in: %s\n",
				j.pb);
			res = -EREMOTEIO;
		}
	}
	else {
		res = -EREMOTEIO;
	}
	json_free(&j);

	return res;
}

static int unfichier_mkdir_wrapped(const char *path, long long *created_id,
				   bool protect_upload)
{
	int	    res;
	struct walk w;

	find_path(path, &w);
		/* These two tests should not happen. Fuse checks before call */
	if (NULL == w.parent)
		return -ENOENT;		/* Dir component of path not found */
	if (NULL != w.res.dir)
		return -EEXIST;

	if (protect_upload) {
		if (forbidden_write(w.parent, true, NULL))
			return -EACCES;

		if (reserved_names(path, w.parent->id))
			return -EACCES;

		res = forbidden_chars(path, TR_DIR);
		if (0 != res)
			return res;
	}

	res = do_mkdir(strrchr(path  , '/') + 1, w.parent->id, created_id);
	refresh(w.parent, REFRESH_MKDIR, false);

	return res;
}

int mkdir_upload(const char *path, const char *dirname, long long parent_id,
		 long long *created_id)
{
	if (def_root == root.name)
		return unfichier_mkdir_wrapped(path, created_id, false);
	else
		return do_mkdir(dirname, parent_id, created_id);
}

static int unfichier_mkdir(const char *path, mode_t unused)
{
	(void)unused;
	long long   created_id;
	int res;

	DEBUG("<<<(in) unfichier_mkdir `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_mkdir_wrapped(path, &created_id, true);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_mkdir `%s` res=%d\n"
			  , path, res);
	return res;
}

#ifdef _STATS
/*
 * @brief initialises stats buffer if needed and return the correct size
 *
 * @param the stats buffer
 * @param size of the buffer
 * @param offset from where to read
 * @return actual size considering real buffer length
 */
static size_t init_stat_buf(struct stats *s, size_t size, off_t offset)
{
	size_t stat_sz;

	if (0 == s->size) {
		/* Note that several read threads could lock, but only
		 * the first that locked will fill the buffer, the next
		 * one will read a non zero size and only return it.
		 * Also rcu lock since it is using the lfile head. */
		lock(&s->initmutex, NULL);

		/* Read size again since it will already be changed if this
		 * was not the first to lock. */
		if (0 == s->size) {
			rcu_register_thread();
			rcu_read_lock();

			out_stats((struct stats *)s);

			rcu_read_unlock();
			rcu_unregister_thread();
		}

		unlock(&s->initmutex, NULL);
	}

	stat_sz = s->size;

	if (offset + size >= stat_sz) {
		if (offset >= stat_sz)
			return 0;
		else
			return stat_sz - offset;
	}
	return size;
}
#endif

/*
 * @brief fuse callback for read
 *
 * Takes care of "special" cases such as: stats, refresh and write/read,
 * otherwise delegates to afsr_read.
 *
 * @param path to the file being read
 * @param buffer where we store read data
 * @param size of the buffer
 * @param offset from where we read
 * @param fuse internal pointer with possible private data
 * @return number of bytes read (should be size unless error or end of file)
 */
static int unfichier_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	struct lfile *s;
	struct fi_to_fh *fifh;
	int ret;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));

	if (TYPE_SPECIAL == fifh->type) {
		/* This is the constant address we have for refresh */
		if (fifh->fh == (uintptr_t)params1f.refresh_file)
			return 0;

#ifdef _STATS
		/* This is the stat file */
		struct stats *stts = (struct stats *)((uintptr_t)(fifh->fh));
		size = init_stat_buf(stts, size, offset);
		memcpy(buf, stts->buf + offset, size);
		return size;
#endif
	}
	else if (TYPE_WRITE == fifh->type) {
		return write_read(buf, size, offset,
				  (struct wfile *)((uintptr_t)(fifh->fh)));
	}

	s = (struct lfile *)((uintptr_t)(fifh->fh));

	DEBUG(	">> unfichier_read: %p (%"PRIu64",%lu) --------\n",
		s, offset, size);

	if (offset + size > s->size) {
		if (offset >= s->size)
			return 0;
		else
			size = s->size - offset;
	}

	ret = aw_read(path, buf, size, offset, fi);
	if (0 > ret && -EBUSY != ret)
		OR(s->counter, FL_ERROR);
	else
		if (0 == offset && s->has_encfs_key && size >= 8 )
			encfs_copy_key(buf, s->encfs_key);
	return ret;
}


/*
 * @brief this is the read_buf callback as fuse wants it!
 *
 *
 * @param path of the file to read.
 * @param buf vector to be allocated and populated.
 * @param size of requested data.
 * @param offset in that file where to start reading data.
 * @param fuse file info pointer as returned by the open/create fuse operation
 * @return number of bytes read, if negative: error.
 */
static int unfichier_read_buf(const char *path, struct fuse_bufvec **bufp,
		size_t size, off_t offset, struct fuse_file_info *fi)
{
	struct lfile *s;
	struct fi_to_fh *fifh;
	int ret;


	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));

	if (TYPE_SPECIAL == fifh->type) {
		/* This is the constant address we have for refresh */
		if (fifh->fh == (uintptr_t)params1f.refresh_file) {
			*bufp = init_bufp(0);
			return 0;
		}

#ifdef _STATS
		/* This is the stat file */
		struct stats *stts = (struct stats *)((uintptr_t)(fifh->fh));
		size = init_stat_buf(stts, size, offset);
		*bufp = init_bufp(size);
		memcpy((*bufp)->buf->mem, stts->buf + offset, size);
		return size;
#endif
	}
	else if (TYPE_WRITE == fifh->type) {
		*bufp = init_bufp(size);
		ret = write_read((*bufp)->buf->mem, size, offset,
				 (struct wfile *)((uintptr_t)(fifh->fh)));
		if (0 <= ret)
			(*bufp)->buf[0].size = ret;
		else
			(*bufp)->buf[0].size = 0;
		return ret;
	}

	s = (struct lfile *)((uintptr_t)(fifh->fh));

	DEBUG(	">> unfichier_read_buf: %p (%"PRIu64",%lu) --------\n",
		s, offset, size);

	if (offset + size > s->size)
		size = (offset >= s->size) ? 0 : s->size - offset;

	ret = aw_read_buf(path, bufp, size, offset, fi);
	if (0 > ret && -EBUSY != ret)
		OR(s->counter, FL_ERROR);
	else
		if (0 == offset && s->has_encfs_key
				&& (*bufp)->buf[0].size >= 8 ) {
			DEBUG("<< unfichier_read_buf: rewriting key: %#018"PRIX64"\n",
			      *((uint64_t *)(s->encfs_key)));
			encfs_copy_key((*bufp)->buf[0].mem, s->encfs_key);
		}
	return ret;
}


#ifdef _STATS
static struct stats *last_stats = NULL;
#endif

void unfichier_destroy(void * priv_data)
{
	lprintf(LOG_NOTICE, "Entering destroy.\n");

#ifdef _STATS
	/* Do one last stats if STATS are on and needed */
	if (NULL != params.stat_file &&
	    (NULL != log_fh || params1f.dry_run || params.foreground)) {
		last_stats = malloc(sizeof(struct stats));
		last_stats->size = 0;
		out_stats(last_stats);
	}
#endif
	STORE(exiting, true);

	write_destroy();
}

static struct fuse_operations unfichier_oper = {
	.init		= unfichier_init,
	.destroy	= unfichier_destroy,
	.create		= unfichier_create,
	.write		= unfichier_write,
	.getattr	= unfichier_getattr,
	.readdir	= unfichier_readdir,
	.open		= unfichier_open,
	.read		= unfichier_read,
	.read_buf	= unfichier_read_buf,
	.release	= unfichier_release,
	.statfs		= unfichier_statfs,
	.rename		= unfichier_rename,
	.unlink		= unfichier_unlink,
	.rmdir		= unfichier_rmdir,
	.link		= unfichier_link,
	.mkdir		= unfichier_mkdir,
	.utimens	= unfichier_utimens,
	.chmod		= unfichier_chmod,
	.chown		= unfichier_chown,
	.flush		= unfichier_flush,
	.fsync		= unfichier_fsync,
};

/* Note: this long_names list must be alphabetically sorted */
extern const char *long_names[];	/* Defined in     1fichierfs_option.c */
extern const unsigned int s_long_names;	/* Issue #3 - see 1fichierfs_option.c */
static const char end_expand[] = "Alo";

void cleanup(struct fuse_args *args)
{
	unsigned int i;
	struct lfiles *cur;

	lprintf(LOG_INFO,"Cleaning upon fuse unmount.\n");

	curl_global_cleanup();
	fuse_opt_free_args(args);

	if (def_user_agent != params.user_agent)
		free(params.user_agent);
	if (def_type != params.filesystem_subtype)
		free(params.filesystem_subtype);
	if (def_fsname != params.filesystem_name)
		free(params.filesystem_name);
	free(params1f.no_ssl.a);
	free(params1f.encfs.a);
	if (NULL != root.dent)
		refresh(&root, REFRESH_EXIT, false);
	if (def_root != root.name)
		free(root.name);
	else
		free(params1f.remote_root);	/* For dry-run */
	cur = LOAD(live);
	if (NULL != cur) {
		for (i = 0; i < cur->n_lfiles; i++)
			free_lfile(cur->a_lfiles[i]);
		free(cur);
	}

	rcu_free_struct(upload_d.dent, rcu_free_dentries);

	STORE(live, NULL);  /* For final stats not to crash, since it's freed!*/

	rcu_unregister_thread();
	rcu_exit();

	free(params.ca_file);
	free(params1f.refresh_file);
	free(params1f.api_key);
	free(params1f.ftp_user);
	free(auth_header);
	free(params.log_file);
#ifdef _STATS
	if (NULL != params.stat_file) {
		if (NULL != last_stats) {
			if (NULL != log_fh)
				fputs(last_stats->buf, log_fh);
			else
				fprintf(stderr, "%s", last_stats->buf);
			free(last_stats);
		}
		free(params.stat_file);
	}
#endif
#ifdef _MEMCOUNT
	struct stats stts;
	stts.size = 0;
	out_stats_mem(stts.buf, stts.size, STAT_MAX_SIZE);
	lprintf(LOG_INFO, "Last memory status for leak check.\n");
	if (NULL != log_fh)
		fputs(stts.buf, log_fh);
	else if (params1f.dry_run || params.foreground)
		fprintf(stderr, "%s", stts.buf);
#endif

	if (NULL != log_fh)
		fclose(log_fh);
}

/*
 * @brief initialises the remote root when needed
 *
 * @param none
 * @return none
 */
static void init_remote_root()
{
	struct walk w;
	struct dir remote;

	find_path(params1f.remote_root, &w);
	if (NULL == w.res.dir)
		lprintf( LOG_ERR, "root path: `%s` does not exist.\n"
				, params1f.remote_root);
	if (!w.is_dir)
		lprintf( LOG_ERR, "root path: `%s` is not a directory.\n"
				, params1f.remote_root);
	lprintf(LOG_INFO,"Mounting only the remote path: %s\n"
			, params1f.remote_root);
	/* Do so because w.res.dir cannot be derefenced after refresh */
	memmove(&remote, w.res.dir, sizeof(struct dir));
	refresh(&root, REFRESH_INIT_ROOT, false);
	root.id		= remote.id;
	if (0 != root.id)	/* Shared roots don't have a cdate */
		root.cdate = remote.cdate;
	root.email_off  = remote.email_off;
	/* Note the encfs and no_ssl flags of w.res.dir are irrelevant because
	 * paths are made relative to the remote root. The root's flags encfs
	 * and no_ssl stay relevant since clean_paths (in 1fichierfs_options.c)
	 * sets those on the root when the remote root is equal or a suddir
	 * of one of the paths. */
	root.shared	= remote.shared;
	root.readonly	= remote.readonly;
	root.hidden	= remote.hidden;
	root.name	= params1f.remote_root;
	root.parent	= NULL;	/* For no_ssl, encfs lookups */
	if (root.readonly)
		params1f.readonly = true;
}

/*
 * @brief reads the root of the account
 *
 * Initialize root now to check the API Key, ans that the server responds,
 * so that if it fails the program exits before starting the fuse daemon.
 *
 * Then look if the upload directory can be found. It is limited to be
 * directly under root (for now) and if different mount path was specified
 * the real root of the account will be refreshed and freed.
 *
 * If a not NULL remote path is passed, checks it exists and is a directory,
 * then refreshes the tree whole tree and set this remote path as root.
 * The global 'root' will get the remote name: needed for e-mail on shares
 *
 * @param none
 * @return none
 */

static void init_root()
{
	struct dentries *dent;

	upload_d.name= (char *)upload_dir + 1;

	init_dir(&root, &dent);
	root.cdate = params.mount_st.st_mtim.tv_sec;

	write_init_foreground1(params1f.remote_root);

	if (NULL != params1f.remote_root && !params1f.dry_run)
		init_remote_root();

	write_init_foreground2(params1f.remote_root);
}


/******************************
 */
int main(int argc, char *argv[])
{
	unsigned int ret;
	struct parse_data data = {false, NULL, 0UL, 0UL, &unfichier_oper};
	struct fuse_args exp_args = {argc, argv, false};
	struct fuse_args args;
	struct timespec t;

	clock_gettime(CLOCK_REALTIME, &t);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &main_start);

	expand_args(&exp_args, long_names, s_long_names, end_expand);

	/* First pass = counting (for memory allocation) + help, version, etc */
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	args.allocated = false;
	ret = fuse_opt_parse(&args, &data, unfichier_opts, unfichier_opt_count);
	fuse_opt_free_args(&args);

	if (0 == ret) {
		check_args_first_pass(&data);

	/* Second pass = parse all arguments + populate allocated memory      */
		args.argc = exp_args.argc;
		args.argv = exp_args.argv;
		ret = fuse_opt_parse(&args,
				     &data,
				     unfichier_opts,
				     unfichier_opt_proc);
	}

	if (exp_args.allocated)
		free(exp_args.argv);
	if (0 != ret)
		lprintf(LOG_ERR, "parsing arguments.\n");


	/* Check arguments, and output debug information */
	check_args_second_pass(&t);
	auth_header = malloc(sizeof(auth_bearer) + strlen(params1f.api_key));
	memcpy(auth_header, auth_bearer, lengthof(auth_bearer));
	strcpy(auth_header + lengthof(auth_bearer), params1f.api_key);

#ifdef _MEMCOUNT
	if (CURLE_OK != curl_global_init_mem(CURL_GLOBAL_ALL,
					     fs_alloc,
					     aw_free,
					     aw_realloc,
					     fs_strdup,
					     fs_calloc))
#else
	if (CURLE_OK != curl_global_init_mem(CURL_GLOBAL_ALL,
					     malloc,
					     free,
					     realloc,
					     strdup,
					     calloc))
#endif
		lprintf( LOG_ERR, "initializing curl: curl_global_init().\n" );

	rcu_register_thread(); /* Unregister is inside cleanup  */
	init_root();

	if (!params1f.dry_run) {
		sigset_t set;

		check_args_third_pass(&args);

		/* Starting the fuse mount now!
		*/
		lprintf(LOG_NOTICE,"Init done, starting fuse mount.\n");

		init_done  = true;

		/* see: https://curl.se/mail/lib-2018-12/0076.html
		 * CURLOPT_NOSIGNAL is not enough with multi-threading, better
		 * to completely ignore SIGPIPE on all threads, although it
		 * seems to matter only when exiting, which is minor. */
		sigemptyset(&set);
		sigaddset(&set, SIGPIPE);
		pthread_sigmask(SIG_BLOCK, &set, NULL);

		if (params.no_splice_read)
			unfichier_oper.read_buf = NULL;

		ret = fuse_main(args.argc,
				args.argv,
				&unfichier_oper,
				NULL);

		aw_destroy();
	}

	/* Cleaning */
	cleanup(&args);

	return ret;
}
