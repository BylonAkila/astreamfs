/*
 * astreamfs and derivative work (1fichierfs) common code
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#define STREAM_STRUCT void
#include "astreamfs_worker.h" /* worker_mem would be ok, but _STATS is needed */
#include "astreamfs_util.h"

/*
 * @brief Global parameters of the daemon.
 *
 */
struct params params= {
/*	struct stat mount_st;	 */	{0},
/*	bool	foreground;	 */	false,
/*	bool	debug;		 */	false,
/*	bool	noatime;	 */	false,
/*	bool	noexec;		 */	false,
/*	bool	nosuid;		 */	false,
/*	bool	no_splice_read;  */	false,
/*	unsigned int  log_level; */	LOG_WARNING,
/*	unsigned int  threads    */	0,
/*	unsigned long log_size   */	0L,
/*	char *log_file;		 */	NULL,
/*	char *user_agent;	 */	NULL,
/*	char *filesystem_name;	 */	NULL,
/*	char *filesystem_subtype;*/	NULL,
/*	char *ca_file;		 */	NULL,
#ifdef _STATS
/* 	char * stat_file	*/	NULL,
#endif
	  };

bool init_done = false;
FILE *log_fh = NULL;
struct timespec main_start;
#ifdef _DEBUG
	/* fOR log-size handling */
	static off_t log_offset = 0;
	static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

struct aw_info engine_info;	/* For stats and DL token type, init here. */

/*
 * @brief log utility.
 *
 * The log utility printf either on terminal, on a file or to syslog depending
 * on the situation.
 *
 * We have here global external variables, macro and function definition.
 *
 * The use is with the macro lprintf(LOG_xxx, ...)
 * LOG_xxx is a log level as defined in syslog.h
 * The rest are the same paramaters as printf.
 */

void lprintf(int level, const char *pFormat, ... )
{
	char msg[1024];
	static const char *msg_err_lvl[]= {
		"EMERGENCY",
		"ALERT",
		"CRITICAL",
		"ERROR",
		"WARNING",
		"NOTICE",
		"INFO",
		"DEBUG" };
	int s, t;
	va_list ap;
	struct timespec now;

	if ( level <= params.log_level ) {
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		if (now.tv_nsec < main_start.tv_nsec) {
			now.tv_nsec += 1000000000L;
			now.tv_sec -= 1L;
		}
		now.tv_sec  -= main_start.tv_sec;
		now.tv_nsec -= main_start.tv_nsec;

		/* Line header. Keep char constants short! */
		s = sprintf(msg, "[%s %5lu.%03lu] %s: ",
			   log_prefix, now.tv_sec, now.tv_nsec / 1000000,
			   msg_err_lvl[level]);

		va_start(ap, pFormat);
		t = vsnprintf(msg + s, sizeof(msg) - s, pFormat, ap);
		va_end(ap);
		t += s;
		/* In case of msg overflow, add the final \n. */
		if (t >= sizeof(msg)) {
			msg[sizeof(msg) - 2]='\n';
			t = sizeof(msg) - 1;
		}

		if (NULL == log_fh) {
			if (init_done && !params.foreground)
				syslog(LOG_DAEMON | level, "%s", msg);
			else
				fprintf(stderr, "%s", msg);
		}
		else {
#ifdef _DEBUG
			int err;
			#define MB (1024*1024)

/* Note for LOG_DEBUG level that logs a lot, this section is locking.
 * Anyway fprintf/flush does already lock... but to be fairer for paralellism
 * there should be a separate logger. The only disadvantage of a separate
 * logger is that some messages could be lost when a crash occurs. */

			if (0 != params.log_size) {
				err = pthread_mutex_lock(&log_mutex);
				if (0 != err)
					exit(err);
				log_offset += t;
				if (log_offset > MB * params.log_size) {
					err = fseek(log_fh, 0L, SEEK_SET);
					if (0 != err)
						exit(err);
					log_offset = t;
				}
			}
#endif
			fprintf(log_fh, "%s", msg);
			fflush(log_fh);
#ifdef _DEBUG
			if (0 != params.log_size) {
				err = pthread_mutex_unlock(&log_mutex);
				if (0 != err)
					exit(err);
			}
#endif
		}
	}
	if (( init_done && level <= LOG_CRIT) ||
	    (!init_done && level <= LOG_ERR ))
		exit(EXIT_FAILURE);
}

/*
 * @brief Memory allocation utility.
 *
 * Simplifies handling potential error on memory allocation.
 * Makes use of functions exposed by astreamfs_worker_mem
 *
 */

#ifdef _MEMCOUNT

/* This one is provided only to initialise libcurl, otherwise used as macro */
void *fs_alloc(size_t size)
{
	return aw_aligned_alloc(alignof(max_align_t), size);
}

void *fs_calloc(size_t nmemb, size_t size)
{
	void *p;
	p = aw_aligned_alloc(alignof(max_align_t), nmemb * size);
	if (NULL != p)
		memset(p, '\0', nmemb * size);
	return p;
}

char *fs_strdup (const char *s)
{
	size_t len;
	void *new;
	if (NULL == s)
		return NULL;
	len = strlen(s) + 1;
	new = aw_aligned_alloc(alignof(max_align_t), len);
	if (NULL == new)
		return NULL;
	return (char *)memcpy(new, s, len);
}
#endif

/*
 * @brief Mutex lock-unlock utility.
 *
 * Simplifies handling and logging of mutex lock/unlock.
 * When the state pointer locked is NULL, the function locks/unlocks the
 * mutex and logs.
 * When the state pointer is not NULL, if the state corresponds to the action
 * the function does nothing but logging, otherwise the lock/unlock happens
 * and the state is changed to reflect the new mutex state.
 *
 * @param: mutex pointer to lock/unlock
 * @param: pointer to the state of the mutex (locked= true/false)
 * @return: none
 */

void lock(pthread_mutex_t *mutex, bool *locked)
{
	int err;

	if (NULL != locked && *locked)
		return;
	err= pthread_mutex_lock(mutex);
	DEBUG("lock(%p) %d\n", mutex, (NULL != locked)?*locked : 999);
	if (NULL != locked)
		*locked = true;
	if (0 != err)
		lprintf(LOG_CRIT,"pthread_mutex_lock failed with %d\n", err);
}

void unlock(pthread_mutex_t *mutex, bool *locked)
{
	int err;
#ifdef _DEBUG
	int tmp = 999;
#endif

	if (NULL != locked) {
#ifdef _DEBUG
		tmp = *locked;
#endif
		if (!*locked)
			return;
		else
			*locked = false;
	}
	DEBUG("unlock(%p) %d\n", mutex, tmp);
	err= pthread_mutex_unlock(mutex);
	if (0 != err)
		lprintf(LOG_CRIT,"pthread_mutex_lock failed with %d\n", err);
}

/*
 * @brief semaphore wait, post, init, etc... utlity.
 *
 * Simplifies handling and logging of sem_wait, sem_post, sem_trywait
 * trywait will always return 0 or EGAIN and fail (crit) on other errors.
 * timedwait will always return 0 or ETIMEDOUT and fail (crit) on other errors.
 *
 * @param: semaphore pointer
 * @return: ret code of the function
 */
int wait(sem_t *sem)
{
	int err;

	err = sem_wait(sem);
	if (0 != err)
		lprintf(LOG_CRIT,"sem_wait failed with %d\n", err);
	DEBUG( "sem_wait(%p)\n", sem);
	return err;
}

int post(sem_t *sem)
{
	int err;

	err = sem_post(sem);
	if (0 != err)
		lprintf(LOG_CRIT,"sem_post failed with %d\n", err);
	DEBUG( "sem_post(%p)\n", sem);
	return err;
}

int trywait(sem_t *sem)
{
	int err, tmp;
	errno = 0;

	err = sem_trywait(sem);
	tmp = errno;
	if (0 != err && EAGAIN != errno)
		lprintf(LOG_CRIT,"sem_trywait failed with %d errno=%d\n"
				, err, tmp);
	DEBUG( "sem_trywait(%p) err=%d errno=%d\n", sem, err, tmp);
	errno = tmp;
	return err;
}

int timedwait(sem_t *sem, struct timespec *until)
{
	int err, tmp;
	errno = 0;

	err = sem_timedwait(sem, until);
	tmp = errno;
	if (0 != err && ETIMEDOUT != errno)
		lprintf(LOG_CRIT,"sem_timedwait failed with %d errno=%d\n"
				, err, tmp);
	DEBUG( "sem_timedwait(%p) err=%d errno=%d\n", sem, err, tmp);
	errno = tmp;
	return err;
}

/*
 * @brief allocates and initialises a fuse_bufvec suitable for read_buf
 *
 * @param size for data buffer.
 * @return the allocated and initialised fuse_bufvec pointer.
 */
struct fuse_bufvec * init_bufp(size_t size)
{
	struct fuse_bufvec *bufp;

	bufp = malloc(sizeof(struct fuse_bufvec));
	memset(bufp, '\0', sizeof(struct fuse_bufvec));
	aw_release(bufp);
	bufp->count = 1;
	bufp->buf[0].fd   = -1;
	bufp->buf[0].size = size;
	if (0 != size) {
		bufp->buf[0].mem  = malloc(size);
		aw_release(bufp->buf[0].mem);
	}
	return bufp;
}

/*
 * @brief Global contention logging and counting.
 *
 * @param: spin count
 * @param: function name
 * @return: none
 */
void spin_add(unsigned long spin, const char* fct_name)
{
	if (0 == spin)
		return;
	lprintf(LOG_INFO, "%lu contention(s) in %s\n", spin, fct_name);
	aw_count_contentions(spin);
}


/*
 * @brief initialises the aw_worker + stats if necessary
 *
 * @param operations structure for the worker
 * @param aw_info to return, n_threads to be set as the desired nb of threads
 * @return private data fuse context
 */
void *worker_init(const struct aw_operations *aw_ops,
			struct aw_info       *engine_info)
{
	struct fuse_context *context;
	int  i;

	i = aw_init(aw_ops, engine_info->n_threads, engine_info->n_threads);
	if (0 != i)
		lprintf(LOG_CRIT, 
			"%d: could not initialise asynchronous reader.\n", i);

	aw_get_info(engine_info);
	lprintf(LOG_INFO,
		"Asynchronous stream engine succesfully initialised with %u threads, %u streams, %u block size, %u max gap.\n",
		engine_info->n_threads , engine_info->n_streams,
		engine_info->block_size, engine_info->max_gap );

#ifdef _STATS
	if (NULL != params.stat_file) {
		i = aw_init_stats();
		if (0 != i)
			lprintf(LOG_CRIT, 
				"%d: during statistics initialisation.\n", i);
	}
#endif

	context = fuse_get_context();
	return context->private_data;
}


/*
 * @brief auxilliary function to wait on a socket
 *
 * @param the socket to wait on
 * @param events to wait for
 * @param timeout in milliseconds
 * @return -1 if error, otherwise the length of the rq string.
 */
int wait_on_socket(curl_socket_t sockfd, int events, long timeout_ms)
{
	struct pollfd pfd;
	int res;
 
	pfd.fd = sockfd;
	pfd.events = events; 

	res = poll(&pfd, 1, timeout_ms);
	
	/* timeout or error: 0 or -1 */
	if (0 >= res)
		return res;

	/* Error on the soket */
	if (0 != (pfd.revents & (POLLERR | POLLHUP | POLLNVAL)))
		return -1;

	/* Should be 1 here. */
	return res;
}


/** curl_raw temporary code */
 
#define HTTP_HEADERS_MORE   0
#define HTTP_HEADERS_DONE   1
#define HTTP_HEADERS_ERROR -1

/* Consume headers from the buffer
 *
 * @param structure to gather header information
 * @param buffer where the headers are stored
 * @param offset of the start of an header in the buffer
 * @param length of the buffer
 * @return status: continue, end, error
 */
static int consume_http_headers(size_t (*callback)(char *p, size_t, size_t, void *),
				void *userdata,
				char *rq, off_t *off, size_t *sz)
{
	size_t size;
	char *p, *q;

	for(p = q = rq + *off; q < rq + (*sz) - 1; q++)
		if (*q == '\x0d' && *(q + 1) == '\x0a') {
			if (p == q) {
				/* Empty line, headers ended */
				*off = q + 2 - rq;
				return HTTP_HEADERS_DONE;
			}
			size = q - p + 2;
			if (callback(p, size, 1, userdata) != size) {
				DEBUG("header callback stopped the request\n");
				return HTTP_HEADERS_ERROR;
			}
			q++;
			p = q + 1; /* Start of next header */
		}
	/* Exited loop without finding CR-LF at the end of the buffer */
	if (MAX_RQ_SZ == *sz) {
		/* Header is too long to fit in the MAX_RQ_SZ buffer */
		if (0 == *off)
			return HTTP_HEADERS_ERROR;
		/* Time to shift the remaining bytes */
		*sz -= p - rq;
		DEBUG("shifting header %ld\n", (long)*sz);
		memmove(rq, p, *sz);
		*off = 0;
	}
	else {
		*off = p - rq;
	}
	return HTTP_HEADERS_MORE;
}

static const char request_template[] = "GET /%s HTTP/1.1\r\nHost: %s%s\r\n%sRange: bytes=%s\r\n\r\n";
static const char user_agent_fmt[] = "User-Agent: %s\r\n";
static const char http1_1[] = "HTTP/1.1";
static const char transfer_encoding[] = "Transfer-Encoding:";
static const char content_range[] = "Content-Range:";

struct common_checks {
	bool http1_1_checked;
	bool chunked;
	size_t (*callback)(char *p, size_t, size_t, void *);
	void *userdata;
};

/*
 * @brief utility that grabs the status code from the first response line
 *
 * @param header buffer
 * @param buffer size
 * @param where to store the result
 * @return none. Stores 0 in result on error.
 */
void http_response_status_code(char *buf, size_t sz, int *http_code)
{
	char *p, *q;
	p = buf + lengthof(http1_1);
	q = buf + sz;
	while (p < q && isblank((unsigned char)*p))  /* Skip spaces */
		p++;
	if (p + 3 >= q ||			  /* Grap status code */
	    !isdigit((unsigned char)* p)      ||
	    !isdigit((unsigned char)*(p + 1)) ||
	    !isdigit((unsigned char)*(p + 2)) ||
	    !isblank((unsigned char)*(p + 3)) ) {
		lprintf(LOG_ERR ,
			"Malformed HTTP response code: %.*s\n",
			sz, buf);
		*http_code = 0;
		return;
	}
	*http_code = 100 * (* p      - '0') + 
		      10 * (*(p + 1) - '0') +
			   (*(p + 2) - '0');
}

/*
 * @brief utility that grabs the file size from the range response header
 *
 * @param header buffer
 * @param buffer size
 * @param where to store the size
 * @return true if ok, false if error
 */
bool http_file_size(char *buf, size_t sz, off_t *file_size)
{
	long long val;
	char *p, *q;
	
	if (lengthof(content_range) >= sz)   /* Too short not content-range!*/
		return true;
	if (0 != strncasecmp(buf, content_range, lengthof(content_range)))
		return true;
	
	/* This is Content-Range header, extract file size */
	p = buf + lengthof(content_range);
	q = buf + sz;
	while (p < q && '/' != *p)
		p++;
	if (p == q) {
		lprintf(LOG_ERR ,
			"Malformed Content-Range response header: %.*s\n",
			sz, buf);
		return false;
	}
	errno = 0;
	val = strtoll(p + 1, NULL, 10);
	if (0 != errno) {
		lprintf(LOG_ERR ,
			"error converting file size: %.*s\n",
			sz, buf);
		return false;
	}
	*file_size = val;
	return true;
}

/* Common check since so far is only supported:
 * - HTTP/1.1
 * - No chunked encoded
 *
 * Note: this is the same prototype as curl's header function
 * 
 * @param buffer with the header
 * @param size of each member of data
 * @param number of members of data
 * @param pointer to user data
 * @return either size*nemb if Ok or anyting else when error
 */
static size_t headers_common_checks(char *buf, size_t size, size_t nmemb
					     , void * userdata)
{
	struct common_checks *cck = (struct common_checks *)userdata;
	size_t sz = size * nmemb - 2; 	/* Readbility, -2 to remove CR-LF */

	if (!cck->http1_1_checked) {
		if (lengthof(http1_1) > sz ||
		    0 != strncasecmp(buf, http1_1, lengthof(http1_1))) {
			lprintf(LOG_ERR ,
				"This is not HTTP/1.1 protocol: %.*s\n",
				sz, buf);
			return 0;
		}
		cck->http1_1_checked = true;
	}
	else {
		if (lengthof(transfer_encoding) < sz &&
		    0 == strncasecmp(buf, transfer_encoding
					, lengthof(transfer_encoding))) {
			cck->chunked = true;
		}
	}
	return cck->callback(buf, size, nmemb, cck->userdata);
}

/*
 * @brief builds and sends the request
 *
 * @param curl handle
 * @param the url
 * @param additionnal headers
 * @param the user's callback (same as curl's header callback)
 * @param the userdata to be passed to the user's callback
 * @param the offset for range size
 * @param size of the range (0 means up to end of the file)
 * @param timeout indication (if needed pass not NULL)
 * @return -1 if error, otherwise the length of the rq string.
 */
struct curl_raw_handle 
	*send_request(	CURL *curl, char *url, const char *hdr,
			size_t (*callback)(char *p, size_t, size_t, void *),
			void *userdata, off_t off, size_t size, bool *timeout)
{
	size_t len, nsend_total, nrecv_total, nsend;
	int    rqsz, res, sockfd;
	char   range[2*STRLEN_MAX_INT64 + 2]; /* 2: '-' and \0 */
	char   rq[MAX_RQ_SZ + 1]; /* +1 for \0 when reding headers */
	char   user_agent_hdr[(NULL == params.user_agent) ? 
				1 : strlen(params.user_agent) + 
				    sizeof(user_agent_fmt)];
	struct curl_raw_handle *crh;
	struct common_checks cck;
	bool tmout;

	if (NULL == timeout)
		timeout = &tmout;	/* Avoids retesting if not passed */
	*timeout = false;
	/** Build the request */
	len = strlen(url);
	char buf[len + 1], *host, *path = NULL;
	strcpy(buf, url);
	host = strchr(buf, '/');
	if (NULL != host && '/' == *(host + 1)) {
		char *p;
		/* This is the // marking the end of the scheme */
		host += 2;
		path = strchr(host, '/');
		if (NULL != path) {
			*path = '\0'; /* host becomes a C string */
			path += 1;    /* path points to start of path */
		}
		p = strchr(host, ':');	/* If any, removes port from host */
		if (NULL != p)
			*p = '\0';
	}
	if (NULL == host || NULL == path) {
		lprintf(LOG_ERR, "cnx only malformed url: %s\n", url);
		return NULL;
	}

	if (0 == size)
		sprintf(range, "%"PRIu64"-", (uint64_t)off);
	else
		/* 'Single' requests get a precise range */
		sprintf(range, "%"PRIu64"-%"PRIu64
			     , (uint64_t)off, (uint64_t)(off + size - 1));
	if (NULL == params.user_agent)
		user_agent_hdr[0] = '\0';
	else
		sprintf(user_agent_hdr, user_agent_fmt, params.user_agent);
	rqsz = snprintf(rq, MAX_RQ_SZ, request_template
			  , path, host, hdr, user_agent_hdr, range);
	if (MAX_RQ_SZ <= rqsz) {
		lprintf(LOG_ERR, "request is too long: %s\n", rq);
		return NULL;
	}

	/** Send the request */
	/* Do the "connect only" */
	CURL_EASY_SETOPT(curl, CURLOPT_CONNECT_ONLY, 1L, "%ld");

	/* And force it to http 1.1 since there is no point doing 2.0+ */
	CURL_EASY_SETOPT(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1
			     , "%ld");
		
	res = curl_easy_perform(curl);
				
	if (CURLE_OK != res) {
		lprintf(LOG_ERR, "curl connect only: %s\n"
			       , curl_easy_strerror(res));
		return NULL;
	}

	/* Extract the socket from the curl handle, needed for waiting*/
	res = curl_easy_getinfo(curl, CURLINFO_ACTIVESOCKET, &sockfd);
 
	if (CURLE_OK != res) {
		lprintf(LOG_ERR, "cnx only get socket: %s\n"
			       , curl_easy_strerror(res));
		return NULL;
	}

	nsend_total = 0;
	do {
		do {
			nsend = 0;
			res = curl_easy_send(curl,
					     rq + nsend_total,
					     rqsz - nsend_total,
					     &nsend);
			nsend_total += nsend;
 
			if (nsend_total < rqsz && CURLE_AGAIN == res) {
				int ret;
				ret = wait_on_socket(sockfd, POLLOUT, 10000L);

				if (0 == ret) {
					lprintf(LOG_ERR,
						"cnx only socket timeout\n");
					*timeout = true;
					return NULL;
				}
				if (-1 == ret) {
					lprintf(LOG_ERR,
						"cnx only socket error\n");
					return NULL;
				}
			}
		} while(nsend_total < rqsz && CURLE_AGAIN == res);
			
		if (CURLE_OK != res) {
			lprintf(LOG_ERR ,
				"cnx only curl send error: %s\n",
				curl_easy_strerror(res));
			return NULL;
		}
 
		DEBUG("cnx sent %ld bytes.\n", (long)nsend);

/* Define that to dump the request/response header */
// #define DUMP_RQ
#ifdef DUMP_RQ
		DEBUG("Rq was:\n%s\n",rq);
#endif
	} while (nsend_total < rqsz);

	/* Start reading the response's http 1.1 header */
	nrecv_total = 0;
	off = 0;
	cck.http1_1_checked = false;
	cck.chunked = false;
	cck.callback = callback;
	cck.userdata = userdata;
	do {
		size_t nrecv;
		
		do {

			res = curl_easy_recv(curl,
					     rq + nrecv_total,
					     MAX_RQ_SZ - nrecv_total,
					     &nrecv);

			nrecv_total += nrecv;

			if (0 == nrecv && CURLE_AGAIN == res) {
				int ret;
				ret = wait_on_socket(sockfd, POLLIN, 10000L);

				if (0 == ret) {
					lprintf(LOG_ERR,
						"cnx only socket timeout\n");
					*timeout = true;
					return NULL;
				}
				if (-1 == ret) {
					lprintf(LOG_ERR,
						"cnx only socket error\n");
					return NULL;
				}
			}
		} while(0 == nrecv && CURLE_AGAIN == res);

		rq[nrecv_total] = '\0';

		res = consume_http_headers(headers_common_checks, &cck,
					   rq, &off, &nrecv_total);

	} while (HTTP_HEADERS_MORE == res);

#ifdef DUMP_RQ
	DEBUG("Header dump:\n%.*s", nrecv_total, rq);
#endif
	if (HTTP_HEADERS_ERROR == res)
		return NULL;

	DEBUG("cnx only recv %ld bytes of header, remains %ld.\n",
		(long)nrecv_total, (long)(nrecv_total - off));

	/* From here, all is Ok with header, allocate the raw struct */
	nrecv_total -= off;
	crh = malloc(sizeof(struct curl_raw_handle) + nrecv_total);
	crh->curl		= curl;
	crh->sockfd		= sockfd;
	crh->used		= 0;
	crh->chunked		= cck.chunked;
	crh->remains		= nrecv_total;
	crh->transfer_complete	= false;
	crh->transfer_rc	= 0;
	if (0 != nrecv_total)
		memcpy(crh->overflow, rq + off, nrecv_total);	
	return crh;
}

/* Function called on system error in astream_worker (apart from malloc).
 *
 *	System errors are typically semaphore error codes or the like.
 *	The result of the system call is passed to the function.
 *	The function should either exit the program, considering
 *	such errors are critical thus never return.
 *	Returning to the caller after such errors will probably
 *	trigger undefined behaviour.
 *
 * @param the error (0 = no error)
 * @param line where the error happened
 * @return the error (but see comment, it should not return!)
 */
int astreamfs_error(int err, int line)
{
	if (0 != err)
		lprintf(LOG_CRIT,
			"system error %d in the read engine at line %d.\n",
			err, line);
	return err;
}

/* Optimisation callback to store an opaque optimisation data
 *
 * @param struct fi from fuse (returned at open)
 * @param the last opaque optimisation data to store
 * @return none
 */
void astreamfs_store_last_opt(struct fuse_file_info *fi, uint32_t opt)
{
	struct fi_to_fh *fifh;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));
	STORE(fifh->last_opt, opt, relaxed);
}

/* Optimisation callback to get the opaque optimisation data
 *  When streaming, the optimisation avoid a potentially costly
 *  search on all the streams to find the right one.
 * 
 * @param struct fi from fuse (returned at open)
 * @return the last opaque optimisation data
 */
uint32_t astreamfs_load_last_opt(struct fuse_file_info *fi)
{
	struct fi_to_fh *fifh;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));
	return LOAD(fifh->last_opt, relaxed);
}

/* Optimisation callback to get the unique remote file reference
 *  from the struct fuse_file_info pointer 'fi'.
 *
 *  See detailed comments in astreamfs_worker.h about the unique
 *  remote file reference.
 *
 * @param struct fi from fuse (returned at open)
 * @return the 'fh' private user data
 */
uint64_t astreamfs_fh_from_fi(struct fuse_file_info *fi)
{
	struct fi_to_fh *fifh;

	fifh = (struct fi_to_fh *)((uintptr_t)(fi->fh));
	return fifh->fh;
}

/* callback to read a chunk of a stream
 *
 *	This has the same semantics as the system read function,
 *	except for return value on error. Instead of returning -1
 *	and setting errno, the error must be returned as a negative
 *	value, same as expected by fuse (eg return -errno)
 *
 *	Since polling is used by the main loop of the asynchronous
 *	reader, it is perfectly fine if this read callback does not
 *	retrieve as much data as expected. It can also happen that
 *	read returns no data although the socket was ready (eg openssl
 *	buffering), in such case it should return -EAGAIN. This
 *	discriminates from returning no data with no error, which
 *	means End of File: only case this function should return 0.
 *
 * @param opaque stream handle (returned by start_stream) to read from
 * @param buffer where to store read data
 * @param count max bytes expected
 * @return Ok: number of bytes read, KO: negative error
 */
ssize_t astreamfs_read_callback(void *sh, void *buf, size_t count)
{
	struct curl_raw_handle *crh = (struct curl_raw_handle *)sh;
	size_t written, sz;
	int res;

	if (crh->chunked)
		return -ENOSYS;	/* /TODO: this means: not implemented! */

	if (0 != crh->remains) {/* Using the initial overflow if some remains */
		sz = (count > crh->remains) ? crh->remains : count;
		memcpy(buf, crh->overflow + crh->used, sz);
		crh->remains -= sz;
		crh->used    += sz;
		if (sz == count)
			return sz;
		written = sz;
	}
	else {
		if (crh->transfer_complete)
			return (CURLE_OK == crh->transfer_rc) ? 0 : -EIO;

		written = 0;
	}

	res = curl_easy_recv(crh->curl, ((char *)buf) + written
				      , count - written, &sz);
	if (CURLE_OK == res) {
		if (0 == sz) {
			crh->transfer_complete = true;
			crh->transfer_rc = res;
		}
		written += sz;
		return written;
	}

	/* CURLE_AGAIN on the first read after the header should return
	 * written when there was an overflow copied to the buffer */
	if (CURLE_AGAIN == res)
		return (0 == written) ? -EAGAIN : written;

	crh->transfer_complete = true;
	crh->transfer_rc = res;
	return (CURLE_RECV_ERROR == res) ? -EREMOTEIO : -EIO;
}

#if defined(_MEMCOUNT) || defined(_STATS)
/*
 * @brief raw formater with divider and exposant matrix
 *
 *   See format_fr() comments
 *
 * @param number to format
 * @param buffer to store the result (must be at least SZ_BUF_HR size)
 * @param divider to use (eg 1000, 1024)
 * @param exposant matrix
 * @return the buffer formatted.
 */
const char expo[]= { 'K', 'M', 'G', 'T', 'P', 'E' };

static char *raw_format_hr(unsigned long long num, char *buf,
			   int divider, const char *expo )
{
	unsigned long frac;
	int i;
	bool wide;

	if (num < 99999) {
		sprintf(buf, "%5lu", (unsigned long)num);
		return buf;
	}

	if (0 ==  (expo[0] & '\x80'))
		i = -1;
	else
		i = 0;
	for (; num >= 9999; i++) {
		frac = num % divider;
		num /= divider;
	}

	if (0 ==  (expo[i] & '\x80')) {
		wide = false;
	}
	else {
		wide = true;
		i--;
	}

	if (num >= 100) {
		sprintf(buf, "%4lu%c", (unsigned long)num
				     , expo[i]);
		if (wide) {
			buf[5] = expo[i+1];
			buf[6] = '\0';
		}
		return buf;
	}
	if (num >= 10) {
		sprintf(buf, "%lu.%lu%c", (unsigned long)num
					, (frac * 10) / divider
					, expo[i]);
	}
	else {
		sprintf(buf, "%lu.%.02lu%c", (unsigned long)num
					   , (frac * 100) / divider
					   , expo[i]);
	}
	if (wide) {
		buf[5] = expo[i+1];
		buf[6] = '\0';
	}
	return buf;
}

/*
 * @brief format an unsigned long long as "human readable" à la 'curl'
 *
 *   The result is always 5 characters long (+ '\0')
 *   If possible display 5 digits (only if num is under 99999)
 *   otherwise display max 4 digits and a possible exponent (K, M, G, etc...)
 *   According to the number of digit in the integer part, displays fraction
 *   digits.
 *   Examples: 24370  1792K  92.1M  1.04G
 *
 * @param number to format
 * @param buffer to store the result (must be at least SZ_BUF_HR size)
 * @return the buffer formatted.
 */
char *format_hr(unsigned long long num, char *buf)
{
	return raw_format_hr(num, buf, KILO, expo);
}
#endif

#ifdef _STATS

/*
 * @brief format an signed long long as "human readable" à la 'curl'
 *
 *   Same as format_hr, but for signed numbers
 *
 * @param number to format
 * @param buffer to store the result (must be at least SZ_BUF_HR size)
 * @return the buffer formatted.
 */
char *format_sr(long long num, char *buf)
{
	unsigned long frac;
	unsigned long long pos;
	int i;

	if (num >= 0)
		return format_hr(num, buf);
	pos = num * -1;
	if (pos <= 9999) {
		sprintf(buf, "%5ld", (long)num);
	}
	else {
		for (i = -1; pos >= 999; i++) {
			frac = pos % KILO;
			pos /= KILO;
		}
		if (pos >= 10) {
			sprintf(buf, "%4ld%c", (long)pos * -1
					     , expo[i]);
		}
		else {
			sprintf(buf, "%ld.%lu%c", (long)pos * -1
						, (frac * 10) / KILO
						, expo[i]);
		}
	}
	return buf;
}

/*
 * @brief formats very small quantities (one digit)
 *
 * @param quatity
 * @return digit (or * if more than 9).
 */
char small_value(unsigned int v)
{
	if (9 < v)
		return '*';
	return ('0' + v);
}

/*
 * @brief returns the current time index
 *
 *  Gets the current time (MONOTONIC_COARSE).
 *  Compute the index from the delay (now - main_start) shifting sec and nsec
 *  according to the speed resolution (SPEED_RES)
 *
 * @param timespec pointer where the current timespec will be stored
 * @return time index.
 */
tm_index_t tm_to_tm_index(struct timespec *now)
{
	clock_gettime(CLOCK_MONOTONIC_COARSE, now);
	now->tv_sec -= main_start.tv_sec;
	return  ((tm_index_t)now->tv_sec ) * SPEED_RES +
		((tm_index_t)now->tv_nsec) / (CLOCK_RES / SPEED_RES);
}

/*
 * @brief returns a time average
 *
 *  Gets the average of the time stored in the timespec for n items
 *
 * @param timespec
 * @param number of items for the average
 * @param buffer to store the result
 * @return the result.
 */
const char *ts_avg_to_str(struct timespec *t,
			  unsigned long long n, char *buf)
{
	unsigned long long res;
	unsigned long sec;
	static const char undef_avg[]= "      ";
	char frac[SZ_BUF_TIME];

	if (0 == n)
		return undef_avg;
	res  = (((uint64_t)(t->tv_sec) * CLOCK_RES) + t->tv_nsec) / n;

	sec  = (unsigned long)(res / CLOCK_RES);
	sprintf(frac, "%lu",  (unsigned long)(CLOCK_RES + res % CLOCK_RES));
	if (0 == sec)
		sprintf(buf, ".%.05s", frac + 1);
	else if (10 > sec)
		sprintf(buf, "%lu.%.04s", sec, frac + 1);
	else if (100 > sec)
		sprintf(buf, "%lu.%.03s", sec, frac + 1);
	else
		sprintf(buf, "%6lu", sec);
	return buf;
}

/*
 * @brief displays the stats header: uptime
 *
 * @param pointer to the stats structure
 * @param where to store the timespec for now
 * @param where to store the tm_index for now
 * @return size written so far.
 */
size_t out_stat_header(struct stats *stts, struct timespec *now
					 , tm_index_t *tm_now)
{
	unsigned int days, hours, minutes;
	

	/****************************
	 * Part: Uptime
	 */
	*tm_now  = tm_to_tm_index(now);
	days     =  now->tv_sec / 86400;
	hours    = (now->tv_sec % 86400) / 3600;
	minutes  = (now->tv_sec %  3600) /   60;
	if (0 != days)
		SNPRINTF("Uptime: %ud %.02u:%.02u:%.02u\n\n",
			 days, hours, minutes,(unsigned int)(now->tv_sec % 60));
	else
		SNPRINTF("Uptime: %.02u:%.02u:%.02u\n\n",
			 hours, minutes, (unsigned int)(now->tv_sec % 60));

	return stts->size;
}

/*
 * @brief displays the readers statistic part
 *
 * @param pointer to the stats structure
 * @param where file stats (needed for the list of active files)
 * @param function to get the reference of active files
 * @param user data to pass to that function
 * @return size written so far.
 */
size_t out_stat_read(struct stats *stts,
		     struct aw_stats *f_stats,
		     const char *(ref)(uint64_t, char *, void *),
		     void *userdata)
{
	unsigned int i, j;
	uint64_t last_fh;
	struct aw_stats t_stats[engine_info.n_threads + 1];
	struct aw_solo_stats s_stats;
	char avg0[SZ_BUF_TIME], max0[SZ_BUF_TIME];
	char avg1[SZ_BUF_TIME], max1[SZ_BUF_TIME];
	char buf0[SZ_BUF_HR], buf1[SZ_BUF_HR], buf2[SZ_BUF_HR], buf3[SZ_BUF_HR];
	char buf4[SZ_BUF_HR], buf5[SZ_BUF_HR], buf6[SZ_BUF_HR];
	char bufref[SZ_BUF_REF];

	static const char d1[] = { '\xe2', '\x94', '\x9c',
				   '\xe2', '\x94', '\x80', '\0' };
	static const char d2[] = { '\xe2', '\x94', '\x94',
				   '\xe2', '\x94', '\x80', '\0' };

	/****************************
	 * Part: Readers (Total)
	 */
	SNPRINTF("Readers:\n%s",
"    Files    Streams  Start streams             Fuse requests\n"
"   Cur/Tot.  Cur/Tot. AvgTim MaxTim Down. N.Req AvgTim MaxTim Speed Av.Sp Over.\n");

	aw_get_stats(&t_stats[0], f_stats, &s_stats);

	for (i = 1; i < engine_info.n_threads + 1; i++) {
		if (0 == t_stats[i].n_reads)
			continue;
	/* Thread (if any stat is present) */
		SNPRINTF("%.2u:%2u %s  %2u %s %s %s %s %s %s %s %s %s %s\n",
			 i, t_stats[i].u.g.active_files,
			 format_hr(t_stats[i].u.g.n_files, buf0),
			 t_stats[i].active_streams,
			 format_hr(t_stats[i].n_streams, buf1),
			 ts_avg_to_str(&t_stats[i].start_times.t_sum,
					t_stats[i].n_streams, avg0),
			 ts_avg_to_str(&t_stats[i].start_times.t_max, 1, max0),
			 format_hr(t_stats[i].size , buf2),
			 format_hr(t_stats[i].n_reads, buf3),
			 ts_avg_to_str(&t_stats[i].read_times.t_sum,
					t_stats[i].n_reads, avg1),
			 ts_avg_to_str(&t_stats[i].read_times.t_max, 1, max1),
			 format_hr(t_stats[i].cur_speed,buf4),
			 format_hr(t_stats[i].avg_speed,buf5),
			 format_sr(t_stats[i].overread ,buf6)
			);
	/* Files on the Thread (if any) */
		/* get last fh (for the decoration!) */
		last_fh = 0;
		for (j = 0; j < engine_info.n_streams; j++)
			if (0 != f_stats[j].u.f.fh &&
			    i - 1 == f_stats[j].u.f.tid)
				last_fh = f_stats[j].u.f.fh;
		/* When no fh (last_fh = 0) skip files detail: no active files*/
		if (0 == last_fh)
			continue;
		for (j = 0; j < engine_info.n_streams; j++) {
			if (0 == f_stats[j].u.f.fh ||
			    i - 1 != f_stats[j].u.f.tid)
				continue;
			SNPRINTF("%s%s %c/%c %2u %s %s %s %s %s %s %s %s %s %s\n",
				 ((f_stats[j].u.f.fh == last_fh) ? d2 : d1),
				 ref(f_stats[j].u.f.fh, bufref, userdata),
				 small_value(f_stats[j].u.f.n_rq),
				 small_value(f_stats[j].u.f.n_buf),
				 f_stats[j].active_streams,
				 format_hr(f_stats[j].n_streams, buf1),
				 ts_avg_to_str(&f_stats[j].start_times.t_sum,
						f_stats[j].n_streams, avg0),
				 ts_avg_to_str(&f_stats[j].start_times.t_max,
						1, max0),
				 format_hr(f_stats[j].size , buf2),
				 format_hr(f_stats[j].n_reads, buf3),
				 ts_avg_to_str(&f_stats[j].read_times.t_sum,
						f_stats[j].n_reads, avg1),
				 ts_avg_to_str(&f_stats[j].read_times.t_max,
						1, max1),
				 format_hr(f_stats[j].cur_speed,buf4),
				 format_hr(f_stats[j].avg_speed,buf5),
				 format_sr(f_stats[j].overread ,buf6)
			);
		}

	}
	SNPRINTF("%s",
"-------------------------------------------------------------------------------\n");
	SNPRINTF("Tt %2u %s  %2u %s %s %s %s %s %s %s %s %s %s\n",
		 t_stats[0].u.g.active_files,
		 format_hr(t_stats[0].u.g.n_files, buf0),
		 t_stats[0].active_streams,
		 format_hr(t_stats[0].n_streams, buf1),
		 ts_avg_to_str(&t_stats[0].start_times.t_sum,
				t_stats[0].n_streams, avg0),
		 ts_avg_to_str(&t_stats[0].start_times.t_max, 1, max0),
		 format_hr(t_stats[0].size , buf2),
		 format_hr(t_stats[0].n_reads, buf3),
		 ts_avg_to_str(&t_stats[0].read_times.t_sum,
				t_stats[0].n_reads, avg1),
		 ts_avg_to_str(&t_stats[0].read_times.t_max, 1, max1),
		 format_hr(t_stats[0].cur_speed,buf4),
		 format_hr(t_stats[0].avg_speed,buf5),
		 format_sr(t_stats[0].overread ,buf6)
		);
	/* Solo if any */
	if (0 != s_stats.solo_n_reads) {
		double avspd, tsum;

		tsum = ((double)s_stats.solo_times.t_sum.tv_sec * CLOCK_RES +
			(double)s_stats.solo_times.t_sum.tv_nsec) / CLOCK_RES;
		avspd = (double)s_stats.solo_size / tsum;

		SNPRINTF("Solo:                               %s %s %s %s       %s\n",
			 format_hr(s_stats.solo_size , buf2),
			 format_hr(s_stats.solo_n_reads, buf3),
			 ts_avg_to_str(&s_stats.solo_times.t_sum,
					s_stats.solo_n_reads, avg1),
			 ts_avg_to_str(&s_stats.solo_times.t_max, 1, max1),
			 format_hr((unsigned long long)avspd,buf5)
			);
	}
	SNPRINTF("%s", "\n");

	return stts->size;
}

#endif

#ifdef _MEMCOUNT
/*
 * @brief prepare the content of the memory stats.
 * Note: _MEMCOUNT can be defined and no stats
 * 
 * @param buffer where to write the statistics
 * @param offset in the buffer (written so far)
 * @param max size of the buffer
 * @return 
 */
size_t out_stats_mem(char *buf, size_t offset, size_t max)
{
	struct aw_mem_stats m_stats;
	char buf1[SZ_BUF_HR], buf2[SZ_BUF_HR];
	size_t n;

/* Macro to check buffer filling after each snprintf */
#define CHECK_BUF(n,o,m) if (n >= m - o) { return m - 1; } else { o += n; }

	aw_get_mem_stats(&m_stats, memory_order_acquire);

	n = snprintf(buf + offset, max - offset,
		     "             Number Memory\nAllocations:  %s  %s\n",
		     format_hr(m_stats.n_alloc, buf1),
		     format_hr(m_stats.mem_alloced, buf2));
	CHECK_BUF(n, offset, max)
	n = snprintf(buf + offset, max - offset,
		     "Frees      :  %s  %s\n",
		     format_hr(m_stats.n_free, buf1),
		     format_hr(m_stats.mem_freed, buf2));
	CHECK_BUF(n, offset, max)
	n = snprintf(buf + offset, max - offset,
		     "Difference :  %s  %s\n",
		     format_hr(m_stats.n_alloc - m_stats.n_free, buf1),
		     format_hr(m_stats.mem_alloced - m_stats.mem_freed, buf2));
	CHECK_BUF(n, offset, max)
	n = snprintf(buf + offset, max - offset,
		     "Contentions:  %s  %s\n\n",
		     format_hr(m_stats.contentions, buf1),
		     format_hr(m_stats.max_spins  , buf2));
	CHECK_BUF(n, offset, max)
	return offset;
}
#endif

/*
 * @brief bsearch callback to compare long names options
 *
 * @param pointer on key
 * @param pointer on a member of the search elements
 * @return <0 >0 or ==0 according to order of key/element
 */
static int long_names_cmp(const void *k, const void *m)
{
	unsigned char c1, c2;
	unsigned const char *p = *(unsigned char **)m;
	unsigned const char *l = k;

	do {
		c1 = *l++;
		c2 = *p++;
		if ('=' == c2 || '\0' == c2)
			return ('\0' == c1) ? 0 : 1;
	} while (c1 == c2);

	return (c1 < c2) ? -1 : 1;
}

/*
 * @brief Expands arguments from the command line
 *
 * This is needed since fuse_opt_parse does not understand GNU style combined
 * single letters arguments. For example, this will transform
 *  	-JOLl7
 * into
 * 	-J -O -L -l7
 *
 * Also, fuse_opt_parse is picky: the function handles long arguments so that
 * --arg value, is accepted as --arg=value.
 * This expansion is not done for "fuse-like" arguments listed after -o, since
 * it would not make sense. They MUST be specified as: -o arg=value
 *
 * This function uses its parameter in/out initialized as argc/argv in input.
 * If there is no expansion to do, the param remains unchanged.
 * When expansion is needed, the count is changed accordingly and memory is
 * allocated in one chunk for both the new *argv[] structure and the new
 * arguments strings space. The caller must then free argv when done with
 * newly allocated argument strings.
 *
 * @param (in/out) fuse_args struct pointer
 * @param array of long names that call for a second argument
 * @param number of elements in the array of long names
 * @param in one string, short arguments that call for a second argument
 * @return none
 */
void expand_args(struct fuse_args *args,
		 const char **long_names, size_t n_long_names,
		 const char *end_expand)
{
	unsigned int i, j, k, m, argc;
	int new_args;
	char **argv, *p;
	size_t mem_needed;

	new_args = 0;
	mem_needed = 0;
	argc = args->argc;
	argv = args->argv;
	for (i=1; i < argc; i++) {
		if ('-' != argv[i][0] || '\0' == argv[i][0])
			continue;
		if ('-' == argv[i][1] ) {
			if ('\0' == argv[i][2])
				break;
			if ('=' == argv[i][strlen(argv[i]) -1])
				lprintf(LOG_ERR,
					"`%s` is not a valid option.\n",
					argv[i]);
			if ( i < argc - 1 &&
			     NULL != bsearch(argv[i] + 2, long_names,
					     n_long_names, sizeof(char *),
					     long_names_cmp)) {
				new_args--;
				mem_needed += strlen(argv[i]) +
					      strlen(argv[i + 1]) + 2;
				i++;
			}
			continue;
		}
		if ('\0' == argv[i][1])
			lprintf(LOG_ERR, "`-` is not a valid option.\n");
		for (j=1; '\0' != argv[i][j + 1]; j++) {
			if (NULL != strchr(end_expand, argv[i][j]))
				break;
			new_args++;
			mem_needed += 3;
		}
		if (j > 1) {
			if ( '\0' == argv[i][j+1] )
				mem_needed +=  3;
			else
				mem_needed += strlen(argv[i]+j) + 2;
		}
	}

	if (0 == mem_needed)
		return;

	args->argc += new_args;
	args->argv = malloc(sizeof(char *) * (argc + new_args) + mem_needed);
	args->allocated= true;
	args->argv[0]= argv[0];
	p=(char *)(args->argv + args->argc);
	for (i = 1, j = 1; i < argc; i++, j++) {
		args->argv[j]= argv[i];
		if ('-' != argv[i][0] || '\0' == argv[i][0])
			continue;
		if ('-' == argv[i][1]) {
			if ('\0' == argv[i][2]) {
				break;
			}
			if ( i < argc - 1 &&
			     NULL != bsearch(argv[i] + 2, long_names,
					     n_long_names, sizeof(char *),
					     long_names_cmp)) {
				args->argv[j] = p;
				m = strlen(argv[i]);
				memcpy(p, argv[i], m);
				p += m;
				*p = '=';
				p++;
				i++;
				m = strlen(argv[i]);
				strcpy(p, argv[i]);
				p += m + 1;
			}
			continue;
		}
		if ('\0' == argv[i][2])
			continue;
		for (k=1, j--; '\0' != argv[i][k]; k++) {
			j++;
			if (NULL != strchr(end_expand, argv[i][k])) {
				if (k > 1) {
					args->argv[j] = p;
					*p = '-';
					p++;
					m = strlen(argv[i] + k) + 1;
					memcpy(p, argv[i] + k, m);
					p += m;
				}
				break;
			}
			args->argv[j] = p;
			p[0] = '-';
			p[1] = argv[i][k];
			p[2] = '\0';
			p += 3;
		}
	}
	for (; i < argc; i++, j++)
		args->argv[j] = argv[i];
}


/*
 * @brief checks the mount directory path
 *
 * Note: strerror is not thread safe, but more portable than strerror_r, and
 *       since this is used before daemonizing and multithreading, there is
 *       no race condition here.
 *
 * @param path of the mountpoint to check
 * @return none
 */
void check_mount_path(const char *path)
{
	if (NULL == path)
		lprintf(LOG_ERR, "no mountpoint path specified.\n");
	if (-1 == stat(path, &params.mount_st)) {
		lprintf(LOG_ERR , "getting stats of `%s`: %s\n"
				, path, strerror(errno));
	}
	if ( ! S_ISDIR(params.mount_st.st_mode) )
		lprintf(LOG_ERR, "`%s` is not a directory\n",  path);
	params.mount_st.st_nlink = 2;
}


/*
 * @brief logs the start date-time and start message
 *
 * Note: main should start with clock_gettime to pass the start time.
 *
 * @param pointer to time_t representing the start date
 * @return none
 */
void start_message(time_t *t)
{
	struct tm local_time;
	char buf[80];
	if (NULL == localtime_r(t, &local_time))
		lprintf(LOG_CRIT, "error %d on localtime.\n", errno);
	if (0 == strftime(buf, sizeof(buf), "%A %e %B %Y at %X",
			  &local_time))
		lprintf(LOG_CRIT, "error on strftime.\n", errno);
	lprintf(LOG_NOTICE, "started: %s\n", buf);
	lprintf(LOG_INFO, "successfuly parsed arguments.\n");
}

/*
 * @brief extracts an unsigned integer from an argument string
 *
 * @param the argument string
 * @return the integer (unsigned 64 bits)
 */
uint64_t read_int(const char *arg)
{
	uint64_t i = 0;
	const char *p;

	if ('-' == *arg && '-' != *(arg +1))
		arg += 2;
	else
		for (; '\0' != *arg; arg++)
			if ('=' == *arg) {
				arg++;
				break;
			}
	for(p = arg; isdigit(*p) && '\0' != *p; p++) {
		/* Max uint64 is 18446744073709551615 (=2^64 -1).
		 * This is the overflow test here. */
		if ( 1844674407370955161 <  i ||
		    (1844674407370955161 == i && '5' < *p ))
			lprintf(LOG_ERR,
				"Number overflow: %s\n", arg);
		i = i*10 + *p - '0';
	}
	if ('\0' != *p)
		lprintf(LOG_ERR,
			"malformed argument. Expecting number, got: %s\n", arg);
	return i;
}

/*
 * @brief extracts and stores a string from a long argument, or -o fuse form.
 *
 * It does also free a previously allocated parameter in case we repeat
 * multiple times the same option.
 *
 * @param the argument string
 * @param where to store it
 * @return none
 */
void read_str(const char *arg, char **where)
{
	char *p;

	p = strchr(arg, '=');
	if (NULL == p)
		lprintf(LOG_ERR, "malformed argument: %s\n", arg);
	free(*where);
	*where = malloc(strlen(p));
	strcpy(*where, p + 1);
}


/*
 * @brief string utility: identical length at begining of 2 strings 
 *
 * @param string 1
 * @param string 2
 * @return how long the strings are equal
 */
size_t eqlen(const char *s1, const char *s2)
{
	size_t len = 0;
	
	while ('\0' != *s1 && *s1++ == *s2++ )
		len++;

	return len; 
}


