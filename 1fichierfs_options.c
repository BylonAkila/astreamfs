/*
 * 1fichierfs: deals with the option line and options checking
 *
 *
 * Copyright (C) 2018-2025  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <locale.h>
#include <ctype.h>


#include "1fichierfs.h"

/*******************************************************
 *  These are the things you can change at compile time.
 */
const char def_user_agent[] = PROG_NAME"/"PROG_VERSION;
const char def_type[]   = PROG_NAME;		/* fs type as shown by mount  */
const char def_fsname[] = "1fichier.com";	/* fs name as shown by mount  */

/********************************************************
 *  Do NOT change the following definitions or variables.
 *  Things would break!
 *  These definitions and variables are either system dependent,
 *  or depend on how 1fichier works.
 */

/* Global immutable constant strings for various use */
static const char ro_opt[]="-oro,fsname=";
static const char rw_opt[]="-orw,big_writes,fsname=";
static const char subtype[]=",subtype=";

/* Character table of base64 as used in the API Key  */
static char base64url[128] =
{
/* 0x00 */	0, 0, 0, 0, 0, 0, 0, 0,	 0, 0, 0, 0, 0, 0, 0, 0,
/* 0x10 */	0, 0, 0, 0, 0, 0, 0, 0,	 0, 0, 0, 0, 0, 0, 0, 0,
/* 0x20 */	0, 0, 0, 0, 0, 0, 0, 0,	 0, 0, 0, 0, 0, 1, 0, 0,
/* 0x30 */	1, 1, 1, 1, 1, 1, 1, 1,	 1, 1, 0, 0, 0, 1, 0, 0,
/* 0x40 */	0, 1, 1, 1, 1, 1, 1, 1,	 1, 1, 1, 1, 1, 1, 1, 1,
/* 0x50 */	1, 1, 1, 1, 1, 1, 1, 1,	 1, 1, 1, 0, 0, 0, 0, 1,
/* 0x60 */	0, 1, 1, 1, 1, 1, 1, 1,	 1, 1, 1, 1, 1, 1, 1, 1,
/* 0x70 */	1, 1, 1, 1, 1, 1, 1, 1,	 1, 1, 1, 0, 0, 0, 0, 0,
};


enum {
     KEY_HELP,
     KEY_VERSION,
     KEY_DEBUG,
     KEY_FOREGROUND,
     KEY_RO,
     KEY_RW,
     KEY_UID,
     KEY_GID,
     KEY_NO_SPLICE_READ,
     KEY_SPLICE_READ,
     KEY_NOATIME,
     KEY_NOEXEC,
     KEY_NOSUID,
     KEY_FS_NAME,
     KEY_FS_SUBTYPE,
     KEY_LOG_FILE,
     KEY_LOG_LEVEL,
     KEY_LOG_SIZE,
     KEY_CURL_IPV4,
     KEY_CURL_IPV6,
     KEY_CURL_USER_AGENT,
     KEY_CURL_INSECURE,
     KEY_CURL_CACERT,
     KEY_1FICHIER_API_KEY,
     KEY_1FICHIER_STAT_FILE,
     KEY_1FICHIER_REFRESH_FILE,
     KEY_1FICHIER_REFRESH_HIDDEN,
     KEY_1FICHIER_REFRESH_TIME,
     KEY_1FICHIER_NO_SSL,
     KEY_1FICHIER_ENCFS,
     KEY_1FICHIER_ROOT,
     KEY_1FICHIER_FTP_USER,
     KEY_1FICHIER_NO_UPLOAD,
     KEY_1FICHIER_DRY_RUN,
     KEY_1FICHIER_RESUME_DELAY,
     KEY_1FICHIER_RAW_NAMES,
     KEY_1FICHIER_NETWORK_WAIT,
     KEY_1FICHIER_THREADS,
};

static const char arg_log_file[]	= "--log-file=%s";
static const char arg_log_level[]	= "--log-level=%i";
static const char arg_log_size[]	= "--log-size=%i";
static const char arg_ipv4[]		= "--ipv4";
static const char arg_ipv6[]		= "--ipv6";
static const char arg_user_agent[]	= "--user-agent=%s";
static const char arg_insecure[]	= "--insecure";
static const char arg_cacert[]		= "--cacert=%s";
static const char arg_api_key[]		= "--api-key=%s";
static const char arg_stat_file[]	= "--stat-file=%s";
static const char arg_refresh_file[]	= "--refresh-file=%s";
static const char arg_refresh_time[]	= "--refresh-time=%i";
static const char arg_refresh_hidden[]	= "--refresh-hidden";
static const char arg_no_ssl[]		= "--no-ssl=%s";
static const char arg_encfs[]		= "--encfs=%s";
static const char arg_root[]		= "--root=%s";
static const char arg_ftp_user[]	= "--ftp-user=%s";
static const char arg_no_upload[]	= "--no-upload";
static const char arg_dry_run[]		= "--dry-run";
static const char arg_resume_delay[]	= "--resume-delay=%i";
static const char arg_raw_names[]	= "--raw-names";
static const char arg_network_wait[]	= "--network-wait=%i";
static const char arg_threads[]		= "--threads=%i";
static const char arg_debug[]		= "--debug";

/* This const table must be alphabetically ordered */
const char *long_names[] = {
	arg_api_key		+ O_OFFSET,
	arg_cacert		+ O_OFFSET,
	arg_encfs		+ O_OFFSET,
	arg_ftp_user		+ O_OFFSET,
	arg_log_file		+ O_OFFSET,
	arg_log_level		+ O_OFFSET,
	arg_log_size		+ O_OFFSET,
	arg_no_ssl		+ O_OFFSET,
	arg_refresh_file	+ O_OFFSET,
	arg_refresh_time	+ O_OFFSET,
	arg_resume_delay	+ O_OFFSET,
	arg_root		+ O_OFFSET,
	arg_stat_file		+ O_OFFSET,
	arg_user_agent		+ O_OFFSET,
};
const unsigned int s_long_names = (sizeof(long_names) / sizeof(long_names[0]));

#define IS_O_ARG(arg) ('-' != arg[0])	/* o args: only ones not start with - */

#define SZ_LONG_ARG_USER_AGENT (lengthof(arg_user_agent) - SZ_STD_ARG_TYPE)
#define SZ_O_ARG_USER_AGENT (SZ_LONG_ARG_USER_AGENT - O_OFFSET)

#define SZ_LONG_ARG_CACERT (lengthof(arg_cacert) - SZ_STD_ARG_TYPE)
#define SZ_O_ARG_CACERT (SZ_LONG_ARG_CACERT - O_OFFSET)

#define SZ_LONG_ARG_API_KEY (lengthof(arg_api_key) - SZ_STD_ARG_TYPE)
#define SZ_O_ARG_API_KEY (SZ_LONG_ARG_API_KEY - O_OFFSET)

#define SZ_LONG_ARG_NO_SSL (lengthof(arg_no_ssl) - SZ_STD_ARG_TYPE)
#define SZ_O_ARG_NO_SSL (SZ_LONG_ARG_NO_SSL - O_OFFSET)

#define SZ_LONG_ARG_ENCFS (lengthof(arg_encfs) - SZ_STD_ARG_TYPE)
#define SZ_O_ARG_ENCFS (SZ_LONG_ARG_ENCFS - O_OFFSET)

const struct fuse_opt unfichier_opts[] = {
  /* our specific options */
  FUSE_ALL_ARG( "-l %i"	, arg_log_level	 , (int)KEY_LOG_LEVEL		),
  FUSE_L_O_ARG(		  arg_log_file	 , (int)KEY_LOG_FILE		),
  FUSE_L_O_ARG(		  arg_log_size	 , (int)KEY_LOG_SIZE		),

  /* 1fichier specific options */
  FUSE_L_O_ARG(		  arg_api_key	, (int)KEY_1FICHIER_API_KEY	),
  FUSE_L_O_ARG(		  arg_stat_file	, (int)KEY_1FICHIER_STAT_FILE	),
  FUSE_L_O_ARG(	arg_refresh_file	, (int)KEY_1FICHIER_REFRESH_FILE),
  FUSE_L_O_ARG( arg_refresh_time	, (int)KEY_1FICHIER_REFRESH_TIME),
  FUSE_L_O_ARG( arg_refresh_hidden	, (int)KEY_1FICHIER_REFRESH_HIDDEN),
  FUSE_L_O_ARG(		  arg_no_ssl	, (int)KEY_1FICHIER_NO_SSL	),
  FUSE_L_O_ARG(		  arg_encfs	, (int)KEY_1FICHIER_ENCFS	),
  FUSE_L_O_ARG(		  arg_root	, (int)KEY_1FICHIER_ROOT	),
  FUSE_L_O_ARG(		  arg_ftp_user	, (int)KEY_1FICHIER_FTP_USER	),
  FUSE_L_O_ARG(		  arg_no_upload	, (int)KEY_1FICHIER_NO_UPLOAD	),
  FUSE_L_O_ARG(		  arg_dry_run	, (int)KEY_1FICHIER_DRY_RUN	),
  FUSE_L_O_ARG(		  arg_raw_names	, (int)KEY_1FICHIER_RAW_NAMES	),
  FUSE_L_O_ARG(	arg_network_wait	, (int)KEY_1FICHIER_NETWORK_WAIT),
  FUSE_L_O_ARG(		  arg_threads	, (int)KEY_1FICHIER_THREADS     ),
  FUSE_L_O_ARG(	arg_resume_delay	, (int)KEY_1FICHIER_RESUME_DELAY),

  /* curl options we understand */
  FUSE_ALL_ARG( "-4"	, arg_ipv4	 , (int)KEY_CURL_IPV4		),
  FUSE_ALL_ARG( "-6"	, arg_ipv6	 , (int)KEY_CURL_IPV6		),
  FUSE_ALL_ARG( "-A %s"	, arg_user_agent , (int)KEY_CURL_USER_AGENT	),
  FUSE_ALL_ARG( "-k"	, arg_insecure	 , (int)KEY_CURL_INSECURE	),
  FUSE_L_O_ARG(		  arg_cacert	 , (int)KEY_CURL_CACERT		),

  /* fuse standard options */
  FUSE_OPT_KEY( "-h"			, (int)KEY_HELP			),
  FUSE_OPT_KEY( "--help"		, (int)KEY_HELP			),
  FUSE_OPT_KEY( "-V"			, (int)KEY_VERSION		),
  FUSE_OPT_KEY( "--version"		, (int)KEY_VERSION		),
  FUSE_OPT_KEY( "ro"			, (int)KEY_RO			),
  FUSE_OPT_KEY( "rw"			, (int)KEY_RW			),
  FUSE_ALL_ARG( "-d"	, arg_debug	, (int)KEY_DEBUG		),
  FUSE_OPT_KEY( "-f"			, (int)KEY_FOREGROUND		),
  FUSE_OPT_KEY( "-o "			, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "-s"			, (int)FUSE_OPT_KEY_KEEP	),

  FUSE_OPT_KEY( "allow_other"		, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "allow_root"		, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "auto_unmount"		, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "nonempty"		, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "default_permissions"	, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "fsname=%s"		, (int)KEY_FS_NAME		),
  FUSE_OPT_KEY( "subtype=%s"		, (int)KEY_FS_SUBTYPE		),
  FUSE_OPT_KEY( "large_read"		, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "max_read=%i"		, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "hard_remove"		, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "noatime"		, (int)KEY_NOATIME		),
  FUSE_OPT_KEY( "noexec"		, (int)KEY_NOEXEC		),
  FUSE_OPT_KEY( "nosuid"		, (int)KEY_NOSUID		),
  FUSE_OPT_KEY( "nodev"			, (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "uid=%i"		, (int)KEY_UID			),
  FUSE_OPT_KEY( "gid=%i"		, (int)KEY_GID			),

  FUSE_OPT_KEY( "use_ino"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "readdir_ino"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "direct_io"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "kernel_cache"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "auto_cache"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "noauto_cache"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "umask=%s"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "entry_timeout=%s"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "negative_timeout=%s"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "attr_timeout=%s"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "ac_attr_timeout=%s"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "noforget"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "remember=%s"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "nopath"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "modules=%s"		 , (int)FUSE_OPT_KEY_KEEP ),
  /* Discard max_write to prevent the user from specifying a low value that
   * would hurt performance */
  FUSE_OPT_KEY( "max_write=%i"		 , (int)FUSE_OPT_KEY_DISCARD ),
  FUSE_OPT_KEY( "max_readahead=%i"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "max_background=%i"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "congestion_threshold=%i", (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "async_read"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "sync_read"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "atomic_o_trunc"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "big_writes"		 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "no_remote_lock"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "no_remote_flock"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "no_remote_posix_lock"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "splice_write"		 , (int)FUSE_OPT_KEY_DISCARD ),
  FUSE_OPT_KEY( "no_splice_write"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "splice_move"		 , (int)FUSE_OPT_KEY_DISCARD ),
  FUSE_OPT_KEY( "no_splice_move"	 , (int)FUSE_OPT_KEY_KEEP ),
  FUSE_OPT_KEY( "splice_read"		 , (int)KEY_SPLICE_READ),
  FUSE_OPT_KEY( "no_splice_read"	 , (int)KEY_NO_SPLICE_READ),
  FUSE_OPT_END
};

#define MSG_ERR_ADD_OPT_ARG "adding option argument (fuse_opt_add_arg).\n"

/*
 * @brief reads the API key from a file
 *
 * The file must contain only the API key string.
 * If path is "", read from stdin.
 * If path starts with ~/ it is extended with the home of the specifie UID
 * (or euid if none).
 *
 * The first line of the file (or of input/redirect) must contain the key.
 * No leading or trailing characters, even "spaces" are allowed.
 *
 * @param path of the file to look into.
 * @return none
 */
static void get_api_key_from_file(const char *path)
{
#define MAX_KEY_SZ (64)

	FILE *fd;
	size_t sz;
	struct stat st;
	struct passwd *pw;
	char full_path[PATH_MAX];
	char key[MAX_KEY_SZ];
	uid_t uid;

	if ('\0' == path[0]) {
		fd = stdin;
	}
	else {

		if ('~' == path[0] && '/' == path[1]) {
			errno = 0;
			uid = (-1 == params1f.uid) ? geteuid() : params1f.uid;
			pw = getpwuid(uid);
			if (NULL == pw || 0 != errno)
				lprintf(LOG_ERR , "errno=%u on getpwuid: %s.\n"
						, errno, strerror(errno));
			if (strlen(pw->pw_dir) + strlen(path) > PATH_MAX)
				lprintf(LOG_ERR ,"path lenght overflow: %s%s.\n"
						, pw->pw_dir, path + 1 );
			strcpy(full_path, pw->pw_dir);
			strcpy(full_path + strlen(pw->pw_dir), path + 1);
			path = full_path;
		}
		if (-1 == stat(path, &st)) {
			lprintf(LOG_ERR , "getting stats of `%s`: %s\n"
					, path, strerror(errno));
		}
		if ( ! S_ISREG(st.st_mode) )
			lprintf(LOG_ERR , "`%s` is not a regular file.\n"
					, path);
		errno = 0;
		fd = fopen(path, "r");
		if (NULL == fd)
			lprintf(LOG_ERR,
				"Could not open `%s` to read the API Key: %s\n",
				path, strerror(errno));
	}

	errno = 0;
	if (NULL == fgets(key, MAX_KEY_SZ, fd)) {
		if (0 != errno)
			lprintf(LOG_ERR,
				"%s: reading API key from %s.\n",
				strerror(errno),
				(STDIN_FILENO == fd) ? "stdin":"file");
	}
	sz = strlen(key);
	if ('\n' == key[sz - 1])
		key[--sz] = '\0';

	params1f.api_key = strdup(key);

	if ('\0' != path[0])
		fclose(fd);
}

/*
 * @brief count the length of a path segment
 *
 * It is called with the path pointer next character after the '/' delimiter
 * (or start of the path for 1st segment if it does not start with '/') and
 * computes the normalized length of this path segment, potentially recursively
 * calling itself for the next segments.
 *
 * When a segment is made only of '.' it is simply ignored.
 * When a segment is made of '..' rewind is incremented so that the previous
 * caller rewinds its length.
 *
 * @param path of the segment
 * @param rewind counter for '..' segments
 * @param bool whether already normalized
 * @return length of this path segment
 */
static size_t path_segment_len(const char *segment, unsigned int *rewind,
			       bool *already_normalized)
{
	size_t len;
	const char *p;

	/* Skip leading slashes but the first */
	for (p = segment; '/' == *p; p++)
		*already_normalized = false;
	/* Empty segment: happens when trailing slash or empty path */
	if ('\0' == *p) {
		*already_normalized = false;
		return 0;
	}
	segment = p;	/* set it to past all the leading slashes */
	/* Count chars until the end of next segment or end of string */
	while ('\0' != *p && '/' != *p)
		p++;
	/* Possibly recurse */
	if ('/' == *p) {
		len = path_segment_len(++p, rewind, already_normalized);
		/* If last segment is void don't count the '/' separator */
		if (0 == len)
			p--;
	}
	else {
		len = 0;
	}
	/* Ignore segments made of only '.' */
	if ('.' == *segment) {
		if ('/' == *(segment + 1) || '\0' == *(segment + 1)) {
			*already_normalized = false;
			return len;
		}
	/* Increase rewind for segments made of '..' */
		if ( '.' == *(segment + 1) &&
		    ('/' == *(segment + 2) || '\0' == *(segment + 2))) {
			*already_normalized = false;
			*rewind += 1;
			return len;
		}
	}
	/* If there was a 'rewind', ignore the current segment */
	if (0 != *rewind) {
		*rewind -= 1;
		return len;
	}
	/* Finally in "normal" situations adds the segment length */
	return len + (p - segment);
}

/*
 * @brief return length of normalized path
 *
 * The flag abs:
 * - when positive, forces the path to be absolute (as if it started with '/')
 * - when negative, forces the path to be relative, stripping leading '/'
 * - when zero, does not change the nature of the path. Hence the path stays
 *	        absolute or relative when it starts by '/' or not.
 *
 * When path is NULL, returns zero.
 *
 * Trailing slashes are always ignored (unless the path is only '/')
 * When the path resolves to '/' (absolute or relative root), the function
 * will return 1 when absolute is requested, 0 if relative is requested, and
 * if abs is zero, 1 or 0 depending on path being absolute or relative.
 *
 * @param path
 * @param see comment above
 * @param if not null, bool whether already normalized
 * @return length of normalized path
 */
static size_t normalized_path_len(const char *path, int abs,
				  bool *already_normalized)
{
	size_t len;
	bool norm = true;
	unsigned int rewind = 0;

	if (NULL == path)
		return 0;

	len = path_segment_len(path + (('/' == *path) ? 1 : 0), &rewind, &norm);

	/* len is zero when path resolves to '/' absolute or relative.
	 * Examples: empty string (relative '/'), '/', foo/bar/../.. */
	if (0 == len) {
		if (0 < abs) {
			len = 1;
			norm = ('/' == *path && '\0' == *(path + 1));
		}
		else {
			if (0 > abs) {
				norm = ('\0' == *path);
			}
			else {
				if ('/' == *path) {
					len = 1;
					norm = ('\0' == *(path + 1));
				}
				else {
					norm = ('\0' == *path);
				}
			}
		}
	}
	else {
		if (0 < abs) { /* Want absolute path */
			len += 1;
			if ('/' != *path)
				norm = false;
		}
		else {
			if (0 > abs) { /* Want relative path */
				if ('/' == *path)
					norm = false;
			}
			else {
				if ('/' == *path)
					len += 1;
			}
		}
	}

	if (NULL != already_normalized)
		*already_normalized = norm;

	return len;
}


struct lengths {
	size_t		len;
	bool		is_rewind;
	struct lengths *prev;
};

/*
 * @brief count the length of a path segment
 *
 * It is called with the path pointer next character after the '/' delimiter
 * (or start of the path for 1st segment if it does not start with '/')
 *
 * The copy happens backwards because of '..' that make the string 'rewind'.
 * To compute where to place the segment, the function uses the lengths
 * chained structures.
 * It returns the number of rewinds expected so that when returned, the
 * caller knows whether to copy its content or not.
 *
 * @param path of the segment
 * @param destination buffer
 * @param chain of segment lengths
 * @return number of rewinds
 */
static unsigned int path_segment_cpy(const char *segment, char *dest,
				     struct lengths *segs_len)
{
	const char *p;
	struct lengths cur_len;
	size_t off = 0;
	unsigned int rewind;

	/* Skip leading slashes but the first */
	for (p = segment; '/' == *p; p++);

	segment = p;	/* set it to past all the leading slashes */

	/* Count chars until the end of next segment or end of string */
	while ('\0' != *p && '/' != *p)
		p++;

	/* Current default lenght, might be altered with '.' or '..' */
	cur_len.len	  = p - segment;
	cur_len.is_rewind = false;
	cur_len.prev	  = segs_len;

	/* Ignore segments made of only '.' */
	if ('.' == *segment) {
		if ('/' == *(segment + 1) || '\0' == *(segment + 1)) {
			cur_len.len = 0;
		}
		else {
	/* Rewind for segments made of '..' */
			if ( '.' == *(segment + 1) &&
			    ('/' == *(segment + 2) || '\0' == *(segment + 2))) {
				cur_len.len	  = 0;
				cur_len.is_rewind = true;
			}
		}
	}

	/* Possibly recurse */
	if ('/' == *p)
		rewind = path_segment_cpy(p + 1, dest, &cur_len);
	else
		rewind = 0;

	/* Compute the offset in dest where to copy the current segment.
	 * It is computed also when last empty segment to place '\0'
	 * When rewind this is not needed, and neither is the last segment */
	if (0 == rewind) {
		if (0 != cur_len.len || '\0' == *p) {
			struct lengths *ls;
			unsigned int rewd;

			rewd = (cur_len.is_rewind) ? 1 : 0;
			for (ls = segs_len; NULL != ls; ls = ls->prev)
				if (ls->is_rewind) {
					rewd ++;
				}
				else {
					if (0 != ls->len) {
						if (0 != rewd)
							rewd --;
						else
							off += ls->len + 1;
					}
					/* '/.' does not change rewind or off */
				}
		}

		if ('\0' == *p)
			dest[off + cur_len.len -
			     ((0 == cur_len.len && 0 != off) ? 1 : 0)] = '\0';
	}

	/* When current is '.' or '..' special case */
	if (0 == cur_len.len)
		return rewind + ((cur_len.is_rewind) ? 1 : 0);

	/* If there was a 'rewind', do not copy the current segment */
	if (0 != rewind)
		return rewind - 1;

	/* Finally in "normal" situations, copy the segment */
	memcpy(dest + off, segment, cur_len.len);
	if (NULL != segs_len && 0 != off)
		dest[off - 1] = '/';

	return 0;
}

/*
 * @brief return normalized path
 *
 * The flag abs is as in normalized_path_len
 *
 * When path is NULL, returns NULL.
 * The buffer passed to store the normalized path must be big enough. If it
 * is NULL, the function will allocate memory (via malloc).
 *
 * Trailing slashes are always ignored (unless the path is only '/').
 * When the path resolves to '/' (absolute or relative root), the function
 * will return '/' when absolute is requested, empty string (pointer to a string
 * with only '\0') if relative is requested, one or the other if abs is zero,
 * depending on path being absolute or relative.
 *
 * NULL can be returned if p is NULL or if n was NULL and malloc failed.
 * If NULL is returned, len will not be populated.
 *
 * When the path is already normalized and NULL is passed as destination,
 * there will be no allocation and the returned value is the path itself.
 * When the destination is not NULL, if the path is already normalized, the
 * function has the same effect of strcpy of the path to dest.
 *
 * @param path
 * @param destination where to store the normalized path
 * @param see comment above in normalized_path_len
 * @return normalized path
 */
static char *normalized_path(const char *path, char *dest, int abs)
{
	char *r;
	bool norm;

	if (NULL == path)
		return NULL;
	if (NULL == dest) {
		size_t sz;
		sz = normalized_path_len(path, abs, &norm);
		if (norm)
			return (char *)path;
		dest = malloc(sz + 1);
		if (NULL == dest)
			return NULL;
	}

	r = dest;
	if (0 < abs || (0 == abs && '/' == *path))
		*r++ = '/';

	(void)path_segment_cpy(path + (('/' == *path) ? 1 : 0), r, NULL);

	return dest;
}

/*
 * @brief compute memory needed for encfs and no-ssl paths
 *
 * @param arg the path argument
 * @param offset for long args (where path starts really)
 * @param offset for -o style arguments
 * @return memory needed
 */
static size_t needmem(const char* arg, unsigned long offlong
				     , unsigned long off_o)
{
	unsigned long off;

	off = IS_O_ARG(arg) ? off_o : offlong;
	return normalized_path_len(arg + off, 0, NULL) + 1 + sizeof(char *);
}

/*
 * @brief store encfs and no-ssl paths
 *
 * @param arg the path argument
 * @param offset for long args (where path starts really)
 * @param offset for -o style arguments
 * @return memory needed
 */
static void storepath(const char* arg, unsigned long offlong
				     , unsigned long off_o
				     , unsigned long *cur_sz
				     , struct paths  *ppaths)
{
	unsigned long off;
	char *p;

	off = IS_O_ARG(arg) ? off_o : offlong;
	p = ((char *)(ppaths->a)) + *cur_sz;
	ppaths->a[ppaths->n] = p;
	normalized_path(arg + off, p, 0);
	*cur_sz += strlen(p) + 1;
	ppaths->n += 1;
}

/*
 * @brief process special files: stat and refresh
 *
 * Special files must be at the root, if not (once normalised) a warning is
 * printed and the option is ignored.
 *
 * @param the argument with the special file
 * @param warning message template
 * @return allocated file name
 */
static char *get_special(const char *arg, const char *msg)
{
	char *p, *q = NULL;

	read_str(arg, &q);
	p = normalized_path(q, NULL, -1);
	if (q != p)
		free(q);

	if ('\0' == p[0] || NULL != strchr(p, '/')) {
		lprintf(LOG_WARNING,
			"--%s ignored: `%s` is invalid for the %s, must be to a file at root level.\n",
			msg, p, msg);
		free(p);
		p = NULL;
	}
	return p;
}

/*
 * @brief replace an allocated string parameter
 *
 * @param the value to store
 * @param where to replace
 * @param NULL value
 * @return none
 */
static void param_replace(char *new, char ** where, const char *nil)
{
	if (NULL != new) {
		if (nil != *where)
			free(*where);
		*where = new;
	}
}


/*
 * @brief processing function called by fuse_opt_parse
 *
 * See fuse_opt.h for this function specifics.
 *
 * @param data is the user data passed to the fuse_opt_parse() function
 * @param arg is the whole argument or option
 * @param key determines why the processing function was called
 * @param outargs the current output argument list
 * @return -1 on error, 0 if arg is to be discarded, 1 if arg should be kept
 */
int unfichier_opt_proc(void *parse_data,
			 const char *arg,
			 int key,
			 struct fuse_args *outargs)
{
	struct parse_data *d = (struct parse_data *)parse_data;
	unsigned long off;
	size_t sz;
	char *p;
	uint64_t i64;
	locale_t locale;

	switch (key) {
	case FUSE_OPT_KEY_NONOPT :
		return 1;

	case KEY_DEBUG:
		params.debug = true;
		    /* debug implies foreground: fall through */
	case KEY_FOREGROUND:
		params.foreground = true;
		return 1;
	case KEY_NOATIME:
		params.noatime = true;
		return 1;
	case KEY_NOEXEC:
		params.noexec = true;
		return 1;
	case KEY_NOSUID:
		params.nosuid = true;
		return 1;
	case KEY_NO_SPLICE_READ:
		params.no_splice_read = true;
		return 1;
	case KEY_SPLICE_READ:
		params.no_splice_read = false;
		return 1;
	case KEY_1FICHIER_NETWORK_WAIT:
		i64 = read_int(arg);
		if (i64 > ((uint64_t)UINT_MAX))
			lprintf(LOG_ERR,
				"number is too big for network-wait: %"PRIu64".\n",
				i64);
		params1f.network_wait = (unsigned int)i64;
		break;

	case KEY_1FICHIER_THREADS:
		i64 = read_int(arg);
		if (i64 > UINT_MAX)
			lprintf(LOG_ERR, "threads overflow: %s\n", arg);
		params.threads = i64;
		break;

	case KEY_CURL_INSECURE:
		params1f.insecure = true;
		break;
	case KEY_CURL_IPV4:
		params1f.curl_IP_resolve = CURL_IPRESOLVE_V4;
		break;
	case KEY_CURL_IPV6:
		params1f.curl_IP_resolve = CURL_IPRESOLVE_V6;
		break;
	case KEY_1FICHIER_RAW_NAMES:
		params1f.raw_names = true;
		break;
	case KEY_LOG_LEVEL:
		params.log_level = (unsigned int)read_int(arg);
		if (MAX_LEVEL < params.log_level) {
			lprintf(LOG_WARNING,
				"log level=%u must be between 0 and %u: assuming %u.\n",
				params.log_level, MAX_LEVEL, MAX_LEVEL);
			params.log_level = MAX_LEVEL;
		}
		break;
	case KEY_LOG_SIZE:
#ifdef _DEBUG
		params.log_size = (unsigned long)read_int(arg);
#else
		lprintf(LOG_WARNING,
			"--log-size ignored. Please run make with DEBUG=1\n");
#endif
		break;
	case KEY_LOG_FILE:
		if (NULL != log_fh)
			fclose(log_fh);
		read_str(arg, (char **)&params.log_file);
		log_fh = fopen(params.log_file, "w");
		if (NULL == log_fh)
			lprintf(LOG_ERR, "cannot open %s.\n", params.log_file);
		break;
	case KEY_FS_NAME:
		read_str(arg, &params.filesystem_name);
		break;
	case KEY_FS_SUBTYPE:
		read_str(arg, &params.filesystem_subtype);
		break;
	case KEY_CURL_USER_AGENT:
		if (def_user_agent != params.user_agent)
			free(params.user_agent);
		off = IS_O_ARG(arg) ? SZ_O_ARG_USER_AGENT :
				     ( '-' == arg[1] ? SZ_LONG_ARG_USER_AGENT
						     : SZ_SHORT_ARG );
		sz = strlen(arg + off);
		if (0 == sz) {
			params.user_agent = NULL;
		}
		else {
			params.user_agent = malloc(strlen(arg + off) + 1);
			strcpy(params.user_agent, arg + off);
		}
		break;
	case KEY_CURL_CACERT:
		read_str(arg, &params.ca_file);
		break;
	case KEY_1FICHIER_API_KEY:
		free(params1f.api_key);
		off = IS_O_ARG(arg) ? SZ_O_ARG_API_KEY : SZ_LONG_ARG_API_KEY;
		if ('@' != arg[off])
			params1f.api_key = strdup(arg + off);
		else
			get_api_key_from_file(arg + off + 1);
		break;
	case KEY_1FICHIER_STAT_FILE:
#ifdef _STATS
		param_replace(get_special(arg, "stat-file"),
			      &params.stat_file,
			      NULL);
#else
		lprintf(LOG_WARNING,
			"--stat-file ignored: the executable was built without stats. Please run make without NOSTATS=1\n");
#endif
		break;
	case KEY_1FICHIER_REFRESH_FILE:
		param_replace(get_special(arg, "refresh-file"),
			      &params1f.refresh_file,
			      NULL);
		break;
	case KEY_1FICHIER_FTP_USER:
		read_str(arg, &params1f.ftp_user);
		sz = strlen(params1f.ftp_user);
		if (sz < 5 || sz > 64)
			lprintf(LOG_ERR,
				"ftp_user name must be between 5 and 64 characters.\n");
		locale = newlocale(0, "C", (locale_t) 0);
		if ((locale_t) 0 == locale)
			lprintf(LOG_ERR, "could not create a C locale.\n");
		for (p = params1f.ftp_user; *p != 0; p++) {
			if (!isalnum_l(*p, locale) &&
			    !isdigit_l(*p, locale) && *p != '-')
				lprintf(LOG_ERR,
					"ftp_user name characters allowed are: a-z A-Z 0-9 and -\n");
			*p = tolower_l(*p, locale);
		}
		freelocale(locale);
		break;
	case KEY_1FICHIER_REFRESH_HIDDEN:
		params1f.refresh_hidden = true;
		break;
	case KEY_1FICHIER_REFRESH_TIME:
		i64 = read_int(arg);
		if (i64 > ((uint64_t)ULONG_LONG_MAX) / 60ULL)
			lprintf(LOG_ERR,
				"number is too big for refresh-time: %"PRIu64".\n",
				i64);
		params1f.refresh_time = i64 * 60;	/* Minutes to seconds */
		DEBUG("%"PRIu64" %lu\n", i64, params1f.refresh_time);
		break;

	case KEY_1FICHIER_RESUME_DELAY:
		i64 = read_int(arg);
		if (i64 > ((uint64_t)ULONG_LONG_MAX))	/* Only for 32 bits */
			lprintf(LOG_ERR,
				"number is too big for resume-delay : %"PRIu64".\n",
				i64);
		params1f.resume_delay = i64;
		break;

	case KEY_1FICHIER_NO_SSL: /* paths corrected to start & end with '/'  */
		storepath(arg, SZ_LONG_ARG_NO_SSL, SZ_O_ARG_NO_SSL,
			  &d->sz_no_ssl,
			  &params1f.no_ssl);
		break;

	case KEY_1FICHIER_ENCFS: /* paths corrected to start & end with '/'  */
		storepath(arg, SZ_LONG_ARG_ENCFS, SZ_O_ARG_ENCFS,
			  &d->sz_encfs,
			  &params1f.encfs);
		break;

	case KEY_1FICHIER_ROOT:
		read_str(arg, &(params1f.remote_root));
		p = normalized_path(params1f.remote_root, NULL, 1);
		if (p != params1f.remote_root)
			free(params1f.remote_root);
		if ('\0' == p[1]) {    /* This is '/': useless since default! */
			free(p);
			p = NULL;
		}
		params1f.remote_root = p;
		break;

		/* Alreday handled those on the 1st pass */
	case KEY_1FICHIER_NO_UPLOAD:
	case KEY_1FICHIER_DRY_RUN:
	case KEY_UID:
	case KEY_GID:
	case KEY_RO:
	case KEY_RW:
		break;

	default:
		lprintf( LOG_ERR, "unknown option: %s.\n", arg);
		break;
	}
	return 0;
}

/*
 * @brief counting function called by fuse_opt_parse
 *
 * This callback handles --help, --version, and counts the number of
 * no_ssl paths so that we can allocate memory those entries.
 * It does also keep track of the presence of the API Key
 *
 * See fuse_opt.h for this function specifics.
 *
 * @param data is the user data passed to the fuse_opt_parse() function
 * @param arg is the whole argument or option
 * @param key determines why the processing function was called
 * @param outargs the current output argument list
 * @return -1 on error, 0 if arg is to be discarded, 1 if arg should be kept
 */
 int unfichier_opt_count(void *parse_data,
			  const char *arg,
			  int key,
			  struct fuse_args *outargs)
{
	struct parse_data *d = (struct parse_data *)parse_data;
	curl_version_info_data *curl_info;
	uint64_t ull;

	switch (key) {
	case KEY_HELP :
		printf(	"Usage: 1fichierfs [--api-key='MyKey'] [options] mountpoint\n"
			"\n"
			"All options can be used the standard way, or the fuse way with -o\n"
			"\n"
			"1fichierfs options:\n"
			"    --api-key=MyKey   The API key of the 1fichier.com account.\n"
			"                      If the API key starts with @, it is interpreted as\n"
			"                      a filename which contains the API Key.\n"
			"                      @ alone means stdin.\n"
			"                      If this option is not present, the user will\n"
			"                      be prompted for the API Key.\n"
			"    -l --log-level=N  Verbosity level. The levels are those of syslog.\n"
			"                      The higher, the most verbose. Default: 4 (warning).\n"
			"       --log-file=filepath\n"
			"                      Log will be saved to that file instead of stderr\n"
			"                      (when foreground) or syslog.\n"
#ifdef _DEBUG
			"       --log-size=size\n"
			"                      Maximum size (MB) of the log file before wrapping.\n"
			"                      Default: 0 means no limit.\n"
#endif
			"    --raw-names       Names from the storage are passed as is intead of\n"
			"                      being translated to avoid forbidden characters.\n"
			"    --network-wait=seconds\n"
			"                      Default: 60 (1 minute).\n"
			"                      How long to wait for network to become ready when\n"
			"                      1fichierfs starts.\n"
			"    --refresh-file=filename\n"
			"    --refresh-hidden\n"
			"    --refresh-time=M  These parameters control how the directory tree\n"
			"                      is refreshed from the server.\n"
			"                      By default, no refresh is done.\n"
			"                      refresh-file shows an empty file on the root of the\n"
			"                      mount. Opening, this file triggers a refresh.\n"
			"                      If refresh-hidden is specified, the file will no be\n"
			"                      shown. Trying to open it will return 'no such file'\n"
			"                      but will still trigger a refresh.\n"
			"                      refresh-time specifies a timer in minutes after which\n"
			"                      a refresh will be done.\n"
			"    --root=path       Mount this remote directory instead of the whole storage.\n"
			"    --no-ssl=path     Downloads will occur without SSL/TLS for the whole\n"
			"                      tree under 'path'. Can be specified multiple times.\n"
			"    --encfs=path      Allows encfs to rename files for the whole tree\n"
			"                      under 'path'. Can be specified multiple times.\n"
			"                      For these two options non absolute paths are relative\n"
			"                      to the path provided in --root.\n"
#ifdef _STATS
			"    --stat-file=filename\n"
			"                      Pseudo-file to get live statistics from the driver.\n"
#endif
			"    --ftp-user=name   Use this FTP user instead of a name derived from the\n"
			"                      default prefix.\n"
			"    --no-upload       Do not allow uploading files to the 1fichier account.\n"
			"                      Unlike -o ro, rename, del, etc... are still allowed.\n"
			"    --dry-run         Does NOT mount, only simulate and log the resume process.\n"
			"    --resume-delay=seconds\n"
			"                      Default: 900 (15 minutes).\n"
			"                      How many seconds must have elapsed to consider safe\n"
			"                      resume an interrupted upload process.\n"
			"    --threads=N       Requests a number of threads for the stream asynchronous\n"
			"                      engine. The number of threads is brought in the range\n"
			"                      [1 : max(8, available cores)].\n"
			"                      Default is max(1, min(8, available cores/2))\n"
			"\n"
			"curl options used by 1fichierfs (`man curl` for detail):\n"
			"    -4 --ipv4\n"
			"    -6 --ipv6\n"
			"    -k --insecure\n"
			"       --cacert\n"
			"    -A --user-agent\n"
			"                      The default user agent is: %s\n"
			"                      With -A '' no user-agent header will be sent.\n"
			"\n"
			"General options:\n"
			"    -o opt[,opt...]   mount options\n"
			"    -h	 --help        print help\n"
			"    -V	 --version     print version\n"
			"\n", def_user_agent);
		if (0 != fuse_opt_add_arg(outargs, "-ho"))
			lprintf(LOG_ERR, MSG_ERR_ADD_OPT_ARG);

		exit((0 == fuse_main(outargs->argc,
				     outargs->argv,
				     d->unfichier_oper,
				     NULL)) ? EXIT_SUCCESS : EXIT_FAILURE);


	case KEY_VERSION :
		fprintf(stderr, "Copyright (C) 2018-2025  Alain Bénédetti\n"
			"This program comes with ABSOLUTELY NO WARRANTY.\n"
			"This is free software, and you are welcome to redistribute it\n"
			"under the conditions of the GPLv3 or later, at your convenience.\n"
			"Full license text can be found here: https://www.gnu.org/licenses/gpl-3.0.html\n\n");
		fprintf(stderr, PROG_NAME": version "PROG_VERSION"\n");
#ifdef _DEBUG
	#ifdef _STATS
		fprintf(stderr, "Build option: DEBUG\n");
	#else
		fprintf(stderr, "Build options: DEBUG, NOSTATS\n");
	#endif
#else
	#ifdef _STATS
		fprintf(stderr, "Build option: none\n");
	#else
		#ifdef _MEMCOUNT
		fprintf(stderr, "Build options: NOSTATS, _MEMCOUNT\n");
		#else
		fprintf(stderr, "Build option: NOSTATS\n");
		#endif
	#endif
#endif
		curl_info = curl_version_info(CURLVERSION_NOW);
		if (NULL != curl_info) {
			if (curl_info->age >= 0) {
				fprintf(stderr	, "libcurl/%s (%s) %s\n"
						, curl_info->version
						, curl_info->host
						, curl_info->ssl_version);
			}
		}
		if (0 != fuse_opt_add_arg(outargs, "--version"))
			lprintf( LOG_ERR, MSG_ERR_ADD_OPT_ARG );

		exit((0 == fuse_main(outargs->argc,
				     outargs->argv,
				     d->unfichier_oper,
				     NULL)) ? EXIT_SUCCESS : EXIT_FAILURE);

	case KEY_RO:
		params1f.readonly = true;
		break;
	case KEY_RW:
		params1f.readonly = false;
		break;
	case KEY_1FICHIER_NO_UPLOAD:
		params1f.no_upload = true;
		break;
	case KEY_1FICHIER_DRY_RUN:
		params1f.dry_run = true;
		break;

	case KEY_UID:
		ull = read_int(arg);
		if (ull > UINT32_MAX)
			lprintf(LOG_ERR, "UID overflow: %s\n", arg);
		params1f.uid = ull;
		break;

	case KEY_GID:
		ull = read_int(arg);
		if (ull > UINT32_MAX)
			lprintf(LOG_ERR, "GID overflow: %s\n", arg);
		params1f.gid = ull;
		break;

	case KEY_1FICHIER_API_KEY:
		d->got_API_key = true;
		break;

	case KEY_1FICHIER_NO_SSL:
		params1f.no_ssl.n++;
		d->sz_no_ssl += needmem(arg, SZ_LONG_ARG_NO_SSL
					   , SZ_O_ARG_NO_SSL);
		break;

	case KEY_1FICHIER_ENCFS:
		params1f.encfs.n++;
		d->sz_encfs += needmem(arg, SZ_LONG_ARG_ENCFS
					  , SZ_O_ARG_ENCFS);
		break;

	case FUSE_OPT_KEY_NONOPT :
		if (NULL == d->mount_path)
			d->mount_path = arg;
		else
			lprintf(LOG_ERR, "Too many arguments: `%s`\n", arg);
		break;
	}
	return 0;
}


/*
 * @brief after the counting function (above) checks that all is Ok
 *
 * Checks the mount (common code with astreamfs), gets the API key if not
 * present, and allocates memory for no_ssl paths (if any)
 *
 * @param data is the user data passed to the fuse_opt_parse() function
 * @return none
 */

void check_args_first_pass(struct parse_data *data)
{
	if (!params1f.dry_run) {
		check_mount_path(data->mount_path);
		if (params1f.readonly)
			params.mount_st.st_mode &= ~(S_IWUSR| S_IWGRP| S_IWOTH);

		if (-1 != params1f.uid)
			params.mount_st.st_uid = params1f.uid;
		if (-1 != params1f.gid)
			params.mount_st.st_gid = params1f.gid;
	}

	if (!data->got_API_key) {
		free(params1f.api_key);
		fprintf(stderr, "Please, enter API key: ");
		get_api_key_from_file("");
	}

	if (0 != params1f.no_ssl.n) {
		params1f.no_ssl.a = malloc(data->sz_no_ssl);
		data->sz_no_ssl = sizeof(char *) * params1f.no_ssl.n;
		params1f.no_ssl.n = 0;
	}
	if (0 != params1f.encfs.n) {
		params1f.encfs.a = malloc(data->sz_encfs);
		data->sz_encfs = sizeof(char *) * params1f.encfs.n;
		params1f.encfs.n = 0;
	}
	params.user_agent = (char *)def_user_agent;
}

static int scmp(const void *a, const void *b)
{
	const char *sa = *((const char **)a);
	const char *sb = *((const char **)b);
	return strcmp(sa, sb);
}

/*
 * @brief remove all paths
 *
 * @param paths to zero
 * @return true
 */
static bool zero_paths(struct paths *ppaths)
{
	free(ppaths->a);
	ppaths->a = NULL;
	ppaths->n = 0;
	return true;
}

/*
 * @brief clean paths for no_ssl and encfs
 *
 * The end result should be a sorted list of non-redundant paths relative to
 * the root.
 *
 * @param the paths structure pointer
 * @return true if the list amounts to whole path, false otherwise
 */
static bool clean_paths(struct paths *ppaths)
{
	size_t root_len, len, lens[ppaths->n];
	bool need_shrinking = false;
	unsigned long i, j;
	char **p, *path;

	/** Remove paths outside of the remote root */
	if (NULL == params1f.remote_root) {
		/* When root is '/' all paths are OK, but still flag if one
		 * has been given as absolute */
		root_len = 0;
		for (i = 0; i < ppaths->n; i++)
			if ('/' == ppaths->a[i][0]) {
				if ('\0' == ppaths->a[i][1])
					return zero_paths(ppaths);
				ppaths->a[i] += 1;
				need_shrinking = true;
			}
			else {
				if ('\0' == ppaths->a[i][0])
					return zero_paths(ppaths);
			}
	}
	else {
		root_len = strlen(params1f.remote_root);
		/* i: index of items that stay
		 * j: incremented whether the item stays or not */
		for (i = j = 0; j < ppaths->n; j++) {
			if ('/' == ppaths->a[j][0]) {
				need_shrinking = true;
				len = eqlen(params1f.remote_root, ppaths->a[j]);
				if ( '\0' == ppaths->a[j][len] &&
				    ('\0' == params1f.remote_root[len] ||
				     '/'  == params1f.remote_root[len]   ) )
					/* root subdir or identical to path */
					return zero_paths(ppaths);
				if ( '\0' == params1f.remote_root[len] &&
				     '/'  == ppaths->a[j][len] ) {
					/* path is subdir of root */
						ppaths->a[j] += 1 + root_len;
				}
				else {
					/* path and root in different trees */
						lprintf(LOG_NOTICE, "path `%s` ignored, not a subdir of remote root `%s`\n"
								  , ppaths->a[j]
								  , params1f.remote_root);
						continue; /* don't increment i */
				}
			}
			else {
				if ('\0' == ppaths->a[j][0])
					return zero_paths(ppaths);
			}
			ppaths->a[i] = ppaths->a[j];
			i++;
		}
		ppaths->n -= (j - i);
	}

	/** From here all the paths are "visible" (even when remote root is
	 * not '/') and are relative. Sort them now. */

	qsort(ppaths->a, ppaths->n, sizeof(char *), scmp);

	/** Remove redundant: identical and subdirs */
	for (i = j = 1; j < ppaths->n; j++) {
		if (ppaths->a[j] == strstr(ppaths->a[j], ppaths->a[i - 1])) {
			lprintf(LOG_NOTICE, "path `%s` ignored, identical or subdir of path `%s`\n"
					  , ppaths->a[j], ppaths->a[i - 1]);
			need_shrinking = true;
			continue; /* don't increment */
		}
		ppaths->a[i] = ppaths->a[j];
		i++;
	}
	ppaths->n -= (j - i);

/*
 * valgrind --leak-check=full --track-origins=yes /media/fdn/Data/home/dev/astreamfs/1fichierfs ~/1fichier -o ipv4,api-key=@/tmp/1fichier.key,refresh-file=\!000_refresh.txt,refresh-time=20,log-level=7,log-file=/tmp/debug.txt,stat-file=.stats,no-ssl=//Boo/,no-ssl=Divers//Test/,no-ssl=/.crypt,encfs=//Boo2/../Boo/2/.,no-ssl=/Boo --dry-run
 */

	/** Shrink paths if needed */
	if (need_shrinking) {
		len = 0;
		for (i = 0; i < ppaths->n; i++) {
			lens[i] = strlen(ppaths->a[i]) + 1;
			len += lens[i] + sizeof(char *);
		}
		p = malloc(len);
		path = (char *)(&p[ppaths->n]);
		for (i = 0; i < ppaths->n; i++) {
			p[i] = path;
			memcpy(path, ppaths->a[i], lens[i]);
			path += lens[i];
		}
		free(ppaths->a);
		ppaths->a = p;
	}

	return false;
}


/*
 * @brief consistency check on arguments passed to the command line
 *
 * This is the final check after the second pass on arguments.
 *
 * Also prints out the parameters used if verbosity is >= INFO (6).
 *
 * @param pointer on start time of the program (REALTIME) for display purpose.
 * @return none
 */

void check_args_second_pass(struct timespec *t)
{
	unsigned long i;
	char *p;

	start_message(&t->tv_sec);
	st_mode = params.mount_st.st_mode & ~(S_IFMT | S_ISVTX);
	st_mode |= S_IFREG;
	if (params.noexec)
		st_mode &= ~(S_IXUSR | S_IXGRP | S_IXOTH);
	if (params.nosuid)
		st_mode &= ~S_ISUID;

	lprintf(LOG_INFO, "log level is %d.\n", params.log_level);
#ifdef _DEBUG
	if (0 != params.log_size &&
	    (params.log_level != LOG_DEBUG || NULL == log_fh)) {
		lprintf(LOG_WARNING, "log-size is ignored, when not debug level (7) or no log file specified\n");
		params.log_size = 0;
	}
	if (params.log_level == LOG_DEBUG && !params.foreground
					  && NULL == log_fh)
		lprintf(LOG_ERR, "Please specify a log file when at debug level (7) and not foreground.\n");
#else
	if (params.log_level == LOG_DEBUG) {
		lprintf(LOG_WARNING, "debug messages not present in this executable, using --log-level=6 instead. Please run make with DEBUG=1\n");
	}
#endif
	if (NULL == params.log_file) {
		if (params.foreground)
			lprintf(LOG_INFO, "Logging on console\n");
		else
			lprintf(LOG_INFO, "Logging to syslog\n");
	}
	else {
		lprintf(LOG_INFO, "Logging to file: %s\n", params.log_file);
		if (params.log_level == LOG_DEBUG) {
			if (0 == params.log_size)
				lprintf(LOG_WARNING, "Log size is unlimited\n");
			else
				lprintf(LOG_INFO, "Log file wraps at %ldMB\n",
					params.log_size);
		}
	}
	p= params1f.api_key;
	if (NULL == p || '\0' == *p)
		lprintf(LOG_ERR, "API key is NULL or empty.\n");
	do {
		if ((unsigned int)*p > 128 || 0 == base64url[(unsigned int)*p])
			lprintf(LOG_ERR,
				"Invalid character '%c' in API key\n", *p);
		p++;
	} while ('\0' != *p);
	if (NULL == params1f.refresh_file) {
		if (params1f.refresh_hidden) {
			lprintf(LOG_WARNING,
				"refresh-hidden ignored: no refresh-file specified.\n");
			params1f.refresh_hidden = false;
		}
		lprintf(LOG_INFO, "No refresh file specified.\n");
	}
	else {
		lprintf(LOG_INFO, "Refresh file is: %s\n",
			params1f.refresh_file);
		if (params1f.refresh_hidden)
			lprintf(LOG_INFO, "This refresh file is hidden.\n");
	}
	if (0 == params1f.refresh_time)
		lprintf(LOG_INFO, "Remote tree will be cached forever.\n");
	else
		lprintf(LOG_INFO, "Remote tree will be cached for %ld minutes.\n",
			params1f.refresh_time / 60);
#ifdef _STATS
	if (NULL == params.stat_file)
		lprintf(LOG_INFO, "No stat file specified.\n");
	else
		lprintf(LOG_INFO, "Stat file is: %s\n",params.stat_file);
#endif
	if (NULL == params.user_agent)
		lprintf(LOG_INFO, "no user_agent string will be sent\n");
	else
		lprintf(LOG_INFO, "user-agent=%s\n", params.user_agent );
	if (params.foreground)
		lprintf(LOG_INFO, "foreground.\n");
	if (params.debug)
		lprintf(LOG_INFO, "debug.\n");
	if (NULL == params.filesystem_name)
		params.filesystem_name= (char *)def_fsname;
	else
		lprintf(LOG_INFO, "filesystem_name=%s\n"
				, params.filesystem_name);
	if (NULL == params.filesystem_subtype)
		params.filesystem_subtype= (char *)def_type;
	else
		lprintf(LOG_INFO, "filesystem_subtype=%s\n"
				, params.filesystem_subtype);
	if (NULL != params1f.remote_root)
		lprintf(LOG_INFO, "Mounting remote directory=%s\n"
				, params1f.remote_root);
	if (0 != params1f.no_ssl.n) {
		if (clean_paths(&params1f.no_ssl)) {
			root.no_ssl = true;
			lprintf(LOG_INFO, "no-ssl for the whole mount.\n");
		}
		else {
			root.test_no_ssl = true;
			for (i = 0; i < params1f.no_ssl.n; i++)
				lprintf(LOG_INFO, "no-ssl for path=%s\n"
						, params1f.no_ssl.a[i]);
		}
	}
	if (0 != params1f.encfs.n) {
		if (clean_paths(&params1f.encfs)) {
			root.encfs = true;
			lprintf(LOG_INFO, "encfs for the whole mount.\n");
		}
		else {
			root.test_encfs = true;
			for (i = 0; i < params1f.encfs.n; i++)
				lprintf(LOG_INFO, "encfs  for path=%s\n"
						, params1f.encfs.a[i]);
		}
	}
}

/*
 * @brief since mounting a RO share can change options this is separated
 *
 * This simply adds options to the mount: RO/RW, mount name and type.
 *
 * @param fuse_args strucutre pointer on the arguments
 * @return none
 */
void check_args_third_pass(struct fuse_args *args)
{
	size_t s_name, s_type, s;
	s_name = strlen(params.filesystem_name);
	s_type = strlen(params.filesystem_subtype);
	{
		char add_opt[( (lengthof(rw_opt) > lengthof(ro_opt)) ?
				lengthof(rw_opt) : lengthof(ro_opt)    ) +
			     lengthof(subtype)	+
			     s_name		+
			     s_type		+ 1];
		if (params1f.readonly) {
			s = lengthof(ro_opt);
			memcpy(add_opt, ro_opt, s );
		}
		else {
			s = lengthof(rw_opt);
			memcpy(add_opt, rw_opt, s );
		}
		memcpy(add_opt + s, params.filesystem_name, s_name);
		memcpy(add_opt + s + s_name, subtype, lengthof(subtype));
		memcpy(add_opt + s + s_name + lengthof(subtype),
		       params.filesystem_subtype,
		       s_type + 1);
		lprintf(LOG_INFO, "adding options=%s\n",add_opt);
		fuse_opt_add_arg(args, add_opt);
	}
}
